# This script is reading in all total elapsed times from each different cmdenv output file, and copies the values into a .csv 
# for easy access. 

import glob
outfile = open("ib_je_10k_e_e.csv","w") #name of the output file gets determined in the first string

for filename in glob.glob("*.txt"): #getting the filenames of the next file with .txt ending
	infile =open(filename,"r") #open the file as read
	if not infile: break #if all files were read in, break
	timelist = [0] # initialize the list
	while True: 
		line = infile.readline() #einlesen einer zeile, pointer auf die naechste
		if not line: break # if not, end of document is reached
		epos = line.find("Elapsed: ")
		if epos!=-1:	# "Elapsed: " is found, searching position of "s (" copying chars in between into output
			spos = line.find("s (")
			if spos==-1: continue # "s (" not found, getting out of the if statement
			timelist.append(line[epos+9:spos])   #adding the element to the list
		else: continue	#epos not found in this line, countinuing
	outfile.write(max(timelist))
	outfile.write(";")
	print max(timelist) #groesstes(letztes) element nehmen ausdrucken[DEBUG]
outfile.close()
