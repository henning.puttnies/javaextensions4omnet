package jUDPApp;

import org.omnetpp.simkernel.JSimpleModule;
import org.omnetpp.simkernel.SWIGTYPE_p_simsignal_t;
import org.omnetpp.simkernel.SimTime;
import org.omnetpp.simkernel.cMessage;
import org.omnetpp.simkernel.cObjectFactory;
/**
 * This class is a Java implementation of the INET module UDPBasicApp.
 * More information  on the UDPBasicApp can be found in inet/applications/udpapp/UDPBasicApp.ned. 
 * If a destAddress is set, it will periodically send out Packets 
 * to another Host. This Host has to be specified by destAddress and destPort.
 * If no destAddress is set, it will act as a UDPSink and delete
 * all packets received. 
 *  
 * To change the data sent via a custom Payload you have to modify the code in: 
 * JUDPBasicApp.java: 
 * 		- declare the variables and their default values in the class
 * 		- set the entries in the sendPacket() method 
 * JUDPBasicApp.ned: 
 * 		- declare the variables and their default values in the NED-file
 * jUDP_Wrapper.ini: 
 * 		- set the entries to the desired value
 * MyPayload.msg(jSimple): 
 * 		- declare the variables and their default values in the message-file
 * 		- after declaring a new variable, you have to recompile the message file by
 * 		invoking "opp_msgc MyPayload.msg" in the jsimple folder
 */


public class JUDPBasicApp extends JSimpleModule {

	// Constants for kinds of self message
	protected static final short START = 1;
	protected static final short SEND = 2;
	protected static final short STOP = 3;

	// Constants for kinds of UDPControlInfo
	protected static final short UDP_I_DATA = 0;
	protected static final short UDP_I_ERROR = 1;
	
	// addresses and ports
	protected InetAddress destAddress ;
	protected int destPort = -1;
	protected InetAddress localAddress;
	protected int localPort = -1;
	
	//simTime
	protected SimTime startTime;
	protected SimTime stopTime;
	protected SimTime simTimeZero = new SimTime(0); // reference simulation start time
	
	//additional Options
	protected boolean receiveBroadcast;
	protected int typeOfService = -1;

	// msg control
	protected String packetName; // displaystring and packetname
	protected int timeToLive = -1;
	protected volatile double sendInterval = 0;
	protected volatile int messageLength = 0; 
	
	// msg contents
	protected int testint = 0;
	protected String customMessage = "";
	protected boolean customPayload = false;
	//declare custom msg-variables with their default value here

	// state
	protected DatagramSocket socket;
	protected cMessage selfMsg;

	// statistics
	protected int numSent = 0;
	protected int numReceived = 0;
	protected int maxNumSent = -1;
	//protected boolean send = true;
	protected SWIGTYPE_p_simsignal_t sentPk = registerSignal("sentPk"); // signal registration for statistics
	protected SWIGTYPE_p_simsignal_t rcvdPk = registerSignal("rcvdPk");

	protected void initialize() {
		//statistics
		numSent = 0;
		numReceived = 0;
		maxNumSent = par("maxNumSent").longValue();
		// WATCH(numSent);
		// WATCH(numReceived);
		// TODO: add WATCH functionality?
		
		//simTime
		startTime = new SimTime(par("startTime").doubleValue()); 
		stopTime = new SimTime(par("stopTime").doubleValue());
		if (stopTime.dbl() >= simTimeZero.dbl() && stopTime.dbl() < startTime.dbl()) // check if set Times are correct
			  System.err.println("Invalid startTime/stopTime parameters");
		

		// send a self message to start this node
		selfMsg = new cMessage("sendTimer");
		selfMsg.setKind(START);
		scheduleAt(simTime(), selfMsg);
	}

	/**
	 * Simple handleMessage() method. Distinguishes between self messages and
	 * messages from other modules. Gets called by the simulation, once a message is received 
	 * by the application.
	 * @param msg 
	 * 			message that arrived at the Module
	 */
	protected void handleMessage(cMessage msg){

		if (msg.isSelfMessage()) {
			switch (msg.getKind()) {
				case START:
					processStart();
					break;

				case SEND:
					processSend();
					break;

				case STOP:
					processStop();
					break;

				default:
					ev.println("Error at " + getFullPath() + ": received self message of unknown kind");
			}

		} else if (msg.getKind() == UDP_I_DATA) {
			// msg comes from another App and gets processed
			processPacket(msg);

		} else if (msg.getKind() == UDP_I_ERROR) {
			ev.println("Error at: " + getFullPath());
			cancelAndDelete(msg);
		}
		else{
			System.err.println("Unrecognized message " + msg.getClassName());
		}
		if(this.hasGUI()){
			String dispstr = "rcvd" + numReceived + "sent" + numSent; 
			this.setDisplayString(dispstr);
		}

	}
	
	/**
	 * This Method sets up and binds the socket. If a destination Address is
	 * set, an initial sendCommand is sent. This method
	 * gets called by {@link #handleMessage(cMessage) handleMessage} from a selfMsg with the kind START.  
	 */
	protected void processStart() {
		// setup socket
		socket = new DatagramSocket();
		socket.setOutputGate(gate("udpOut"));
		
		// Parse parameters
		localPort = par("localPort").longValue();
		destPort = par("destPort").longValue();
		
		//set localAddress as a My_InetAddress
		localAddress = new InetAddress(par("localAddress").stdstringValue()); 
		destAddress = new InetAddress(par("destAddress").stdstringValue());
		
		//binding the socket
		socket.bind(localAddress, localPort); 
		setSocketOptions();
		
		if (destAddress.getAddressAsInt() !=0)// the app can only send if a destinationAddress is declared
		{
				selfMsg.setKind(SEND);
				processSend();
			
		}
		else{
			if(stopTime.dbl()>=simTimeZero.dbl()){
				selfMsg.setKind(STOP);
				scheduleAt(stopTime,selfMsg);
			}
		}		
	}
	/**
	 * This method sets the socket options as stated in the jUDP_Wrapper.ini
	 */
	protected void setSocketOptions(){
		//TODO:join LocalMulticast and multicastInterface not implemented?
		
		//setting ttl: maximum sending duration for the message. If it takes longer, the message gets deleted
		timeToLive = par("timeToLive").longValue();
		if(timeToLive != -1)
			socket.setTimeToLive(timeToLive);
		
		//setting tos: 
		typeOfService = par("typeOfService").longValue();
		char tos = (char)(typeOfService + '0');
		if(typeOfService != -1)
			socket.setTypeOfService(tos);
		
		//setting receive Broadcast: specifies, if the app is able to receive a Broadcast
		receiveBroadcast = par("receiveBroadcast").boolValue();
		if (receiveBroadcast)
	        socket.setBroadcast(true);
	}
	
	
	/**
	 * This method gets calles by {@link #handleMessage(cMessage) handleMessage}
	 * if a message with the KIND UDP_I_DATA is received. It records the number of received
	 * packets, emits a signal for statistics and deletes the received packet. 
	 * @param msg 
	 * 			received message, that is passed from handleMessage()
	 */
	protected void processPacket(cMessage msg){
		emit(rcvdPk, msg);
		//ev.println("Received packet: " + socket.getReceivedPacketInfo(msg));
		cancelAndDelete(msg);
		numReceived++;
		
	}
	
	/**
	 * This method periodically sends out messages in a set sendInterval. 
	 * sendInterval is set via the *.ini file. 
	 */
	protected void processSend() {
		// if you want to measure the time needed to simulate the whole process, it is necessary to 
		// get a comparable simulation duration in SimTime. For this purpose, you can set maxNumSent to a value
		// that is different from the default(-1). As soon, as numSent reaches the maximum number, the simulation ends
		// and the elapsed time gets printed i.e. as output in cmdenv. 
		// The preconfigured ini file for this can be found in jUDP_Wrapper/simulations/runningtimetest.ini
		
		if(maxNumSent !=-1) // checking if maxNumSent if not the default value
			if(numSent >= maxNumSent){
				endSimulation(); // ends the Simulation with a runtime exception
			}
		
		sendPacket(); // actual sending method
		//setting up the timer for the next send-event via selfMsg with the Kind SEND at the time nextsend
		SimTime nextsend = simTime().add(par("sendInterval").doubleValue());
		if (stopTime.dbl()< simTimeZero.dbl() || nextsend.dbl()<stopTime.dbl()){
			selfMsg.setKind(SEND);
			scheduleAt(nextsend,selfMsg);
		}
		else{
			selfMsg.setKind(STOP);
			scheduleAt(stopTime,selfMsg);
		}
	}
	
	/**
	 * This method creates the message to be sent and sends it to another module. 
	 */
	protected void sendPacket(){
		// parsing values from ini
		messageLength = par("messageLength").longValue();
		packetName = par("packetName").stdstringValue();
		customPayload  = par("customPayload").boolValue();
		
		String pcktname = packetName + "-" + numSent;
		//sending customPayload
		if(customPayload){
			//msgparameters have to be added in here
			//par custom entries here
			testint = par("testint").longValue();
			customMessage = par("customMessage").stdstringValue();
			
			cMessage customMsg  = cMessage.cast(cObjectFactory.createOne("inet::MyPayload")); // packet from jsimple
			customMsg.setField("name", pcktname);
			customMsg.setKind(UDP_I_DATA);
			customMsg.setField("bitLength", Integer.toString(messageLength));
			customMsg.setField("sequenceNumber",Integer.toString(numSent));
			
			// setField("fieldname",string) custom entries here
			customMsg.setField("msgInt", Integer.toString(testint));			
			socket.sendTo(customMsg,destAddress,destPort);
			
			// statistics
			emit(sentPk,customMsg);
			//calling the destructor for the java object
			customMsg.delete();
			
		}
		//normal sending
		else{
			cMessage msg = cMessage.cast(cObjectFactory.createOne("inet::ApplicationPacket"));
			msg.setField("name", pcktname);
			msg.setKind(UDP_I_DATA);
			msg.setField("bitLength", Integer.toString(messageLength));
			msg.setField("sequenceNumber",Integer.toString(numSent));
			socket.sendTo(msg, destAddress, destPort);
			
			//statistics
			emit(sentPk,msg);
			//calling the destructor for the java object
			msg.delete();
		}
		
		//statistics
		numSent++;
		
	}
	
	/**
	 * This method stops the app at StopTime. It is deleting the selfMsg and closing the socket. 
	 */
	protected void processStop(){
		try{
			socket.close();
		}catch(Exception exp){
			exp.printStackTrace();
		}
		cancelAndDelete(selfMsg); //deleting selfMsg and canceling the sendTimers
	}
	
	
	/**
	 * This method records the scalar statistics data at the end of the simulation.
	 */
	protected void finish() {	
		recordScalar("packets sent: ",numSent);
		recordScalar("packets received",numReceived);
		cancelAndDelete(selfMsg);
	}
}