package jUDPApp;

import org.omnetpp.simkernel.*;

/**
 * This class was build to work like the Java equivalent
 * (java.net.DatagramSocket). It has some of the original methods. It is partly
 * a recreation of the C++ class UDPSocket, which is a UDP-Wrapper written for
 * Omnet. Because this class is written in Java some adjustments had to be made
 * (using objects instead of pointers, using workaround methods instead of INET
 * methods).
 * <p>
 * 
 * To use a DatagramSocket, first instantiate it and set the outputGate (this is
 * a difference to java.net.DatagramSocket). This has to be made due to the fact
 * that this class is sending messages to a UDP object in the simulation and has
 * to be connected to it accordingly. After setting up the socket, you can use
 * the {@link #sendTo(cMessage,InetAddress,int) sentTo} method to send packets
 * to other modules.
 * 
 * <pre>
 * <code>
 * 		DatagramSocket socket = new DatagramSocket();
 * 		socket.setOutputGate(gate("udpOut"));
 * 	socket.bind(localAddress, localPort); // localAddress being a InetAddress
 * 	
 * 	cMessage msg = cMessage.cast(cObjectFactory.createOne("inet::ApplicationPacket"));
 * 	socket.sendTo(msg, destAddress, destPort);
 * 
 * </code>
 * </pre>
 *
 * Note: The constructor can have some issues with the JVM if it is called with
 * the gate outside of a method (global). This can lead to a segmentation fault.
 * To avoid this, call the constructor in your initialize function or call
 * {@link #setOutputGate(cGate) setOutputGate}later.
 * 
 * <p>
 * UDPCommands: more information found in
 * inet/transportlayer/contract/udp/UDPControlInfo.msg
 */
public class DatagramSocket {
	// Message-kind (Out)
	/**
	 * This message-kind shows the UDPModule that this message contains data and
	 * is not a notification or anything like that (used in Send() and
	 * SendTo()). More information can be found in
	 * inet/transportlayer/contract/udp/UDPControlinfo
	 * */
	public static final short UDP_C_DATA = 0;
	public static final short UDP_C_BIND = 1;
	public static final short UDP_C_CONNECT = 2;
	public static final short UDP_C_SETOPTION = 3;
	public static final short UDP_C_CLOSE = 4;
	// Message-kind (Out) End

	// Message-kind (In)
	/**
	 * Use this message-kind to check if the received message is a
	 * data-container, rather than an error-message (used in MessageIsData() and
	 * getMessageKind())
	 * */
	public static final short UDP_I_DATA = 0;
	/**
	 * Use this message-kind to check if the received message is an
	 * error-message, rather than just a data-container (used in
	 * MessageIsError() and getMessageKind())
	 * */
	public static final short UDP_I_ERROR = 1;
	// Message-kind (In) End

	// Attributes
	private InetAddress my_InetAddress; // The InetAddress given at
										// creation
	private int port; // The port given at creation

	private int ttl;
	private boolean isBound; // Is this socket currently bound to anything?
	private boolean isConnected; // Is this socket currently connected to
									// anything?
	private boolean isClosed; // Is this socket currently closed?
	private boolean isBroadcast; // Is this socket set to broadcasting?
	// Attributes End

	// Additional Gate handler
	private cGate outputGate;

	/**
	 * This method sends a message to the outputGate, which has to be set before
	 * using a socket. The outputGate should be the gate which is connected to
	 * the UDP-Module.
	 * 
	 * @param msg
	 *            The message you want to send to the UDPModule.
	 * */
	protected void sendToUDP(cMessage msg) {
		if (outputGate == null)
			throw new IllegalArgumentException(
					"outputGate must be set before sending a message to the UDP gate");
		else
			(cSimpleModule.cast(outputGate.getOwnerModule())).send(msg,
					outputGate);
	}

	// Additional Gate handler End

	// Additional SockId logic
	/**
	 * Static to ensure it is equal in the whole program newSockId is used to
	 * store the new socketID. This was made to keep sockId local and still be
	 * able to have a static value, which can be incremented
	 */
	private static int newSockId;
	private int sockId; // This value is unique and is given to every socket
						// created
	private static int maxSockId = Integer.MAX_VALUE;

	/**
	 * This recreates the unique creation of a socket-ID in UDP (in Omnet) If an
	 * error occurs, it will return -1
	 */
	protected static synchronized int createNewSocketID() {
		/*
		 * It was not possible to use any predefined methods from INET or C++.
		 * It was e.g. not possible to use the C++ method
		 * getEnvir()->getUniqueNumber();
		 * 
		 * The solution to this problem is a very basic method protected "static
		 * synchronized int createNewSocketID()". This method starts at 0 and
		 * counts upwards every time it is called. If the Java keyword "static"
		 * is used, the method is created only once. Every object of the given
		 * class uses the same method and the method is blocked, when it is
		 * already in use ("synchronized").
		 */
		if (newSockId < maxSockId)
			return newSockId++;
		else
			return -1;

	}

	// Additional SockId logic End

	// Constructor
	/**
	 * Create a new empty UDP-Socket with a unique socketId
	 * */
	public DatagramSocket() {

		sockId = createNewSocketID();

		if (sockId == -1)
			throw new IllegalArgumentException(
					"Error: sockId was set to -1. Too many sockets were created!");
	}

	/**
	 * Create a new UDP-Socket with a port and a default InetAddress
	 * 
	 * @param port
	 *            The port-value this socket can be bound to
	 * */
	public DatagramSocket(int port) {
		this.port = port;
		this.my_InetAddress = new InetAddress(1);
		sockId = createNewSocketID();

		if (sockId == -1)
			throw new IllegalArgumentException(
					"Error: sockId was set to -1. Too many sockets were created!");

	}

	/**
	 * Create a new UDP-Socket with a port and a given InetAddress
	 * 
	 * @param port
	 *            The given port-value this socket can be bound to
	 * @param iaddr
	 *            The given InetAddress this socket can be bound to
	 * */
	public DatagramSocket(InetAddress iaddr, int port) {
		this.my_InetAddress = iaddr;
		this.port = port;
		sockId = createNewSocketID();

		if (sockId == -1)
			throw new IllegalArgumentException(
					"Error: sockId was set to -1. Too many sockets were created!");
	}

	/**
	 * Create a new UDP-Socket with a port and a given InetAddress in form of a
	 * formated string
	 * */
	public DatagramSocket(String ip, int port) {
		this.my_InetAddress = new InetAddress(ip);
		this.port = port;
		sockId = createNewSocketID();

		if (sockId == -1)
			throw new IllegalArgumentException(
					"Error: sockId was set to -1. Too many sockets were created!");
	}

	/**
	 * This is the recommended constructor of the DatagramSocket class
	 * */
	public DatagramSocket(String ip, int port, cGate gateOut) {
		this.my_InetAddress = new InetAddress(ip);
		this.port = port;
		sockId = createNewSocketID();
		setOutputGate(gateOut);

		if (sockId == -1)
			throw new IllegalArgumentException(
					"Error: sockId was set to -1. Too many sockets were created!");
	}

	// Constructor End

	// Methods

	public void bind() throws IllegalArgumentException {
		bind(this.my_InetAddress, this.port);
	}

	/**
	 * This method is used to bind the socket to a given InetAddress.
	 * <p>
	 * 
	 * <p>
	 * Detailed description: We create a new UDPBindCommand object of type
	 * cObject. cObject is the Default class allows setting the fields. The
	 * actual fields are documented in the INET documentation.
	 * <p>
	 * 
	 * After creating the UDPBindCommand, we need to set its fields correctly
	 * (using conf.setField("fieldName",fieldValue)). Please note that all
	 * values are given as a string rather than their original type.
	 * 
	 * <p>
	 * For this UDP wrapper, we created a new UDPBindCommand in the jsimple
	 * project (c.f. jsimple/jUDPControlInfo.msg.). This was necessary because
	 * the fields localAddr is of type L3Address. Although L3Addresses has a
	 * string constructor, it is not used by default.
	 * 
	 * 
	 * <p>
	 * The line cMessage msg = new cMessage("BIND",UDP_C_BIND); creates a new
	 * message with name "BIND" and the type of a binding message. This is done
	 * by giving the parameter UDP_C_BIND as a second constructor value. The
	 * kind values are also noted in the INET documentation.
	 * 
	 * The {@link #connect(InetAddress, int) connect} and {@link #close() close}
	 * functions work similarly.
	 * <p>
	 * 
	 * The last important thing to do is to add the command object to a message
	 * in the controlInfo field. This is done by typing
	 * msg.setControlInfo(conf);.
	 * 
	 * @throws IllegalArgumentException
	 *             If the socket is already bound, the port if wrong or the
	 *             InetAddress is invalid, a exception gets thrown
	 * @param localAddr
	 *            The local InetAddress this socket will be bound to
	 * @param localPort
	 *            The local port this socket will be bound to
	 * */
	public void bind(InetAddress localAddr, int localPort)
			throws IllegalArgumentException {
		if (!isBound) {
			// checking if the given port is in the correct range of allowd port
			// numbers
			if (localPort < -1 || localPort > 65535) {
				throw new IllegalArgumentException("Port is invalid: port = "
						+ localPort, null);
				// checking if the localAddress is valid
			} else if (localAddr == null)
				throw new IllegalArgumentException("InetAddress is empty", null);

			// setup UDPBindCommand
			cObject conf = cObject.cast(cObjectFactory
					.createOne("inet::UDPBindCommand")); // creates
															// UDPBindCommand
															// from
															// jSimple/jUDPControlInfo
			conf.setField("sockId", Integer.toString(this.sockId)); // sets all
																	// nesccesary
																	// fields in
																	// the
																	// UDPBindCommand
			conf.setField("localAddr", localAddr.getIP());
			conf.setField("localPort", Integer.toString(localPort));

			cMessage msg = new cMessage("BIND", UDP_C_BIND); // creates the
																// BIND-message
																// itself
			msg.setControlInfo(conf); // control object gets set
			sendToUDP(msg); // msg gets sent to the corresponding UDP-Module
			isBound = true;
			setMyInetAddress(localAddr); // the socket is now bound, so
											// localAddress and Port get set
			setPort(localPort);
		} else
			throw new IllegalArgumentException("Socket is already bound!", null);
	}

	/**
	 * Connect a socket to a given InetAddress and a port if the socket is
	 * unconnected and the port parameter is passed correctly.
	 * 
	 * @throws IllegalArgumentException
	 *             Throws a exception if the socket is already connected, the
	 *             port is wrong or the InetAddress is empty
	 * @param remoteAddr
	 *            The remote address this socket will be connected to
	 * @param remotePort
	 *            The remote port this socket will be connected to
	 * */
	public void connect(InetAddress remoteAddr, int remotePort)
			throws IllegalArgumentException {
		if (!isConnected) {
			if (port <= 0 || port > 65535) {
				throw new IllegalArgumentException("Port is invalid: port = "
						+ remotePort, null);
			} else if (remoteAddr == null)
				throw new IllegalArgumentException("InetAddress is empty", null);

			cObject conf = cObject.cast(cObjectFactory
					.createOne("inet::UDPConnectCommand")); // creates a
															// UDPSConnectCommand
															// from the
															// jSimple/jUDPControlInfo
															// Folder
			conf.setField("sockId", Integer.toString(this.sockId));
			conf.setField("remoteAddr", remoteAddr.getIP());
			conf.setField("remotePort", Integer.toString(remotePort));

			cMessage msg = new cMessage("CONNECT", UDP_C_CONNECT);
			msg.setControlInfo(conf);
			sendToUDP(msg);

			isConnected = true;
		} else
			throw new IllegalArgumentException("Socket is already connected!",
					null);
	}

	/**
	 * Close this instance of the class DatagramSocket
	 * 
	 * @throws Exception
	 *             Throws an exception if the socket is already closed
	 * */
	public void close() throws Exception {
		if (!isClosed) {
			cObject conf = cObject.cast(cObjectFactory
					.createOne("inet::UDPCloseCommand"));// creates a
															// UDPCloseCommand
															// from the
															// jSimple/jUDPControlInfo
															// Folder
			conf.setField("sockId", Integer.toString(this.sockId));

			cMessage msg = new cMessage("CLOSE", UDP_C_CLOSE);
			msg.setControlInfo(conf); // setting the UDPCloseCommand as a
										// controlInfo

			sendToUDP(msg);// sending the CLOSE msg to the UDP-module

			isClosed = true;
		} else
			throw new Exception("Socket is already closed");
	}

	/**
	 * Send a cMessage to a destination, specified with an InetAddress and a
	 * port.
	 * 
	 * @param msg
	 *            The cMessage that will be send.
	 * @param destAddr
	 *            The destination address of this packet.
	 * @param destPort
	 *            The destination port of this packet.
	 * */
	public void sendTo(cMessage msg, InetAddress destAddr, int destPort) {
		msg.setKind(UDP_C_DATA);

		cObject conf = cObject.cast(cObjectFactory
				.createOne("inet::UDPSendCommand")); // creates a UDPSendCommand
														// from the
														// jSimple/jUDPControlInfo
														// Folder
		// setting all parameters in to the Send Command
		conf.setField("sockId", Integer.toString(this.sockId));
		conf.setField("destAddr", destAddr.getIP());
		conf.setField("destPort", Integer.toString(destPort));
		conf.setField("srcAddr", this.getMyInetAddress().getIP());
		// setting the sendCommand as controlInfo
		msg.setControlInfo(conf);
		// sending the message to the UDP-Module
		sendToUDP(msg);
	}

	/**
	 * This method sends a packet to the UDPModule. Send will only work if the
	 * Socket got connected earlier.
	 * 
	 * @param msg
	 *            The message that will be send.
	 * */
	public void send(cMessage msg) {
		msg.setKind(UDP_C_DATA);
		cObject conf = cObject.cast(cObjectFactory
				.createOne("inet::UDPSendCommand"));
		conf.setField("sockId", Integer.toString(this.sockId));
		msg.setControlInfo(conf);
		sendToUDP(msg);
	}

	/**
	 * This method sets the broadcast option to the given value 'broadcast'.
	 * 
	 * @param broadcast
	 *            Decides whether or not broadcast is set
	 * */
	public void setBroadcast(boolean broadcast) {
		cMessage msg = new cMessage("SetBroadcast", UDP_C_SETOPTION);
		cObject conf = cObject.cast(cObjectFactory
				.createOne("inet::UDPSetBroadcastCommand"));
		conf.setField("sockId", Integer.toString(this.sockId));
		conf.setField("broadcast", Boolean.toString(broadcast));
		msg.setControlInfo(conf);
		sendToUDP(msg);

		isBroadcast = broadcast;
	}

	/**
	 * This method sets the time to live option for this socket to the given
	 * parameter.
	 * 
	 * @param ttl
	 *            The integer value that will be set in the sockOption ttl
	 * */
	public void setTimeToLive(int ttl) {
		cMessage msg = new cMessage("SetTTL", UDP_C_SETOPTION); // creates a
																// SetTTL msg
																// with the Kind
																// UDP_C_SETOPTION
		cObject conf = cObject.cast(cObjectFactory
				.createOne("inet::UDPSetTimeToLiveCommand")); // creates a
																// UDPTimeToLiveCommand
		conf.setField("sockId", Integer.toString(this.sockId));
		conf.setField("ttl", Integer.toString(ttl));
		msg.setControlInfo(conf);
		sendToUDP(msg);

		this.ttl = ttl;
	}

	/**
	 * This method sets the type of service of this socket. For more information
	 * about this, take a look at the documentation of the
	 * inet::UDPSetTypeOfServiceCommand.
	 * 
	 * @param tos
	 *            specifies the Type of Service of the Socket
	 * */

	public void setTypeOfService(char tos) {
		cMessage msg = new cMessage("SetTOS", UDP_C_SETOPTION);
		cObject conf = cObject.cast(cObjectFactory
				.createOne("inet::UDPSetTypeOfServiceCommand"));
		conf.setField("sockId", Integer.toString(this.sockId));
		conf.setField("tos", Character.toString(tos));
		msg.setControlInfo(conf);

		sendToUDP(msg);
	}

	// Methods End

	// Additional Methods
	// These methods are convenience methods
	/**
	 * Returns 0 if the message-kind is data, 1 if the message-kind is error, -1
	 * else.
	 * 
	 * @param msg
	 *            The message you want to know the kind of
	 * */
	public int getUDPMessageKind(cMessage msg) {
		if (!isNull(msg)) {
			if (isData(msg))
				return 0;
			else if (isError(msg))
				return 1;
			else
				return -1;
		} else
			return -1;
	}

	/**
	 * This method returns a string with all the important informations about
	 * the message msg like srcAddress, destAddress, destPort, name and
	 * byteLength
	 * 
	 * @param msg
	 *            a cMessage, which you want to know more about
	 * @return retsr: string that contains all the information from the message
	 */
	public String getReceivedPacketInfo(cMessage msg) {
		cObject ctrl = msg.getControlInfo(); // get the controlInfo from the
												// given msg
		String retstr;

		String srcAddr = ctrl.getField("srcAddr"); // reading all needed fields
		String destAddr = ctrl.getField("destAddr");
		String destPort = ctrl.getField("destPort");
		String name = msg.getName();
		String byteLength = msg.getField("byteLength");

		if (ctrl.hasField("srcPort")) { // if the socket is not bound, no
										// srcPort for the incoming msg is set
										// by inet::UDP.cc
			String srcPort = ctrl.getField("srcPort");
			retstr = ("msg " + name + "(" + byteLength + ") " + srcAddr + ":"
					+ srcPort + "-->" + destAddr + ":" + destPort);
		} else {
			retstr = ("msg " + name + "(" + byteLength + ") " + srcAddr + "-->"
					+ destAddr + ":" + destPort);
		}
		return retstr;
	}

	/**
	 * Returns true if the message-kind is data, else false.
	 * 
	 * @param msg
	 *            The message you want to test for a data kind
	 * */
	public boolean isData(cMessage msg) {
		if (!isNull(msg)) {
			if (msg.getKind() == UDP_I_DATA)
				return true;
			else
				return false;
		} else
			return false;
	}

	/**
	 * Returns true if the message-kind is error, else false.
	 * 
	 * @param msg
	 *            The message you want to test for an error kind
	 * */
	public boolean isError(cMessage msg) {
		if (!isNull(msg)) {
			if (msg.getKind() == UDP_I_ERROR)
				return true;
			else
				return false;
		} else
			return false;
	}

	/**
	 * Returns true if the message is null.
	 * */
	private boolean isNull(cMessage msg) {
		return msg == null;
	}

	/**
	 * This is an override of the native toString method. It returns some
	 * information as a string.
	 *
	 * @return isClosed, isBroadcast, port, IP, isBound and isConnected as sting
	 *
	 * */
	@Override
	public String toString() {
		return "Port: " + this.port + "  IP: " + getIP() + "  is bound: "
				+ isBound + "  is connected: " + isConnected + "  isClosed: "
				+ isClosed + "  is broadcast: " + isBroadcast;
	}

	// Additional Methods end

	// Getter and Setter
	/**
	 * Returns the InetAddres of this socket
	 * 
	 * @return InetAddress of this socket
	 * */
	public InetAddress getMyInetAddress() {
		return my_InetAddress;
	}

	/**
	 * This method sets the myInetAddress of this Socket. Use this method with
	 * caution. Changing an InetAddress can lead to unexpected behavior.
	 * 
	 * @param addr
	 *            The InetAddress you want to set
	 * */
	public void setMyInetAddress(InetAddress addr) {
		my_InetAddress = addr;
	}

	/**
	 * Returns the IP-address of this socket as a string
	 * 
	 * @return Ip-address as string.
	 * */
	public String getIP() {
		return this.my_InetAddress.getIP();
	}

	/**
	 * Returns the port of this socket
	 * 
	 * @return port of this socket as int
	 * */
	public int getPort() {
		return port;
	}

	/**
	 * This method sets the port of this socket. Use this method with caution.
	 * Changing a port can lead to unexpected behavior.
	 * 
	 * @param port
	 *            The port-value you want to assign
	 * */
	public void setPort(int port) {
		if (port > 0 && port < 65536)
			this.port = port;
	}

	/**
	 * Returns the sockId from this socket (this value is unique)
	 * 
	 * @return socketId of this socket as int
	 * */
	public int getSockId() {
		return sockId;
	}

	/**
	 * This method should not be used! The sockId should be unique and changing
	 * it can lead to unexpected behaviour!
	 * 
	 * @param val
	 *            The sockId value you want to assign
	 * */
	protected void setSockId(int val) {
		if (val > 0)
			sockId = val;
		else
			sockId = 0;
	} // The user shouldn't be able to change this value

	/**
	 * Returns the outputGate that all messages are sent to
	 * 
	 * @return outGate as cGate.
	 * */
	public cGate getOutputGate() {
		return outputGate;
	}

	/**
	 * This method sets the output gate of the socket. It has to be called
	 * before using the socket to send or even bind! The outputGate should be
	 * the gate which is connected to the UDP-Module
	 * */
	public void setOutputGate(cGate gate) {
		outputGate = gate;
	}

	/**
	 * Return true if this port is already bound, else false
	 * */
	public boolean isBound() {
		return isBound;
	}

	/**
	 * Returns true if this socket is already connected, else false
	 * */
	public boolean isConnected() {
		return isConnected;
	}

	/**
	 * Returns true if this socket is closed, else false
	 * */
	public boolean isClosed() {
		return isClosed;
	}

	/**
	 * Returns true if broadcast is set, else false
	 * */
	public boolean getBroadcast() {
		return isBroadcast;
	}

	/**
	 * Returns the value of the ttl (time to live) parameter
	 * */
	public int getTtl() {
		return this.ttl;
	}
	// Getter and Setter End
}
