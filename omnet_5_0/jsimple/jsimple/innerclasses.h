#include <omnetpp.h>

// swig cannot handle inner classes, so wrap the ones we need in global classes

namespace omnetpp{



class cModule_GateIterator
{
  private:
    omnetpp::cModule::GateIterator it;
  public:
    cModule_GateIterator(const omnetpp::cModule *m) : it(m) {}
    omnetpp::cGate *get() const {return it();}
    bool end() const {return it.end();}
    void next() {it++;}
    void advance(int k) {
           for(int i=0;i<k;i++){
               it++;
           }
       }
};

class cModule_SubmoduleIterator
{
  private:
    omnetpp::cModule::SubmoduleIterator it;
  public:
    cModule_SubmoduleIterator(const omnetpp::cModule *m) : it(m) {}
    omnetpp::cModule *get() const {return it();}
    bool end() const {return it.end();}
    void next() {it++;}
};

class cModule_ChannelIterator
{
  private:
    omnetpp::cModule::ChannelIterator it;
  public:
    cModule_ChannelIterator(const omnetpp::cModule *m) : it(m) {}
    omnetpp::cChannel *get() const {return const_cast<cModule_ChannelIterator*>(this)->it();}
    bool end() const {return it.end();}
    void next() {++it;}
};


}
