%{
#if OMNETPP_VERSION == 0x0400
#define GET_FIELD_AS_STRING(desc,self,fieldId,index) \
    char buf[200]; desc->getFieldValueAsString(self, fieldId, index, buf, 200); return buf;
#else
#define GET_FIELD_AS_STRING(desc,self,fieldId,index) \
    return desc->getFieldValueAsString(self, fieldId, index);
#endif


  static omnetpp::cClassDescriptor *findDescriptor(omnetpp::cObject *p)
  {
      omnetpp::cClassDescriptor *desc = p->getDescriptor();
      if (!desc)
          printf("no descriptor for class %s", p->getClassName());
      return desc;
  }

  static int findField(omnetpp::cClassDescriptor *desc, void *object, const char *fieldName)
  {
      int n = desc->getFieldCount();
      for (int i=0; i<n; i++)
          if (!strcmp(desc->getFieldName(i), fieldName))
              return i;
      return -1;
  }

  static int getFieldID(omnetpp::cClassDescriptor *desc, void *object, const char *fieldName)
  {
      int id = findField(desc, object, fieldName);
      if (id==-1)
          printf("no `%s' field in class %s", fieldName, desc->getName());
      return id;
  }
%}

%extend omnetpp::cObject {

  bool hasField(const char *fieldName)
  {
      omnetpp::cClassDescriptor *desc = findDescriptor(self);
      return findField(desc, self, fieldName)!=-1;
  }

  std::string getField(const char *fieldName)
  {
      omnetpp::cClassDescriptor *desc = findDescriptor(self);
      int fieldId = getFieldID(desc, self, fieldName);
      GET_FIELD_AS_STRING(desc,self,fieldId,0);
  }

  std::string getArrayField(const char *fieldName, int index)
  {
      omnetpp::cClassDescriptor *desc = findDescriptor(self);
      int fieldId = getFieldID(desc, self, fieldName);
      GET_FIELD_AS_STRING(desc,self,fieldId,index);
  }

  void setField(const char *fieldName, const char *value)
  {
      omnetpp::cClassDescriptor *desc = findDescriptor(self);
      int fieldId = getFieldID(desc, self, fieldName);
      desc->setFieldValueAsString(self, fieldId, 0, value);
  }

  void setArrayField(const char *fieldName, int index, const char *value)
  {
      omnetpp::cClassDescriptor *desc = findDescriptor(self);
      int fieldId = getFieldID(desc, self, fieldName);
      desc->setFieldValueAsString(self, fieldId, index, value); //XXX check out of bounds!!!
  }

  bool isFieldArray(const char *fieldName)
  {
      omnetpp::cClassDescriptor *desc = findDescriptor(self);
      int fieldId = getFieldID(desc, self, fieldName);
      return desc->getFieldIsArray(fieldId);
  }

  bool isFieldCompound(const char *fieldName)
  {
      omnetpp::cClassDescriptor *desc = findDescriptor(self);
      int fieldId = getFieldID(desc, self, fieldName);
      return desc->getFieldIsCompound(fieldId);
  }
}
