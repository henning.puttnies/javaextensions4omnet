//
// Copyright (C) 2000 Institut fuer Telematik, Universitaet Karlsruhe
// Copyright (C) 2004,2011 Andras Varga
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//

#include "MyUdpApp.h"

#include "inet/applications/base/ApplicationPacket_m.h"
#include "inet/networklayer/common/L3AddressResolver.h"
#include "inet/common/ModuleAccess.h"
#include "inet/common/lifecycle/NodeOperations.h"
#include "inet/transportlayer/contract/udp/UDPControlInfo_m.h"


namespace inet {

Define_Module(MyUdpApp);

simsignal_t MyUdpApp::sentPkSignal = registerSignal("sentPk");
simsignal_t MyUdpApp::rcvdPkSignal = registerSignal("rcvdPk");


MyUdpApp::~MyUdpApp()
{
    cancelAndDelete(selfMsg);
}

void MyUdpApp::initialize(int stage)
{
    ApplicationBase::initialize(stage);

    if (stage == INITSTAGE_LOCAL) {
        numSent = 0;
        numReceived = 0;
        WATCH(numSent);
        WATCH(numReceived);

        //custom Data has to be updated here. this copies entries from omnetpp.ini into the values of the module
        customMessage = par ("customMessage");
        testint = par("testint");

        maxNumSent = par("maxNumSent");
        localPort = par("localPort");
        destPort = par("destPort");
        startTime = par("startTime").doubleValue();
        stopTime = par("stopTime").doubleValue();
        packetName = par("packetName");
        if (stopTime >= SIMTIME_ZERO && stopTime < startTime)
            throw cRuntimeError("Invalid startTime/stopTime parameters");
        selfMsg = new cMessage("sendTimer");
        dummyL3AddressResolver = new MyL3AddressResolver();
        dummyL3Address = new L3Address();
    }
}

void MyUdpApp::finish()
{
    recordScalar("packets sent", numSent);
    recordScalar("packets received", numReceived);

    ApplicationBase::finish();
}

void MyUdpApp::setSocketOptions()
{
    int timeToLive = par("timeToLive");
    if (timeToLive != -1)
        socket.setTimeToLive(timeToLive);

    int typeOfService = par("typeOfService");
    if (typeOfService != -1)
        socket.setTypeOfService(typeOfService);

    const char *multicastInterface = par("multicastInterface");
    if (multicastInterface[0]) {
        IInterfaceTable *ift = getModuleFromPar<IInterfaceTable>(par("interfaceTableModule"), this);
        InterfaceEntry *ie = ift->getInterfaceByName(multicastInterface);
        if (!ie)
            throw cRuntimeError("Wrong multicastInterface setting: no interface named \"%s\"", multicastInterface);
        socket.setMulticastOutputInterface(ie->getInterfaceId());
    }

    bool receiveBroadcast = par("receiveBroadcast");
    if (receiveBroadcast)
        socket.setBroadcast(true);

    bool joinLocalMulticastGroups = par("joinLocalMulticastGroups");
    if (joinLocalMulticastGroups) {
        MulticastGroupList mgl = getModuleFromPar<IInterfaceTable>(par("interfaceTableModule"), this)->collectMulticastGroups();
        socket.joinLocalMulticastGroups(mgl);
    }
}

L3Address MyUdpApp::chooseDestAddr()
{
    int k = intrand(destAddresses.size());
    if (destAddresses[k].isLinkLocal()) {    // KLUDGE for IPv6
        const char *destAddrs = par("destAddresses");
        cStringTokenizer tokenizer(destAddrs);
        const char *token = nullptr;

        for (int i = 0; i <= k; ++i)
            token = tokenizer.nextToken();
        destAddresses[k] = L3AddressResolver().resolve(token);
    }
    return destAddresses[k];
}


void MyUdpApp::sendPacket()         // sending Method
{
    std::ostringstream namestr;
    namestr << packetName << "-" << numSent;
    MyPayload *payload = MyUdpApp::generateMessage(testint,customMessage,namestr.str().c_str());
    payload->setByteLength(par("messageLength").longValue());
    payload->setSequenceNumber(numSent);

    // original behaviour
    L3Address destAddr = chooseDestAddr();

    emit(sentPkSignal, payload);
    socket.sendTo(payload, destAddr, destPort);
    numSent++;
}

MyPayload* MyUdpApp::generateMessage(int in,const char *str, const char *name)
{
    MyPayload *res = new MyPayload(name);
    res->setMsgInt(in);
    res->setMsgStr(str);
    // insert additional data for MyPayload here
    // data has to be initialized in MyUdpApp.h, MyUdpApp.ned, MyPayload.msg and in the MyUdpApp::initialize()

    return res;
}

void MyUdpApp::socketBind()
{
    socket.setOutputGate(gate("udpOut"));
    const char *localAddress = par("localAddress");
    socket.bind(*localAddress ? L3AddressResolver().resolve(localAddress) : L3Address(), localPort);
    setSocketOptions();

    const char *destAddrs = par("destAddresses");
    cStringTokenizer tokenizer(destAddrs);
    const char *token;

    while ((token = tokenizer.nextToken()) != nullptr) {
        L3Address result;
        //L3AddressResolver().tryResolve(token, result);
        dummyL3AddressResolver->tryResolve(token, result);
        if (result.isUnspecified())
            EV_ERROR << "cannot resolve destination address: " << token << endl;
        else
            destAddresses.push_back(result);
    }

}
void MyUdpApp::processStart()
{
    if (!destAddresses.empty()) {
        selfMsg->setKind(SEND);
        processSend();
    }
    else {
        if (stopTime >= SIMTIME_ZERO) {
            selfMsg->setKind(STOP);
            scheduleAt(stopTime, selfMsg);
        }
    }
}

void MyUdpApp::processSend()
{
    // if you want to measure the time needed to simulate the whole process, it is necessary to
    // get a comparable simulation duration in SimTime. For this purpose, you can set maxNumSent to a value
    // that is different from the default(-1). As soon, as numSent reaches the maximum number, the simulation ends
    // and the elapsed time gets printed i.e. as output in cmdenv. The preconfigured ini file for this can be found in jUDP_Wrapper/simulations/runningtimetest.ini

    if(maxNumSent !=-1) // checking if maxNumSent if not the default value
        if(numSent >= maxNumSent){
            endSimulation(); // ending the simulation
          }


    sendPacket();
    simtime_t d = simTime() + par("sendInterval").doubleValue();

    if (stopTime < SIMTIME_ZERO || d < stopTime) {
        selfMsg->setKind(SEND);
        scheduleAt(d, selfMsg);
    }
    else {
        selfMsg->setKind(STOP);
        scheduleAt(stopTime, selfMsg);
    }
}


void MyUdpApp::processStop()
{
   socket.close();
}

void MyUdpApp::handleMessageWhenUp(cMessage *msg)
{
       if (msg->isSelfMessage()) {
            ASSERT(msg == selfMsg);
            switch (selfMsg->getKind()) {
                case START:
                    processStart();
                    break;

                case SEND:
                    processSend();
                    break;

                case STOP:
                    processStop();
                    break;

                default:
                    throw cRuntimeError("Invalid kind %d in self message", (int)selfMsg->getKind());
            }
        }
        else if (msg->getKind() == UDP_I_DATA) {
            processPacket(PK(msg));

        }
        else if (msg->getKind() == UDP_I_ERROR) {
            EV_WARN << "Ignoring UDP error report\n";
            delete msg;
        }
        else {
            throw cRuntimeError("Unrecognized message (%s)%s", msg->getClassName(), msg->getName());
        }

        if (hasGUI()) {
            char buf[40];
            sprintf(buf, "rcvd: %d pks\nsent: %d pks", numReceived, numSent);
            getDisplayString().setTagArg("t", 0, buf);
        }
}

void MyUdpApp::processPacket(cPacket *pk)  //receiving method
{
    emit(rcvdPkSignal, pk);
    EV_INFO << "Received packet: " << UDPSocket::getReceivedPacketInfo(pk) << endl;
    delete pk;
    numReceived++;
}

bool MyUdpApp::handleNodeStart(IDoneCallback *doneCallback)
{
    simtime_t start = std::max(startTime, simTime());
    socketBind();       //has to be called earlier than in UdpBasicApp, otherwise the first messages get dropped
    if ((stopTime < SIMTIME_ZERO) || (start < stopTime) || (start == stopTime && startTime == stopTime)) {
        selfMsg->setKind(START);
        scheduleAt(start, selfMsg);
    }
    return true;
}

bool MyUdpApp::handleNodeShutdown(IDoneCallback *doneCallback)
{
    if (selfMsg)
        cancelEvent(selfMsg);
    //TODO if(socket.isOpened()) socket.close();
    return true;
}

void MyUdpApp::handleNodeCrash()
{
    if (selfMsg)
        cancelEvent(selfMsg);
}

} // namespace inet

