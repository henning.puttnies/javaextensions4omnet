# Java Extensions 4 OMNeT++ (JE4O) 

This project enables writing simulation modules using the Java programming language for the OMNeT++ network simulator. 

Please note that this project heavily bases on JSimpleModule, which was developed by the OMNeT++ core team. Many thanks to them: https://github.com/omnetpp/jsimplemodule

It is also possible to have a mixed language network (e.g. some modules are written in C++ and some in Java). We also show how to interface to the INET library, as an example of how to interface between the Java world and existing C++ libs. There are two ways to use this project. You can either download a VM that runs out of the box or you can build up everything on your own using this repository. As a starting point, we highly recommend using the VM, as the JE4O base on Java Native Interface (JNI) and there are many pitfalls during the setup process.


An example of a simple setup that consists both, Java simulation
modules and C++ modules might look as follows (here: from the INET framework, Blue:
Java code, Red: INET code (C++ and *.ned)):

![java_and_cpp_simple](java_and_cpp_simple.png)

This simulation model is derived from the INET example ”inet/examples/ethernet/lans/twoHosts.ini”. We used
the EtherHost from INET and connected it with our own implementation called myEtherHost. The module myEtherHost
consists of EtherLLC, EtherQoSQueue, and IEtherMAC from INET written in C++ and EtherEchoSrv, which is a Java
module (c.f., our paper "Java Extensions for OMNeT++ 5.0").

An example of a more complex interface between Java application layer simulation models and OMNeT++'s INET library can look as follows: 

![java_and_cpp_simple](java_and_cpp_complex.png)

As depicted, the  Java application layer simulation model uses the jUDPWrapper to interface with INET. The
jUDPWrapper translates the socket-based API calls into messages to the INET udp module.
Writing a Java application layer simulation model using the jUDPWrapper is similar to writing C++ application layer simulation model with INET (c.f., our paper "jUDPWrapper: A Lightweight Approach to Access the OMNeT++/INET UDP Functionality from Java").


## Approach 1: Using the VM for JE4O  (recommended)

##### 1. Setup the VM:
- Download the VM image: https://unibox.uni-rostock.de/dl/fi63hr2MKwtk1RaysdMrzgnV/.zip
- Use any VM software that supports *.ova files (tested on Oracle VM VirtualBox 5.2.12)
- Include the *.ova file into your VM software (e.g. in VirtualBox via: File => import appliance)


##### 2. Login to the VM:
- User name: omnetuser
- PW: omnetuser
- Hint: you should change this right after setting up the VM!
  - open a terminal
  - command: sudo passwd omnetuser


##### 3.Start the JE4O 
- Using OMNeT++ 5.0:
	- open a terminal
	- command: cd /home/omnetuser/Omnet/omnetpp_5_0
	- command: . setenv
	- command: omnetpp
- Using OMNeT++ 5.4:
	- open a terminal
	- command: cd /home/omnetuser/Omnet/omnetpp_5_4
	- command: . setenv
	- command: omnetpp
		
		
##### 4.  Explore and run the examples
- Find the following projects in /home/omnetuser/Omnet/omnetpp-5.X/sim_workspace (build them via: right click on <project_name> => build):
	- inet: famous OMNeT++/INET framework
	- jsamples: examples to demonstrate the capabilities of the JE4O (similar to the jsamples for OMNeT++ 4.6 provided by OMNeT++'s core developers) 
		- Command for starting: right click on jsamples/jsamples.launch => run as => jsamples
	- jsimple: executable for the JE4O
	- jUDP_Wrapper: A wrapper for using INET's UDP functionality from the Java world
      - Command for starting: right click on jUDP_Wrapper/jUDP_Wrapper.launch => run as => jUDP_Wrapper
      - Please note that the timetestjava launch configurations stop with an endSimulation() exception. This is the intended behavior as the simulation should stop after a fixed number of exchanged packets.
	- generate_javaExtensions: Project to generate Java Extensions (jsimple)
		- Open this project only if you want to regenerate the Java Extensions for OMNeT++
	- InetUdpExtendExampel: A simple C++ example how to use the UDP functionality of INET





## Approach 2: Using this repository to build the JE4O


##### 1. Requirements
- Omnet++ 5.0 or Omnet++ 5.4
- Inet 3.4.0
- Java (JDK, tested with Java 8/ OpenJDK 1.8.0_275)
- OS Requirements:
  - We tested this using Ubuntu LTS 16.04. (OMNeT++ provides detailed installation instructions for Ubuntu, Fedora, Red Hat, and OpenSUSE.)
  - Moreover, basically the same steps apply using Windows, too.



##### 2. Clone the repository
- open a terminal
- command: cd 
- command: mkdir je4o
- command: cd je4o
- command: git clone https://gitlab.amd.e-technik.uni-rostock.de/henning.puttnies/javaextensions4omnet.git



##### 3. Import sources into your   
- start OMNeT++ and create an empty workspace:
  - command: omnetpp => type "/home/user_name/je4o/test_workspace" into the field 
- (If you need it: Install INET framework. You will need this, e.g., for the jUDP_Wrapper. If you need help here, see hints below.)
- Import all projects belonging to the JE4O into your workspace: File => Import => Existing Projects into Workspace => Next => Browse to home/je4o/JavaExtension4Omnet/omnet_5_x => OK 
- At this point, you have two options:
  - If you want to use git, leave "copy projects into workspace" unchecked => Finish
 - (If you do to want to use git, select "copy projects into workspace" => Finish)
 - Please note that your should always separate your git repository from your Eclipse workspace. Therefore, this point depends on whether or not you are using git.
- You can close the project generate_javaExtension as you will not need it right now:
	-command: right click on  generate_javaExtension => Close Project



##### 4. Build the jsimple project

You need a simulation executable containing JSimpleModule and several other simple modules written in C++. THe jsimple project will generate this executable. Therefore, the following files are compiled and linked:
- JSimpleModule.h/cc
- JMessage.h/cc
- simkernel_wrap.cc
- SimkernelJNI_registerNatives.cc
- ...

The JSimpleModule written in C++ was wrapped using the SWIG library. You can access it from the "Java world" via JNI (Java Native Interface). Consequently, you need the JNI headers to compile and the simulation executable needs to be linked against the JVM.

When running opp_(n)makemake, the file jsimple/makefrag(.vc) gets built into the makefile. It modifies the variabeles INCLUDE_PATH and LIBS accordingly. You should change the file jsimple/makefrag according to your system.

- Step 1: Compile some message (MSG) files for interfacing between the Java world and the C++ world:
	- command: cd ~/je4o/test_workspace/jsimple
	- command (compiles all files here and in subdirs): ```opp_msgc */*.msg```
- Step 2: Build jsimple: 
  - open: the file jsimple/makefrag and change it according to your system.
  - Command: right click on jsimple => build

##### 5 Install Java Development Tools
As you will use Java to write code, you will need Eclipse's Java development tools for building. Although OMNeT++ itsel is Eclipse-based, it does to come with the Java development tools by default. Consequently, you have to install it.


- Install the  Java development tools by:
  - Command: help => Install new Software => in the workwith select the official Eclipse site (e.g., ```Luna - http://download.eclipse.org/releases/luna``` => as text filter use ```Java``` => the menu will take some time to update... =>   select the ```Eclipse Java Development Tools``` => Next => Next => Accept => Finish => select to trust the Eclipse.org Foundation's certificate => OK => Restart OMNeT++
- Please note that you can switch the perspective to Java by: 
  - Command: Window => Open Perspective => Other => Java

##### 6. Build the jsamples project 
- Check your CLASSPATH2 variable. 
	- command: right click on jsamples => Run As => Run Configurations => OMNeT++ Simulation => jsamples => Environment => CLASSPATH2 => Edit
	- It should look like this: ```${pro-ject_loc:/jsimple}/simkernel.jar <mark>${system_property:path.separator}${project_loc:/jsamples}/bin```
- You can use this also to use additional jar files in your simulation. E.g.: ```${pro-ject_loc:/jsimple}/simkernel.jar${system_property:path.separator}${project_loc:/jsamples}/bin${system_property:path.separator}/path_to_myJar/myJar.jar```
- Build jsamples via: right click on jsamples => build Project
- Now, you can launch the Jsamples via:
	- Command: double click on jsamples/jsamples.launch
	- (Alternative command: right click on jsamples/jsamples.launch => Run as => jsamples)


##### 7. Build the jUDP_Wrapper project 
- Again, check your CLASSPATH2 variable. 
	- command: jUDP_Wrapper => Run As => Run Configurations => jUDP_Wrapper => Environment => CLASSPATH2 => Edit
	- It should look like this:
		- ```${pro-ject_loc:/jsimple}/simkernel.jar${system_property:path.separator}${project_loc:/jUDP_Wrapper}/bin```
- Again, you can use this also to use additional jar files in your simulation. E.g.:
	- ```${pro-ject_loc:/jsimple}/simkernel.jar${system_property:path.separator}${project_loc:/jUDP_Wrapper}/bin${system_property:path.separator}/path_to_myJar/myJar.jar``` 
- Build jUDP_Wrapper via: right click on jUDP_Wrapper => build Project
- Now, you can launch the jUDP_Wrapper via:
	- Command: double click on jUDP_Wrapper/jUDP_Wrapper.launch
	- (Alternative command: right click on jUDP_Wrapper/jUDP_Wrapper.launch => Run As => Run Configurations => jUDP_Wrapper)

## Regenerating the JE4O

You can newly generate the JE40 using the project generate_javaExtensions. This might only be relevant, if you want to generate Java Extensions for a newer OMNeT++ version. We recommend doing a SWIG tutorial first and afterwards having a look into the generate_javaExtensions/Simkernel.i file. Generating the JE40 for OMNeT++ versions 5.X should be relatively straight forward. 



## Hints
- Using the VM, we recommend to create a snapshot of the initial state of the VM for the safe of backups
- Using the VM, the keyboard layout is currently set to German but you can easily change it to your needs at the Ubuntu desktop
- Debugging is possible using Java Remote Debugging (just Google it, uncomment the corresponding lines 44+45 in JUtil.cc, and rebuild jsimple)
- To speed up OMNeT++, modify the following file similar to an eclipse.ini file: /home/user_name/Omnet/omnetpp-5.0/ide/omnetpp.ini
- You can find further documentation here:
    - jUDP_Wrapper has NED and Javadoc documentations
	- More info on how to use the Java Extensions (mostly the original documentation from the Java extensions for OMNeT++ 4.6 provided by the OMNeT++ core developers): /home/omnetuser/git/omnetpp_5_X/jsimple/README
	- Using the VM, more info on how to generate Java Extensions (e.g., for update to a new OMNeT++ version): /home/omnetuser/swig/swig-1.3.40/Doc/Manual  (e.g. index.html or Java.html)
- makefrag.vc needs the JAVA_HOME variable, you can define it either as an environment variable, or add it directly into jsimple/makefrag.vc (recommended). If you get a compiler error message that <jni.h> is not found or a linker message that the "jvm" library is not found, review the settings in makefrag.vc, and re-run opp_(n)makemake. To generate the makefiles use:
	- Command: opp_makemake -f --deep
	- Command: make
- You might have trouble downloading the INET library getting the following error: https://github.com/omnetpp/omnetpp/issues/693
  - In order to solve that, download the needed INET version from the following link an import it into your workspace: https://github.com/inet-framework/inet/releases



## Papers

#### 2018
[Henning Puttnies, Peter Danielis, Leonard Thiele, Dirk Timmermann
jUDPWrapper: A Lightweight Approach to Access the OMNeT++/INET UDP Functionality from Java
In Proceedings of the OMNeT++ Community Summit 2018, Vol. 56, pp. 1-10, Pisa, Italy, September 2018](https://easychair.org/publications/paper/G5pm)

#### 2017
[Henning Puttnies, Peter Danielis, Christian Koch, Dirk Timmermann:
Java Extensions for OMNeT++ 5.0
In Proceedings of the OMNeT++ Community Summit 2017, pp. 38-42, Bremen, Germany, September 2017](https://arxiv.org/abs/1709.02823)

