// generated using registernatives.pl

#include <stdio.h>
#include <stdlib.h>
#include <jni.h>

extern "C" {
void Java_org_omnetpp_simkernel_SimkernelJNI_INT8_1MIN_1get(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_INT16_1MIN_1get(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_INT32_1MIN_1get(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_INT64_1MIN_1get(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_INT8_1MAX_1get(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_INT16_1MAX_1get(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_INT32_1MAX_1get(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_INT64_1MAX_1get(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_UINT8_1MAX_1get(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_UINT16_1MAX_1get(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_UINT32_1MAX_1get(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1StringMap_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1StringMap_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_StringMap_1size(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_StringMap_1empty(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_StringMap_1clear(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_StringMap_1get(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_StringMap_1set(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_StringMap_1del(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_StringMap_1has_1key(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_delete_1StringMap(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cXMLElementVector_1size(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cXMLElementVector_1isEmpty(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cXMLElementVector_1add(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cXMLElementVector_1get(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cXMLElementVector(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_StringVector_1size(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_StringVector_1isEmpty(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_StringVector_1add(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_StringVector_1get(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_delete_1StringVector(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_getEv(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_getDefaultList(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_getRegisteredComponentTypes(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_getRegisteredNedFunctions(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_getRegisteredClasses(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_getRegisteredEnums(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_getRegisteredClassDescriptors(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_getRegisteredConfigOptions(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cModule_1GateIterator(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1GateIterator_1get(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1GateIterator_1end(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1GateIterator_1next(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1GateIterator_1advance(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cModule_1GateIterator(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cModule_1SubmoduleIterator(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1SubmoduleIterator_1get(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1SubmoduleIterator_1end(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1SubmoduleIterator_1next(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cModule_1SubmoduleIterator(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cModule_1ChannelIterator(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1ChannelIterator_1get(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1ChannelIterator_1end(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1ChannelIterator_1next(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cModule_1ChannelIterator(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_OMNETPP_1VERSION_1get(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_OMNETPP_1BUILDNUM_1get(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_PI_1get(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_INT64_1MAX_1DBL_1get(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_SIMTIME_1S_1get(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_SIMTIME_1MS_1get(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_SIMTIME_1US_1get(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_SIMTIME_1NS_1get(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_SIMTIME_1PS_1get(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_SIMTIME_1FS_1get(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_SIMTIME_1AS_1get(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_SimTime_1ZERO_1get(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_SimTime_1format_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_SimTime_1format_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_SimTime_1format_1_1SWIG_12(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_SimTime_1format_1_1SWIG_13(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_SimTime_1format_1_1SWIG_14(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_SimTime_1format_1_1SWIG_15(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_SimTime_1format_1_1SWIG_16(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1SimTime_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1SimTime_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1SimTime_1_1SWIG_12(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1SimTime_1_1SWIG_13(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1SimTime_1_1SWIG_14(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_SimTime_1assign_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_SimTime_1assign_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_SimTime_1assign_1_1SWIG_12(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_SimTime_1assign_1_1SWIG_13(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_SimTime_1assign_1_1SWIG_14(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_SimTime_1assign_1_1SWIG_16(JNIEnv *jenv, jclass jcls,...);
// XXX This methods leads into a crash of registerNatives ==> commented out
//void Java_org_omnetpp_simkernel_SimkernelJNI_SimTime_1assign_1_1SWIG_110(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_SimTime_1isZero(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_SimTime_1dbl(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_SimTime_1inUnit(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_SimTime_1trunc(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_SimTime_1remainderForUnit(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_SimTime_1split(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_SimTime_1str_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_SimTime_1str_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_SimTime_1ustr_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_SimTime_1ustr_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_SimTime_1raw(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_SimTime_1setRaw(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_SimTime_1getMaxTime(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_SimTime_1getScale(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_SimTime_1getScaleExp(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_SimTime_1setScaleExp(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_SimTime_1parse(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_SimTime_1add_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_SimTime_1substract_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_SimTime_1add_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_SimTime_1substract_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_delete_1SimTime(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cObject_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cObject_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cObject(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cObject_1getClassName(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cObject_1getName(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cObject_1isName(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cObject_1getFullName(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cObject_1getFullPath(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cObject_1getThisPtr(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cObject_1str(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cObject_1info(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cObject_1detailedInfo(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cObject_1dup(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cObject_1getOwner(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cObject_1isOwnedObject(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cObject_1forEachChild(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cObject_1findObject_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cObject_1findObject_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cObject_1copyNotSupported(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cObject_1hasField(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cObject_1getField(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cObject_1getArrayField(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cObject_1setField(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cObject_1setArrayField(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cObject_1isFieldArray(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cObject_1isFieldCompound(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cObject_1cast(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cNamedObject_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cNamedObject_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cNamedObject_1_1SWIG_12(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cNamedObject_1_1SWIG_13(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cNamedObject(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cNamedObject_1assign(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cNamedObject_1setName(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cNamedObject_1getName(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cNamedObject_1setNamePooling(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cNamedObject_1getNamePooling(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cOwnedObject_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cOwnedObject_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cOwnedObject_1_1SWIG_12(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cOwnedObject_1_1SWIG_13(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cOwnedObject(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cOwnedObject_1assign(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cOwnedObject_1getOwner(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cOwnedObject_1isOwnedObject(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cOwnedObject_1isSoftOwner(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cOwnedObject_1getDefaultOwner(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cOwnedObject_1getTotalObjectCount(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cOwnedObject_1getLiveObjectCount(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cOwnedObject_1resetObjectCounters(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cNoncopyableOwnedObject_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cNoncopyableOwnedObject_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cNoncopyableOwnedObject_1_1SWIG_12(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cNoncopyableOwnedObject_1dup(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cNoncopyableOwnedObject(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cDefaultList_1takeAllObjectsFrom(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cDefaultList_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cDefaultList_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cDefaultList(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cDefaultList_1isSoftOwner(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cDefaultList_1str(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cDefaultList_1forEachChild(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cDefaultList_1getPerformFinalGC(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cDefaultList_1setPerformFinalGC(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cDefaultList_1defaultListSize(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cDefaultList_1defaultListGet(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cDefaultList_1defaultListContains(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cDefaultList_1cast(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cIListener(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cIListener_1receiveSignal_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cIListener_1receiveSignal_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cIListener_1receiveSignal_1_1SWIG_12(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cIListener_1receiveSignal_1_1SWIG_13(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cIListener_1receiveSignal_1_1SWIG_14(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cIListener_1receiveSignal_1_1SWIG_15(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cIListener_1receiveSignal_1_1SWIG_16(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cIListener_1finish(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cIListener_1subscribedTo(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cIListener_1unsubscribedFrom(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cIListener_1getSubscribeCount(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cListener_1receiveSignal_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cListener_1receiveSignal_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cListener_1receiveSignal_1_1SWIG_12(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cListener_1receiveSignal_1_1SWIG_13(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cListener_1receiveSignal_1_1SWIG_14(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cListener_1receiveSignal_1_1SWIG_15(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cListener_1receiveSignal_1_1SWIG_16(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cListener(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cListener(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1getLogLevel(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1setLogLevel(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1setComponentType(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1reallocParamv(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1recordParametersAsScalars(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1parametersFinalized(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1addResultRecorders(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1initialized(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1callRefreshDisplay(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1hasDisplayString(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1clearSignalState(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1clearSignalRegistrations(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1setCheckSignals(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1getCheckSignals(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1getResultRecorders(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1invalidateCachedResultRecorderLists(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cComponent(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1forEachChild(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1getThisPtr(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1finalizeParameters(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1getProperties(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1getComponentType(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1getSimulation(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1getId(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1getNedTypeName(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1getComponentKind(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1isModule(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1isChannel(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1getParentModule(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1getSystemModule(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1callInitialize_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1callInitialize_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1callFinish(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1getNumParams(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1par_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1par_1_1SWIG_12(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1findPar(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1hasPar(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1getRNG(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1intrand_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1intrand_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1dblrand_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1dblrand_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1uniform_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1uniform_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1uniform_1_1SWIG_12(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1uniform_1_1SWIG_13(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1exponential_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1exponential_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1exponential_1_1SWIG_12(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1exponential_1_1SWIG_13(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1normal_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1normal_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1normal_1_1SWIG_12(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1normal_1_1SWIG_13(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1truncnormal_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1truncnormal_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1truncnormal_1_1SWIG_12(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1truncnormal_1_1SWIG_13(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1gamma_1d_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1gamma_1d_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1beta_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1beta_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1erlang_1k_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1erlang_1k_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1chi_1square_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1chi_1square_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1student_1t_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1student_1t_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1cauchy_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1cauchy_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1triang_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1triang_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1lognormal_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1lognormal_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1weibull_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1weibull_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1pareto_1shifted_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1pareto_1shifted_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1intuniform_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1intuniform_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1bernoulli_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1bernoulli_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1binomial_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1binomial_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1geometric_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1geometric_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1negbinomial_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1negbinomial_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1poisson_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1poisson_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1registerSignal(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1getSignalName(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1emit_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1emit_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1emit_1_1SWIG_12(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1emit_1_1SWIG_13(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1emit_1_1SWIG_14(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1emit_1_1SWIG_15(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1emit_1_1SWIG_16(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1emit_1_1SWIG_17(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1emit_1_1SWIG_18(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1emit_1_1SWIG_19(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1emit_1_1SWIG_110(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1emit_1_1SWIG_111(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1emit_1_1SWIG_112(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1emit_1_1SWIG_113(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1emit_1_1SWIG_116(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1emit_1_1SWIG_117(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1emit_1_1SWIG_118(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1emit_1_1SWIG_119(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1emit_1_1SWIG_128(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1emit_1_1SWIG_129(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1emit_1_1SWIG_130(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1emit_1_1SWIG_131(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1mayHaveListeners(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1hasListeners(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1subscribe_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1subscribe_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1unsubscribe_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1unsubscribe_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1isSubscribed_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1isSubscribed_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1getLocalListenedSignals(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1getLocalSignalListeners(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1hasGUI(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1getDisplayString(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1setDisplayString(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1bubble(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1resolveResourcePath(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1recordScalar_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1recordScalar_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1recordScalar_1_1SWIG_12(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1recordScalar_1_1SWIG_13(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1recordStatistic_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1recordStatistic_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1recordStatistic_1_1SWIG_12(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1recordStatistic_1_1SWIG_13(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1cast(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cChannel_1setSourceGate(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cChannel_1setNedConnectionElementId(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cChannel_1getNedConnectionElementId(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cChannel_1initializeChannel(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cChannel_1finalizeParameters(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cChannel_1callRefreshDisplay(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cChannel(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cChannel_1str(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cChannel_1callInitialize_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cChannel_1callInitialize_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cChannel_1callFinish(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cChannel_1getComponentKind(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cChannel_1getParentModule(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cChannel_1getChannelType(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cChannel_1getProperties(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cChannel_1getSourceGate(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cChannel_1isTransmissionChannel(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cChannel_1getNominalDatarate(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cChannel_1calculateDuration(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cChannel_1getTransmissionFinishTime(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cChannel_1isBusy(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cChannel_1forceTransmissionFinishTime(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cChannel_1cast(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cIdealChannel_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cIdealChannel_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cIdealChannel(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cIdealChannel_1create(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cIdealChannel_1getNominalDatarate(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cIdealChannel_1isTransmissionChannel(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cIdealChannel_1calculateDuration(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cIdealChannel_1getTransmissionFinishTime(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cIdealChannel_1isBusy(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cIdealChannel_1forceTransmissionFinishTime(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cIdealChannel_1cast(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cDelayChannel_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cDelayChannel_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cDelayChannel(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cDelayChannel_1create(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cDelayChannel_1isTransmissionChannel(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cDelayChannel_1getNominalDatarate(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cDelayChannel_1calculateDuration(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cDelayChannel_1getTransmissionFinishTime(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cDelayChannel_1isBusy(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cDelayChannel_1forceTransmissionFinishTime(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cDelayChannel_1setDelay(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cDelayChannel_1setDisabled(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cDelayChannel_1getDelay(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cDelayChannel_1isDisabled(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cDelayChannel_1initialize(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cDelayChannel_1cast(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cDatarateChannel_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cDatarateChannel_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cDatarateChannel(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cDatarateChannel_1create(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cDatarateChannel_1str(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cDatarateChannel_1isTransmissionChannel(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cDatarateChannel_1setDelay(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cDatarateChannel_1setDatarate(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cDatarateChannel_1setBitErrorRate(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cDatarateChannel_1setPacketErrorRate(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cDatarateChannel_1setDisabled(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cDatarateChannel_1getDelay(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cDatarateChannel_1getDatarate(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cDatarateChannel_1getBitErrorRate(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cDatarateChannel_1getPacketErrorRate(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cDatarateChannel_1isDisabled(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cDatarateChannel_1getNominalDatarate(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cDatarateChannel_1calculateDuration(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cDatarateChannel_1getTransmissionFinishTime(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cDatarateChannel_1isBusy(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cDatarateChannel_1forceTransmissionFinishTime(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cDatarateChannel_1initialize(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cDatarateChannel_1cast(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1setRecordEvents(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1isRecordEvents(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1clearNamePools(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1gateCount(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1gateByOrdinal(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1callRefreshDisplay(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1getCanvasIfExists(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1getOsgCanvasIfExists(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cModule(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cModule(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1forEachChild(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1setName(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1getFullName(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1getFullPath(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1str(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1addGate_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1addGate_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1setGateSize(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1getOrCreateFirstUnconnectedGate(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1getOrCreateFirstUnconnectedGatePair(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1finalizeParameters(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1buildInside(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1isSimple(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1getComponentKind(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1isPlaceholder(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1getModuleType(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1getProperties(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1isVector(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1getIndex(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1getVectorSize(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1size(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1hasSubmodules(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1findSubmodule_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1findSubmodule_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1getSubmodule_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1getSubmodule_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1getModuleByPath(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1gate_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1gate_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1gateHalf_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1gateHalf_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1hasGate_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1hasGate_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1findGate_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1findGate_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1gate_1_1SWIG_14(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1deleteGate(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1getGateNames(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1gateType(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1isGateVector(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1gateSize(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1gateBaseId(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1checkInternalConnections(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1arrived(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1getAncestorPar(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1getCanvas(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1getOsgCanvas(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1setBuiltinAnimationsAllowed(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1getBuiltinAnimationsAllowed(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1callInitialize_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1callInitialize_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1callFinish(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1scheduleStart(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1deleteModule(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1changeParentTo(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1cast(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_nullptr_1get(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cSimpleModule_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cSimpleModule_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cSimpleModule_1_1SWIG_12(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cSimpleModule(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cSimpleModule_1str(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cSimpleModule_1forEachChild(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cSimpleModule_1scheduleStart(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cSimpleModule_1deleteModule(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cSimpleModule_1usesActivity(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cSimpleModule_1isTerminated(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cSimpleModule_1snapshot_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cSimpleModule_1snapshot_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cSimpleModule_1snapshot_1_1SWIG_12(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cSimpleModule_1send_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cSimpleModule_1send_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cSimpleModule_1send_1_1SWIG_12(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cSimpleModule_1send_1_1SWIG_13(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cSimpleModule_1sendDelayed_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cSimpleModule_1sendDelayed_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cSimpleModule_1sendDelayed_1_1SWIG_12(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cSimpleModule_1sendDelayed_1_1SWIG_13(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cSimpleModule_1sendDirect_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cSimpleModule_1sendDirect_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cSimpleModule_1sendDirect_1_1SWIG_12(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cSimpleModule_1sendDirect_1_1SWIG_13(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cSimpleModule_1sendDirect_1_1SWIG_14(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cSimpleModule_1sendDirect_1_1SWIG_15(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cSimpleModule_1sendDirect_1_1SWIG_16(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cSimpleModule_1sendDirect_1_1SWIG_17(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cSimpleModule_1scheduleAt(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cSimpleModule_1cancelEvent(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cSimpleModule_1cancelAndDelete(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cSimpleModule_1wait(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cSimpleModule_1waitAndEnqueue(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cSimpleModule_1endSimulation(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cSimpleModule_1halt(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cSimpleModule_1error(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cSimpleModule_1cast(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponentType_1getPackageProperty(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponentType_1isAvailable(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponentType_1isInnerType(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponentType_1getCxxNamespace(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponentType_1getSourceFileName(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponentType_1checkSignal_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponentType_1checkSignal_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponentType_1getSignalDeclaration(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cComponentType(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponentType_1getFullName(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponentType_1getNedSource(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponentType_1find(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponentType_1get(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cComponentType_1cast(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cModuleType_1isNetwork(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cModuleType_1isSimple(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cModuleType_1create_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cModuleType_1create_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cModuleType_1createScheduleInit(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cModuleType_1find(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cModuleType_1get(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cModuleType_1cast(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cModuleType(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cChannelType_1create(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cChannelType_1find(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cChannelType_1get(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cChannelType_1getIdealChannelType(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cChannelType_1getDelayChannelType(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cChannelType_1getDatarateChannelType(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cChannelType_1cast(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cChannelType(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cArray_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cArray_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cArray_1_1SWIG_12(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cArray_1_1SWIG_13(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cArray_1_1SWIG_14(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cArray(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cArray_1assign(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cArray_1dup(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cArray_1str(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cArray_1forEachChild(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cArray_1size(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cArray_1clear(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cArray_1getCapacity(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cArray_1setCapacity(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cArray_1add(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cArray_1addAt(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cArray_1set(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cArray_1find_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cArray_1find_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cArray_1get_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cArray_1get_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cArray_1exist_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cArray_1exist_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cArray_1remove_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cArray_1remove_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cArray_1remove_1_1SWIG_12(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cArray_1setTakeOwnership(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cArray_1getTakeOwnership(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cArray_1cast(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cQueue_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cQueue_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cQueue_1_1SWIG_12(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cQueue(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cQueue_1assign(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cQueue_1dup(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cQueue_1str(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cQueue_1forEachChild(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cQueue_1insert(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cQueue_1insertBefore(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cQueue_1insertAfter(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cQueue_1remove(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cQueue_1pop(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cQueue_1clear(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cQueue_1front(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cQueue_1back(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cQueue_1getLength(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cQueue_1isEmpty(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cQueue_1length(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cQueue_1empty(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cQueue_1get(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cQueue_1contains(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cQueue_1setTakeOwnership(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cQueue_1getTakeOwnership(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cQueue_1cast(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cStatistic(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cStatistic_1assign(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cStatistic_1isWeighted(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cStatistic_1collect_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cStatistic_1collect_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cStatistic_1collectWeighted_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cStatistic_1collectWeighted_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cStatistic_1collectWeighted_1_1SWIG_12(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cStatistic_1collectWeighted_1_1SWIG_13(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cStatistic_1merge(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cStatistic_1clear(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cStatistic_1getCount(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cStatistic_1getSum(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cStatistic_1getSqrSum(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cStatistic_1getMin(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cStatistic_1getMax(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cStatistic_1getMean(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cStatistic_1getStddev(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cStatistic_1getVariance(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cStatistic_1getSumWeights(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cStatistic_1getWeightedSum(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cStatistic_1getSqrSumWeights(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cStatistic_1getWeightedSqrSum(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cStatistic_1saveToFile(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cStatistic_1loadFromFile(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cStatistic_1record(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cStatistic_1recordWithUnit(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cStatistic_1recordAs_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cStatistic_1recordAs_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cStatistic_1collect2_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cStatistic_1collect2_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cStatistic_1collect2_1_1SWIG_12(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cStatistic_1collect2_1_1SWIG_13(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cStatistic_1random(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cStatistic_1clearResult(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cStatistic_1cast(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cStdDev_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cStdDev_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cStdDev_1_1SWIG_12(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cStdDev_1_1SWIG_13(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cStdDev(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cStdDev_1assign(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cStdDev_1dup(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cStdDev_1str(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cStdDev_1isWeighted(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cStdDev_1collect_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cStdDev_1collect_1_1SWIG_11_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cStdDev_1collectWeighted_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cStdDev_1collectWeighted_1_1SWIG_11_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cStdDev_1collectWeighted_1_1SWIG_11_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cStdDev_1collectWeighted_1_1SWIG_11_12(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cStdDev_1merge(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cStdDev_1getCount(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cStdDev_1getSum(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cStdDev_1getSqrSum(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cStdDev_1getMin(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cStdDev_1getMax(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cStdDev_1getMean(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cStdDev_1getStddev(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cStdDev_1getVariance(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cStdDev_1getSumWeights(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cStdDev_1getWeightedSum(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cStdDev_1getSqrSumWeights(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cStdDev_1getWeightedSqrSum(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cStdDev_1draw(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cStdDev_1clear(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cStdDev_1saveToFile(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cStdDev_1loadFromFile(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cStdDev_1cast(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cWeightedStdDev_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cWeightedStdDev_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cWeightedStdDev_1_1SWIG_12(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cWeightedStdDev_1assign(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cWeightedStdDev_1dup(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cWeightedStdDev(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cHistogram_1MODE_1DOUBLES_1get(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cHistogram_1collectIntoHistogram_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cHistogram_1collectIntoHistogram_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cHistogram_1dump(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cHistogram_1assertSanity(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cHistogram_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cHistogram_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cHistogram_1_1SWIG_12(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cHistogram_1_1SWIG_13(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cHistogram_1_1SWIG_14(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cHistogram_1_1SWIG_15(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cHistogram_1_1SWIG_16(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cHistogram_1_1SWIG_17(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cHistogram_1assign(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cHistogram(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cHistogram_1dup(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cHistogram_1collect_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cHistogram_1collectWeighted_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cHistogram_1clear(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cHistogram_1saveToFile(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cHistogram_1loadFromFile(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cHistogram_1merge(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cHistogram_1setStrategy(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cHistogram_1getStrategy(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cHistogram_1binsAlreadySetUp(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cHistogram_1setUpBins(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cHistogram_1setBinEdges(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cHistogram_1createUniformBins(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cHistogram_1prependBins(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cHistogram_1appendBins(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cHistogram_1extendBinsTo_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cHistogram_1extendBinsTo_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cHistogram_1mergeBins(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cHistogram_1getBinEdges(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cHistogram_1getBinValues(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cHistogram_1getNumBins(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cHistogram_1getBinEdge(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cHistogram_1getBinValue(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cHistogram_1getUnderflowSumWeights(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cHistogram_1getOverflowSumWeights(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cHistogram_1getNumUnderflows(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cHistogram_1getNumOverflows(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cHistogram_1setMode(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cHistogram_1setRange(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cHistogram_1setNumPrecollectedValues(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cHistogram_1setRangeExtensionFactor(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cHistogram_1setAutoExtend(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cHistogram_1setNumBinsHint(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cHistogram_1setBinSizeHint(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cHistogram_1setRangeAuto_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cHistogram_1setRangeAuto_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cHistogram_1setRangeAuto_1_1SWIG_12(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cHistogram_1setRangeAutoLower_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cHistogram_1setRangeAutoLower_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cHistogram_1setRangeAutoLower_1_1SWIG_12(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cHistogram_1setRangeAutoUpper_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cHistogram_1setRangeAutoUpper_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cHistogram_1setRangeAutoUpper_1_1SWIG_12(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cHistogram_1setNumCells(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cHistogram_1setCellSize(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cPSquare_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cPSquare_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cPSquare_1_1SWIG_12(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cPSquare_1_1SWIG_13(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cPSquare(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cPSquare_1assign(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cPSquare_1dup(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cPSquare_1binsAlreadySetUp(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cPSquare_1setUpBins(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cPSquare_1collect_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cPSquare_1collect_1_1SWIG_11_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cPSquare_1collectWeighted_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cPSquare_1collectWeighted_1_1SWIG_11_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cPSquare_1collectWeighted_1_1SWIG_11_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cPSquare_1collectWeighted_1_1SWIG_11_12(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cPSquare_1getNumBins(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cPSquare_1getBinEdge(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cPSquare_1getBinValue(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cPSquare_1getNumUnderflows(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cPSquare_1getNumOverflows(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cPSquare_1getUnderflowSumWeights(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cPSquare_1getOverflowSumWeights(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cPSquare_1draw(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cPSquare_1merge(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cPSquare_1saveToFile(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cPSquare_1loadFromFile(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cVarHistogram_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cVarHistogram_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cVarHistogram_1_1SWIG_12(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cVarHistogram_1_1SWIG_13(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cVarHistogram_1_1SWIG_14(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cVarHistogram(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cVarHistogram_1assign(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cVarHistogram_1dup(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cVarHistogram_1clear(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cVarHistogram_1setUpBins(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cVarHistogram_1collectIntoHistogram(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cVarHistogram_1collectWeightedIntoHistogram(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cVarHistogram_1draw(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cVarHistogram_1getPDF(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cVarHistogram_1getCDF(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cVarHistogram_1getBinEdge(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cVarHistogram_1getBinValue(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cVarHistogram_1saveToFile(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cVarHistogram_1loadFromFile(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cVarHistogram_1addBinBound(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cRNG(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cRNG_1selfTest(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cRNG_1getNumbersDrawn(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cRNG_1intRand_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cRNG_1intRandMax(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cRNG_1intRand_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cRNG_1doubleRand(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cRNG_1doubleRandNonz(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cRNG_1doubleRandIncl1(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cRNG_1doubleRandNonzIncl1(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_LCG32_1MAX_1get(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cObjectFactory_1str(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cObjectFactory_1isAbstract(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cObjectFactory_1createOne_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cObjectFactory_1isInstance(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cObjectFactory_1getDescription(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cObjectFactory_1find_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cObjectFactory_1find_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cObjectFactory_1find_1_1SWIG_12(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cObjectFactory_1get_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cObjectFactory_1get_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cObjectFactory_1get_1_1SWIG_12(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cObjectFactory_1createOne_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cObjectFactory_1createOneIfClassIsKnown(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cObjectFactory(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_createOne(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_createOneIfClassIsKnown(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cDisplayString_1setHostObject(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cDisplayString_1dump(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cDisplayString_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cDisplayString_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cDisplayString_1_1SWIG_12(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cDisplayString(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cDisplayString_1assign_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cDisplayString_1assign_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cDisplayString_1str(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cDisplayString_1set(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cDisplayString_1parse(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cDisplayString_1updateWith_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cDisplayString_1updateWith_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cDisplayString_1containsTag(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cDisplayString_1getNumArgs_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cDisplayString_1getTagArg_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cDisplayString_1setTagArg_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cDisplayString_1setTagArg_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cDisplayString_1removeTag_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cDisplayString_1getNumTags(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cDisplayString_1getTagName(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cDisplayString_1getTagIndex(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cDisplayString_1getNumArgs_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cDisplayString_1getTagArg_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cDisplayString_1setTagArg_1_1SWIG_12(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cDisplayString_1insertTag_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cDisplayString_1insertTag_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cDisplayString_1removeTag_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cEnum_1registerNames(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cEnum_1registerValues(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cEnum_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cEnum_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cEnum_1_1SWIG_12(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cEnum_1_1SWIG_13(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cEnum(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cEnum_1assign(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cEnum_1dup(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cEnum_1str(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cEnum_1insert(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cEnum_1bulkInsert(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cEnum_1getStringFor(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cEnum_1lookup_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cEnum_1lookup_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cEnum_1resolve(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cEnum_1getNameValueMap(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cEnum_1find_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cEnum_1find_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cEnum_1get_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cEnum_1get_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cEnvir_1loggingEnabled_1set(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cEnvir_1loggingEnabled_1get(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cEnvir_1suppressNotifications_1set(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cEnvir_1suppressNotifications_1get(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cEnvir_1debugOnErrors_1set(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cEnvir_1debugOnErrors_1get(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cEnvir(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cEnvir_1componentInitBegin(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cEnvir_1messageCreated(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cEnvir_1messageCloned(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cEnvir_1getCurrentEventName(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cEnvir_1getCurrentEventClassName(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cEnvir_1getCurrentEventModule(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cEnvir_1preconfigure(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cEnvir_1configure(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cEnvir_1getXMLDocument_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cEnvir_1getXMLDocument_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cEnvir_1getParsedXMLString_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cEnvir_1getParsedXMLString_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cEnvir_1forgetXMLDocument(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cEnvir_1forgetParsedXMLString(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cEnvir_1flushXMLDocumentCache(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cEnvir_1flushXMLParsedContentCache(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cEnvir_1getExtraStackForEnvir(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cEnvir_1resolveResourcePath_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cEnvir_1resolveResourcePath_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cEnvir_1isGUI(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cEnvir_1isLoggingEnabled(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cEnvir_1isExpressMode(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cEnvir_1log(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cEnvir_1alert(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cEnvir_1printfmsg(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cEnvir_1gets_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cEnvir_1gets_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cEnvir_1askYesNo(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cEnvir_1getImageSize(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cEnvir_1getTextExtent(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cEnvir_1appendToImagePath(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cEnvir_1loadImage_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cEnvir_1loadImage_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cEnvir_1getSubmoduleBounds(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cEnvir_1getZoomLevel(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cEnvir_1getAnimationTime(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cEnvir_1getAnimationSpeed(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cEnvir_1getRemainingAnimationHoldTime(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cEnvir_1getNumRNGs(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cEnvir_1getRNG(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cEnvir_1addResultRecorders(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cEnvir_1getParsimProcId(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cEnvir_1getParsimNumPartitions(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cEnvir_1getUniqueNumber(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cEnvir_1refOsgNode(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cEnvir_1unrefOsgNode(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cEnvir_1ensureDebugger_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cEnvir_1ensureDebugger_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cEnvir_1addLifecycleListener(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cEnvir_1removeLifecycleListener(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cEnvir_1notifyLifecycleListeners_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cEnvir_1notifyLifecycleListeners_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cEnvir_1puts(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cException_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cException_1_1SWIG_12(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cException_1_1SWIG_13(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cException_1_1SWIG_14(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cException_1_1SWIG_15(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cException_1dup(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cException(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cException_1setMessage(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cException_1prependMessage(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cException_1isError(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cException_1getErrorCode(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cException_1what(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cException_1getFormattedMessage(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cException_1getSimulationStage(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cException_1getEventNumber(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cException_1getSimtime(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cException_1hasContext(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cException_1getContextClassName(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cException_1getContextFullPath(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cException_1getContextComponentId(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cException_1getContextComponentKind(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cRuntimeError_1displayed_1set(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cRuntimeError_1displayed_1get(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cRuntimeError_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cRuntimeError_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cRuntimeError_1_1SWIG_12(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cRuntimeError_1_1SWIG_13(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cRuntimeError_1_1SWIG_14(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cRuntimeError_1dup(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cRuntimeError_1cast(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cRuntimeError(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cDeleteModuleException_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cDeleteModuleException_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cDeleteModuleException_1dup(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cDeleteModuleException_1isError(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cDeleteModuleException(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cExpression(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cExpression_1assign(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cExpression_1dup(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cExpression_1str(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cExpression_1evaluate_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cExpression_1boolValue_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cExpression_1intValue_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cExpression_1intValue_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cExpression_1doubleValue_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cExpression_1doubleValue_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cExpression_1stringValue_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cExpression_1xmlValue_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cExpression_1evaluate_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cExpression_1evaluate_1_1SWIG_12(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cExpression_1boolValue_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cExpression_1boolValue_1_1SWIG_12(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cExpression_1intValue_1_1SWIG_12(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cExpression_1intValue_1_1SWIG_13(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cExpression_1intValue_1_1SWIG_14(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cExpression_1doubleValue_1_1SWIG_12(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cExpression_1doubleValue_1_1SWIG_13(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cExpression_1doubleValue_1_1SWIG_14(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cExpression_1stringValue_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cExpression_1stringValue_1_1SWIG_12(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cExpression_1xmlValue_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cExpression_1xmlValue_1_1SWIG_12(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cExpression_1parse(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cExpression_1compare(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cExpression_1containsConstSubexpressions(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cExpression_1evaluateConstSubexpressions(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_FSM_1MAXT_1get(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cFSM_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cFSM_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cFSM_1_1SWIG_12(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cFSM_1assign(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cFSM_1dup(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cFSM_1str(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cFSM_1getState(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cFSM_1getStateName(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cFSM_1isInTransientState(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cFSM_1setState_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cFSM_1setState_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cFSM(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_GATEID_1LBITS_1get(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_GATEID_1HMASK_1get(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_GATEID_1LMASK_1get(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cGate_1NONE_1get(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cGate_1INPUT_1get(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cGate_1OUTPUT_1get(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cGate_1INOUT_1get(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cGate_1getName(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cGate_1getFullName(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cGate_1forEachChild(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cGate_1str(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cGate_1getOwner(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cGate_1deliver(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cGate_1connectTo_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cGate_1connectTo_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cGate_1connectTo_1_1SWIG_12(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cGate_1disconnect(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cGate_1reconnectWith_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cGate_1reconnectWith_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cGate_1getBaseName(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cGate_1getNameSuffix(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cGate_1getProperties(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cGate_1getType(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cGate_1getTypeName(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cGate_1getOwnerModule(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cGate_1getId(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cGate_1isVector(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cGate_1getBaseId(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cGate_1getIndex(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cGate_1getVectorSize(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cGate_1size(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cGate_1getChannel(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cGate_1setDeliverOnReceptionStart(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cGate_1getDeliverOnReceptionStart(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cGate_1getTransmissionChannel(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cGate_1findTransmissionChannel(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cGate_1getIncomingTransmissionChannel(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cGate_1findIncomingTransmissionChannel(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cGate_1getPreviousGate(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cGate_1getNextGate(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cGate_1getConnectionId(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cGate_1getPathStartGate(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cGate_1getPathEndGate(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cGate_1pathContains_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cGate_1pathContains_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cGate_1isConnectedOutside(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cGate_1isConnectedInside(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cGate_1isConnected(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cGate_1isPathOK(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cGate_1getDisplayString(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cGate_1setDisplayString(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cGate_1cast(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_MAX_1PARSIM_1PARTITIONS_1get(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cMessage_1privateDup(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cMessage_1setSentFrom(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cMessage_1setSrcProcId(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cMessage_1getSrcProcId(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cMessage_1getParListPtr(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cMessage_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cMessage_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cMessage_1_1SWIG_12(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cMessage_1_1SWIG_13(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cMessage(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cMessage_1assign(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cMessage_1isPacket(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cMessage_1dup(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cMessage_1str(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cMessage_1forEachChild(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cMessage_1setKind(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cMessage_1setTimestamp_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cMessage_1setTimestamp_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cMessage_1setControlInfo(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cMessage_1removeControlInfo(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cMessage_1getKind(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cMessage_1getTimestamp(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cMessage_1getControlInfo(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cMessage_1getParList(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cMessage_1addPar_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cMessage_1addPar_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cMessage_1par_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cMessage_1par_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cMessage_1findPar(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cMessage_1hasPar(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cMessage_1addObject(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cMessage_1getObject(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cMessage_1hasObject(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cMessage_1removeObject_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cMessage_1removeObject_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cMessage_1isSelfMessage(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cMessage_1getSenderModule(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cMessage_1getSenderGate(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cMessage_1getArrivalModule(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cMessage_1getArrivalGate(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cMessage_1getSenderModuleId(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cMessage_1getSenderGateId(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cMessage_1getArrivalModuleId(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cMessage_1getArrivalGateId(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cMessage_1getCreationTime(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cMessage_1getSendingTime(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cMessage_1getArrivalTime(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cMessage_1arrivedOn_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cMessage_1arrivedOn_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cMessage_1arrivedOn_1_1SWIG_12(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cMessage_1getId(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cMessage_1getTreeId(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cMessage_1getDisplayString(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cMessage_1setArrival_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cMessage_1setArrival_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cMessage_1getTotalMessageCount(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cMessage_1getLiveMessageCount(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cMessage_1resetMessageCounters(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cMessage_1cast(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cMsgPar_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cMsgPar_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cMsgPar_1_1SWIG_12(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cMsgPar_1_1SWIG_13(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cMsgPar(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cMsgPar_1assign_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cMsgPar_1dup(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cMsgPar_1str(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cMsgPar_1forEachChild(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cMsgPar_1setBoolValue(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cMsgPar_1setLongValue(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cMsgPar_1setStringValue(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cMsgPar_1setDoubleValue(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cMsgPar_1setObjectValue(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cMsgPar_1setXMLValue(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cMsgPar_1setTakeOwnership(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cMsgPar_1getTakeOwnership(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cMsgPar_1boolValue(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cMsgPar_1longValue(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cMsgPar_1stringValue(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cMsgPar_1doubleValue(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cMsgPar_1pointerValue(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cMsgPar_1getObjectValue(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cMsgPar_1xmlValue(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cMsgPar_1getType(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cMsgPar_1isNumeric(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cMsgPar_1isConstant(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cMsgPar_1hasChanged(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cMsgPar_1convertToConst(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cMsgPar_1equalsTo(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cMsgPar_1parse_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cMsgPar_1parse_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cMsgPar_1assign_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cMsgPar_1assign_1_1SWIG_12(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cMsgPar_1assign_1_1SWIG_13(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cMsgPar_1assign_1_1SWIG_14(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cMsgPar_1assign_1_1SWIG_15(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cMsgPar_1assign_1_1SWIG_16(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cMsgPar_1assign_1_1SWIG_111(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cMsgPar_1assign_1_1SWIG_112(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cMsgPar_1assign_1_1SWIG_113(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cMsgPar_1cast(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cEvent_1getPreviousEventNumber(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cEvent_1setPreviousEventNumber(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cEvent_1getInsertOrder(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cEvent_1setArrivalTime(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cEvent_1getSrcProcId(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cEvent_1compareBySchedulingOrder(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cEvent(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cEvent_1assign(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cEvent_1dup(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cEvent_1str(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cEvent_1forEachChild(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cEvent_1setSchedulingPriority(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cEvent_1getSchedulingPriority(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cEvent_1isScheduled(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cEvent_1getArrivalTime(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cEvent_1getTargetObject(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cEvent_1isMessage(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cEvent_1isStale(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cEvent_1shouldPrecede(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cEvent_1execute(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cEventHeap_1getUseCb(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cEventHeap_1setUseCb(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cEventHeap_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cEventHeap_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cEventHeap_1_1SWIG_12(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cEventHeap_1_1SWIG_13(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cEventHeap(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cEventHeap_1assign(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cEventHeap_1dup(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cEventHeap_1str(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cEventHeap_1forEachChild(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cEventHeap_1insert(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cEventHeap_1peekFirst(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cEventHeap_1removeFirst(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cEventHeap_1putBackFirst(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cEventHeap_1remove(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cEventHeap_1isEmpty(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cEventHeap_1clear(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cEventHeap_1getLength(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cEventHeap_1get(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cEventHeap_1sort(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cOutVector_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cOutVector_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cOutVector(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cOutVector_1setName(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cOutVector_1str(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cOutVector_1setEnum_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cOutVector_1setEnum_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cOutVector_1setUnit(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cOutVector_1setType(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cOutVector_1setInterpolationMode(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cOutVector_1setMin(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cOutVector_1setMax(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cOutVector_1record_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cOutVector_1record_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cOutVector_1recordWithTimestamp_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cOutVector_1recordWithTimestamp_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cOutVector_1enable(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cOutVector_1disable(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cOutVector_1setEnabled(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cOutVector_1isEnabled(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cOutVector_1setRecordDuringWarmupPeriod(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cOutVector_1getRecordDuringWarmupPeriod(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cOutVector_1getValuesReceived(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cOutVector_1getValuesStored(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cOutVector_1cast(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cPar_1BOOL_1get(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cPar_1DOUBLE_1get(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cPar_1INT_1get(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cPar_1STRING_1get(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cPar_1XML_1get(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cPar_1LONG_1get(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cPar_1read(JNIEnv *jenv, jclass jcls,...);
//XXX
//void Java_org_omnetpp_simkernel_SimkernelJNI_cPar_1finalize(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cPar_1acceptDefault(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cPar_1assign_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cPar_1getName(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cPar_1str(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cPar_1getOwner(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cPar_1forEachChild(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cPar_1getType(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cPar_1getTypeName(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cPar_1isNumeric(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cPar_1isVolatile(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cPar_1isExpression(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cPar_1isShared(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cPar_1isSet(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cPar_1containsValue(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cPar_1getProperties(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cPar_1setBoolValue(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cPar_1setIntValue(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cPar_1setLongValue(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cPar_1setDoubleValue(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cPar_1setStringValue_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cPar_1setXMLValue(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cPar_1setExpression_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cPar_1setExpression_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cPar_1setEvaluationContext(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cPar_1boolValue(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cPar_1intValue(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cPar_1longValue(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cPar_1doubleValue(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cPar_1doubleValueInUnit(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cPar_1getUnit(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cPar_1stringValue(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cPar_1stdstringValue(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cPar_1isEmptyString(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cPar_1xmlValue(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cPar_1getExpression(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cPar_1getEvaluationContext(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cPar_1convertToConst(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cPar_1parse(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cPar_1assign_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cPar_1assign_1_1SWIG_12(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cPar_1assign_1_1SWIG_13(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cPar_1assign_1_1SWIG_14(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cPar_1assign_1_1SWIG_15(JNIEnv *jenv, jclass jcls,...);
// XXX This methods leads into a crash of registerNatives ==> commented out
//void Java_org_omnetpp_simkernel_SimkernelJNI_cPar_1assign_1_1SWIG_111(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cPar_1assign_1_1SWIG_112(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cPar_1assign_1_1SWIG_113(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cPar_1assign_1_1SWIG_114(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cPar_1assign_1_1SWIG_116(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cPar_1cast(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_PARSIM_1ANY_1TAG_1get(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cProperty_1DEFAULTKEY_1set(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cProperty_1DEFAULTKEY_1get(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cProperty_1updateWith(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cProperty_1lock(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cProperty_1isLocked(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cProperty_1setOwner(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cProperty_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cProperty_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cProperty_1_1SWIG_12(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cProperty_1_1SWIG_13(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cProperty(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cProperty_1assign(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cProperty_1setName(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cProperty_1getFullName(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cProperty_1dup(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cProperty_1str(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cProperty_1setIndex(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cProperty_1getIndex(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cProperty_1setIsImplicit(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cProperty_1isImplicit(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cProperty_1getKeys(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cProperty_1containsKey(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cProperty_1addKey(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cProperty_1getNumValues(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cProperty_1setNumValues(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cProperty_1getValue_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cProperty_1getValue_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cProperty_1getValue_1_1SWIG_12(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cProperty_1setValue(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cProperty_1erase(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cProperty_1cast(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cProperties_1lock(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cProperties_1addRef(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cProperties_1removeRef(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cProperties_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cProperties_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cProperties(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cProperties_1assign(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cProperties_1dup(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cProperties_1getName(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cProperties_1str(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cProperties_1getNumProperties(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cProperties_1getNames(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cProperties_1get_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cProperties_1get_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cProperties_1get_1_1SWIG_12(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cProperties_1getAsBool_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cProperties_1getAsBool_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cProperties_1getIndicesFor(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cProperties_1add(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cProperties_1remove(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cProperties_1cast(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cSimulation(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cSimulation(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cSimulation_1forEachChild(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cSimulation_1getFullPath(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cSimulation_1getActiveSimulation(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cSimulation_1getActiveEnvir(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cSimulation_1setActiveSimulation(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cSimulation_1setStaticEnvir(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cSimulation_1getStaticEnvir(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cSimulation_1getEnvir(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cSimulation_1registerComponent(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cSimulation_1deregisterComponent(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cSimulation_1getLastComponentId(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cSimulation_1getModuleByPath(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cSimulation_1getComponent(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cSimulation_1getModule(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cSimulation_1getChannel(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cSimulation_1getSystemModule(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cSimulation_1loadNedSourceFolder(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cSimulation_1loadNedText_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cSimulation_1loadNedText_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cSimulation_1loadNedText_1_1SWIG_12(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cSimulation_1doneLoadingNedFiles(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cSimulation_1getNedPackageForFolder(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cSimulation_1setFES(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cSimulation_1getFES(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cSimulation_1setSimulationTimeLimit(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cSimulation_1callInitialize(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cSimulation_1getSimulationStage(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cSimulation_1getSimTime(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cSimulation_1getEventNumber(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cSimulation_1getWarmupPeriod(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cSimulation_1setWarmupPeriod(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cSimulation_1takeNextEvent(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cSimulation_1putBackEvent(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cSimulation_1executeEvent(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cSimulation_1insertEvent(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cSimulation_1getActivityModule(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cSimulation_1getContext(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cSimulation_1getContextType(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cSimulation_1getContextModule(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cSimulation_1getContextSimpleModule(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cSimulation_1requestTrapOnNextEvent(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cSimulation_1clearTrapOnNextEvent(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cSimulation_1isTrapOnNextEventRequested(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cSimulation_1getUniqueNumber(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cSimulation_1snapshot(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cSimulation_1getFingerprintCalculator(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cSimulation_1setFingerprintCalculator(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cSimulation_1cast(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_simTime(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_getSimulation(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_getEnvir(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cClassDescriptor_1FD_1ISARRAY_1get(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cClassDescriptor_1FD_1ISCOMPOUND_1get(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cClassDescriptor_1FD_1ISPOINTER_1get(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cClassDescriptor_1FD_1ISCOBJECT_1get(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cClassDescriptor_1FD_1ISCOWNEDOBJECT_1get(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cClassDescriptor_1FD_1ISEDITABLE_1get(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cClassDescriptor_1FD_1NONE_1get(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cClassDescriptor(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cClassDescriptor_1getDescriptorFor_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cClassDescriptor_1getDescriptorFor_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cClassDescriptor_1doesSupport(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cClassDescriptor_1getBaseClassDescriptor(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cClassDescriptor_1extendsCObject(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cClassDescriptor_1getNamespace(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cClassDescriptor_1getInheritanceChainLength(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cClassDescriptor_1getPropertyNames(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cClassDescriptor_1getProperty(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cClassDescriptor_1getFieldCount(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cClassDescriptor_1getFieldName(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cClassDescriptor_1findField(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cClassDescriptor_1getFieldTypeFlags(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cClassDescriptor_1getFieldIsArray(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cClassDescriptor_1getFieldIsCompound(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cClassDescriptor_1getFieldIsPointer(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cClassDescriptor_1getFieldIsCObject(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cClassDescriptor_1getFieldIsCOwnedObject(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cClassDescriptor_1getFieldIsEditable(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cClassDescriptor_1getFieldDeclaredOn(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cClassDescriptor_1getFieldTypeString(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cClassDescriptor_1getFieldPropertyNames(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cClassDescriptor_1getFieldProperty(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cClassDescriptor_1getFieldArraySize(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cClassDescriptor_1getFieldDynamicTypeString(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cClassDescriptor_1getFieldValueAsString(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cClassDescriptor_1setFieldValueAsString(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cClassDescriptor_1getFieldStructName(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cClassDescriptor_1getFieldStructValuePointer(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cClassDescriptor_1getFieldAsCObject(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cVisitor(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cVisitor_1process(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cVisitor_1processChildrenOf(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cVisitor_1visit(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cStdVectorWatcherBase_1str(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cStdVectorWatcherBase_1supportsAssignment(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cStdVectorWatcherBase_1getElemTypeName(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cStdVectorWatcherBase_1size(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cStdVectorWatcherBase_1at(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cStdVectorWatcherBase_1getDescriptor(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cStdVectorWatcherBase(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cXMLElement(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cXMLElement_1setNodeValue(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cXMLElement_1appendNodeValue(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cXMLElement(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cXMLElement_1setAttribute(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cXMLElement_1appendChild(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cXMLElement_1insertChildBefore(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cXMLElement_1removeChild(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cXMLElement_1getName(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cXMLElement_1str(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cXMLElement_1getOwner(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cXMLElement_1forEachChild(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cXMLElement_1getTagName(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cXMLElement_1getSourceLocation(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cXMLElement_1getNodeValue(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cXMLElement_1getAttribute(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cXMLElement_1hasAttributes(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cXMLElement_1getAttributes(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cXMLElement_1getXML(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cXMLElement_1getParentNode(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cXMLElement_1hasChildren(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cXMLElement_1getFirstChild(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cXMLElement_1getLastChild(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cXMLElement_1getNextSibling(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cXMLElement_1getPreviousSibling(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cXMLElement_1getFirstChildWithTag(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cXMLElement_1getNextSiblingWithTag(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cXMLElement_1getChildren(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cXMLElement_1getChildrenByTagName(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cXMLElement_1getElementsByTagName(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cXMLElement_1getFirstChildWithAttribute_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cXMLElement_1getFirstChildWithAttribute_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cXMLElement_1getElementById(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_intrand(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_dblrand(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_uniform_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_uniform_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_exponential_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_exponential_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_normal_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_normal_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_truncnormal_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_truncnormal_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_gamma_1d(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_beta(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_erlang_1k(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_chi_1square(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_student_1t(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cauchy(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_triang(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_lognormal(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_weibull(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_pareto_1shifted(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_intuniform(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_bernoulli(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_binomial(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_geometric(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_negbinomial(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_poisson(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cIOutputVectorManager(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cIOutputVectorManager_1startRun(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cIOutputVectorManager_1endRun(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cIOutputVectorManager_1registerVector(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cIOutputVectorManager_1deregisterVector(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cIOutputVectorManager_1setVectorAttribute(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cIOutputVectorManager_1record(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cIOutputVectorManager_1getFileName(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cIOutputVectorManager_1flush(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cIOutputScalarManager(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cIOutputScalarManager_1startRun(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cIOutputScalarManager_1endRun(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cIOutputScalarManager_1recordScalar_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cIOutputScalarManager_1recordScalar_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cIOutputScalarManager_1recordStatistic_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cIOutputScalarManager_1recordStatistic_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cIOutputScalarManager_1getFileName(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cIOutputScalarManager_1flush(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cISnapshotManager(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cISnapshotManager_1startRun(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cISnapshotManager_1endRun(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cISnapshotManager_1getStreamForSnapshot(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cISnapshotManager_1releaseStreamForSnapshot(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cISnapshotManager_1getFileName(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cIEventlogManager(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cIEventlogManager_1startRecording(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cIEventlogManager_1stopRecording(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cIEventlogManager_1flush(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cIEventlogManager_1getFileName(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cIEventlogManager_1simulationEvent(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cIEventlogManager_1bubble(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cIEventlogManager_1messageScheduled(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cIEventlogManager_1messageCancelled(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cIEventlogManager_1beginSend(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cIEventlogManager_1messageSendDirect(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cIEventlogManager_1messageSendHop_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cIEventlogManager_1messageSendHop_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cIEventlogManager_1endSend(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cIEventlogManager_1messageCreated(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cIEventlogManager_1messageCloned(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cIEventlogManager_1messageDeleted(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cIEventlogManager_1moduleReparented(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cIEventlogManager_1componentMethodBegin(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cIEventlogManager_1componentMethodEnd(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cIEventlogManager_1moduleCreated(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cIEventlogManager_1moduleDeleted(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cIEventlogManager_1gateCreated(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cIEventlogManager_1gateDeleted(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cIEventlogManager_1connectionCreated(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cIEventlogManager_1connectionDeleted(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cIEventlogManager_1displayStringChanged(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cIEventlogManager_1logLine(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cIEventlogManager_1stoppedWithException(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_E_1OK_1get(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cErrorMessages(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cErrorMessages_1get(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cErrorMessages(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_resultFilters_1set(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_resultFilters_1get(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_resultRecorders_1set(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_resultRecorders_1get(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_messagePrinters_1set(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_messagePrinters_1get(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_figureTypes_1set(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_figureTypes_1get(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_RELEASE_1OPPSIM_1MAGIC_1NUMBER_1get(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1CodeFragments(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_delete_1CodeFragments(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_CodeFragments_1executeAll(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cRandom(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cRandom_1setRNG(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cRandom_1getRNG(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cRandom_1draw(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cUniform_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cUniform_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cUniform_1_1SWIG_12(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cUniform_1_1SWIG_13(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cUniform_1_1SWIG_14(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cUniform_1_1SWIG_15(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cUniform_1_1SWIG_16(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cUniform_1dup(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cUniform_1assign(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cUniform_1str(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cUniform_1setA(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cUniform_1getA(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cUniform_1setB(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cUniform_1getB(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cUniform_1draw(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cUniform(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cExponential_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cExponential_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cExponential_1_1SWIG_12(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cExponential_1_1SWIG_13(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cExponential_1_1SWIG_14(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cExponential_1_1SWIG_15(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cExponential_1dup(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cExponential_1assign(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cExponential_1str(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cExponential_1setMean(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cExponential_1getMean(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cExponential_1draw(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cExponential(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cNormal_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cNormal_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cNormal_1_1SWIG_12(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cNormal_1_1SWIG_13(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cNormal_1_1SWIG_14(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cNormal_1_1SWIG_15(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cNormal_1_1SWIG_16(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cNormal_1dup(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cNormal_1assign(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cNormal_1str(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cNormal_1setMean(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cNormal_1getMean(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cNormal_1setStddev(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cNormal_1getStddev(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cNormal_1draw(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cNormal(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cTruncNormal_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cTruncNormal_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cTruncNormal_1_1SWIG_12(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cTruncNormal_1_1SWIG_13(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cTruncNormal_1_1SWIG_14(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cTruncNormal_1_1SWIG_15(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cTruncNormal_1_1SWIG_16(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cTruncNormal_1dup(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cTruncNormal_1assign(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cTruncNormal_1str(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cTruncNormal_1setMean(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cTruncNormal_1getMean(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cTruncNormal_1setStddev(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cTruncNormal_1getStddev(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cTruncNormal_1draw(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cTruncNormal(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cGamma_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cGamma_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cGamma_1_1SWIG_12(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cGamma_1_1SWIG_13(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cGamma_1_1SWIG_14(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cGamma_1_1SWIG_15(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cGamma_1_1SWIG_16(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cGamma_1dup(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cGamma_1assign(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cGamma_1str(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cGamma_1setAlpha(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cGamma_1getAlpha(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cGamma_1setTheta(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cGamma_1getTheta(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cGamma_1draw(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cGamma(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cBeta_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cBeta_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cBeta_1_1SWIG_12(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cBeta_1_1SWIG_13(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cBeta_1_1SWIG_14(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cBeta_1_1SWIG_15(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cBeta_1_1SWIG_16(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cBeta_1dup(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cBeta_1assign(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cBeta_1str(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cBeta_1setAlpha1(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cBeta_1getAlpha1(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cBeta_1setAlpha2(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cBeta_1getAlpha2(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cBeta_1draw(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cBeta(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cErlang_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cErlang_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cErlang_1_1SWIG_12(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cErlang_1_1SWIG_13(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cErlang_1_1SWIG_14(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cErlang_1_1SWIG_15(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cErlang_1_1SWIG_16(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cErlang_1dup(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cErlang_1assign(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cErlang_1str(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cErlang_1setK(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cErlang_1getK(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cErlang_1setMean(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cErlang_1getMean(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cErlang_1draw(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cErlang(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cChiSquare_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cChiSquare_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cChiSquare_1_1SWIG_12(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cChiSquare_1_1SWIG_13(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cChiSquare_1_1SWIG_14(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cChiSquare_1_1SWIG_15(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cChiSquare_1dup(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cChiSquare_1assign(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cChiSquare_1str(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cChiSquare_1setK(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cChiSquare_1getK(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cChiSquare_1draw(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cChiSquare(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cStudentT_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cStudentT_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cStudentT_1_1SWIG_12(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cStudentT_1_1SWIG_13(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cStudentT_1_1SWIG_14(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cStudentT_1_1SWIG_15(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cStudentT_1dup(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cStudentT_1assign(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cStudentT_1str(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cStudentT_1setI(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cStudentT_1getI(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cStudentT_1draw(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cStudentT(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cCauchy_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cCauchy_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cCauchy_1_1SWIG_12(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cCauchy_1_1SWIG_13(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cCauchy_1_1SWIG_14(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cCauchy_1_1SWIG_15(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cCauchy_1_1SWIG_16(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cCauchy_1dup(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cCauchy_1assign(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cCauchy_1str(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cCauchy_1setA(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cCauchy_1getA(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cCauchy_1setB(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cCauchy_1getB(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cCauchy_1draw(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cCauchy(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cTriang_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cTriang_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cTriang_1_1SWIG_12(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cTriang_1_1SWIG_13(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cTriang_1_1SWIG_14(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cTriang_1_1SWIG_15(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cTriang_1_1SWIG_16(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cTriang_1_1SWIG_17(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cTriang_1dup(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cTriang_1assign(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cTriang_1str(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cTriang_1setA(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cTriang_1getA(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cTriang_1setB(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cTriang_1getB(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cTriang_1setC(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cTriang_1getC(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cTriang_1draw(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cTriang(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cWeibull_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cWeibull_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cWeibull_1_1SWIG_12(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cWeibull_1_1SWIG_13(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cWeibull_1_1SWIG_14(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cWeibull_1_1SWIG_15(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cWeibull_1_1SWIG_16(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cWeibull_1dup(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cWeibull_1assign(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cWeibull_1str(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cWeibull_1setA(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cWeibull_1getA(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cWeibull_1setB(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cWeibull_1getB(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cWeibull_1draw(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cWeibull(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cParetoShifted_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cParetoShifted_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cParetoShifted_1_1SWIG_12(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cParetoShifted_1_1SWIG_13(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cParetoShifted_1_1SWIG_14(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cParetoShifted_1_1SWIG_15(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cParetoShifted_1_1SWIG_16(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cParetoShifted_1_1SWIG_17(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cParetoShifted_1dup(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cParetoShifted_1assign(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cParetoShifted_1str(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cParetoShifted_1setA(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cParetoShifted_1getA(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cParetoShifted_1setB(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cParetoShifted_1getB(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cParetoShifted_1setC(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cParetoShifted_1getC(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cParetoShifted_1draw(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cParetoShifted(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cIntUniform_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cIntUniform_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cIntUniform_1_1SWIG_12(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cIntUniform_1_1SWIG_13(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cIntUniform_1_1SWIG_14(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cIntUniform_1_1SWIG_15(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cIntUniform_1_1SWIG_16(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cIntUniform_1dup(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cIntUniform_1assign(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cIntUniform_1str(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cIntUniform_1setA(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cIntUniform_1getA(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cIntUniform_1setB(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cIntUniform_1getB(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cIntUniform_1draw(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cIntUniform(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cBernoulli_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cBernoulli_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cBernoulli_1_1SWIG_12(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cBernoulli_1_1SWIG_13(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cBernoulli_1_1SWIG_14(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cBernoulli_1_1SWIG_15(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cBernoulli_1dup(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cBernoulli_1assign(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cBernoulli_1str(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cBernoulli_1setP(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cBernoulli_1getP(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cBernoulli_1draw(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cBernoulli(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cBinomial_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cBinomial_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cBinomial_1_1SWIG_12(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cBinomial_1_1SWIG_13(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cBinomial_1_1SWIG_14(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cBinomial_1_1SWIG_15(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cBinomial_1_1SWIG_16(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cBinomial_1dup(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cBinomial_1assign(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cBinomial_1str(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cBinomial_1setN(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cBinomial_1getN(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cBinomial_1setP(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cBinomial_1getP(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cBinomial_1draw(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cBinomial(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cGeometric_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cGeometric_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cGeometric_1_1SWIG_12(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cGeometric_1_1SWIG_13(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cGeometric_1_1SWIG_14(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cGeometric_1_1SWIG_15(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cGeometric_1dup(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cGeometric_1assign(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cGeometric_1str(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cGeometric_1setP(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cGeometric_1getP(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cGeometric_1draw(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cGeometric(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cNegBinomial_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cNegBinomial_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cNegBinomial_1_1SWIG_12(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cNegBinomial_1_1SWIG_13(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cNegBinomial_1_1SWIG_14(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cNegBinomial_1_1SWIG_15(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cNegBinomial_1_1SWIG_16(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cNegBinomial_1dup(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cNegBinomial_1assign(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cNegBinomial_1str(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cNegBinomial_1setN(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cNegBinomial_1getN(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cNegBinomial_1setP(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cNegBinomial_1getP(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cNegBinomial_1draw(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cNegBinomial(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cPoisson_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cPoisson_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cPoisson_1_1SWIG_12(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cPoisson_1_1SWIG_13(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cPoisson_1_1SWIG_14(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cPoisson_1_1SWIG_15(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cPoisson_1dup(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cPoisson_1assign(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cPoisson_1str(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cPoisson_1setLambda(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cPoisson_1getLambda(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cPoisson_1draw(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cPoisson(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cRegistrationList(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cRegistrationList(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cRegistrationList_1str(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cRegistrationList_1forEachChild(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cRegistrationList_1begin(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cRegistrationList_1end(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cRegistrationList_1add(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cRegistrationList_1size(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cRegistrationList_1get(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cRegistrationList_1find(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cRegistrationList_1lookup_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cRegistrationList_1lookup_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cRegistrationList_1lookup_1_1SWIG_12(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cRegistrationList_1sort(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cGlobalRegistrationList_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cGlobalRegistrationList_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cGlobalRegistrationList(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cGlobalRegistrationList_1getInstance(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cGlobalRegistrationList_1clear(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cGlobalRegistrationList_1begin(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cGlobalRegistrationList_1end(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_CTX_1NONE_1get(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_CTX_1BUILD_1get(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_CTX_1INITIALIZE_1get(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_CTX_1EVENT_1get(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_CTX_1REFRESHDISPLAY_1get(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_CTX_1FINISH_1get(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_CTX_1CLEANUP_1get(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_intCastError_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_intCastError_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_intCastError_1_1SWIG_12(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_intCastError_1_1SWIG_13(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_opp_1strlen(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_opp_1strdup_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_opp_1strcpy(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_opp_1strcmp(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_opp_1strprettytrunc(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_opp_1get_1monotonic_1clock_1nsecs(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_opp_1get_1monotonic_1clock_1usecs(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_opp_1demangle_1typename(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1cMethodCallContextSwitcher(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cMethodCallContextSwitcher(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cMethodCallContextSwitcher_1methodCall(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cMethodCallContextSwitcher_1methodCallSilent_1_1SWIG_10(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cMethodCallContextSwitcher_1methodCallSilent_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_cMethodCallContextSwitcher_1getDepth(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_opp_1strdup_1_1SWIG_11(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_opp_1appendindex(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_double_1to_1str(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_delete_1JSimpleModule(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_JSimpleModule_1retrieveMsgToBeHandled(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_JSimpleModule_1swigJavaPeer(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_JSimpleModule_1swigJavaPeerOf(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_new_1JMessage(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_delete_1JMessage(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_JMessage_1dup(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_JMessage_1info(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_JMessage_1detailedInfo(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_JMessage_1swigSetJavaPeer(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_JMessage_1swigJavaPeer(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_JMessage_1swigJavaPeerOf(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_SWIGcNamedObjectUpcast(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_SWIGcOwnedObjectUpcast(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_SWIGcNoncopyableOwnedObjectUpcast(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_SWIGcDefaultListUpcast(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_SWIGcListenerUpcast(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_SWIGcComponentUpcast(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_SWIGcChannelUpcast(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_SWIGcIdealChannelUpcast(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_SWIGcDelayChannelUpcast(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_SWIGcDatarateChannelUpcast(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_SWIGcModuleUpcast(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_SWIGcSimpleModuleUpcast(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_SWIGcComponentTypeUpcast(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_SWIGcModuleTypeUpcast(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_SWIGcChannelTypeUpcast(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_SWIGcArrayUpcast(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_SWIGcQueueUpcast(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_SWIGcStdDevUpcast(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_SWIGcWeightedStdDevUpcast(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_SWIGcRNGUpcast(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_SWIGcObjectFactoryUpcast(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_SWIGcEnumUpcast(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_SWIGcRuntimeErrorUpcast(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_SWIGcDeleteModuleExceptionUpcast(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_SWIGcExpressionUpcast(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_SWIGcFSMUpcast(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_SWIGcGateUpcast(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_SWIGcMessageUpcast(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_SWIGcMsgParUpcast(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_SWIGcEventUpcast(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_SWIGcOutVectorUpcast(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_SWIGcParUpcast(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_SWIGcPropertyUpcast(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_SWIGcPropertiesUpcast(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_SWIGcSimulationUpcast(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_SWIGcClassDescriptorUpcast(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_SWIGcXMLElementUpcast(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_SWIGcIOutputVectorManagerUpcast(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_SWIGcIOutputScalarManagerUpcast(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_SWIGcISnapshotManagerUpcast(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_SWIGcIEventlogManagerUpcast(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_SWIGcRandomUpcast(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_SWIGcUniformUpcast(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_SWIGcExponentialUpcast(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_SWIGcNormalUpcast(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_SWIGcTruncNormalUpcast(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_SWIGcGammaUpcast(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_SWIGcBetaUpcast(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_SWIGcErlangUpcast(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_SWIGcChiSquareUpcast(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_SWIGcStudentTUpcast(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_SWIGcCauchyUpcast(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_SWIGcTriangUpcast(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_SWIGcWeibullUpcast(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_SWIGcParetoShiftedUpcast(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_SWIGcIntUniformUpcast(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_SWIGcBernoulliUpcast(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_SWIGcBinomialUpcast(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_SWIGcGeometricUpcast(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_SWIGcNegBinomialUpcast(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_SWIGcPoissonUpcast(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_SWIGcRegistrationListUpcast(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_SWIGJSimpleModuleUpcast(JNIEnv *jenv, jclass jcls,...);
void Java_org_omnetpp_simkernel_SimkernelJNI_SWIGJMessageUpcast(JNIEnv *jenv, jclass jcls,...);
};

static JNINativeMethod SimkernelJNI_methods[] = {
    { (char *)"INT8_MIN_get", (char *)"()I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_INT8_1MIN_1get },
    { (char *)"INT16_MIN_get", (char *)"()I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_INT16_1MIN_1get },
    { (char *)"INT32_MIN_get", (char *)"()I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_INT32_1MIN_1get },
    { (char *)"INT64_MIN_get", (char *)"()J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_INT64_1MIN_1get },
    { (char *)"INT8_MAX_get", (char *)"()I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_INT8_1MAX_1get },
    { (char *)"INT16_MAX_get", (char *)"()I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_INT16_1MAX_1get },
    { (char *)"INT32_MAX_get", (char *)"()I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_INT32_1MAX_1get },
    { (char *)"INT64_MAX_get", (char *)"()J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_INT64_1MAX_1get },
    { (char *)"UINT8_MAX_get", (char *)"()I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_UINT8_1MAX_1get },
    { (char *)"UINT16_MAX_get", (char *)"()I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_UINT16_1MAX_1get },
    { (char *)"UINT32_MAX_get", (char *)"()J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_UINT32_1MAX_1get },
    { (char *)"new_StringMap__SWIG_0", (char *)"()J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1StringMap_1_1SWIG_10 },
    { (char *)"new_StringMap__SWIG_1", (char *)"(JLorg/omnetpp/simkernel/StringMap;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1StringMap_1_1SWIG_11 },
    { (char *)"StringMap_size", (char *)"(JLorg/omnetpp/simkernel/StringMap;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_StringMap_1size },
    { (char *)"StringMap_empty", (char *)"(JLorg/omnetpp/simkernel/StringMap;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_StringMap_1empty },
    { (char *)"StringMap_clear", (char *)"(JLorg/omnetpp/simkernel/StringMap;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_StringMap_1clear },
    { (char *)"StringMap_get", (char *)"(JLorg/omnetpp/simkernel/StringMap;Ljava/lang/String;)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_StringMap_1get },
    { (char *)"StringMap_set", (char *)"(JLorg/omnetpp/simkernel/StringMap;Ljava/lang/String;Ljava/lang/String;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_StringMap_1set },
    { (char *)"StringMap_del", (char *)"(JLorg/omnetpp/simkernel/StringMap;Ljava/lang/String;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_StringMap_1del },
    { (char *)"StringMap_has_key", (char *)"(JLorg/omnetpp/simkernel/StringMap;Ljava/lang/String;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_StringMap_1has_1key },
    { (char *)"delete_StringMap", (char *)"(J)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_delete_1StringMap },
    { (char *)"cXMLElementVector_size", (char *)"(JLorg/omnetpp/simkernel/cXMLElementVector;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cXMLElementVector_1size },
    { (char *)"cXMLElementVector_isEmpty", (char *)"(JLorg/omnetpp/simkernel/cXMLElementVector;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cXMLElementVector_1isEmpty },
    { (char *)"cXMLElementVector_add", (char *)"(JLorg/omnetpp/simkernel/cXMLElementVector;JLorg/omnetpp/simkernel/cXMLElement;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cXMLElementVector_1add },
    { (char *)"cXMLElementVector_get", (char *)"(JLorg/omnetpp/simkernel/cXMLElementVector;I)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cXMLElementVector_1get },
    { (char *)"delete_cXMLElementVector", (char *)"(J)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cXMLElementVector },
    { (char *)"StringVector_size", (char *)"(JLorg/omnetpp/simkernel/StringVector;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_StringVector_1size },
    { (char *)"StringVector_isEmpty", (char *)"(JLorg/omnetpp/simkernel/StringVector;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_StringVector_1isEmpty },
    { (char *)"StringVector_add", (char *)"(JLorg/omnetpp/simkernel/StringVector;Ljava/lang/String;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_StringVector_1add },
    { (char *)"StringVector_get", (char *)"(JLorg/omnetpp/simkernel/StringVector;I)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_StringVector_1get },
    { (char *)"delete_StringVector", (char *)"(J)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_delete_1StringVector },
    { (char *)"getEv", (char *)"()J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_getEv },
    { (char *)"getDefaultList", (char *)"()J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_getDefaultList },
    { (char *)"getRegisteredComponentTypes", (char *)"()J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_getRegisteredComponentTypes },
    { (char *)"getRegisteredNedFunctions", (char *)"()J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_getRegisteredNedFunctions },
    { (char *)"getRegisteredClasses", (char *)"()J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_getRegisteredClasses },
    { (char *)"getRegisteredEnums", (char *)"()J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_getRegisteredEnums },
    { (char *)"getRegisteredClassDescriptors", (char *)"()J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_getRegisteredClassDescriptors },
    { (char *)"getRegisteredConfigOptions", (char *)"()J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_getRegisteredConfigOptions },
    { (char *)"new_cModule_GateIterator", (char *)"(JLorg/omnetpp/simkernel/cModule;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cModule_1GateIterator },
    { (char *)"cModule_GateIterator_get", (char *)"(JLorg/omnetpp/simkernel/cModule_GateIterator;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1GateIterator_1get },
    { (char *)"cModule_GateIterator_end", (char *)"(JLorg/omnetpp/simkernel/cModule_GateIterator;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1GateIterator_1end },
    { (char *)"cModule_GateIterator_next", (char *)"(JLorg/omnetpp/simkernel/cModule_GateIterator;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1GateIterator_1next },
    { (char *)"cModule_GateIterator_advance", (char *)"(JLorg/omnetpp/simkernel/cModule_GateIterator;I)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1GateIterator_1advance },
    { (char *)"delete_cModule_GateIterator", (char *)"(J)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cModule_1GateIterator },
    { (char *)"new_cModule_SubmoduleIterator", (char *)"(JLorg/omnetpp/simkernel/cModule;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cModule_1SubmoduleIterator },
    { (char *)"cModule_SubmoduleIterator_get", (char *)"(JLorg/omnetpp/simkernel/cModule_SubmoduleIterator;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1SubmoduleIterator_1get },
    { (char *)"cModule_SubmoduleIterator_end", (char *)"(JLorg/omnetpp/simkernel/cModule_SubmoduleIterator;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1SubmoduleIterator_1end },
    { (char *)"cModule_SubmoduleIterator_next", (char *)"(JLorg/omnetpp/simkernel/cModule_SubmoduleIterator;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1SubmoduleIterator_1next },
    { (char *)"delete_cModule_SubmoduleIterator", (char *)"(J)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cModule_1SubmoduleIterator },
    { (char *)"new_cModule_ChannelIterator", (char *)"(JLorg/omnetpp/simkernel/cModule;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cModule_1ChannelIterator },
    { (char *)"cModule_ChannelIterator_get", (char *)"(JLorg/omnetpp/simkernel/cModule_ChannelIterator;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1ChannelIterator_1get },
    { (char *)"cModule_ChannelIterator_end", (char *)"(JLorg/omnetpp/simkernel/cModule_ChannelIterator;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1ChannelIterator_1end },
    { (char *)"cModule_ChannelIterator_next", (char *)"(JLorg/omnetpp/simkernel/cModule_ChannelIterator;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1ChannelIterator_1next },
    { (char *)"delete_cModule_ChannelIterator", (char *)"(J)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cModule_1ChannelIterator },
    { (char *)"OMNETPP_VERSION_get", (char *)"()I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_OMNETPP_1VERSION_1get },
    { (char *)"OMNETPP_BUILDNUM_get", (char *)"()I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_OMNETPP_1BUILDNUM_1get },
    { (char *)"PI_get", (char *)"()D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_PI_1get },
    { (char *)"INT64_MAX_DBL_get", (char *)"()D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_INT64_1MAX_1DBL_1get },
    { (char *)"SIMTIME_S_get", (char *)"()I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_SIMTIME_1S_1get },
    { (char *)"SIMTIME_MS_get", (char *)"()I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_SIMTIME_1MS_1get },
    { (char *)"SIMTIME_US_get", (char *)"()I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_SIMTIME_1US_1get },
    { (char *)"SIMTIME_NS_get", (char *)"()I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_SIMTIME_1NS_1get },
    { (char *)"SIMTIME_PS_get", (char *)"()I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_SIMTIME_1PS_1get },
    { (char *)"SIMTIME_FS_get", (char *)"()I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_SIMTIME_1FS_1get },
    { (char *)"SIMTIME_AS_get", (char *)"()I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_SIMTIME_1AS_1get },
    { (char *)"SimTime_ZERO_get", (char *)"()J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_SimTime_1ZERO_1get },
    { (char *)"SimTime_format__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/SimTime;ILjava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_SimTime_1format_1_1SWIG_10 },
    { (char *)"SimTime_format__SWIG_1", (char *)"(JLorg/omnetpp/simkernel/SimTime;ILjava/lang/String;Ljava/lang/String;ZLjava/lang/String;)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_SimTime_1format_1_1SWIG_11 },
    { (char *)"SimTime_format__SWIG_2", (char *)"(JLorg/omnetpp/simkernel/SimTime;ILjava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_SimTime_1format_1_1SWIG_12 },
    { (char *)"SimTime_format__SWIG_3", (char *)"(JLorg/omnetpp/simkernel/SimTime;ILjava/lang/String;Ljava/lang/String;)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_SimTime_1format_1_1SWIG_13 },
    { (char *)"SimTime_format__SWIG_4", (char *)"(JLorg/omnetpp/simkernel/SimTime;ILjava/lang/String;)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_SimTime_1format_1_1SWIG_14 },
    { (char *)"SimTime_format__SWIG_5", (char *)"(JLorg/omnetpp/simkernel/SimTime;I)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_SimTime_1format_1_1SWIG_15 },
    { (char *)"SimTime_format__SWIG_6", (char *)"(JLorg/omnetpp/simkernel/SimTime;)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_SimTime_1format_1_1SWIG_16 },
    { (char *)"new_SimTime__SWIG_0", (char *)"()J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1SimTime_1_1SWIG_10 },
    { (char *)"new_SimTime__SWIG_1", (char *)"(D)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1SimTime_1_1SWIG_11 },
    { (char *)"new_SimTime__SWIG_2", (char *)"(JLorg/omnetpp/simkernel/cPar;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1SimTime_1_1SWIG_12 },
    { (char *)"new_SimTime__SWIG_3", (char *)"(JI)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1SimTime_1_1SWIG_13 },
    { (char *)"new_SimTime__SWIG_4", (char *)"(JLorg/omnetpp/simkernel/SimTime;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1SimTime_1_1SWIG_14 },
    { (char *)"SimTime_assign__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/SimTime;JLorg/omnetpp/simkernel/SimTime;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_SimTime_1assign_1_1SWIG_10 },
    { (char *)"SimTime_assign__SWIG_1", (char *)"(JLorg/omnetpp/simkernel/SimTime;JLorg/omnetpp/simkernel/cPar;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_SimTime_1assign_1_1SWIG_11 },
    { (char *)"SimTime_assign__SWIG_2", (char *)"(JLorg/omnetpp/simkernel/SimTime;D)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_SimTime_1assign_1_1SWIG_12 },
    { (char *)"SimTime_assign__SWIG_3", (char *)"(JLorg/omnetpp/simkernel/SimTime;S)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_SimTime_1assign_1_1SWIG_13 },
    { (char *)"SimTime_assign__SWIG_4", (char *)"(JLorg/omnetpp/simkernel/SimTime;I)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_SimTime_1assign_1_1SWIG_14 },
    { (char *)"SimTime_assign__SWIG_6", (char *)"(JLorg/omnetpp/simkernel/SimTime;J)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_SimTime_1assign_1_1SWIG_16 },
    // XXX This methods leads into a crash of registerNatives ==> commented out
    //{ (char *)"SimTime_assign__SWIG_10", (char *)"(JLorg/omnetpp/simkernel/SimTime;Lorg/omnetpp/simkernel/java;.Lorg/omnetpp/simkernel/math;.Lorg/omnetpp/simkernel/BigInteger;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_SimTime_1assign_1_1SWIG_110 },
    { (char *)"SimTime_isZero", (char *)"(JLorg/omnetpp/simkernel/SimTime;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_SimTime_1isZero },
    { (char *)"SimTime_dbl", (char *)"(JLorg/omnetpp/simkernel/SimTime;)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_SimTime_1dbl },
    { (char *)"SimTime_inUnit", (char *)"(JLorg/omnetpp/simkernel/SimTime;I)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_SimTime_1inUnit },
    { (char *)"SimTime_trunc", (char *)"(JLorg/omnetpp/simkernel/SimTime;I)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_SimTime_1trunc },
    { (char *)"SimTime_remainderForUnit", (char *)"(JLorg/omnetpp/simkernel/SimTime;I)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_SimTime_1remainderForUnit },
    { (char *)"SimTime_split", (char *)"(JLorg/omnetpp/simkernel/SimTime;IJJLorg/omnetpp/simkernel/SimTime;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_SimTime_1split },
    { (char *)"SimTime_str__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/SimTime;)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_SimTime_1str_1_1SWIG_10 },
    { (char *)"SimTime_str__SWIG_1", (char *)"(JLorg/omnetpp/simkernel/SimTime;Ljava/lang/String;)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_SimTime_1str_1_1SWIG_11 },
    { (char *)"SimTime_ustr__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/SimTime;)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_SimTime_1ustr_1_1SWIG_10 },
    { (char *)"SimTime_ustr__SWIG_1", (char *)"(JLorg/omnetpp/simkernel/SimTime;I)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_SimTime_1ustr_1_1SWIG_11 },
    { (char *)"SimTime_raw", (char *)"(JLorg/omnetpp/simkernel/SimTime;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_SimTime_1raw },
    { (char *)"SimTime_setRaw", (char *)"(JLorg/omnetpp/simkernel/SimTime;J)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_SimTime_1setRaw },
    { (char *)"SimTime_getMaxTime", (char *)"()J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_SimTime_1getMaxTime },
    { (char *)"SimTime_getScale", (char *)"()J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_SimTime_1getScale },
    { (char *)"SimTime_getScaleExp", (char *)"()I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_SimTime_1getScaleExp },
    { (char *)"SimTime_setScaleExp", (char *)"(I)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_SimTime_1setScaleExp },
    { (char *)"SimTime_parse", (char *)"(Ljava/lang/String;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_SimTime_1parse },
    { (char *)"SimTime_add__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/SimTime;JLorg/omnetpp/simkernel/SimTime;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_SimTime_1add_1_1SWIG_10 },
    { (char *)"SimTime_substract__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/SimTime;JLorg/omnetpp/simkernel/SimTime;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_SimTime_1substract_1_1SWIG_10 },
    { (char *)"SimTime_add__SWIG_1", (char *)"(JLorg/omnetpp/simkernel/SimTime;D)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_SimTime_1add_1_1SWIG_11 },
    { (char *)"SimTime_substract__SWIG_1", (char *)"(JLorg/omnetpp/simkernel/SimTime;D)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_SimTime_1substract_1_1SWIG_11 },
    { (char *)"delete_SimTime", (char *)"(J)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_delete_1SimTime },
    { (char *)"new_cObject__SWIG_0", (char *)"()J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cObject_1_1SWIG_10 },
    { (char *)"new_cObject__SWIG_1", (char *)"(JLorg/omnetpp/simkernel/cObject;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cObject_1_1SWIG_11 },
    { (char *)"delete_cObject", (char *)"(J)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cObject },
    { (char *)"cObject_getClassName", (char *)"(JLorg/omnetpp/simkernel/cObject;)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cObject_1getClassName },
    { (char *)"cObject_getName", (char *)"(JLorg/omnetpp/simkernel/cObject;)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cObject_1getName },
    { (char *)"cObject_isName", (char *)"(JLorg/omnetpp/simkernel/cObject;Ljava/lang/String;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cObject_1isName },
    { (char *)"cObject_getFullName", (char *)"(JLorg/omnetpp/simkernel/cObject;)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cObject_1getFullName },
    { (char *)"cObject_getFullPath", (char *)"(JLorg/omnetpp/simkernel/cObject;)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cObject_1getFullPath },
    { (char *)"cObject_getThisPtr", (char *)"(JLorg/omnetpp/simkernel/cObject;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cObject_1getThisPtr },
    { (char *)"cObject_str", (char *)"(JLorg/omnetpp/simkernel/cObject;)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cObject_1str },
    { (char *)"cObject_info", (char *)"(JLorg/omnetpp/simkernel/cObject;)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cObject_1info },
    { (char *)"cObject_detailedInfo", (char *)"(JLorg/omnetpp/simkernel/cObject;)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cObject_1detailedInfo },
    { (char *)"cObject_dup", (char *)"(JLorg/omnetpp/simkernel/cObject;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cObject_1dup },
    { (char *)"cObject_getOwner", (char *)"(JLorg/omnetpp/simkernel/cObject;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cObject_1getOwner },
    { (char *)"cObject_isOwnedObject", (char *)"(JLorg/omnetpp/simkernel/cObject;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cObject_1isOwnedObject },
    { (char *)"cObject_forEachChild", (char *)"(JLorg/omnetpp/simkernel/cObject;JLorg/omnetpp/simkernel/cVisitor;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cObject_1forEachChild },
    { (char *)"cObject_findObject__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cObject;Ljava/lang/String;Z)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cObject_1findObject_1_1SWIG_10 },
    { (char *)"cObject_findObject__SWIG_1", (char *)"(JLorg/omnetpp/simkernel/cObject;Ljava/lang/String;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cObject_1findObject_1_1SWIG_11 },
    { (char *)"cObject_copyNotSupported", (char *)"(JLorg/omnetpp/simkernel/cObject;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cObject_1copyNotSupported },
    { (char *)"cObject_hasField", (char *)"(JLorg/omnetpp/simkernel/cObject;Ljava/lang/String;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cObject_1hasField },
    { (char *)"cObject_getField", (char *)"(JLorg/omnetpp/simkernel/cObject;Ljava/lang/String;)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cObject_1getField },
    { (char *)"cObject_getArrayField", (char *)"(JLorg/omnetpp/simkernel/cObject;Ljava/lang/String;I)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cObject_1getArrayField },
    { (char *)"cObject_setField", (char *)"(JLorg/omnetpp/simkernel/cObject;Ljava/lang/String;Ljava/lang/String;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cObject_1setField },
    { (char *)"cObject_setArrayField", (char *)"(JLorg/omnetpp/simkernel/cObject;Ljava/lang/String;ILjava/lang/String;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cObject_1setArrayField },
    { (char *)"cObject_isFieldArray", (char *)"(JLorg/omnetpp/simkernel/cObject;Ljava/lang/String;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cObject_1isFieldArray },
    { (char *)"cObject_isFieldCompound", (char *)"(JLorg/omnetpp/simkernel/cObject;Ljava/lang/String;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cObject_1isFieldCompound },
    { (char *)"cObject_cast", (char *)"(JLorg/omnetpp/simkernel/cObject;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cObject_1cast },
    { (char *)"new_cNamedObject__SWIG_0", (char *)"()J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cNamedObject_1_1SWIG_10 },
    { (char *)"new_cNamedObject__SWIG_1", (char *)"(Ljava/lang/String;Z)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cNamedObject_1_1SWIG_11 },
    { (char *)"new_cNamedObject__SWIG_2", (char *)"(Ljava/lang/String;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cNamedObject_1_1SWIG_12 },
    { (char *)"new_cNamedObject__SWIG_3", (char *)"(JLorg/omnetpp/simkernel/cNamedObject;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cNamedObject_1_1SWIG_13 },
    { (char *)"delete_cNamedObject", (char *)"(J)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cNamedObject },
    { (char *)"cNamedObject_assign", (char *)"(JLorg/omnetpp/simkernel/cNamedObject;JLorg/omnetpp/simkernel/cNamedObject;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cNamedObject_1assign },
    { (char *)"cNamedObject_setName", (char *)"(JLorg/omnetpp/simkernel/cNamedObject;Ljava/lang/String;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cNamedObject_1setName },
    { (char *)"cNamedObject_getName", (char *)"(JLorg/omnetpp/simkernel/cNamedObject;)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cNamedObject_1getName },
    { (char *)"cNamedObject_setNamePooling", (char *)"(JLorg/omnetpp/simkernel/cNamedObject;Z)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cNamedObject_1setNamePooling },
    { (char *)"cNamedObject_getNamePooling", (char *)"(JLorg/omnetpp/simkernel/cNamedObject;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cNamedObject_1getNamePooling },
    { (char *)"new_cOwnedObject__SWIG_0", (char *)"()J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cOwnedObject_1_1SWIG_10 },
    { (char *)"new_cOwnedObject__SWIG_1", (char *)"(Ljava/lang/String;Z)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cOwnedObject_1_1SWIG_11 },
    { (char *)"new_cOwnedObject__SWIG_2", (char *)"(Ljava/lang/String;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cOwnedObject_1_1SWIG_12 },
    { (char *)"new_cOwnedObject__SWIG_3", (char *)"(JLorg/omnetpp/simkernel/cOwnedObject;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cOwnedObject_1_1SWIG_13 },
    { (char *)"delete_cOwnedObject", (char *)"(J)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cOwnedObject },
    { (char *)"cOwnedObject_assign", (char *)"(JLorg/omnetpp/simkernel/cOwnedObject;JLorg/omnetpp/simkernel/cOwnedObject;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cOwnedObject_1assign },
    { (char *)"cOwnedObject_getOwner", (char *)"(JLorg/omnetpp/simkernel/cOwnedObject;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cOwnedObject_1getOwner },
    { (char *)"cOwnedObject_isOwnedObject", (char *)"(JLorg/omnetpp/simkernel/cOwnedObject;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cOwnedObject_1isOwnedObject },
    { (char *)"cOwnedObject_isSoftOwner", (char *)"(JLorg/omnetpp/simkernel/cOwnedObject;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cOwnedObject_1isSoftOwner },
    { (char *)"cOwnedObject_getDefaultOwner", (char *)"()J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cOwnedObject_1getDefaultOwner },
    { (char *)"cOwnedObject_getTotalObjectCount", (char *)"()I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cOwnedObject_1getTotalObjectCount },
    { (char *)"cOwnedObject_getLiveObjectCount", (char *)"()I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cOwnedObject_1getLiveObjectCount },
    { (char *)"cOwnedObject_resetObjectCounters", (char *)"()V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cOwnedObject_1resetObjectCounters },
    { (char *)"new_cNoncopyableOwnedObject__SWIG_0", (char *)"(Ljava/lang/String;Z)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cNoncopyableOwnedObject_1_1SWIG_10 },
    { (char *)"new_cNoncopyableOwnedObject__SWIG_1", (char *)"(Ljava/lang/String;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cNoncopyableOwnedObject_1_1SWIG_11 },
    { (char *)"new_cNoncopyableOwnedObject__SWIG_2", (char *)"()J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cNoncopyableOwnedObject_1_1SWIG_12 },
    { (char *)"cNoncopyableOwnedObject_dup", (char *)"(JLorg/omnetpp/simkernel/cNoncopyableOwnedObject;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cNoncopyableOwnedObject_1dup },
    { (char *)"delete_cNoncopyableOwnedObject", (char *)"(J)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cNoncopyableOwnedObject },
    { (char *)"cDefaultList_takeAllObjectsFrom", (char *)"(JLorg/omnetpp/simkernel/cDefaultList;JLorg/omnetpp/simkernel/cDefaultList;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cDefaultList_1takeAllObjectsFrom },
    { (char *)"new_cDefaultList__SWIG_0", (char *)"(Ljava/lang/String;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cDefaultList_1_1SWIG_10 },
    { (char *)"new_cDefaultList__SWIG_1", (char *)"()J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cDefaultList_1_1SWIG_11 },
    { (char *)"delete_cDefaultList", (char *)"(J)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cDefaultList },
    { (char *)"cDefaultList_isSoftOwner", (char *)"(JLorg/omnetpp/simkernel/cDefaultList;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cDefaultList_1isSoftOwner },
    { (char *)"cDefaultList_str", (char *)"(JLorg/omnetpp/simkernel/cDefaultList;)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cDefaultList_1str },
    { (char *)"cDefaultList_forEachChild", (char *)"(JLorg/omnetpp/simkernel/cDefaultList;JLorg/omnetpp/simkernel/cVisitor;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cDefaultList_1forEachChild },
    { (char *)"cDefaultList_getPerformFinalGC", (char *)"(JLorg/omnetpp/simkernel/cDefaultList;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cDefaultList_1getPerformFinalGC },
    { (char *)"cDefaultList_setPerformFinalGC", (char *)"(JLorg/omnetpp/simkernel/cDefaultList;Z)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cDefaultList_1setPerformFinalGC },
    { (char *)"cDefaultList_defaultListSize", (char *)"(JLorg/omnetpp/simkernel/cDefaultList;)I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cDefaultList_1defaultListSize },
    { (char *)"cDefaultList_defaultListGet", (char *)"(JLorg/omnetpp/simkernel/cDefaultList;I)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cDefaultList_1defaultListGet },
    { (char *)"cDefaultList_defaultListContains", (char *)"(JLorg/omnetpp/simkernel/cDefaultList;JLorg/omnetpp/simkernel/cOwnedObject;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cDefaultList_1defaultListContains },
    { (char *)"cDefaultList_cast", (char *)"(JLorg/omnetpp/simkernel/cObject;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cDefaultList_1cast },
    { (char *)"delete_cIListener", (char *)"(J)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cIListener },
    { (char *)"cIListener_receiveSignal__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cIListener;JLorg/omnetpp/simkernel/cComponent;IZJLorg/omnetpp/simkernel/cObject;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cIListener_1receiveSignal_1_1SWIG_10 },
    { (char *)"cIListener_receiveSignal__SWIG_1", (char *)"(JLorg/omnetpp/simkernel/cIListener;JLorg/omnetpp/simkernel/cComponent;IIJLorg/omnetpp/simkernel/cObject;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cIListener_1receiveSignal_1_1SWIG_11 },
    { (char *)"cIListener_receiveSignal__SWIG_2", (char *)"(JLorg/omnetpp/simkernel/cIListener;JLorg/omnetpp/simkernel/cComponent;IJJLorg/omnetpp/simkernel/cObject;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cIListener_1receiveSignal_1_1SWIG_12 },
    { (char *)"cIListener_receiveSignal__SWIG_3", (char *)"(JLorg/omnetpp/simkernel/cIListener;JLorg/omnetpp/simkernel/cComponent;IDJLorg/omnetpp/simkernel/cObject;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cIListener_1receiveSignal_1_1SWIG_13 },
    { (char *)"cIListener_receiveSignal__SWIG_4", (char *)"(JLorg/omnetpp/simkernel/cIListener;JLorg/omnetpp/simkernel/cComponent;IJLorg/omnetpp/simkernel/SimTime;JLorg/omnetpp/simkernel/cObject;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cIListener_1receiveSignal_1_1SWIG_14 },
    { (char *)"cIListener_receiveSignal__SWIG_5", (char *)"(JLorg/omnetpp/simkernel/cIListener;JLorg/omnetpp/simkernel/cComponent;ILjava/lang/String;JLorg/omnetpp/simkernel/cObject;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cIListener_1receiveSignal_1_1SWIG_15 },
    { (char *)"cIListener_receiveSignal__SWIG_6", (char *)"(JLorg/omnetpp/simkernel/cIListener;JLorg/omnetpp/simkernel/cComponent;IJLorg/omnetpp/simkernel/cObject;JLorg/omnetpp/simkernel/cObject;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cIListener_1receiveSignal_1_1SWIG_16 },
    { (char *)"cIListener_finish", (char *)"(JLorg/omnetpp/simkernel/cIListener;JLorg/omnetpp/simkernel/cComponent;I)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cIListener_1finish },
    { (char *)"cIListener_subscribedTo", (char *)"(JLorg/omnetpp/simkernel/cIListener;JLorg/omnetpp/simkernel/cComponent;I)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cIListener_1subscribedTo },
    { (char *)"cIListener_unsubscribedFrom", (char *)"(JLorg/omnetpp/simkernel/cIListener;JLorg/omnetpp/simkernel/cComponent;I)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cIListener_1unsubscribedFrom },
    { (char *)"cIListener_getSubscribeCount", (char *)"(JLorg/omnetpp/simkernel/cIListener;)I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cIListener_1getSubscribeCount },
    { (char *)"cListener_receiveSignal__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cListener;JLorg/omnetpp/simkernel/cComponent;IZJLorg/omnetpp/simkernel/cObject;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cListener_1receiveSignal_1_1SWIG_10 },
    { (char *)"cListener_receiveSignal__SWIG_1", (char *)"(JLorg/omnetpp/simkernel/cListener;JLorg/omnetpp/simkernel/cComponent;IIJLorg/omnetpp/simkernel/cObject;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cListener_1receiveSignal_1_1SWIG_11 },
    { (char *)"cListener_receiveSignal__SWIG_2", (char *)"(JLorg/omnetpp/simkernel/cListener;JLorg/omnetpp/simkernel/cComponent;IJJLorg/omnetpp/simkernel/cObject;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cListener_1receiveSignal_1_1SWIG_12 },
    { (char *)"cListener_receiveSignal__SWIG_3", (char *)"(JLorg/omnetpp/simkernel/cListener;JLorg/omnetpp/simkernel/cComponent;IDJLorg/omnetpp/simkernel/cObject;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cListener_1receiveSignal_1_1SWIG_13 },
    { (char *)"cListener_receiveSignal__SWIG_4", (char *)"(JLorg/omnetpp/simkernel/cListener;JLorg/omnetpp/simkernel/cComponent;IJLorg/omnetpp/simkernel/SimTime;JLorg/omnetpp/simkernel/cObject;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cListener_1receiveSignal_1_1SWIG_14 },
    { (char *)"cListener_receiveSignal__SWIG_5", (char *)"(JLorg/omnetpp/simkernel/cListener;JLorg/omnetpp/simkernel/cComponent;ILjava/lang/String;JLorg/omnetpp/simkernel/cObject;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cListener_1receiveSignal_1_1SWIG_15 },
    { (char *)"cListener_receiveSignal__SWIG_6", (char *)"(JLorg/omnetpp/simkernel/cListener;JLorg/omnetpp/simkernel/cComponent;IJLorg/omnetpp/simkernel/cObject;JLorg/omnetpp/simkernel/cObject;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cListener_1receiveSignal_1_1SWIG_16 },
    { (char *)"new_cListener", (char *)"()J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cListener },
    { (char *)"delete_cListener", (char *)"(J)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cListener },
    { (char *)"cComponent_getLogLevel", (char *)"(JLorg/omnetpp/simkernel/cComponent;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1getLogLevel },
    { (char *)"cComponent_setLogLevel", (char *)"(JLorg/omnetpp/simkernel/cComponent;J)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1setLogLevel },
    { (char *)"cComponent_setComponentType", (char *)"(JLorg/omnetpp/simkernel/cComponent;JLorg/omnetpp/simkernel/cComponentType;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1setComponentType },
    { (char *)"cComponent_reallocParamv", (char *)"(JLorg/omnetpp/simkernel/cComponent;I)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1reallocParamv },
    { (char *)"cComponent_recordParametersAsScalars", (char *)"(JLorg/omnetpp/simkernel/cComponent;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1recordParametersAsScalars },
    { (char *)"cComponent_parametersFinalized", (char *)"(JLorg/omnetpp/simkernel/cComponent;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1parametersFinalized },
    { (char *)"cComponent_addResultRecorders", (char *)"(JLorg/omnetpp/simkernel/cComponent;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1addResultRecorders },
    { (char *)"cComponent_initialized", (char *)"(JLorg/omnetpp/simkernel/cComponent;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1initialized },
    { (char *)"cComponent_callRefreshDisplay", (char *)"(JLorg/omnetpp/simkernel/cComponent;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1callRefreshDisplay },
    { (char *)"cComponent_hasDisplayString", (char *)"(JLorg/omnetpp/simkernel/cComponent;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1hasDisplayString },
    { (char *)"cComponent_clearSignalState", (char *)"()V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1clearSignalState },
    { (char *)"cComponent_clearSignalRegistrations", (char *)"()V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1clearSignalRegistrations },
    { (char *)"cComponent_setCheckSignals", (char *)"(Z)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1setCheckSignals },
    { (char *)"cComponent_getCheckSignals", (char *)"()Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1getCheckSignals },
    { (char *)"cComponent_getResultRecorders", (char *)"(JLorg/omnetpp/simkernel/cComponent;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1getResultRecorders },
    { (char *)"cComponent_invalidateCachedResultRecorderLists", (char *)"()V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1invalidateCachedResultRecorderLists },
    { (char *)"delete_cComponent", (char *)"(J)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cComponent },
    { (char *)"cComponent_forEachChild", (char *)"(JLorg/omnetpp/simkernel/cComponent;JLorg/omnetpp/simkernel/cVisitor;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1forEachChild },
    { (char *)"cComponent_getThisPtr", (char *)"(JLorg/omnetpp/simkernel/cComponent;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1getThisPtr },
    { (char *)"cComponent_finalizeParameters", (char *)"(JLorg/omnetpp/simkernel/cComponent;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1finalizeParameters },
    { (char *)"cComponent_getProperties", (char *)"(JLorg/omnetpp/simkernel/cComponent;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1getProperties },
    { (char *)"cComponent_getComponentType", (char *)"(JLorg/omnetpp/simkernel/cComponent;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1getComponentType },
    { (char *)"cComponent_getSimulation", (char *)"(JLorg/omnetpp/simkernel/cComponent;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1getSimulation },
    { (char *)"cComponent_getId", (char *)"(JLorg/omnetpp/simkernel/cComponent;)I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1getId },
    { (char *)"cComponent_getNedTypeName", (char *)"(JLorg/omnetpp/simkernel/cComponent;)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1getNedTypeName },
    { (char *)"cComponent_getComponentKind", (char *)"(JLorg/omnetpp/simkernel/cComponent;)I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1getComponentKind },
    { (char *)"cComponent_isModule", (char *)"(JLorg/omnetpp/simkernel/cComponent;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1isModule },
    { (char *)"cComponent_isChannel", (char *)"(JLorg/omnetpp/simkernel/cComponent;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1isChannel },
    { (char *)"cComponent_getParentModule", (char *)"(JLorg/omnetpp/simkernel/cComponent;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1getParentModule },
    { (char *)"cComponent_getSystemModule", (char *)"(JLorg/omnetpp/simkernel/cComponent;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1getSystemModule },
    { (char *)"cComponent_callInitialize__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cComponent;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1callInitialize_1_1SWIG_10 },
    { (char *)"cComponent_callInitialize__SWIG_1", (char *)"(JLorg/omnetpp/simkernel/cComponent;I)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1callInitialize_1_1SWIG_11 },
    { (char *)"cComponent_callFinish", (char *)"(JLorg/omnetpp/simkernel/cComponent;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1callFinish },
    { (char *)"cComponent_getNumParams", (char *)"(JLorg/omnetpp/simkernel/cComponent;)I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1getNumParams },
    { (char *)"cComponent_par__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cComponent;I)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1par_1_1SWIG_10 },
    { (char *)"cComponent_par__SWIG_2", (char *)"(JLorg/omnetpp/simkernel/cComponent;Ljava/lang/String;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1par_1_1SWIG_12 },
    { (char *)"cComponent_findPar", (char *)"(JLorg/omnetpp/simkernel/cComponent;Ljava/lang/String;)I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1findPar },
    { (char *)"cComponent_hasPar", (char *)"(JLorg/omnetpp/simkernel/cComponent;Ljava/lang/String;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1hasPar },
    { (char *)"cComponent_getRNG", (char *)"(JLorg/omnetpp/simkernel/cComponent;I)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1getRNG },
    { (char *)"cComponent_intrand__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cComponent;II)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1intrand_1_1SWIG_10 },
    { (char *)"cComponent_intrand__SWIG_1", (char *)"(JLorg/omnetpp/simkernel/cComponent;I)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1intrand_1_1SWIG_11 },
    { (char *)"cComponent_dblrand__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cComponent;I)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1dblrand_1_1SWIG_10 },
    { (char *)"cComponent_dblrand__SWIG_1", (char *)"(JLorg/omnetpp/simkernel/cComponent;)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1dblrand_1_1SWIG_11 },
    { (char *)"cComponent_uniform__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cComponent;DDI)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1uniform_1_1SWIG_10 },
    { (char *)"cComponent_uniform__SWIG_1", (char *)"(JLorg/omnetpp/simkernel/cComponent;DD)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1uniform_1_1SWIG_11 },
    { (char *)"cComponent_uniform__SWIG_2", (char *)"(JLorg/omnetpp/simkernel/cComponent;JLorg/omnetpp/simkernel/SimTime;JLorg/omnetpp/simkernel/SimTime;I)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1uniform_1_1SWIG_12 },
    { (char *)"cComponent_uniform__SWIG_3", (char *)"(JLorg/omnetpp/simkernel/cComponent;JLorg/omnetpp/simkernel/SimTime;JLorg/omnetpp/simkernel/SimTime;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1uniform_1_1SWIG_13 },
    { (char *)"cComponent_exponential__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cComponent;DI)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1exponential_1_1SWIG_10 },
    { (char *)"cComponent_exponential__SWIG_1", (char *)"(JLorg/omnetpp/simkernel/cComponent;D)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1exponential_1_1SWIG_11 },
    { (char *)"cComponent_exponential__SWIG_2", (char *)"(JLorg/omnetpp/simkernel/cComponent;JLorg/omnetpp/simkernel/SimTime;I)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1exponential_1_1SWIG_12 },
    { (char *)"cComponent_exponential__SWIG_3", (char *)"(JLorg/omnetpp/simkernel/cComponent;JLorg/omnetpp/simkernel/SimTime;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1exponential_1_1SWIG_13 },
    { (char *)"cComponent_normal__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cComponent;DDI)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1normal_1_1SWIG_10 },
    { (char *)"cComponent_normal__SWIG_1", (char *)"(JLorg/omnetpp/simkernel/cComponent;DD)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1normal_1_1SWIG_11 },
    { (char *)"cComponent_normal__SWIG_2", (char *)"(JLorg/omnetpp/simkernel/cComponent;JLorg/omnetpp/simkernel/SimTime;JLorg/omnetpp/simkernel/SimTime;I)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1normal_1_1SWIG_12 },
    { (char *)"cComponent_normal__SWIG_3", (char *)"(JLorg/omnetpp/simkernel/cComponent;JLorg/omnetpp/simkernel/SimTime;JLorg/omnetpp/simkernel/SimTime;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1normal_1_1SWIG_13 },
    { (char *)"cComponent_truncnormal__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cComponent;DDI)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1truncnormal_1_1SWIG_10 },
    { (char *)"cComponent_truncnormal__SWIG_1", (char *)"(JLorg/omnetpp/simkernel/cComponent;DD)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1truncnormal_1_1SWIG_11 },
    { (char *)"cComponent_truncnormal__SWIG_2", (char *)"(JLorg/omnetpp/simkernel/cComponent;JLorg/omnetpp/simkernel/SimTime;JLorg/omnetpp/simkernel/SimTime;I)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1truncnormal_1_1SWIG_12 },
    { (char *)"cComponent_truncnormal__SWIG_3", (char *)"(JLorg/omnetpp/simkernel/cComponent;JLorg/omnetpp/simkernel/SimTime;JLorg/omnetpp/simkernel/SimTime;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1truncnormal_1_1SWIG_13 },
    { (char *)"cComponent_gamma_d__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cComponent;DDI)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1gamma_1d_1_1SWIG_10 },
    { (char *)"cComponent_gamma_d__SWIG_1", (char *)"(JLorg/omnetpp/simkernel/cComponent;DD)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1gamma_1d_1_1SWIG_11 },
    { (char *)"cComponent_beta__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cComponent;DDI)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1beta_1_1SWIG_10 },
    { (char *)"cComponent_beta__SWIG_1", (char *)"(JLorg/omnetpp/simkernel/cComponent;DD)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1beta_1_1SWIG_11 },
    { (char *)"cComponent_erlang_k__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cComponent;JDI)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1erlang_1k_1_1SWIG_10 },
    { (char *)"cComponent_erlang_k__SWIG_1", (char *)"(JLorg/omnetpp/simkernel/cComponent;JD)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1erlang_1k_1_1SWIG_11 },
    { (char *)"cComponent_chi_square__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cComponent;JI)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1chi_1square_1_1SWIG_10 },
    { (char *)"cComponent_chi_square__SWIG_1", (char *)"(JLorg/omnetpp/simkernel/cComponent;J)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1chi_1square_1_1SWIG_11 },
    { (char *)"cComponent_student_t__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cComponent;JI)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1student_1t_1_1SWIG_10 },
    { (char *)"cComponent_student_t__SWIG_1", (char *)"(JLorg/omnetpp/simkernel/cComponent;J)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1student_1t_1_1SWIG_11 },
    { (char *)"cComponent_cauchy__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cComponent;DDI)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1cauchy_1_1SWIG_10 },
    { (char *)"cComponent_cauchy__SWIG_1", (char *)"(JLorg/omnetpp/simkernel/cComponent;DD)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1cauchy_1_1SWIG_11 },
    { (char *)"cComponent_triang__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cComponent;DDDI)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1triang_1_1SWIG_10 },
    { (char *)"cComponent_triang__SWIG_1", (char *)"(JLorg/omnetpp/simkernel/cComponent;DDD)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1triang_1_1SWIG_11 },
    { (char *)"cComponent_lognormal__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cComponent;DDI)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1lognormal_1_1SWIG_10 },
    { (char *)"cComponent_lognormal__SWIG_1", (char *)"(JLorg/omnetpp/simkernel/cComponent;DD)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1lognormal_1_1SWIG_11 },
    { (char *)"cComponent_weibull__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cComponent;DDI)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1weibull_1_1SWIG_10 },
    { (char *)"cComponent_weibull__SWIG_1", (char *)"(JLorg/omnetpp/simkernel/cComponent;DD)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1weibull_1_1SWIG_11 },
    { (char *)"cComponent_pareto_shifted__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cComponent;DDDI)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1pareto_1shifted_1_1SWIG_10 },
    { (char *)"cComponent_pareto_shifted__SWIG_1", (char *)"(JLorg/omnetpp/simkernel/cComponent;DDD)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1pareto_1shifted_1_1SWIG_11 },
    { (char *)"cComponent_intuniform__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cComponent;III)I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1intuniform_1_1SWIG_10 },
    { (char *)"cComponent_intuniform__SWIG_1", (char *)"(JLorg/omnetpp/simkernel/cComponent;II)I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1intuniform_1_1SWIG_11 },
    { (char *)"cComponent_bernoulli__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cComponent;DI)I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1bernoulli_1_1SWIG_10 },
    { (char *)"cComponent_bernoulli__SWIG_1", (char *)"(JLorg/omnetpp/simkernel/cComponent;D)I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1bernoulli_1_1SWIG_11 },
    { (char *)"cComponent_binomial__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cComponent;IDI)I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1binomial_1_1SWIG_10 },
    { (char *)"cComponent_binomial__SWIG_1", (char *)"(JLorg/omnetpp/simkernel/cComponent;ID)I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1binomial_1_1SWIG_11 },
    { (char *)"cComponent_geometric__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cComponent;DI)I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1geometric_1_1SWIG_10 },
    { (char *)"cComponent_geometric__SWIG_1", (char *)"(JLorg/omnetpp/simkernel/cComponent;D)I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1geometric_1_1SWIG_11 },
    { (char *)"cComponent_negbinomial__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cComponent;IDI)I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1negbinomial_1_1SWIG_10 },
    { (char *)"cComponent_negbinomial__SWIG_1", (char *)"(JLorg/omnetpp/simkernel/cComponent;ID)I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1negbinomial_1_1SWIG_11 },
    { (char *)"cComponent_poisson__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cComponent;DI)I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1poisson_1_1SWIG_10 },
    { (char *)"cComponent_poisson__SWIG_1", (char *)"(JLorg/omnetpp/simkernel/cComponent;D)I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1poisson_1_1SWIG_11 },
    { (char *)"cComponent_registerSignal", (char *)"(Ljava/lang/String;)I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1registerSignal },
    { (char *)"cComponent_getSignalName", (char *)"(I)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1getSignalName },
    { (char *)"cComponent_emit__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cComponent;IZJLorg/omnetpp/simkernel/cObject;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1emit_1_1SWIG_10 },
    { (char *)"cComponent_emit__SWIG_1", (char *)"(JLorg/omnetpp/simkernel/cComponent;IZ)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1emit_1_1SWIG_11 },
    { (char *)"cComponent_emit__SWIG_2", (char *)"(JLorg/omnetpp/simkernel/cComponent;IIJLorg/omnetpp/simkernel/cObject;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1emit_1_1SWIG_12 },
    { (char *)"cComponent_emit__SWIG_3", (char *)"(JLorg/omnetpp/simkernel/cComponent;II)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1emit_1_1SWIG_13 },
    { (char *)"cComponent_emit__SWIG_4", (char *)"(JLorg/omnetpp/simkernel/cComponent;IJJLorg/omnetpp/simkernel/cObject;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1emit_1_1SWIG_14 },
    { (char *)"cComponent_emit__SWIG_5", (char *)"(JLorg/omnetpp/simkernel/cComponent;IJ)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1emit_1_1SWIG_15 },
    { (char *)"cComponent_emit__SWIG_6", (char *)"(JLorg/omnetpp/simkernel/cComponent;IDJLorg/omnetpp/simkernel/cObject;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1emit_1_1SWIG_16 },
    { (char *)"cComponent_emit__SWIG_7", (char *)"(JLorg/omnetpp/simkernel/cComponent;ID)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1emit_1_1SWIG_17 },
    { (char *)"cComponent_emit__SWIG_8", (char *)"(JLorg/omnetpp/simkernel/cComponent;IJLorg/omnetpp/simkernel/SimTime;JLorg/omnetpp/simkernel/cObject;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1emit_1_1SWIG_18 },
    { (char *)"cComponent_emit__SWIG_9", (char *)"(JLorg/omnetpp/simkernel/cComponent;IJLorg/omnetpp/simkernel/SimTime;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1emit_1_1SWIG_19 },
    { (char *)"cComponent_emit__SWIG_10", (char *)"(JLorg/omnetpp/simkernel/cComponent;ILjava/lang/String;JLorg/omnetpp/simkernel/cObject;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1emit_1_1SWIG_110 },
    { (char *)"cComponent_emit__SWIG_11", (char *)"(JLorg/omnetpp/simkernel/cComponent;ILjava/lang/String;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1emit_1_1SWIG_111 },
    { (char *)"cComponent_emit__SWIG_12", (char *)"(JLorg/omnetpp/simkernel/cComponent;IJLorg/omnetpp/simkernel/cObject;JLorg/omnetpp/simkernel/cObject;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1emit_1_1SWIG_112 },
    { (char *)"cComponent_emit__SWIG_13", (char *)"(JLorg/omnetpp/simkernel/cComponent;IJLorg/omnetpp/simkernel/cObject;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1emit_1_1SWIG_113 },
    { (char *)"cComponent_emit__SWIG_16", (char *)"(JLorg/omnetpp/simkernel/cComponent;ICJLorg/omnetpp/simkernel/cObject;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1emit_1_1SWIG_116 },
    { (char *)"cComponent_emit__SWIG_17", (char *)"(JLorg/omnetpp/simkernel/cComponent;IC)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1emit_1_1SWIG_117 },
    { (char *)"cComponent_emit__SWIG_18", (char *)"(JLorg/omnetpp/simkernel/cComponent;ISJLorg/omnetpp/simkernel/cObject;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1emit_1_1SWIG_118 },
    { (char *)"cComponent_emit__SWIG_19", (char *)"(JLorg/omnetpp/simkernel/cComponent;IS)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1emit_1_1SWIG_119 },
    { (char *)"cComponent_emit__SWIG_28", (char *)"(JLorg/omnetpp/simkernel/cComponent;IFJLorg/omnetpp/simkernel/cObject;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1emit_1_1SWIG_128 },
    { (char *)"cComponent_emit__SWIG_29", (char *)"(JLorg/omnetpp/simkernel/cComponent;IF)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1emit_1_1SWIG_129 },
    { (char *)"cComponent_emit__SWIG_30", (char *)"(JLorg/omnetpp/simkernel/cComponent;IJJLorg/omnetpp/simkernel/cObject;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1emit_1_1SWIG_130 },
    { (char *)"cComponent_emit__SWIG_31", (char *)"(JLorg/omnetpp/simkernel/cComponent;IJ)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1emit_1_1SWIG_131 },
    { (char *)"cComponent_mayHaveListeners", (char *)"(JLorg/omnetpp/simkernel/cComponent;I)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1mayHaveListeners },
    { (char *)"cComponent_hasListeners", (char *)"(JLorg/omnetpp/simkernel/cComponent;I)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1hasListeners },
    { (char *)"cComponent_subscribe__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cComponent;IJLorg/omnetpp/simkernel/cIListener;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1subscribe_1_1SWIG_10 },
    { (char *)"cComponent_subscribe__SWIG_1", (char *)"(JLorg/omnetpp/simkernel/cComponent;Ljava/lang/String;JLorg/omnetpp/simkernel/cIListener;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1subscribe_1_1SWIG_11 },
    { (char *)"cComponent_unsubscribe__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cComponent;IJLorg/omnetpp/simkernel/cIListener;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1unsubscribe_1_1SWIG_10 },
    { (char *)"cComponent_unsubscribe__SWIG_1", (char *)"(JLorg/omnetpp/simkernel/cComponent;Ljava/lang/String;JLorg/omnetpp/simkernel/cIListener;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1unsubscribe_1_1SWIG_11 },
    { (char *)"cComponent_isSubscribed__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cComponent;IJLorg/omnetpp/simkernel/cIListener;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1isSubscribed_1_1SWIG_10 },
    { (char *)"cComponent_isSubscribed__SWIG_1", (char *)"(JLorg/omnetpp/simkernel/cComponent;Ljava/lang/String;JLorg/omnetpp/simkernel/cIListener;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1isSubscribed_1_1SWIG_11 },
    { (char *)"cComponent_getLocalListenedSignals", (char *)"(JLorg/omnetpp/simkernel/cComponent;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1getLocalListenedSignals },
    { (char *)"cComponent_getLocalSignalListeners", (char *)"(JLorg/omnetpp/simkernel/cComponent;I)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1getLocalSignalListeners },
    { (char *)"cComponent_hasGUI", (char *)"(JLorg/omnetpp/simkernel/cComponent;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1hasGUI },
    { (char *)"cComponent_getDisplayString", (char *)"(JLorg/omnetpp/simkernel/cComponent;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1getDisplayString },
    { (char *)"cComponent_setDisplayString", (char *)"(JLorg/omnetpp/simkernel/cComponent;Ljava/lang/String;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1setDisplayString },
    { (char *)"cComponent_bubble", (char *)"(JLorg/omnetpp/simkernel/cComponent;Ljava/lang/String;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1bubble },
    { (char *)"cComponent_resolveResourcePath", (char *)"(JLorg/omnetpp/simkernel/cComponent;Ljava/lang/String;)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1resolveResourcePath },
    { (char *)"cComponent_recordScalar__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cComponent;Ljava/lang/String;DLjava/lang/String;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1recordScalar_1_1SWIG_10 },
    { (char *)"cComponent_recordScalar__SWIG_1", (char *)"(JLorg/omnetpp/simkernel/cComponent;Ljava/lang/String;D)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1recordScalar_1_1SWIG_11 },
    { (char *)"cComponent_recordScalar__SWIG_2", (char *)"(JLorg/omnetpp/simkernel/cComponent;Ljava/lang/String;JLorg/omnetpp/simkernel/SimTime;Ljava/lang/String;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1recordScalar_1_1SWIG_12 },
    { (char *)"cComponent_recordScalar__SWIG_3", (char *)"(JLorg/omnetpp/simkernel/cComponent;Ljava/lang/String;JLorg/omnetpp/simkernel/SimTime;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1recordScalar_1_1SWIG_13 },
    { (char *)"cComponent_recordStatistic__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cComponent;JLorg/omnetpp/simkernel/cStatistic;Ljava/lang/String;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1recordStatistic_1_1SWIG_10 },
    { (char *)"cComponent_recordStatistic__SWIG_1", (char *)"(JLorg/omnetpp/simkernel/cComponent;JLorg/omnetpp/simkernel/cStatistic;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1recordStatistic_1_1SWIG_11 },
    { (char *)"cComponent_recordStatistic__SWIG_2", (char *)"(JLorg/omnetpp/simkernel/cComponent;Ljava/lang/String;JLorg/omnetpp/simkernel/cStatistic;Ljava/lang/String;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1recordStatistic_1_1SWIG_12 },
    { (char *)"cComponent_recordStatistic__SWIG_3", (char *)"(JLorg/omnetpp/simkernel/cComponent;Ljava/lang/String;JLorg/omnetpp/simkernel/cStatistic;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1recordStatistic_1_1SWIG_13 },
    { (char *)"cComponent_cast", (char *)"(JLorg/omnetpp/simkernel/cObject;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponent_1cast },
    { (char *)"cChannel_setSourceGate", (char *)"(JLorg/omnetpp/simkernel/cChannel;JLorg/omnetpp/simkernel/cGate;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cChannel_1setSourceGate },
    { (char *)"cChannel_setNedConnectionElementId", (char *)"(JLorg/omnetpp/simkernel/cChannel;I)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cChannel_1setNedConnectionElementId },
    { (char *)"cChannel_getNedConnectionElementId", (char *)"(JLorg/omnetpp/simkernel/cChannel;)I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cChannel_1getNedConnectionElementId },
    { (char *)"cChannel_initializeChannel", (char *)"(JLorg/omnetpp/simkernel/cChannel;I)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cChannel_1initializeChannel },
    { (char *)"cChannel_finalizeParameters", (char *)"(JLorg/omnetpp/simkernel/cChannel;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cChannel_1finalizeParameters },
    { (char *)"cChannel_callRefreshDisplay", (char *)"(JLorg/omnetpp/simkernel/cChannel;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cChannel_1callRefreshDisplay },
    { (char *)"delete_cChannel", (char *)"(J)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cChannel },
    { (char *)"cChannel_str", (char *)"(JLorg/omnetpp/simkernel/cChannel;)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cChannel_1str },
    { (char *)"cChannel_callInitialize__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cChannel;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cChannel_1callInitialize_1_1SWIG_10 },
    { (char *)"cChannel_callInitialize__SWIG_1", (char *)"(JLorg/omnetpp/simkernel/cChannel;I)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cChannel_1callInitialize_1_1SWIG_11 },
    { (char *)"cChannel_callFinish", (char *)"(JLorg/omnetpp/simkernel/cChannel;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cChannel_1callFinish },
    { (char *)"cChannel_getComponentKind", (char *)"(JLorg/omnetpp/simkernel/cChannel;)I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cChannel_1getComponentKind },
    { (char *)"cChannel_getParentModule", (char *)"(JLorg/omnetpp/simkernel/cChannel;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cChannel_1getParentModule },
    { (char *)"cChannel_getChannelType", (char *)"(JLorg/omnetpp/simkernel/cChannel;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cChannel_1getChannelType },
    { (char *)"cChannel_getProperties", (char *)"(JLorg/omnetpp/simkernel/cChannel;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cChannel_1getProperties },
    { (char *)"cChannel_getSourceGate", (char *)"(JLorg/omnetpp/simkernel/cChannel;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cChannel_1getSourceGate },
    { (char *)"cChannel_isTransmissionChannel", (char *)"(JLorg/omnetpp/simkernel/cChannel;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cChannel_1isTransmissionChannel },
    { (char *)"cChannel_getNominalDatarate", (char *)"(JLorg/omnetpp/simkernel/cChannel;)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cChannel_1getNominalDatarate },
    { (char *)"cChannel_calculateDuration", (char *)"(JLorg/omnetpp/simkernel/cChannel;JLorg/omnetpp/simkernel/cMessage;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cChannel_1calculateDuration },
    { (char *)"cChannel_getTransmissionFinishTime", (char *)"(JLorg/omnetpp/simkernel/cChannel;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cChannel_1getTransmissionFinishTime },
    { (char *)"cChannel_isBusy", (char *)"(JLorg/omnetpp/simkernel/cChannel;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cChannel_1isBusy },
    { (char *)"cChannel_forceTransmissionFinishTime", (char *)"(JLorg/omnetpp/simkernel/cChannel;JLorg/omnetpp/simkernel/SimTime;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cChannel_1forceTransmissionFinishTime },
    { (char *)"cChannel_cast", (char *)"(JLorg/omnetpp/simkernel/cObject;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cChannel_1cast },
    { (char *)"new_cIdealChannel__SWIG_0", (char *)"(Ljava/lang/String;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cIdealChannel_1_1SWIG_10 },
    { (char *)"new_cIdealChannel__SWIG_1", (char *)"()J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cIdealChannel_1_1SWIG_11 },
    { (char *)"delete_cIdealChannel", (char *)"(J)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cIdealChannel },
    { (char *)"cIdealChannel_create", (char *)"(Ljava/lang/String;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cIdealChannel_1create },
    { (char *)"cIdealChannel_getNominalDatarate", (char *)"(JLorg/omnetpp/simkernel/cIdealChannel;)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cIdealChannel_1getNominalDatarate },
    { (char *)"cIdealChannel_isTransmissionChannel", (char *)"(JLorg/omnetpp/simkernel/cIdealChannel;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cIdealChannel_1isTransmissionChannel },
    { (char *)"cIdealChannel_calculateDuration", (char *)"(JLorg/omnetpp/simkernel/cIdealChannel;JLorg/omnetpp/simkernel/cMessage;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cIdealChannel_1calculateDuration },
    { (char *)"cIdealChannel_getTransmissionFinishTime", (char *)"(JLorg/omnetpp/simkernel/cIdealChannel;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cIdealChannel_1getTransmissionFinishTime },
    { (char *)"cIdealChannel_isBusy", (char *)"(JLorg/omnetpp/simkernel/cIdealChannel;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cIdealChannel_1isBusy },
    { (char *)"cIdealChannel_forceTransmissionFinishTime", (char *)"(JLorg/omnetpp/simkernel/cIdealChannel;JLorg/omnetpp/simkernel/SimTime;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cIdealChannel_1forceTransmissionFinishTime },
    { (char *)"cIdealChannel_cast", (char *)"(JLorg/omnetpp/simkernel/cObject;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cIdealChannel_1cast },
    { (char *)"new_cDelayChannel__SWIG_0", (char *)"(Ljava/lang/String;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cDelayChannel_1_1SWIG_10 },
    { (char *)"new_cDelayChannel__SWIG_1", (char *)"()J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cDelayChannel_1_1SWIG_11 },
    { (char *)"delete_cDelayChannel", (char *)"(J)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cDelayChannel },
    { (char *)"cDelayChannel_create", (char *)"(Ljava/lang/String;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cDelayChannel_1create },
    { (char *)"cDelayChannel_isTransmissionChannel", (char *)"(JLorg/omnetpp/simkernel/cDelayChannel;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cDelayChannel_1isTransmissionChannel },
    { (char *)"cDelayChannel_getNominalDatarate", (char *)"(JLorg/omnetpp/simkernel/cDelayChannel;)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cDelayChannel_1getNominalDatarate },
    { (char *)"cDelayChannel_calculateDuration", (char *)"(JLorg/omnetpp/simkernel/cDelayChannel;JLorg/omnetpp/simkernel/cMessage;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cDelayChannel_1calculateDuration },
    { (char *)"cDelayChannel_getTransmissionFinishTime", (char *)"(JLorg/omnetpp/simkernel/cDelayChannel;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cDelayChannel_1getTransmissionFinishTime },
    { (char *)"cDelayChannel_isBusy", (char *)"(JLorg/omnetpp/simkernel/cDelayChannel;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cDelayChannel_1isBusy },
    { (char *)"cDelayChannel_forceTransmissionFinishTime", (char *)"(JLorg/omnetpp/simkernel/cDelayChannel;JLorg/omnetpp/simkernel/SimTime;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cDelayChannel_1forceTransmissionFinishTime },
    { (char *)"cDelayChannel_setDelay", (char *)"(JLorg/omnetpp/simkernel/cDelayChannel;D)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cDelayChannel_1setDelay },
    { (char *)"cDelayChannel_setDisabled", (char *)"(JLorg/omnetpp/simkernel/cDelayChannel;Z)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cDelayChannel_1setDisabled },
    { (char *)"cDelayChannel_getDelay", (char *)"(JLorg/omnetpp/simkernel/cDelayChannel;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cDelayChannel_1getDelay },
    { (char *)"cDelayChannel_isDisabled", (char *)"(JLorg/omnetpp/simkernel/cDelayChannel;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cDelayChannel_1isDisabled },
    { (char *)"cDelayChannel_initialize", (char *)"(JLorg/omnetpp/simkernel/cDelayChannel;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cDelayChannel_1initialize },
    { (char *)"cDelayChannel_cast", (char *)"(JLorg/omnetpp/simkernel/cObject;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cDelayChannel_1cast },
    { (char *)"new_cDatarateChannel__SWIG_0", (char *)"(Ljava/lang/String;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cDatarateChannel_1_1SWIG_10 },
    { (char *)"new_cDatarateChannel__SWIG_1", (char *)"()J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cDatarateChannel_1_1SWIG_11 },
    { (char *)"delete_cDatarateChannel", (char *)"(J)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cDatarateChannel },
    { (char *)"cDatarateChannel_create", (char *)"(Ljava/lang/String;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cDatarateChannel_1create },
    { (char *)"cDatarateChannel_str", (char *)"(JLorg/omnetpp/simkernel/cDatarateChannel;)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cDatarateChannel_1str },
    { (char *)"cDatarateChannel_isTransmissionChannel", (char *)"(JLorg/omnetpp/simkernel/cDatarateChannel;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cDatarateChannel_1isTransmissionChannel },
    { (char *)"cDatarateChannel_setDelay", (char *)"(JLorg/omnetpp/simkernel/cDatarateChannel;D)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cDatarateChannel_1setDelay },
    { (char *)"cDatarateChannel_setDatarate", (char *)"(JLorg/omnetpp/simkernel/cDatarateChannel;D)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cDatarateChannel_1setDatarate },
    { (char *)"cDatarateChannel_setBitErrorRate", (char *)"(JLorg/omnetpp/simkernel/cDatarateChannel;D)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cDatarateChannel_1setBitErrorRate },
    { (char *)"cDatarateChannel_setPacketErrorRate", (char *)"(JLorg/omnetpp/simkernel/cDatarateChannel;D)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cDatarateChannel_1setPacketErrorRate },
    { (char *)"cDatarateChannel_setDisabled", (char *)"(JLorg/omnetpp/simkernel/cDatarateChannel;Z)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cDatarateChannel_1setDisabled },
    { (char *)"cDatarateChannel_getDelay", (char *)"(JLorg/omnetpp/simkernel/cDatarateChannel;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cDatarateChannel_1getDelay },
    { (char *)"cDatarateChannel_getDatarate", (char *)"(JLorg/omnetpp/simkernel/cDatarateChannel;)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cDatarateChannel_1getDatarate },
    { (char *)"cDatarateChannel_getBitErrorRate", (char *)"(JLorg/omnetpp/simkernel/cDatarateChannel;)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cDatarateChannel_1getBitErrorRate },
    { (char *)"cDatarateChannel_getPacketErrorRate", (char *)"(JLorg/omnetpp/simkernel/cDatarateChannel;)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cDatarateChannel_1getPacketErrorRate },
    { (char *)"cDatarateChannel_isDisabled", (char *)"(JLorg/omnetpp/simkernel/cDatarateChannel;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cDatarateChannel_1isDisabled },
    { (char *)"cDatarateChannel_getNominalDatarate", (char *)"(JLorg/omnetpp/simkernel/cDatarateChannel;)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cDatarateChannel_1getNominalDatarate },
    { (char *)"cDatarateChannel_calculateDuration", (char *)"(JLorg/omnetpp/simkernel/cDatarateChannel;JLorg/omnetpp/simkernel/cMessage;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cDatarateChannel_1calculateDuration },
    { (char *)"cDatarateChannel_getTransmissionFinishTime", (char *)"(JLorg/omnetpp/simkernel/cDatarateChannel;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cDatarateChannel_1getTransmissionFinishTime },
    { (char *)"cDatarateChannel_isBusy", (char *)"(JLorg/omnetpp/simkernel/cDatarateChannel;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cDatarateChannel_1isBusy },
    { (char *)"cDatarateChannel_forceTransmissionFinishTime", (char *)"(JLorg/omnetpp/simkernel/cDatarateChannel;JLorg/omnetpp/simkernel/SimTime;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cDatarateChannel_1forceTransmissionFinishTime },
    { (char *)"cDatarateChannel_initialize", (char *)"(JLorg/omnetpp/simkernel/cDatarateChannel;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cDatarateChannel_1initialize },
    { (char *)"cDatarateChannel_cast", (char *)"(JLorg/omnetpp/simkernel/cObject;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cDatarateChannel_1cast },
    { (char *)"cModule_setRecordEvents", (char *)"(JLorg/omnetpp/simkernel/cModule;Z)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1setRecordEvents },
    { (char *)"cModule_isRecordEvents", (char *)"(JLorg/omnetpp/simkernel/cModule;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1isRecordEvents },
    { (char *)"cModule_clearNamePools", (char *)"()V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1clearNamePools },
    { (char *)"cModule_gateCount", (char *)"(JLorg/omnetpp/simkernel/cModule;)I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1gateCount },
    { (char *)"cModule_gateByOrdinal", (char *)"(JLorg/omnetpp/simkernel/cModule;I)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1gateByOrdinal },
    { (char *)"cModule_callRefreshDisplay", (char *)"(JLorg/omnetpp/simkernel/cModule;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1callRefreshDisplay },
    { (char *)"cModule_getCanvasIfExists", (char *)"(JLorg/omnetpp/simkernel/cModule;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1getCanvasIfExists },
    { (char *)"cModule_getOsgCanvasIfExists", (char *)"(JLorg/omnetpp/simkernel/cModule;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1getOsgCanvasIfExists },
    { (char *)"new_cModule", (char *)"()J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cModule },
    { (char *)"delete_cModule", (char *)"(J)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cModule },
    { (char *)"cModule_forEachChild", (char *)"(JLorg/omnetpp/simkernel/cModule;JLorg/omnetpp/simkernel/cVisitor;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1forEachChild },
    { (char *)"cModule_setName", (char *)"(JLorg/omnetpp/simkernel/cModule;Ljava/lang/String;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1setName },
    { (char *)"cModule_getFullName", (char *)"(JLorg/omnetpp/simkernel/cModule;)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1getFullName },
    { (char *)"cModule_getFullPath", (char *)"(JLorg/omnetpp/simkernel/cModule;)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1getFullPath },
    { (char *)"cModule_str", (char *)"(JLorg/omnetpp/simkernel/cModule;)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1str },
    { (char *)"cModule_addGate__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cModule;Ljava/lang/String;IZ)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1addGate_1_1SWIG_10 },
    { (char *)"cModule_addGate__SWIG_1", (char *)"(JLorg/omnetpp/simkernel/cModule;Ljava/lang/String;I)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1addGate_1_1SWIG_11 },
    { (char *)"cModule_setGateSize", (char *)"(JLorg/omnetpp/simkernel/cModule;Ljava/lang/String;I)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1setGateSize },
    { (char *)"cModule_getOrCreateFirstUnconnectedGate", (char *)"(JLorg/omnetpp/simkernel/cModule;Ljava/lang/String;CZZ)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1getOrCreateFirstUnconnectedGate },
    { (char *)"cModule_getOrCreateFirstUnconnectedGatePair", (char *)"(JLorg/omnetpp/simkernel/cModule;Ljava/lang/String;ZZJLorg/omnetpp/simkernel/cGate;JLorg/omnetpp/simkernel/cGate;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1getOrCreateFirstUnconnectedGatePair },
    { (char *)"cModule_finalizeParameters", (char *)"(JLorg/omnetpp/simkernel/cModule;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1finalizeParameters },
    { (char *)"cModule_buildInside", (char *)"(JLorg/omnetpp/simkernel/cModule;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1buildInside },
    { (char *)"cModule_isSimple", (char *)"(JLorg/omnetpp/simkernel/cModule;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1isSimple },
    { (char *)"cModule_getComponentKind", (char *)"(JLorg/omnetpp/simkernel/cModule;)I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1getComponentKind },
    { (char *)"cModule_isPlaceholder", (char *)"(JLorg/omnetpp/simkernel/cModule;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1isPlaceholder },
    { (char *)"cModule_getModuleType", (char *)"(JLorg/omnetpp/simkernel/cModule;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1getModuleType },
    { (char *)"cModule_getProperties", (char *)"(JLorg/omnetpp/simkernel/cModule;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1getProperties },
    { (char *)"cModule_isVector", (char *)"(JLorg/omnetpp/simkernel/cModule;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1isVector },
    { (char *)"cModule_getIndex", (char *)"(JLorg/omnetpp/simkernel/cModule;)I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1getIndex },
    { (char *)"cModule_getVectorSize", (char *)"(JLorg/omnetpp/simkernel/cModule;)I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1getVectorSize },
    { (char *)"cModule_size", (char *)"(JLorg/omnetpp/simkernel/cModule;)I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1size },
    { (char *)"cModule_hasSubmodules", (char *)"(JLorg/omnetpp/simkernel/cModule;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1hasSubmodules },
    { (char *)"cModule_findSubmodule__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cModule;Ljava/lang/String;I)I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1findSubmodule_1_1SWIG_10 },
    { (char *)"cModule_findSubmodule__SWIG_1", (char *)"(JLorg/omnetpp/simkernel/cModule;Ljava/lang/String;)I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1findSubmodule_1_1SWIG_11 },
    { (char *)"cModule_getSubmodule__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cModule;Ljava/lang/String;I)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1getSubmodule_1_1SWIG_10 },
    { (char *)"cModule_getSubmodule__SWIG_1", (char *)"(JLorg/omnetpp/simkernel/cModule;Ljava/lang/String;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1getSubmodule_1_1SWIG_11 },
    { (char *)"cModule_getModuleByPath", (char *)"(JLorg/omnetpp/simkernel/cModule;Ljava/lang/String;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1getModuleByPath },
    { (char *)"cModule_gate__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cModule;Ljava/lang/String;I)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1gate_1_1SWIG_10 },
    { (char *)"cModule_gate__SWIG_1", (char *)"(JLorg/omnetpp/simkernel/cModule;Ljava/lang/String;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1gate_1_1SWIG_11 },
    { (char *)"cModule_gateHalf__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cModule;Ljava/lang/String;II)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1gateHalf_1_1SWIG_10 },
    { (char *)"cModule_gateHalf__SWIG_1", (char *)"(JLorg/omnetpp/simkernel/cModule;Ljava/lang/String;I)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1gateHalf_1_1SWIG_11 },
    { (char *)"cModule_hasGate__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cModule;Ljava/lang/String;I)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1hasGate_1_1SWIG_10 },
    { (char *)"cModule_hasGate__SWIG_1", (char *)"(JLorg/omnetpp/simkernel/cModule;Ljava/lang/String;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1hasGate_1_1SWIG_11 },
    { (char *)"cModule_findGate__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cModule;Ljava/lang/String;I)I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1findGate_1_1SWIG_10 },
    { (char *)"cModule_findGate__SWIG_1", (char *)"(JLorg/omnetpp/simkernel/cModule;Ljava/lang/String;)I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1findGate_1_1SWIG_11 },
    { (char *)"cModule_gate__SWIG_4", (char *)"(JLorg/omnetpp/simkernel/cModule;I)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1gate_1_1SWIG_14 },
    { (char *)"cModule_deleteGate", (char *)"(JLorg/omnetpp/simkernel/cModule;Ljava/lang/String;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1deleteGate },
    { (char *)"cModule_getGateNames", (char *)"(JLorg/omnetpp/simkernel/cModule;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1getGateNames },
    { (char *)"cModule_gateType", (char *)"(JLorg/omnetpp/simkernel/cModule;Ljava/lang/String;)I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1gateType },
    { (char *)"cModule_isGateVector", (char *)"(JLorg/omnetpp/simkernel/cModule;Ljava/lang/String;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1isGateVector },
    { (char *)"cModule_gateSize", (char *)"(JLorg/omnetpp/simkernel/cModule;Ljava/lang/String;)I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1gateSize },
    { (char *)"cModule_gateBaseId", (char *)"(JLorg/omnetpp/simkernel/cModule;Ljava/lang/String;)I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1gateBaseId },
    { (char *)"cModule_checkInternalConnections", (char *)"(JLorg/omnetpp/simkernel/cModule;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1checkInternalConnections },
    { (char *)"cModule_arrived", (char *)"(JLorg/omnetpp/simkernel/cModule;JLorg/omnetpp/simkernel/cMessage;JLorg/omnetpp/simkernel/cGate;JLorg/omnetpp/simkernel/SimTime;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1arrived },
    { (char *)"cModule_getAncestorPar", (char *)"(JLorg/omnetpp/simkernel/cModule;Ljava/lang/String;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1getAncestorPar },
    { (char *)"cModule_getCanvas", (char *)"(JLorg/omnetpp/simkernel/cModule;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1getCanvas },
    { (char *)"cModule_getOsgCanvas", (char *)"(JLorg/omnetpp/simkernel/cModule;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1getOsgCanvas },
    { (char *)"cModule_setBuiltinAnimationsAllowed", (char *)"(JLorg/omnetpp/simkernel/cModule;Z)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1setBuiltinAnimationsAllowed },
    { (char *)"cModule_getBuiltinAnimationsAllowed", (char *)"(JLorg/omnetpp/simkernel/cModule;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1getBuiltinAnimationsAllowed },
    { (char *)"cModule_callInitialize__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cModule;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1callInitialize_1_1SWIG_10 },
    { (char *)"cModule_callInitialize__SWIG_1", (char *)"(JLorg/omnetpp/simkernel/cModule;I)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1callInitialize_1_1SWIG_11 },
    { (char *)"cModule_callFinish", (char *)"(JLorg/omnetpp/simkernel/cModule;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1callFinish },
    { (char *)"cModule_scheduleStart", (char *)"(JLorg/omnetpp/simkernel/cModule;JLorg/omnetpp/simkernel/SimTime;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1scheduleStart },
    { (char *)"cModule_deleteModule", (char *)"(JLorg/omnetpp/simkernel/cModule;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1deleteModule },
    { (char *)"cModule_changeParentTo", (char *)"(JLorg/omnetpp/simkernel/cModule;JLorg/omnetpp/simkernel/cModule;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1changeParentTo },
    { (char *)"cModule_cast", (char *)"(JLorg/omnetpp/simkernel/cObject;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cModule_1cast },
    { (char *)"nullptr_get", (char *)"()I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_nullptr_1get },
    { (char *)"new_cSimpleModule__SWIG_0", (char *)"(J)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cSimpleModule_1_1SWIG_10 },
    { (char *)"new_cSimpleModule__SWIG_1", (char *)"()J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cSimpleModule_1_1SWIG_11 },
    { (char *)"new_cSimpleModule__SWIG_2", (char *)"(Ljava/lang/String;JLorg/omnetpp/simkernel/cModule;J)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cSimpleModule_1_1SWIG_12 },
    { (char *)"delete_cSimpleModule", (char *)"(J)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cSimpleModule },
    { (char *)"cSimpleModule_str", (char *)"(JLorg/omnetpp/simkernel/cSimpleModule;)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cSimpleModule_1str },
    { (char *)"cSimpleModule_forEachChild", (char *)"(JLorg/omnetpp/simkernel/cSimpleModule;JLorg/omnetpp/simkernel/cVisitor;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cSimpleModule_1forEachChild },
    { (char *)"cSimpleModule_scheduleStart", (char *)"(JLorg/omnetpp/simkernel/cSimpleModule;JLorg/omnetpp/simkernel/SimTime;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cSimpleModule_1scheduleStart },
    { (char *)"cSimpleModule_deleteModule", (char *)"(JLorg/omnetpp/simkernel/cSimpleModule;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cSimpleModule_1deleteModule },
    { (char *)"cSimpleModule_usesActivity", (char *)"(JLorg/omnetpp/simkernel/cSimpleModule;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cSimpleModule_1usesActivity },
    { (char *)"cSimpleModule_isTerminated", (char *)"(JLorg/omnetpp/simkernel/cSimpleModule;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cSimpleModule_1isTerminated },
    { (char *)"cSimpleModule_snapshot__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cSimpleModule;JLorg/omnetpp/simkernel/cObject;Ljava/lang/String;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cSimpleModule_1snapshot_1_1SWIG_10 },
    { (char *)"cSimpleModule_snapshot__SWIG_1", (char *)"(JLorg/omnetpp/simkernel/cSimpleModule;JLorg/omnetpp/simkernel/cObject;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cSimpleModule_1snapshot_1_1SWIG_11 },
    { (char *)"cSimpleModule_snapshot__SWIG_2", (char *)"(JLorg/omnetpp/simkernel/cSimpleModule;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cSimpleModule_1snapshot_1_1SWIG_12 },
    { (char *)"cSimpleModule_send__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cSimpleModule;JLorg/omnetpp/simkernel/cMessage;I)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cSimpleModule_1send_1_1SWIG_10 },
    { (char *)"cSimpleModule_send__SWIG_1", (char *)"(JLorg/omnetpp/simkernel/cSimpleModule;JLorg/omnetpp/simkernel/cMessage;Ljava/lang/String;I)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cSimpleModule_1send_1_1SWIG_11 },
    { (char *)"cSimpleModule_send__SWIG_2", (char *)"(JLorg/omnetpp/simkernel/cSimpleModule;JLorg/omnetpp/simkernel/cMessage;Ljava/lang/String;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cSimpleModule_1send_1_1SWIG_12 },
    { (char *)"cSimpleModule_send__SWIG_3", (char *)"(JLorg/omnetpp/simkernel/cSimpleModule;JLorg/omnetpp/simkernel/cMessage;JLorg/omnetpp/simkernel/cGate;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cSimpleModule_1send_1_1SWIG_13 },
    { (char *)"cSimpleModule_sendDelayed__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cSimpleModule;JLorg/omnetpp/simkernel/cMessage;JLorg/omnetpp/simkernel/SimTime;I)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cSimpleModule_1sendDelayed_1_1SWIG_10 },
    { (char *)"cSimpleModule_sendDelayed__SWIG_1", (char *)"(JLorg/omnetpp/simkernel/cSimpleModule;JLorg/omnetpp/simkernel/cMessage;JLorg/omnetpp/simkernel/SimTime;Ljava/lang/String;I)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cSimpleModule_1sendDelayed_1_1SWIG_11 },
    { (char *)"cSimpleModule_sendDelayed__SWIG_2", (char *)"(JLorg/omnetpp/simkernel/cSimpleModule;JLorg/omnetpp/simkernel/cMessage;JLorg/omnetpp/simkernel/SimTime;Ljava/lang/String;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cSimpleModule_1sendDelayed_1_1SWIG_12 },
    { (char *)"cSimpleModule_sendDelayed__SWIG_3", (char *)"(JLorg/omnetpp/simkernel/cSimpleModule;JLorg/omnetpp/simkernel/cMessage;JLorg/omnetpp/simkernel/SimTime;JLorg/omnetpp/simkernel/cGate;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cSimpleModule_1sendDelayed_1_1SWIG_13 },
    { (char *)"cSimpleModule_sendDirect__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cSimpleModule;JLorg/omnetpp/simkernel/cMessage;JLorg/omnetpp/simkernel/cModule;Ljava/lang/String;I)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cSimpleModule_1sendDirect_1_1SWIG_10 },
    { (char *)"cSimpleModule_sendDirect__SWIG_1", (char *)"(JLorg/omnetpp/simkernel/cSimpleModule;JLorg/omnetpp/simkernel/cMessage;JLorg/omnetpp/simkernel/cModule;Ljava/lang/String;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cSimpleModule_1sendDirect_1_1SWIG_11 },
    { (char *)"cSimpleModule_sendDirect__SWIG_2", (char *)"(JLorg/omnetpp/simkernel/cSimpleModule;JLorg/omnetpp/simkernel/cMessage;JLorg/omnetpp/simkernel/cModule;I)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cSimpleModule_1sendDirect_1_1SWIG_12 },
    { (char *)"cSimpleModule_sendDirect__SWIG_3", (char *)"(JLorg/omnetpp/simkernel/cSimpleModule;JLorg/omnetpp/simkernel/cMessage;JLorg/omnetpp/simkernel/cGate;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cSimpleModule_1sendDirect_1_1SWIG_13 },
    { (char *)"cSimpleModule_sendDirect__SWIG_4", (char *)"(JLorg/omnetpp/simkernel/cSimpleModule;JLorg/omnetpp/simkernel/cMessage;JLorg/omnetpp/simkernel/SimTime;JLorg/omnetpp/simkernel/SimTime;JLorg/omnetpp/simkernel/cModule;Ljava/lang/String;I)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cSimpleModule_1sendDirect_1_1SWIG_14 },
    { (char *)"cSimpleModule_sendDirect__SWIG_5", (char *)"(JLorg/omnetpp/simkernel/cSimpleModule;JLorg/omnetpp/simkernel/cMessage;JLorg/omnetpp/simkernel/SimTime;JLorg/omnetpp/simkernel/SimTime;JLorg/omnetpp/simkernel/cModule;Ljava/lang/String;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cSimpleModule_1sendDirect_1_1SWIG_15 },
    { (char *)"cSimpleModule_sendDirect__SWIG_6", (char *)"(JLorg/omnetpp/simkernel/cSimpleModule;JLorg/omnetpp/simkernel/cMessage;JLorg/omnetpp/simkernel/SimTime;JLorg/omnetpp/simkernel/SimTime;JLorg/omnetpp/simkernel/cModule;I)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cSimpleModule_1sendDirect_1_1SWIG_16 },
    { (char *)"cSimpleModule_sendDirect__SWIG_7", (char *)"(JLorg/omnetpp/simkernel/cSimpleModule;JLorg/omnetpp/simkernel/cMessage;JLorg/omnetpp/simkernel/SimTime;JLorg/omnetpp/simkernel/SimTime;JLorg/omnetpp/simkernel/cGate;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cSimpleModule_1sendDirect_1_1SWIG_17 },
    { (char *)"cSimpleModule_scheduleAt", (char *)"(JLorg/omnetpp/simkernel/cSimpleModule;JLorg/omnetpp/simkernel/SimTime;JLorg/omnetpp/simkernel/cMessage;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cSimpleModule_1scheduleAt },
    { (char *)"cSimpleModule_cancelEvent", (char *)"(JLorg/omnetpp/simkernel/cSimpleModule;JLorg/omnetpp/simkernel/cMessage;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cSimpleModule_1cancelEvent },
    { (char *)"cSimpleModule_cancelAndDelete", (char *)"(JLorg/omnetpp/simkernel/cSimpleModule;JLorg/omnetpp/simkernel/cMessage;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cSimpleModule_1cancelAndDelete },
    { (char *)"cSimpleModule_wait", (char *)"(JLorg/omnetpp/simkernel/cSimpleModule;JLorg/omnetpp/simkernel/SimTime;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cSimpleModule_1wait },
    { (char *)"cSimpleModule_waitAndEnqueue", (char *)"(JLorg/omnetpp/simkernel/cSimpleModule;JLorg/omnetpp/simkernel/SimTime;JLorg/omnetpp/simkernel/cQueue;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cSimpleModule_1waitAndEnqueue },
    { (char *)"cSimpleModule_endSimulation", (char *)"(JLorg/omnetpp/simkernel/cSimpleModule;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cSimpleModule_1endSimulation },
    { (char *)"cSimpleModule_halt", (char *)"(JLorg/omnetpp/simkernel/cSimpleModule;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cSimpleModule_1halt },
    { (char *)"cSimpleModule_error", (char *)"(JLorg/omnetpp/simkernel/cSimpleModule;Ljava/lang/String;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cSimpleModule_1error },
    { (char *)"cSimpleModule_cast", (char *)"(JLorg/omnetpp/simkernel/cObject;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cSimpleModule_1cast },
    { (char *)"cComponentType_getPackageProperty", (char *)"(JLorg/omnetpp/simkernel/cComponentType;Ljava/lang/String;)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponentType_1getPackageProperty },
    { (char *)"cComponentType_isAvailable", (char *)"(JLorg/omnetpp/simkernel/cComponentType;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponentType_1isAvailable },
    { (char *)"cComponentType_isInnerType", (char *)"(JLorg/omnetpp/simkernel/cComponentType;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponentType_1isInnerType },
    { (char *)"cComponentType_getCxxNamespace", (char *)"(JLorg/omnetpp/simkernel/cComponentType;)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponentType_1getCxxNamespace },
    { (char *)"cComponentType_getSourceFileName", (char *)"(JLorg/omnetpp/simkernel/cComponentType;)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponentType_1getSourceFileName },
    { (char *)"cComponentType_checkSignal__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cComponentType;IIJLorg/omnetpp/simkernel/cObject;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponentType_1checkSignal_1_1SWIG_10 },
    { (char *)"cComponentType_checkSignal__SWIG_1", (char *)"(JLorg/omnetpp/simkernel/cComponentType;II)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponentType_1checkSignal_1_1SWIG_11 },
    { (char *)"cComponentType_getSignalDeclaration", (char *)"(JLorg/omnetpp/simkernel/cComponentType;Ljava/lang/String;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponentType_1getSignalDeclaration },
    { (char *)"delete_cComponentType", (char *)"(J)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cComponentType },
    { (char *)"cComponentType_getFullName", (char *)"(JLorg/omnetpp/simkernel/cComponentType;)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponentType_1getFullName },
    { (char *)"cComponentType_getNedSource", (char *)"(JLorg/omnetpp/simkernel/cComponentType;)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponentType_1getNedSource },
    { (char *)"cComponentType_find", (char *)"(Ljava/lang/String;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponentType_1find },
    { (char *)"cComponentType_get", (char *)"(Ljava/lang/String;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponentType_1get },
    { (char *)"cComponentType_cast", (char *)"(JLorg/omnetpp/simkernel/cObject;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cComponentType_1cast },
    { (char *)"cModuleType_isNetwork", (char *)"(JLorg/omnetpp/simkernel/cModuleType;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cModuleType_1isNetwork },
    { (char *)"cModuleType_isSimple", (char *)"(JLorg/omnetpp/simkernel/cModuleType;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cModuleType_1isSimple },
    { (char *)"cModuleType_create__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cModuleType;Ljava/lang/String;JLorg/omnetpp/simkernel/cModule;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cModuleType_1create_1_1SWIG_10 },
    { (char *)"cModuleType_create__SWIG_1", (char *)"(JLorg/omnetpp/simkernel/cModuleType;Ljava/lang/String;JLorg/omnetpp/simkernel/cModule;II)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cModuleType_1create_1_1SWIG_11 },
    { (char *)"cModuleType_createScheduleInit", (char *)"(JLorg/omnetpp/simkernel/cModuleType;Ljava/lang/String;JLorg/omnetpp/simkernel/cModule;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cModuleType_1createScheduleInit },
    { (char *)"cModuleType_find", (char *)"(Ljava/lang/String;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cModuleType_1find },
    { (char *)"cModuleType_get", (char *)"(Ljava/lang/String;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cModuleType_1get },
    { (char *)"cModuleType_cast", (char *)"(JLorg/omnetpp/simkernel/cObject;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cModuleType_1cast },
    { (char *)"delete_cModuleType", (char *)"(J)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cModuleType },
    { (char *)"cChannelType_create", (char *)"(JLorg/omnetpp/simkernel/cChannelType;Ljava/lang/String;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cChannelType_1create },
    { (char *)"cChannelType_find", (char *)"(Ljava/lang/String;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cChannelType_1find },
    { (char *)"cChannelType_get", (char *)"(Ljava/lang/String;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cChannelType_1get },
    { (char *)"cChannelType_getIdealChannelType", (char *)"()J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cChannelType_1getIdealChannelType },
    { (char *)"cChannelType_getDelayChannelType", (char *)"()J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cChannelType_1getDelayChannelType },
    { (char *)"cChannelType_getDatarateChannelType", (char *)"()J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cChannelType_1getDatarateChannelType },
    { (char *)"cChannelType_cast", (char *)"(JLorg/omnetpp/simkernel/cObject;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cChannelType_1cast },
    { (char *)"delete_cChannelType", (char *)"(J)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cChannelType },
    { (char *)"new_cArray__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cArray;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cArray_1_1SWIG_10 },
    { (char *)"new_cArray__SWIG_1", (char *)"(Ljava/lang/String;II)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cArray_1_1SWIG_11 },
    { (char *)"new_cArray__SWIG_2", (char *)"(Ljava/lang/String;I)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cArray_1_1SWIG_12 },
    { (char *)"new_cArray__SWIG_3", (char *)"(Ljava/lang/String;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cArray_1_1SWIG_13 },
    { (char *)"new_cArray__SWIG_4", (char *)"()J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cArray_1_1SWIG_14 },
    { (char *)"delete_cArray", (char *)"(J)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cArray },
    { (char *)"cArray_assign", (char *)"(JLorg/omnetpp/simkernel/cArray;JLorg/omnetpp/simkernel/cArray;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cArray_1assign },
    { (char *)"cArray_dup", (char *)"(JLorg/omnetpp/simkernel/cArray;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cArray_1dup },
    { (char *)"cArray_str", (char *)"(JLorg/omnetpp/simkernel/cArray;)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cArray_1str },
    { (char *)"cArray_forEachChild", (char *)"(JLorg/omnetpp/simkernel/cArray;JLorg/omnetpp/simkernel/cVisitor;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cArray_1forEachChild },
    { (char *)"cArray_size", (char *)"(JLorg/omnetpp/simkernel/cArray;)I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cArray_1size },
    { (char *)"cArray_clear", (char *)"(JLorg/omnetpp/simkernel/cArray;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cArray_1clear },
    { (char *)"cArray_getCapacity", (char *)"(JLorg/omnetpp/simkernel/cArray;)I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cArray_1getCapacity },
    { (char *)"cArray_setCapacity", (char *)"(JLorg/omnetpp/simkernel/cArray;I)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cArray_1setCapacity },
    { (char *)"cArray_add", (char *)"(JLorg/omnetpp/simkernel/cArray;JLorg/omnetpp/simkernel/cObject;)I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cArray_1add },
    { (char *)"cArray_addAt", (char *)"(JLorg/omnetpp/simkernel/cArray;IJLorg/omnetpp/simkernel/cObject;)I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cArray_1addAt },
    { (char *)"cArray_set", (char *)"(JLorg/omnetpp/simkernel/cArray;JLorg/omnetpp/simkernel/cObject;)I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cArray_1set },
    { (char *)"cArray_find__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cArray;JLorg/omnetpp/simkernel/cObject;)I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cArray_1find_1_1SWIG_10 },
    { (char *)"cArray_find__SWIG_1", (char *)"(JLorg/omnetpp/simkernel/cArray;Ljava/lang/String;)I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cArray_1find_1_1SWIG_11 },
    { (char *)"cArray_get__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cArray;I)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cArray_1get_1_1SWIG_10 },
    { (char *)"cArray_get__SWIG_1", (char *)"(JLorg/omnetpp/simkernel/cArray;Ljava/lang/String;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cArray_1get_1_1SWIG_11 },
    { (char *)"cArray_exist__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cArray;I)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cArray_1exist_1_1SWIG_10 },
    { (char *)"cArray_exist__SWIG_1", (char *)"(JLorg/omnetpp/simkernel/cArray;Ljava/lang/String;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cArray_1exist_1_1SWIG_11 },
    { (char *)"cArray_remove__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cArray;I)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cArray_1remove_1_1SWIG_10 },
    { (char *)"cArray_remove__SWIG_1", (char *)"(JLorg/omnetpp/simkernel/cArray;Ljava/lang/String;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cArray_1remove_1_1SWIG_11 },
    { (char *)"cArray_remove__SWIG_2", (char *)"(JLorg/omnetpp/simkernel/cArray;JLorg/omnetpp/simkernel/cObject;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cArray_1remove_1_1SWIG_12 },
    { (char *)"cArray_setTakeOwnership", (char *)"(JLorg/omnetpp/simkernel/cArray;Z)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cArray_1setTakeOwnership },
    { (char *)"cArray_getTakeOwnership", (char *)"(JLorg/omnetpp/simkernel/cArray;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cArray_1getTakeOwnership },
    { (char *)"cArray_cast", (char *)"(JLorg/omnetpp/simkernel/cObject;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cArray_1cast },
    { (char *)"new_cQueue__SWIG_0", (char *)"(Ljava/lang/String;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cQueue_1_1SWIG_10 },
    { (char *)"new_cQueue__SWIG_1", (char *)"()J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cQueue_1_1SWIG_11 },
    { (char *)"new_cQueue__SWIG_2", (char *)"(JLorg/omnetpp/simkernel/cQueue;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cQueue_1_1SWIG_12 },
    { (char *)"delete_cQueue", (char *)"(J)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cQueue },
    { (char *)"cQueue_assign", (char *)"(JLorg/omnetpp/simkernel/cQueue;JLorg/omnetpp/simkernel/cQueue;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cQueue_1assign },
    { (char *)"cQueue_dup", (char *)"(JLorg/omnetpp/simkernel/cQueue;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cQueue_1dup },
    { (char *)"cQueue_str", (char *)"(JLorg/omnetpp/simkernel/cQueue;)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cQueue_1str },
    { (char *)"cQueue_forEachChild", (char *)"(JLorg/omnetpp/simkernel/cQueue;JLorg/omnetpp/simkernel/cVisitor;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cQueue_1forEachChild },
    { (char *)"cQueue_insert", (char *)"(JLorg/omnetpp/simkernel/cQueue;JLorg/omnetpp/simkernel/cObject;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cQueue_1insert },
    { (char *)"cQueue_insertBefore", (char *)"(JLorg/omnetpp/simkernel/cQueue;JLorg/omnetpp/simkernel/cObject;JLorg/omnetpp/simkernel/cObject;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cQueue_1insertBefore },
    { (char *)"cQueue_insertAfter", (char *)"(JLorg/omnetpp/simkernel/cQueue;JLorg/omnetpp/simkernel/cObject;JLorg/omnetpp/simkernel/cObject;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cQueue_1insertAfter },
    { (char *)"cQueue_remove", (char *)"(JLorg/omnetpp/simkernel/cQueue;JLorg/omnetpp/simkernel/cObject;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cQueue_1remove },
    { (char *)"cQueue_pop", (char *)"(JLorg/omnetpp/simkernel/cQueue;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cQueue_1pop },
    { (char *)"cQueue_clear", (char *)"(JLorg/omnetpp/simkernel/cQueue;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cQueue_1clear },
    { (char *)"cQueue_front", (char *)"(JLorg/omnetpp/simkernel/cQueue;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cQueue_1front },
    { (char *)"cQueue_back", (char *)"(JLorg/omnetpp/simkernel/cQueue;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cQueue_1back },
    { (char *)"cQueue_getLength", (char *)"(JLorg/omnetpp/simkernel/cQueue;)I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cQueue_1getLength },
    { (char *)"cQueue_isEmpty", (char *)"(JLorg/omnetpp/simkernel/cQueue;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cQueue_1isEmpty },
    { (char *)"cQueue_length", (char *)"(JLorg/omnetpp/simkernel/cQueue;)I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cQueue_1length },
    { (char *)"cQueue_empty", (char *)"(JLorg/omnetpp/simkernel/cQueue;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cQueue_1empty },
    { (char *)"cQueue_get", (char *)"(JLorg/omnetpp/simkernel/cQueue;I)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cQueue_1get },
    { (char *)"cQueue_contains", (char *)"(JLorg/omnetpp/simkernel/cQueue;JLorg/omnetpp/simkernel/cObject;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cQueue_1contains },
    { (char *)"cQueue_setTakeOwnership", (char *)"(JLorg/omnetpp/simkernel/cQueue;Z)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cQueue_1setTakeOwnership },
    { (char *)"cQueue_getTakeOwnership", (char *)"(JLorg/omnetpp/simkernel/cQueue;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cQueue_1getTakeOwnership },
    { (char *)"cQueue_cast", (char *)"(JLorg/omnetpp/simkernel/cObject;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cQueue_1cast },
    { (char *)"delete_cStatistic", (char *)"(J)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cStatistic },
    { (char *)"cStatistic_assign", (char *)"(JLorg/omnetpp/simkernel/cStatistic;JLorg/omnetpp/simkernel/cStatistic;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cStatistic_1assign },
    { (char *)"cStatistic_isWeighted", (char *)"(JLorg/omnetpp/simkernel/cStatistic;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cStatistic_1isWeighted },
    { (char *)"cStatistic_collect__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cStatistic;D)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cStatistic_1collect_1_1SWIG_10 },
    { (char *)"cStatistic_collect__SWIG_1", (char *)"(JLorg/omnetpp/simkernel/cStatistic;JLorg/omnetpp/simkernel/SimTime;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cStatistic_1collect_1_1SWIG_11 },
    { (char *)"cStatistic_collectWeighted__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cStatistic;DD)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cStatistic_1collectWeighted_1_1SWIG_10 },
    { (char *)"cStatistic_collectWeighted__SWIG_1", (char *)"(JLorg/omnetpp/simkernel/cStatistic;JLorg/omnetpp/simkernel/SimTime;D)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cStatistic_1collectWeighted_1_1SWIG_11 },
    { (char *)"cStatistic_collectWeighted__SWIG_2", (char *)"(JLorg/omnetpp/simkernel/cStatistic;DJLorg/omnetpp/simkernel/SimTime;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cStatistic_1collectWeighted_1_1SWIG_12 },
    { (char *)"cStatistic_collectWeighted__SWIG_3", (char *)"(JLorg/omnetpp/simkernel/cStatistic;JLorg/omnetpp/simkernel/SimTime;JLorg/omnetpp/simkernel/SimTime;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cStatistic_1collectWeighted_1_1SWIG_13 },
    { (char *)"cStatistic_merge", (char *)"(JLorg/omnetpp/simkernel/cStatistic;JLorg/omnetpp/simkernel/cStatistic;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cStatistic_1merge },
    { (char *)"cStatistic_clear", (char *)"(JLorg/omnetpp/simkernel/cStatistic;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cStatistic_1clear },
    { (char *)"cStatistic_getCount", (char *)"(JLorg/omnetpp/simkernel/cStatistic;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cStatistic_1getCount },
    { (char *)"cStatistic_getSum", (char *)"(JLorg/omnetpp/simkernel/cStatistic;)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cStatistic_1getSum },
    { (char *)"cStatistic_getSqrSum", (char *)"(JLorg/omnetpp/simkernel/cStatistic;)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cStatistic_1getSqrSum },
    { (char *)"cStatistic_getMin", (char *)"(JLorg/omnetpp/simkernel/cStatistic;)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cStatistic_1getMin },
    { (char *)"cStatistic_getMax", (char *)"(JLorg/omnetpp/simkernel/cStatistic;)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cStatistic_1getMax },
    { (char *)"cStatistic_getMean", (char *)"(JLorg/omnetpp/simkernel/cStatistic;)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cStatistic_1getMean },
    { (char *)"cStatistic_getStddev", (char *)"(JLorg/omnetpp/simkernel/cStatistic;)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cStatistic_1getStddev },
    { (char *)"cStatistic_getVariance", (char *)"(JLorg/omnetpp/simkernel/cStatistic;)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cStatistic_1getVariance },
    { (char *)"cStatistic_getSumWeights", (char *)"(JLorg/omnetpp/simkernel/cStatistic;)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cStatistic_1getSumWeights },
    { (char *)"cStatistic_getWeightedSum", (char *)"(JLorg/omnetpp/simkernel/cStatistic;)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cStatistic_1getWeightedSum },
    { (char *)"cStatistic_getSqrSumWeights", (char *)"(JLorg/omnetpp/simkernel/cStatistic;)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cStatistic_1getSqrSumWeights },
    { (char *)"cStatistic_getWeightedSqrSum", (char *)"(JLorg/omnetpp/simkernel/cStatistic;)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cStatistic_1getWeightedSqrSum },
    { (char *)"cStatistic_saveToFile", (char *)"(JLorg/omnetpp/simkernel/cStatistic;J)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cStatistic_1saveToFile },
    { (char *)"cStatistic_loadFromFile", (char *)"(JLorg/omnetpp/simkernel/cStatistic;J)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cStatistic_1loadFromFile },
    { (char *)"cStatistic_record", (char *)"(JLorg/omnetpp/simkernel/cStatistic;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cStatistic_1record },
    { (char *)"cStatistic_recordWithUnit", (char *)"(JLorg/omnetpp/simkernel/cStatistic;Ljava/lang/String;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cStatistic_1recordWithUnit },
    { (char *)"cStatistic_recordAs__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cStatistic;Ljava/lang/String;Ljava/lang/String;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cStatistic_1recordAs_1_1SWIG_10 },
    { (char *)"cStatistic_recordAs__SWIG_1", (char *)"(JLorg/omnetpp/simkernel/cStatistic;Ljava/lang/String;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cStatistic_1recordAs_1_1SWIG_11 },
    { (char *)"cStatistic_collect2__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cStatistic;DD)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cStatistic_1collect2_1_1SWIG_10 },
    { (char *)"cStatistic_collect2__SWIG_1", (char *)"(JLorg/omnetpp/simkernel/cStatistic;JLorg/omnetpp/simkernel/SimTime;D)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cStatistic_1collect2_1_1SWIG_11 },
    { (char *)"cStatistic_collect2__SWIG_2", (char *)"(JLorg/omnetpp/simkernel/cStatistic;DJLorg/omnetpp/simkernel/SimTime;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cStatistic_1collect2_1_1SWIG_12 },
    { (char *)"cStatistic_collect2__SWIG_3", (char *)"(JLorg/omnetpp/simkernel/cStatistic;JLorg/omnetpp/simkernel/SimTime;JLorg/omnetpp/simkernel/SimTime;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cStatistic_1collect2_1_1SWIG_13 },
    { (char *)"cStatistic_random", (char *)"(JLorg/omnetpp/simkernel/cStatistic;)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cStatistic_1random },
    { (char *)"cStatistic_clearResult", (char *)"(JLorg/omnetpp/simkernel/cStatistic;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cStatistic_1clearResult },
    { (char *)"cStatistic_cast", (char *)"(JLorg/omnetpp/simkernel/cObject;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cStatistic_1cast },
    { (char *)"new_cStdDev__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cStdDev;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cStdDev_1_1SWIG_10 },
    { (char *)"new_cStdDev__SWIG_1", (char *)"(Ljava/lang/String;Z)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cStdDev_1_1SWIG_11 },
    { (char *)"new_cStdDev__SWIG_2", (char *)"(Ljava/lang/String;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cStdDev_1_1SWIG_12 },
    { (char *)"new_cStdDev__SWIG_3", (char *)"()J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cStdDev_1_1SWIG_13 },
    { (char *)"delete_cStdDev", (char *)"(J)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cStdDev },
    { (char *)"cStdDev_assign", (char *)"(JLorg/omnetpp/simkernel/cStdDev;JLorg/omnetpp/simkernel/cStdDev;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cStdDev_1assign },
    { (char *)"cStdDev_dup", (char *)"(JLorg/omnetpp/simkernel/cStdDev;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cStdDev_1dup },
    { (char *)"cStdDev_str", (char *)"(JLorg/omnetpp/simkernel/cStdDev;)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cStdDev_1str },
    { (char *)"cStdDev_isWeighted", (char *)"(JLorg/omnetpp/simkernel/cStdDev;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cStdDev_1isWeighted },
    { (char *)"cStdDev_collect__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cStdDev;D)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cStdDev_1collect_1_1SWIG_10 },
    { (char *)"cStdDev_collect__SWIG_1_0", (char *)"(JLorg/omnetpp/simkernel/cStdDev;JLorg/omnetpp/simkernel/SimTime;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cStdDev_1collect_1_1SWIG_11_10 },
    { (char *)"cStdDev_collectWeighted__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cStdDev;DD)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cStdDev_1collectWeighted_1_1SWIG_10 },
    { (char *)"cStdDev_collectWeighted__SWIG_1_0", (char *)"(JLorg/omnetpp/simkernel/cStdDev;JLorg/omnetpp/simkernel/SimTime;D)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cStdDev_1collectWeighted_1_1SWIG_11_10 },
    { (char *)"cStdDev_collectWeighted__SWIG_1_1", (char *)"(JLorg/omnetpp/simkernel/cStdDev;DJLorg/omnetpp/simkernel/SimTime;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cStdDev_1collectWeighted_1_1SWIG_11_11 },
    { (char *)"cStdDev_collectWeighted__SWIG_1_2", (char *)"(JLorg/omnetpp/simkernel/cStdDev;JLorg/omnetpp/simkernel/SimTime;JLorg/omnetpp/simkernel/SimTime;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cStdDev_1collectWeighted_1_1SWIG_11_12 },
    { (char *)"cStdDev_merge", (char *)"(JLorg/omnetpp/simkernel/cStdDev;JLorg/omnetpp/simkernel/cStatistic;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cStdDev_1merge },
    { (char *)"cStdDev_getCount", (char *)"(JLorg/omnetpp/simkernel/cStdDev;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cStdDev_1getCount },
    { (char *)"cStdDev_getSum", (char *)"(JLorg/omnetpp/simkernel/cStdDev;)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cStdDev_1getSum },
    { (char *)"cStdDev_getSqrSum", (char *)"(JLorg/omnetpp/simkernel/cStdDev;)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cStdDev_1getSqrSum },
    { (char *)"cStdDev_getMin", (char *)"(JLorg/omnetpp/simkernel/cStdDev;)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cStdDev_1getMin },
    { (char *)"cStdDev_getMax", (char *)"(JLorg/omnetpp/simkernel/cStdDev;)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cStdDev_1getMax },
    { (char *)"cStdDev_getMean", (char *)"(JLorg/omnetpp/simkernel/cStdDev;)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cStdDev_1getMean },
    { (char *)"cStdDev_getStddev", (char *)"(JLorg/omnetpp/simkernel/cStdDev;)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cStdDev_1getStddev },
    { (char *)"cStdDev_getVariance", (char *)"(JLorg/omnetpp/simkernel/cStdDev;)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cStdDev_1getVariance },
    { (char *)"cStdDev_getSumWeights", (char *)"(JLorg/omnetpp/simkernel/cStdDev;)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cStdDev_1getSumWeights },
    { (char *)"cStdDev_getWeightedSum", (char *)"(JLorg/omnetpp/simkernel/cStdDev;)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cStdDev_1getWeightedSum },
    { (char *)"cStdDev_getSqrSumWeights", (char *)"(JLorg/omnetpp/simkernel/cStdDev;)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cStdDev_1getSqrSumWeights },
    { (char *)"cStdDev_getWeightedSqrSum", (char *)"(JLorg/omnetpp/simkernel/cStdDev;)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cStdDev_1getWeightedSqrSum },
    { (char *)"cStdDev_draw", (char *)"(JLorg/omnetpp/simkernel/cStdDev;)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cStdDev_1draw },
    { (char *)"cStdDev_clear", (char *)"(JLorg/omnetpp/simkernel/cStdDev;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cStdDev_1clear },
    { (char *)"cStdDev_saveToFile", (char *)"(JLorg/omnetpp/simkernel/cStdDev;J)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cStdDev_1saveToFile },
    { (char *)"cStdDev_loadFromFile", (char *)"(JLorg/omnetpp/simkernel/cStdDev;J)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cStdDev_1loadFromFile },
    { (char *)"cStdDev_cast", (char *)"(JLorg/omnetpp/simkernel/cObject;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cStdDev_1cast },
    { (char *)"new_cWeightedStdDev__SWIG_0", (char *)"(Ljava/lang/String;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cWeightedStdDev_1_1SWIG_10 },
    { (char *)"new_cWeightedStdDev__SWIG_1", (char *)"()J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cWeightedStdDev_1_1SWIG_11 },
    { (char *)"new_cWeightedStdDev__SWIG_2", (char *)"(JLorg/omnetpp/simkernel/cWeightedStdDev;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cWeightedStdDev_1_1SWIG_12 },
    { (char *)"cWeightedStdDev_assign", (char *)"(JLorg/omnetpp/simkernel/cWeightedStdDev;JLorg/omnetpp/simkernel/cWeightedStdDev;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cWeightedStdDev_1assign },
    { (char *)"cWeightedStdDev_dup", (char *)"(JLorg/omnetpp/simkernel/cWeightedStdDev;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cWeightedStdDev_1dup },
    { (char *)"delete_cWeightedStdDev", (char *)"(J)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cWeightedStdDev },
    { (char *)"cHistogram_MODE_DOUBLES_get", (char *)"()I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cHistogram_1MODE_1DOUBLES_1get },
    { (char *)"cHistogram_collectIntoHistogram__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cHistogram;DD)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cHistogram_1collectIntoHistogram_1_1SWIG_10 },
    { (char *)"cHistogram_collectIntoHistogram__SWIG_1", (char *)"(JLorg/omnetpp/simkernel/cHistogram;D)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cHistogram_1collectIntoHistogram_1_1SWIG_11 },
    { (char *)"cHistogram_dump", (char *)"(JLorg/omnetpp/simkernel/cHistogram;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cHistogram_1dump },
    { (char *)"cHistogram_assertSanity", (char *)"(JLorg/omnetpp/simkernel/cHistogram;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cHistogram_1assertSanity },
    { (char *)"new_cHistogram__SWIG_0", (char *)"(Ljava/lang/String;Z)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cHistogram_1_1SWIG_10 },
    { (char *)"new_cHistogram__SWIG_1", (char *)"(Ljava/lang/String;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cHistogram_1_1SWIG_11 },
    { (char *)"new_cHistogram__SWIG_2", (char *)"()J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cHistogram_1_1SWIG_12 },
    { (char *)"new_cHistogram__SWIG_3", (char *)"(Ljava/lang/String;IZ)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cHistogram_1_1SWIG_13 },
    { (char *)"new_cHistogram__SWIG_4", (char *)"(Ljava/lang/String;I)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cHistogram_1_1SWIG_14 },
    { (char *)"new_cHistogram__SWIG_5", (char *)"(Ljava/lang/String;JZ)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cHistogram_1_1SWIG_15 },
    { (char *)"new_cHistogram__SWIG_6", (char *)"(Ljava/lang/String;J)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cHistogram_1_1SWIG_16 },
    { (char *)"new_cHistogram__SWIG_7", (char *)"(JLorg/omnetpp/simkernel/cHistogram;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cHistogram_1_1SWIG_17 },
    { (char *)"cHistogram_assign", (char *)"(JLorg/omnetpp/simkernel/cHistogram;JLorg/omnetpp/simkernel/cHistogram;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cHistogram_1assign },
    { (char *)"delete_cHistogram", (char *)"(J)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cHistogram },
    { (char *)"cHistogram_dup", (char *)"(JLorg/omnetpp/simkernel/cHistogram;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cHistogram_1dup },
    { (char *)"cHistogram_collect__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cHistogram;D)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cHistogram_1collect_1_1SWIG_10 },
    { (char *)"cHistogram_collectWeighted__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cHistogram;DD)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cHistogram_1collectWeighted_1_1SWIG_10 },
    { (char *)"cHistogram_clear", (char *)"(JLorg/omnetpp/simkernel/cHistogram;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cHistogram_1clear },
    { (char *)"cHistogram_saveToFile", (char *)"(JLorg/omnetpp/simkernel/cHistogram;J)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cHistogram_1saveToFile },
    { (char *)"cHistogram_loadFromFile", (char *)"(JLorg/omnetpp/simkernel/cHistogram;J)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cHistogram_1loadFromFile },
    { (char *)"cHistogram_merge", (char *)"(JLorg/omnetpp/simkernel/cHistogram;JLorg/omnetpp/simkernel/cStatistic;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cHistogram_1merge },
    { (char *)"cHistogram_setStrategy", (char *)"(JLorg/omnetpp/simkernel/cHistogram;J)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cHistogram_1setStrategy },
    { (char *)"cHistogram_getStrategy", (char *)"(JLorg/omnetpp/simkernel/cHistogram;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cHistogram_1getStrategy },
    { (char *)"cHistogram_binsAlreadySetUp", (char *)"(JLorg/omnetpp/simkernel/cHistogram;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cHistogram_1binsAlreadySetUp },
    { (char *)"cHistogram_setUpBins", (char *)"(JLorg/omnetpp/simkernel/cHistogram;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cHistogram_1setUpBins },
    { (char *)"cHistogram_setBinEdges", (char *)"(JLorg/omnetpp/simkernel/cHistogram;J)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cHistogram_1setBinEdges },
    { (char *)"cHistogram_createUniformBins", (char *)"(JLorg/omnetpp/simkernel/cHistogram;DDD)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cHistogram_1createUniformBins },
    { (char *)"cHistogram_prependBins", (char *)"(JLorg/omnetpp/simkernel/cHistogram;J)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cHistogram_1prependBins },
    { (char *)"cHistogram_appendBins", (char *)"(JLorg/omnetpp/simkernel/cHistogram;J)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cHistogram_1appendBins },
    { (char *)"cHistogram_extendBinsTo__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cHistogram;DDI)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cHistogram_1extendBinsTo_1_1SWIG_10 },
    { (char *)"cHistogram_extendBinsTo__SWIG_1", (char *)"(JLorg/omnetpp/simkernel/cHistogram;DD)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cHistogram_1extendBinsTo_1_1SWIG_11 },
    { (char *)"cHistogram_mergeBins", (char *)"(JLorg/omnetpp/simkernel/cHistogram;I)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cHistogram_1mergeBins },
    { (char *)"cHistogram_getBinEdges", (char *)"(JLorg/omnetpp/simkernel/cHistogram;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cHistogram_1getBinEdges },
    { (char *)"cHistogram_getBinValues", (char *)"(JLorg/omnetpp/simkernel/cHistogram;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cHistogram_1getBinValues },
    { (char *)"cHistogram_getNumBins", (char *)"(JLorg/omnetpp/simkernel/cHistogram;)I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cHistogram_1getNumBins },
    { (char *)"cHistogram_getBinEdge", (char *)"(JLorg/omnetpp/simkernel/cHistogram;I)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cHistogram_1getBinEdge },
    { (char *)"cHistogram_getBinValue", (char *)"(JLorg/omnetpp/simkernel/cHistogram;I)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cHistogram_1getBinValue },
    { (char *)"cHistogram_getUnderflowSumWeights", (char *)"(JLorg/omnetpp/simkernel/cHistogram;)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cHistogram_1getUnderflowSumWeights },
    { (char *)"cHistogram_getOverflowSumWeights", (char *)"(JLorg/omnetpp/simkernel/cHistogram;)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cHistogram_1getOverflowSumWeights },
    { (char *)"cHistogram_getNumUnderflows", (char *)"(JLorg/omnetpp/simkernel/cHistogram;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cHistogram_1getNumUnderflows },
    { (char *)"cHistogram_getNumOverflows", (char *)"(JLorg/omnetpp/simkernel/cHistogram;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cHistogram_1getNumOverflows },
    { (char *)"cHistogram_setMode", (char *)"(JLorg/omnetpp/simkernel/cHistogram;I)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cHistogram_1setMode },
    { (char *)"cHistogram_setRange", (char *)"(JLorg/omnetpp/simkernel/cHistogram;DD)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cHistogram_1setRange },
    { (char *)"cHistogram_setNumPrecollectedValues", (char *)"(JLorg/omnetpp/simkernel/cHistogram;I)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cHistogram_1setNumPrecollectedValues },
    { (char *)"cHistogram_setRangeExtensionFactor", (char *)"(JLorg/omnetpp/simkernel/cHistogram;D)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cHistogram_1setRangeExtensionFactor },
    { (char *)"cHistogram_setAutoExtend", (char *)"(JLorg/omnetpp/simkernel/cHistogram;Z)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cHistogram_1setAutoExtend },
    { (char *)"cHistogram_setNumBinsHint", (char *)"(JLorg/omnetpp/simkernel/cHistogram;I)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cHistogram_1setNumBinsHint },
    { (char *)"cHistogram_setBinSizeHint", (char *)"(JLorg/omnetpp/simkernel/cHistogram;D)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cHistogram_1setBinSizeHint },
    { (char *)"cHistogram_setRangeAuto__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cHistogram;ID)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cHistogram_1setRangeAuto_1_1SWIG_10 },
    { (char *)"cHistogram_setRangeAuto__SWIG_1", (char *)"(JLorg/omnetpp/simkernel/cHistogram;I)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cHistogram_1setRangeAuto_1_1SWIG_11 },
    { (char *)"cHistogram_setRangeAuto__SWIG_2", (char *)"(JLorg/omnetpp/simkernel/cHistogram;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cHistogram_1setRangeAuto_1_1SWIG_12 },
    { (char *)"cHistogram_setRangeAutoLower__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cHistogram;DID)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cHistogram_1setRangeAutoLower_1_1SWIG_10 },
    { (char *)"cHistogram_setRangeAutoLower__SWIG_1", (char *)"(JLorg/omnetpp/simkernel/cHistogram;DI)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cHistogram_1setRangeAutoLower_1_1SWIG_11 },
    { (char *)"cHistogram_setRangeAutoLower__SWIG_2", (char *)"(JLorg/omnetpp/simkernel/cHistogram;D)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cHistogram_1setRangeAutoLower_1_1SWIG_12 },
    { (char *)"cHistogram_setRangeAutoUpper__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cHistogram;DID)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cHistogram_1setRangeAutoUpper_1_1SWIG_10 },
    { (char *)"cHistogram_setRangeAutoUpper__SWIG_1", (char *)"(JLorg/omnetpp/simkernel/cHistogram;DI)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cHistogram_1setRangeAutoUpper_1_1SWIG_11 },
    { (char *)"cHistogram_setRangeAutoUpper__SWIG_2", (char *)"(JLorg/omnetpp/simkernel/cHistogram;D)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cHistogram_1setRangeAutoUpper_1_1SWIG_12 },
    { (char *)"cHistogram_setNumCells", (char *)"(JLorg/omnetpp/simkernel/cHistogram;I)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cHistogram_1setNumCells },
    { (char *)"cHistogram_setCellSize", (char *)"(JLorg/omnetpp/simkernel/cHistogram;D)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cHistogram_1setCellSize },
    { (char *)"new_cPSquare__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cPSquare;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cPSquare_1_1SWIG_10 },
    { (char *)"new_cPSquare__SWIG_1", (char *)"(Ljava/lang/String;I)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cPSquare_1_1SWIG_11 },
    { (char *)"new_cPSquare__SWIG_2", (char *)"(Ljava/lang/String;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cPSquare_1_1SWIG_12 },
    { (char *)"new_cPSquare__SWIG_3", (char *)"()J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cPSquare_1_1SWIG_13 },
    { (char *)"delete_cPSquare", (char *)"(J)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cPSquare },
    { (char *)"cPSquare_assign", (char *)"(JLorg/omnetpp/simkernel/cPSquare;JLorg/omnetpp/simkernel/cPSquare;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cPSquare_1assign },
    { (char *)"cPSquare_dup", (char *)"(JLorg/omnetpp/simkernel/cPSquare;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cPSquare_1dup },
    { (char *)"cPSquare_binsAlreadySetUp", (char *)"(JLorg/omnetpp/simkernel/cPSquare;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cPSquare_1binsAlreadySetUp },
    { (char *)"cPSquare_setUpBins", (char *)"(JLorg/omnetpp/simkernel/cPSquare;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cPSquare_1setUpBins },
    { (char *)"cPSquare_collect__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cPSquare;D)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cPSquare_1collect_1_1SWIG_10 },
    { (char *)"cPSquare_collect__SWIG_1_0", (char *)"(JLorg/omnetpp/simkernel/cPSquare;JLorg/omnetpp/simkernel/SimTime;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cPSquare_1collect_1_1SWIG_11_10 },
    { (char *)"cPSquare_collectWeighted__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cPSquare;DD)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cPSquare_1collectWeighted_1_1SWIG_10 },
    { (char *)"cPSquare_collectWeighted__SWIG_1_0", (char *)"(JLorg/omnetpp/simkernel/cPSquare;JLorg/omnetpp/simkernel/SimTime;D)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cPSquare_1collectWeighted_1_1SWIG_11_10 },
    { (char *)"cPSquare_collectWeighted__SWIG_1_1", (char *)"(JLorg/omnetpp/simkernel/cPSquare;DJLorg/omnetpp/simkernel/SimTime;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cPSquare_1collectWeighted_1_1SWIG_11_11 },
    { (char *)"cPSquare_collectWeighted__SWIG_1_2", (char *)"(JLorg/omnetpp/simkernel/cPSquare;JLorg/omnetpp/simkernel/SimTime;JLorg/omnetpp/simkernel/SimTime;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cPSquare_1collectWeighted_1_1SWIG_11_12 },
    { (char *)"cPSquare_getNumBins", (char *)"(JLorg/omnetpp/simkernel/cPSquare;)I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cPSquare_1getNumBins },
    { (char *)"cPSquare_getBinEdge", (char *)"(JLorg/omnetpp/simkernel/cPSquare;I)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cPSquare_1getBinEdge },
    { (char *)"cPSquare_getBinValue", (char *)"(JLorg/omnetpp/simkernel/cPSquare;I)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cPSquare_1getBinValue },
    { (char *)"cPSquare_getNumUnderflows", (char *)"(JLorg/omnetpp/simkernel/cPSquare;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cPSquare_1getNumUnderflows },
    { (char *)"cPSquare_getNumOverflows", (char *)"(JLorg/omnetpp/simkernel/cPSquare;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cPSquare_1getNumOverflows },
    { (char *)"cPSquare_getUnderflowSumWeights", (char *)"(JLorg/omnetpp/simkernel/cPSquare;)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cPSquare_1getUnderflowSumWeights },
    { (char *)"cPSquare_getOverflowSumWeights", (char *)"(JLorg/omnetpp/simkernel/cPSquare;)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cPSquare_1getOverflowSumWeights },
    { (char *)"cPSquare_draw", (char *)"(JLorg/omnetpp/simkernel/cPSquare;)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cPSquare_1draw },
    { (char *)"cPSquare_merge", (char *)"(JLorg/omnetpp/simkernel/cPSquare;JLorg/omnetpp/simkernel/cStatistic;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cPSquare_1merge },
    { (char *)"cPSquare_saveToFile", (char *)"(JLorg/omnetpp/simkernel/cPSquare;J)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cPSquare_1saveToFile },
    { (char *)"cPSquare_loadFromFile", (char *)"(JLorg/omnetpp/simkernel/cPSquare;J)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cPSquare_1loadFromFile },
    { (char *)"new_cVarHistogram__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cVarHistogram;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cVarHistogram_1_1SWIG_10 },
    { (char *)"new_cVarHistogram__SWIG_1", (char *)"(Ljava/lang/String;II)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cVarHistogram_1_1SWIG_11 },
    { (char *)"new_cVarHistogram__SWIG_2", (char *)"(Ljava/lang/String;I)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cVarHistogram_1_1SWIG_12 },
    { (char *)"new_cVarHistogram__SWIG_3", (char *)"(Ljava/lang/String;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cVarHistogram_1_1SWIG_13 },
    { (char *)"new_cVarHistogram__SWIG_4", (char *)"()J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cVarHistogram_1_1SWIG_14 },
    { (char *)"delete_cVarHistogram", (char *)"(J)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cVarHistogram },
    { (char *)"cVarHistogram_assign", (char *)"(JLorg/omnetpp/simkernel/cVarHistogram;JLorg/omnetpp/simkernel/cVarHistogram;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cVarHistogram_1assign },
    { (char *)"cVarHistogram_dup", (char *)"(JLorg/omnetpp/simkernel/cVarHistogram;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cVarHistogram_1dup },
    { (char *)"cVarHistogram_clear", (char *)"(JLorg/omnetpp/simkernel/cVarHistogram;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cVarHistogram_1clear },
    { (char *)"cVarHistogram_setUpBins", (char *)"(JLorg/omnetpp/simkernel/cVarHistogram;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cVarHistogram_1setUpBins },
    { (char *)"cVarHistogram_collectIntoHistogram", (char *)"(JLorg/omnetpp/simkernel/cVarHistogram;D)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cVarHistogram_1collectIntoHistogram },
    { (char *)"cVarHistogram_collectWeightedIntoHistogram", (char *)"(JLorg/omnetpp/simkernel/cVarHistogram;DD)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cVarHistogram_1collectWeightedIntoHistogram },
    { (char *)"cVarHistogram_draw", (char *)"(JLorg/omnetpp/simkernel/cVarHistogram;)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cVarHistogram_1draw },
    { (char *)"cVarHistogram_getPDF", (char *)"(JLorg/omnetpp/simkernel/cVarHistogram;D)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cVarHistogram_1getPDF },
    { (char *)"cVarHistogram_getCDF", (char *)"(JLorg/omnetpp/simkernel/cVarHistogram;D)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cVarHistogram_1getCDF },
    { (char *)"cVarHistogram_getBinEdge", (char *)"(JLorg/omnetpp/simkernel/cVarHistogram;I)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cVarHistogram_1getBinEdge },
    { (char *)"cVarHistogram_getBinValue", (char *)"(JLorg/omnetpp/simkernel/cVarHistogram;I)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cVarHistogram_1getBinValue },
    { (char *)"cVarHistogram_saveToFile", (char *)"(JLorg/omnetpp/simkernel/cVarHistogram;J)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cVarHistogram_1saveToFile },
    { (char *)"cVarHistogram_loadFromFile", (char *)"(JLorg/omnetpp/simkernel/cVarHistogram;J)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cVarHistogram_1loadFromFile },
    { (char *)"cVarHistogram_addBinBound", (char *)"(JLorg/omnetpp/simkernel/cVarHistogram;D)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cVarHistogram_1addBinBound },
    { (char *)"delete_cRNG", (char *)"(J)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cRNG },
    { (char *)"cRNG_selfTest", (char *)"(JLorg/omnetpp/simkernel/cRNG;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cRNG_1selfTest },
    { (char *)"cRNG_getNumbersDrawn", (char *)"(JLorg/omnetpp/simkernel/cRNG;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cRNG_1getNumbersDrawn },
    { (char *)"cRNG_intRand__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cRNG;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cRNG_1intRand_1_1SWIG_10 },
    { (char *)"cRNG_intRandMax", (char *)"(JLorg/omnetpp/simkernel/cRNG;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cRNG_1intRandMax },
    { (char *)"cRNG_intRand__SWIG_1", (char *)"(JLorg/omnetpp/simkernel/cRNG;J)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cRNG_1intRand_1_1SWIG_11 },
    { (char *)"cRNG_doubleRand", (char *)"(JLorg/omnetpp/simkernel/cRNG;)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cRNG_1doubleRand },
    { (char *)"cRNG_doubleRandNonz", (char *)"(JLorg/omnetpp/simkernel/cRNG;)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cRNG_1doubleRandNonz },
    { (char *)"cRNG_doubleRandIncl1", (char *)"(JLorg/omnetpp/simkernel/cRNG;)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cRNG_1doubleRandIncl1 },
    { (char *)"cRNG_doubleRandNonzIncl1", (char *)"(JLorg/omnetpp/simkernel/cRNG;)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cRNG_1doubleRandNonzIncl1 },
    { (char *)"LCG32_MAX_get", (char *)"()I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_LCG32_1MAX_1get },
    { (char *)"cObjectFactory_str", (char *)"(JLorg/omnetpp/simkernel/cObjectFactory;)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cObjectFactory_1str },
    { (char *)"cObjectFactory_isAbstract", (char *)"(JLorg/omnetpp/simkernel/cObjectFactory;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cObjectFactory_1isAbstract },
    { (char *)"cObjectFactory_createOne__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cObjectFactory;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cObjectFactory_1createOne_1_1SWIG_10 },
    { (char *)"cObjectFactory_isInstance", (char *)"(JLorg/omnetpp/simkernel/cObjectFactory;JLorg/omnetpp/simkernel/cObject;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cObjectFactory_1isInstance },
    { (char *)"cObjectFactory_getDescription", (char *)"(JLorg/omnetpp/simkernel/cObjectFactory;)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cObjectFactory_1getDescription },
    { (char *)"cObjectFactory_find__SWIG_0", (char *)"(Ljava/lang/String;Ljava/lang/String;Z)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cObjectFactory_1find_1_1SWIG_10 },
    { (char *)"cObjectFactory_find__SWIG_1", (char *)"(Ljava/lang/String;Ljava/lang/String;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cObjectFactory_1find_1_1SWIG_11 },
    { (char *)"cObjectFactory_find__SWIG_2", (char *)"(Ljava/lang/String;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cObjectFactory_1find_1_1SWIG_12 },
    { (char *)"cObjectFactory_get__SWIG_0", (char *)"(Ljava/lang/String;Ljava/lang/String;Z)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cObjectFactory_1get_1_1SWIG_10 },
    { (char *)"cObjectFactory_get__SWIG_1", (char *)"(Ljava/lang/String;Ljava/lang/String;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cObjectFactory_1get_1_1SWIG_11 },
    { (char *)"cObjectFactory_get__SWIG_2", (char *)"(Ljava/lang/String;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cObjectFactory_1get_1_1SWIG_12 },
    { (char *)"cObjectFactory_createOne__SWIG_1", (char *)"(Ljava/lang/String;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cObjectFactory_1createOne_1_1SWIG_11 },
    { (char *)"cObjectFactory_createOneIfClassIsKnown", (char *)"(Ljava/lang/String;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cObjectFactory_1createOneIfClassIsKnown },
    { (char *)"delete_cObjectFactory", (char *)"(J)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cObjectFactory },
    { (char *)"createOne", (char *)"(Ljava/lang/String;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_createOne },
    { (char *)"createOneIfClassIsKnown", (char *)"(Ljava/lang/String;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_createOneIfClassIsKnown },
    { (char *)"cDisplayString_setHostObject", (char *)"(JLorg/omnetpp/simkernel/cDisplayString;JLorg/omnetpp/simkernel/cComponent;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cDisplayString_1setHostObject },
    { (char *)"cDisplayString_dump", (char *)"(JLorg/omnetpp/simkernel/cDisplayString;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cDisplayString_1dump },
    { (char *)"new_cDisplayString__SWIG_0", (char *)"()J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cDisplayString_1_1SWIG_10 },
    { (char *)"new_cDisplayString__SWIG_1", (char *)"(Ljava/lang/String;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cDisplayString_1_1SWIG_11 },
    { (char *)"new_cDisplayString__SWIG_2", (char *)"(JLorg/omnetpp/simkernel/cDisplayString;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cDisplayString_1_1SWIG_12 },
    { (char *)"delete_cDisplayString", (char *)"(J)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cDisplayString },
    { (char *)"cDisplayString_assign__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cDisplayString;JLorg/omnetpp/simkernel/cDisplayString;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cDisplayString_1assign_1_1SWIG_10 },
    { (char *)"cDisplayString_assign__SWIG_1", (char *)"(JLorg/omnetpp/simkernel/cDisplayString;Ljava/lang/String;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cDisplayString_1assign_1_1SWIG_11 },
    { (char *)"cDisplayString_str", (char *)"(JLorg/omnetpp/simkernel/cDisplayString;)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cDisplayString_1str },
    { (char *)"cDisplayString_set", (char *)"(JLorg/omnetpp/simkernel/cDisplayString;Ljava/lang/String;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cDisplayString_1set },
    { (char *)"cDisplayString_parse", (char *)"(JLorg/omnetpp/simkernel/cDisplayString;Ljava/lang/String;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cDisplayString_1parse },
    { (char *)"cDisplayString_updateWith__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cDisplayString;JLorg/omnetpp/simkernel/cDisplayString;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cDisplayString_1updateWith_1_1SWIG_10 },
    { (char *)"cDisplayString_updateWith__SWIG_1", (char *)"(JLorg/omnetpp/simkernel/cDisplayString;Ljava/lang/String;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cDisplayString_1updateWith_1_1SWIG_11 },
    { (char *)"cDisplayString_containsTag", (char *)"(JLorg/omnetpp/simkernel/cDisplayString;Ljava/lang/String;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cDisplayString_1containsTag },
    { (char *)"cDisplayString_getNumArgs__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cDisplayString;Ljava/lang/String;)I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cDisplayString_1getNumArgs_1_1SWIG_10 },
    { (char *)"cDisplayString_getTagArg__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cDisplayString;Ljava/lang/String;I)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cDisplayString_1getTagArg_1_1SWIG_10 },
    { (char *)"cDisplayString_setTagArg__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cDisplayString;Ljava/lang/String;ILjava/lang/String;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cDisplayString_1setTagArg_1_1SWIG_10 },
    { (char *)"cDisplayString_setTagArg__SWIG_1", (char *)"(JLorg/omnetpp/simkernel/cDisplayString;Ljava/lang/String;II)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cDisplayString_1setTagArg_1_1SWIG_11 },
    { (char *)"cDisplayString_removeTag__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cDisplayString;Ljava/lang/String;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cDisplayString_1removeTag_1_1SWIG_10 },
    { (char *)"cDisplayString_getNumTags", (char *)"(JLorg/omnetpp/simkernel/cDisplayString;)I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cDisplayString_1getNumTags },
    { (char *)"cDisplayString_getTagName", (char *)"(JLorg/omnetpp/simkernel/cDisplayString;I)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cDisplayString_1getTagName },
    { (char *)"cDisplayString_getTagIndex", (char *)"(JLorg/omnetpp/simkernel/cDisplayString;Ljava/lang/String;)I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cDisplayString_1getTagIndex },
    { (char *)"cDisplayString_getNumArgs__SWIG_1", (char *)"(JLorg/omnetpp/simkernel/cDisplayString;I)I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cDisplayString_1getNumArgs_1_1SWIG_11 },
    { (char *)"cDisplayString_getTagArg__SWIG_1", (char *)"(JLorg/omnetpp/simkernel/cDisplayString;II)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cDisplayString_1getTagArg_1_1SWIG_11 },
    { (char *)"cDisplayString_setTagArg__SWIG_2", (char *)"(JLorg/omnetpp/simkernel/cDisplayString;IILjava/lang/String;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cDisplayString_1setTagArg_1_1SWIG_12 },
    { (char *)"cDisplayString_insertTag__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cDisplayString;Ljava/lang/String;I)I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cDisplayString_1insertTag_1_1SWIG_10 },
    { (char *)"cDisplayString_insertTag__SWIG_1", (char *)"(JLorg/omnetpp/simkernel/cDisplayString;Ljava/lang/String;)I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cDisplayString_1insertTag_1_1SWIG_11 },
    { (char *)"cDisplayString_removeTag__SWIG_1", (char *)"(JLorg/omnetpp/simkernel/cDisplayString;I)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cDisplayString_1removeTag_1_1SWIG_11 },
    { (char *)"cEnum_registerNames", (char *)"(JLorg/omnetpp/simkernel/cEnum;Ljava/lang/String;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cEnum_1registerNames },
    { (char *)"cEnum_registerValues", (char *)"(JLorg/omnetpp/simkernel/cEnum;I)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cEnum_1registerValues },
    { (char *)"new_cEnum__SWIG_0", (char *)"(Ljava/lang/String;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cEnum_1_1SWIG_10 },
    { (char *)"new_cEnum__SWIG_1", (char *)"()J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cEnum_1_1SWIG_11 },
    { (char *)"new_cEnum__SWIG_2", (char *)"(Ljava/lang/String;Ljava/lang/String;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cEnum_1_1SWIG_12 },
    { (char *)"new_cEnum__SWIG_3", (char *)"(JLorg/omnetpp/simkernel/cEnum;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cEnum_1_1SWIG_13 },
    { (char *)"delete_cEnum", (char *)"(J)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cEnum },
    { (char *)"cEnum_assign", (char *)"(JLorg/omnetpp/simkernel/cEnum;JLorg/omnetpp/simkernel/cEnum;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cEnum_1assign },
    { (char *)"cEnum_dup", (char *)"(JLorg/omnetpp/simkernel/cEnum;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cEnum_1dup },
    { (char *)"cEnum_str", (char *)"(JLorg/omnetpp/simkernel/cEnum;)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cEnum_1str },
    { (char *)"cEnum_insert", (char *)"(JLorg/omnetpp/simkernel/cEnum;ILjava/lang/String;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cEnum_1insert },
    { (char *)"cEnum_bulkInsert", (char *)"(JLorg/omnetpp/simkernel/cEnum;Ljava/lang/String;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cEnum_1bulkInsert },
    { (char *)"cEnum_getStringFor", (char *)"(JLorg/omnetpp/simkernel/cEnum;I)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cEnum_1getStringFor },
    { (char *)"cEnum_lookup__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cEnum;Ljava/lang/String;I)I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cEnum_1lookup_1_1SWIG_10 },
    { (char *)"cEnum_lookup__SWIG_1", (char *)"(JLorg/omnetpp/simkernel/cEnum;Ljava/lang/String;)I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cEnum_1lookup_1_1SWIG_11 },
    { (char *)"cEnum_resolve", (char *)"(JLorg/omnetpp/simkernel/cEnum;Ljava/lang/String;)I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cEnum_1resolve },
    { (char *)"cEnum_getNameValueMap", (char *)"(JLorg/omnetpp/simkernel/cEnum;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cEnum_1getNameValueMap },
    { (char *)"cEnum_find__SWIG_0", (char *)"(Ljava/lang/String;Ljava/lang/String;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cEnum_1find_1_1SWIG_10 },
    { (char *)"cEnum_find__SWIG_1", (char *)"(Ljava/lang/String;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cEnum_1find_1_1SWIG_11 },
    { (char *)"cEnum_get__SWIG_0", (char *)"(Ljava/lang/String;Ljava/lang/String;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cEnum_1get_1_1SWIG_10 },
    { (char *)"cEnum_get__SWIG_1", (char *)"(Ljava/lang/String;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cEnum_1get_1_1SWIG_11 },
    { (char *)"cEnvir_loggingEnabled_set", (char *)"(JLorg/omnetpp/simkernel/cEnvir;Z)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cEnvir_1loggingEnabled_1set },
    { (char *)"cEnvir_loggingEnabled_get", (char *)"(JLorg/omnetpp/simkernel/cEnvir;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cEnvir_1loggingEnabled_1get },
    { (char *)"cEnvir_suppressNotifications_set", (char *)"(JLorg/omnetpp/simkernel/cEnvir;Z)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cEnvir_1suppressNotifications_1set },
    { (char *)"cEnvir_suppressNotifications_get", (char *)"(JLorg/omnetpp/simkernel/cEnvir;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cEnvir_1suppressNotifications_1get },
    { (char *)"cEnvir_debugOnErrors_set", (char *)"(JLorg/omnetpp/simkernel/cEnvir;Z)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cEnvir_1debugOnErrors_1set },
    { (char *)"cEnvir_debugOnErrors_get", (char *)"(JLorg/omnetpp/simkernel/cEnvir;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cEnvir_1debugOnErrors_1get },
    { (char *)"delete_cEnvir", (char *)"(J)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cEnvir },
    { (char *)"cEnvir_componentInitBegin", (char *)"(JLorg/omnetpp/simkernel/cEnvir;JLorg/omnetpp/simkernel/cComponent;I)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cEnvir_1componentInitBegin },
    { (char *)"cEnvir_messageCreated", (char *)"(JLorg/omnetpp/simkernel/cEnvir;JLorg/omnetpp/simkernel/cMessage;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cEnvir_1messageCreated },
    { (char *)"cEnvir_messageCloned", (char *)"(JLorg/omnetpp/simkernel/cEnvir;JLorg/omnetpp/simkernel/cMessage;JLorg/omnetpp/simkernel/cMessage;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cEnvir_1messageCloned },
    { (char *)"cEnvir_getCurrentEventName", (char *)"(JLorg/omnetpp/simkernel/cEnvir;)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cEnvir_1getCurrentEventName },
    { (char *)"cEnvir_getCurrentEventClassName", (char *)"(JLorg/omnetpp/simkernel/cEnvir;)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cEnvir_1getCurrentEventClassName },
    { (char *)"cEnvir_getCurrentEventModule", (char *)"(JLorg/omnetpp/simkernel/cEnvir;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cEnvir_1getCurrentEventModule },
    { (char *)"cEnvir_preconfigure", (char *)"(JLorg/omnetpp/simkernel/cEnvir;JLorg/omnetpp/simkernel/cComponent;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cEnvir_1preconfigure },
    { (char *)"cEnvir_configure", (char *)"(JLorg/omnetpp/simkernel/cEnvir;JLorg/omnetpp/simkernel/cComponent;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cEnvir_1configure },
    { (char *)"cEnvir_getXMLDocument__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cEnvir;Ljava/lang/String;Ljava/lang/String;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cEnvir_1getXMLDocument_1_1SWIG_10 },
    { (char *)"cEnvir_getXMLDocument__SWIG_1", (char *)"(JLorg/omnetpp/simkernel/cEnvir;Ljava/lang/String;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cEnvir_1getXMLDocument_1_1SWIG_11 },
    { (char *)"cEnvir_getParsedXMLString__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cEnvir;Ljava/lang/String;Ljava/lang/String;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cEnvir_1getParsedXMLString_1_1SWIG_10 },
    { (char *)"cEnvir_getParsedXMLString__SWIG_1", (char *)"(JLorg/omnetpp/simkernel/cEnvir;Ljava/lang/String;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cEnvir_1getParsedXMLString_1_1SWIG_11 },
    { (char *)"cEnvir_forgetXMLDocument", (char *)"(JLorg/omnetpp/simkernel/cEnvir;Ljava/lang/String;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cEnvir_1forgetXMLDocument },
    { (char *)"cEnvir_forgetParsedXMLString", (char *)"(JLorg/omnetpp/simkernel/cEnvir;Ljava/lang/String;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cEnvir_1forgetParsedXMLString },
    { (char *)"cEnvir_flushXMLDocumentCache", (char *)"(JLorg/omnetpp/simkernel/cEnvir;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cEnvir_1flushXMLDocumentCache },
    { (char *)"cEnvir_flushXMLParsedContentCache", (char *)"(JLorg/omnetpp/simkernel/cEnvir;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cEnvir_1flushXMLParsedContentCache },
    { (char *)"cEnvir_getExtraStackForEnvir", (char *)"(JLorg/omnetpp/simkernel/cEnvir;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cEnvir_1getExtraStackForEnvir },
    { (char *)"cEnvir_resolveResourcePath__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cEnvir;Ljava/lang/String;JLorg/omnetpp/simkernel/cComponentType;)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cEnvir_1resolveResourcePath_1_1SWIG_10 },
    { (char *)"cEnvir_resolveResourcePath__SWIG_1", (char *)"(JLorg/omnetpp/simkernel/cEnvir;Ljava/lang/String;)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cEnvir_1resolveResourcePath_1_1SWIG_11 },
    { (char *)"cEnvir_isGUI", (char *)"(JLorg/omnetpp/simkernel/cEnvir;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cEnvir_1isGUI },
    { (char *)"cEnvir_isLoggingEnabled", (char *)"(JLorg/omnetpp/simkernel/cEnvir;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cEnvir_1isLoggingEnabled },
    { (char *)"cEnvir_isExpressMode", (char *)"(JLorg/omnetpp/simkernel/cEnvir;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cEnvir_1isExpressMode },
    { (char *)"cEnvir_log", (char *)"(JLorg/omnetpp/simkernel/cEnvir;J)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cEnvir_1log },
    { (char *)"cEnvir_alert", (char *)"(JLorg/omnetpp/simkernel/cEnvir;Ljava/lang/String;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cEnvir_1alert },
    { (char *)"cEnvir_printfmsg", (char *)"(JLorg/omnetpp/simkernel/cEnvir;Ljava/lang/String;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cEnvir_1printfmsg },
    { (char *)"cEnvir_gets__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cEnvir;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cEnvir_1gets_1_1SWIG_10 },
    { (char *)"cEnvir_gets__SWIG_1", (char *)"(JLorg/omnetpp/simkernel/cEnvir;Ljava/lang/String;)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cEnvir_1gets_1_1SWIG_11 },
    { (char *)"cEnvir_askYesNo", (char *)"(JLorg/omnetpp/simkernel/cEnvir;Ljava/lang/String;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cEnvir_1askYesNo },
    { (char *)"cEnvir_getImageSize", (char *)"(JLorg/omnetpp/simkernel/cEnvir;Ljava/lang/String;JJ)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cEnvir_1getImageSize },
    { (char *)"cEnvir_getTextExtent", (char *)"(JLorg/omnetpp/simkernel/cEnvir;JLjava/lang/String;JJJ)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cEnvir_1getTextExtent },
    { (char *)"cEnvir_appendToImagePath", (char *)"(JLorg/omnetpp/simkernel/cEnvir;Ljava/lang/String;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cEnvir_1appendToImagePath },
    { (char *)"cEnvir_loadImage__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cEnvir;Ljava/lang/String;Ljava/lang/String;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cEnvir_1loadImage_1_1SWIG_10 },
    { (char *)"cEnvir_loadImage__SWIG_1", (char *)"(JLorg/omnetpp/simkernel/cEnvir;Ljava/lang/String;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cEnvir_1loadImage_1_1SWIG_11 },
    { (char *)"cEnvir_getSubmoduleBounds", (char *)"(JLorg/omnetpp/simkernel/cEnvir;JLorg/omnetpp/simkernel/cModule;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cEnvir_1getSubmoduleBounds },
    { (char *)"cEnvir_getZoomLevel", (char *)"(JLorg/omnetpp/simkernel/cEnvir;JLorg/omnetpp/simkernel/cModule;)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cEnvir_1getZoomLevel },
    { (char *)"cEnvir_getAnimationTime", (char *)"(JLorg/omnetpp/simkernel/cEnvir;)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cEnvir_1getAnimationTime },
    { (char *)"cEnvir_getAnimationSpeed", (char *)"(JLorg/omnetpp/simkernel/cEnvir;)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cEnvir_1getAnimationSpeed },
    { (char *)"cEnvir_getRemainingAnimationHoldTime", (char *)"(JLorg/omnetpp/simkernel/cEnvir;)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cEnvir_1getRemainingAnimationHoldTime },
    { (char *)"cEnvir_getNumRNGs", (char *)"(JLorg/omnetpp/simkernel/cEnvir;)I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cEnvir_1getNumRNGs },
    { (char *)"cEnvir_getRNG", (char *)"(JLorg/omnetpp/simkernel/cEnvir;I)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cEnvir_1getRNG },
    { (char *)"cEnvir_addResultRecorders", (char *)"(JLorg/omnetpp/simkernel/cEnvir;JLorg/omnetpp/simkernel/cComponent;ILjava/lang/String;JLorg/omnetpp/simkernel/cProperty;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cEnvir_1addResultRecorders },
    { (char *)"cEnvir_getParsimProcId", (char *)"(JLorg/omnetpp/simkernel/cEnvir;)I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cEnvir_1getParsimProcId },
    { (char *)"cEnvir_getParsimNumPartitions", (char *)"(JLorg/omnetpp/simkernel/cEnvir;)I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cEnvir_1getParsimNumPartitions },
    { (char *)"cEnvir_getUniqueNumber", (char *)"(JLorg/omnetpp/simkernel/cEnvir;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cEnvir_1getUniqueNumber },
    { (char *)"cEnvir_refOsgNode", (char *)"(JLorg/omnetpp/simkernel/cEnvir;J)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cEnvir_1refOsgNode },
    { (char *)"cEnvir_unrefOsgNode", (char *)"(JLorg/omnetpp/simkernel/cEnvir;J)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cEnvir_1unrefOsgNode },
    { (char *)"cEnvir_ensureDebugger__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cEnvir;JLorg/omnetpp/simkernel/cRuntimeError;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cEnvir_1ensureDebugger_1_1SWIG_10 },
    { (char *)"cEnvir_ensureDebugger__SWIG_1", (char *)"(JLorg/omnetpp/simkernel/cEnvir;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cEnvir_1ensureDebugger_1_1SWIG_11 },
    { (char *)"cEnvir_addLifecycleListener", (char *)"(JLorg/omnetpp/simkernel/cEnvir;J)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cEnvir_1addLifecycleListener },
    { (char *)"cEnvir_removeLifecycleListener", (char *)"(JLorg/omnetpp/simkernel/cEnvir;J)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cEnvir_1removeLifecycleListener },
    { (char *)"cEnvir_notifyLifecycleListeners__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cEnvir;JJLorg/omnetpp/simkernel/cObject;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cEnvir_1notifyLifecycleListeners_1_1SWIG_10 },
    { (char *)"cEnvir_notifyLifecycleListeners__SWIG_1", (char *)"(JLorg/omnetpp/simkernel/cEnvir;J)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cEnvir_1notifyLifecycleListeners_1_1SWIG_11 },
    { (char *)"cEnvir_puts", (char *)"(JLorg/omnetpp/simkernel/cEnvir;Ljava/lang/String;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cEnvir_1puts },
    { (char *)"new_cException__SWIG_1", (char *)"(I)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cException_1_1SWIG_11 },
    { (char *)"new_cException__SWIG_2", (char *)"(Ljava/lang/String;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cException_1_1SWIG_12 },
    { (char *)"new_cException__SWIG_3", (char *)"(JLorg/omnetpp/simkernel/cObject;I)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cException_1_1SWIG_13 },
    { (char *)"new_cException__SWIG_4", (char *)"(JLorg/omnetpp/simkernel/cObject;Ljava/lang/String;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cException_1_1SWIG_14 },
    { (char *)"new_cException__SWIG_5", (char *)"(JLorg/omnetpp/simkernel/cException;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cException_1_1SWIG_15 },
    { (char *)"cException_dup", (char *)"(JLorg/omnetpp/simkernel/cException;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cException_1dup },
    { (char *)"delete_cException", (char *)"(J)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cException },
    { (char *)"cException_setMessage", (char *)"(JLorg/omnetpp/simkernel/cException;Ljava/lang/String;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cException_1setMessage },
    { (char *)"cException_prependMessage", (char *)"(JLorg/omnetpp/simkernel/cException;Ljava/lang/String;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cException_1prependMessage },
    { (char *)"cException_isError", (char *)"(JLorg/omnetpp/simkernel/cException;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cException_1isError },
    { (char *)"cException_getErrorCode", (char *)"(JLorg/omnetpp/simkernel/cException;)I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cException_1getErrorCode },
    { (char *)"cException_what", (char *)"(JLorg/omnetpp/simkernel/cException;)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cException_1what },
    { (char *)"cException_getFormattedMessage", (char *)"(JLorg/omnetpp/simkernel/cException;)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cException_1getFormattedMessage },
    { (char *)"cException_getSimulationStage", (char *)"(JLorg/omnetpp/simkernel/cException;)I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cException_1getSimulationStage },
    { (char *)"cException_getEventNumber", (char *)"(JLorg/omnetpp/simkernel/cException;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cException_1getEventNumber },
    { (char *)"cException_getSimtime", (char *)"(JLorg/omnetpp/simkernel/cException;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cException_1getSimtime },
    { (char *)"cException_hasContext", (char *)"(JLorg/omnetpp/simkernel/cException;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cException_1hasContext },
    { (char *)"cException_getContextClassName", (char *)"(JLorg/omnetpp/simkernel/cException;)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cException_1getContextClassName },
    { (char *)"cException_getContextFullPath", (char *)"(JLorg/omnetpp/simkernel/cException;)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cException_1getContextFullPath },
    { (char *)"cException_getContextComponentId", (char *)"(JLorg/omnetpp/simkernel/cException;)I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cException_1getContextComponentId },
    { (char *)"cException_getContextComponentKind", (char *)"(JLorg/omnetpp/simkernel/cException;)I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cException_1getContextComponentKind },
    { (char *)"cRuntimeError_displayed_set", (char *)"(JLorg/omnetpp/simkernel/cRuntimeError;Z)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cRuntimeError_1displayed_1set },
    { (char *)"cRuntimeError_displayed_get", (char *)"(JLorg/omnetpp/simkernel/cRuntimeError;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cRuntimeError_1displayed_1get },
    { (char *)"new_cRuntimeError__SWIG_0", (char *)"(I)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cRuntimeError_1_1SWIG_10 },
    { (char *)"new_cRuntimeError__SWIG_1", (char *)"(Ljava/lang/String;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cRuntimeError_1_1SWIG_11 },
    { (char *)"new_cRuntimeError__SWIG_2", (char *)"(JLorg/omnetpp/simkernel/cObject;I)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cRuntimeError_1_1SWIG_12 },
    { (char *)"new_cRuntimeError__SWIG_3", (char *)"(JLorg/omnetpp/simkernel/cObject;Ljava/lang/String;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cRuntimeError_1_1SWIG_13 },
    { (char *)"new_cRuntimeError__SWIG_4", (char *)"(JLorg/omnetpp/simkernel/cRuntimeError;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cRuntimeError_1_1SWIG_14 },
    { (char *)"cRuntimeError_dup", (char *)"(JLorg/omnetpp/simkernel/cRuntimeError;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cRuntimeError_1dup },
    { (char *)"cRuntimeError_cast", (char *)"(JLorg/omnetpp/simkernel/cException;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cRuntimeError_1cast },
    { (char *)"delete_cRuntimeError", (char *)"(J)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cRuntimeError },
    { (char *)"new_cDeleteModuleException__SWIG_0", (char *)"()J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cDeleteModuleException_1_1SWIG_10 },
    { (char *)"new_cDeleteModuleException__SWIG_1", (char *)"(JLorg/omnetpp/simkernel/cDeleteModuleException;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cDeleteModuleException_1_1SWIG_11 },
    { (char *)"cDeleteModuleException_dup", (char *)"(JLorg/omnetpp/simkernel/cDeleteModuleException;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cDeleteModuleException_1dup },
    { (char *)"cDeleteModuleException_isError", (char *)"(JLorg/omnetpp/simkernel/cDeleteModuleException;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cDeleteModuleException_1isError },
    { (char *)"delete_cDeleteModuleException", (char *)"(J)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cDeleteModuleException },
    { (char *)"delete_cExpression", (char *)"(J)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cExpression },
    { (char *)"cExpression_assign", (char *)"(JLorg/omnetpp/simkernel/cExpression;JLorg/omnetpp/simkernel/cExpression;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cExpression_1assign },
    { (char *)"cExpression_dup", (char *)"(JLorg/omnetpp/simkernel/cExpression;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cExpression_1dup },
    { (char *)"cExpression_str", (char *)"(JLorg/omnetpp/simkernel/cExpression;)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cExpression_1str },
    { (char *)"cExpression_evaluate__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cExpression;J)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cExpression_1evaluate_1_1SWIG_10 },
    { (char *)"cExpression_boolValue__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cExpression;J)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cExpression_1boolValue_1_1SWIG_10 },
    { (char *)"cExpression_intValue__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cExpression;JLjava/lang/String;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cExpression_1intValue_1_1SWIG_10 },
    { (char *)"cExpression_intValue__SWIG_1", (char *)"(JLorg/omnetpp/simkernel/cExpression;J)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cExpression_1intValue_1_1SWIG_11 },
    { (char *)"cExpression_doubleValue__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cExpression;JLjava/lang/String;)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cExpression_1doubleValue_1_1SWIG_10 },
    { (char *)"cExpression_doubleValue__SWIG_1", (char *)"(JLorg/omnetpp/simkernel/cExpression;J)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cExpression_1doubleValue_1_1SWIG_11 },
    { (char *)"cExpression_stringValue__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cExpression;J)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cExpression_1stringValue_1_1SWIG_10 },
    { (char *)"cExpression_xmlValue__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cExpression;J)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cExpression_1xmlValue_1_1SWIG_10 },
    { (char *)"cExpression_evaluate__SWIG_1", (char *)"(JLorg/omnetpp/simkernel/cExpression;JLorg/omnetpp/simkernel/cComponent;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cExpression_1evaluate_1_1SWIG_11 },
    { (char *)"cExpression_evaluate__SWIG_2", (char *)"(JLorg/omnetpp/simkernel/cExpression;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cExpression_1evaluate_1_1SWIG_12 },
    { (char *)"cExpression_boolValue__SWIG_1", (char *)"(JLorg/omnetpp/simkernel/cExpression;JLorg/omnetpp/simkernel/cComponent;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cExpression_1boolValue_1_1SWIG_11 },
    { (char *)"cExpression_boolValue__SWIG_2", (char *)"(JLorg/omnetpp/simkernel/cExpression;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cExpression_1boolValue_1_1SWIG_12 },
    { (char *)"cExpression_intValue__SWIG_2", (char *)"(JLorg/omnetpp/simkernel/cExpression;JLorg/omnetpp/simkernel/cComponent;Ljava/lang/String;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cExpression_1intValue_1_1SWIG_12 },
    { (char *)"cExpression_intValue__SWIG_3", (char *)"(JLorg/omnetpp/simkernel/cExpression;JLorg/omnetpp/simkernel/cComponent;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cExpression_1intValue_1_1SWIG_13 },
    { (char *)"cExpression_intValue__SWIG_4", (char *)"(JLorg/omnetpp/simkernel/cExpression;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cExpression_1intValue_1_1SWIG_14 },
    { (char *)"cExpression_doubleValue__SWIG_2", (char *)"(JLorg/omnetpp/simkernel/cExpression;JLorg/omnetpp/simkernel/cComponent;Ljava/lang/String;)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cExpression_1doubleValue_1_1SWIG_12 },
    { (char *)"cExpression_doubleValue__SWIG_3", (char *)"(JLorg/omnetpp/simkernel/cExpression;JLorg/omnetpp/simkernel/cComponent;)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cExpression_1doubleValue_1_1SWIG_13 },
    { (char *)"cExpression_doubleValue__SWIG_4", (char *)"(JLorg/omnetpp/simkernel/cExpression;)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cExpression_1doubleValue_1_1SWIG_14 },
    { (char *)"cExpression_stringValue__SWIG_1", (char *)"(JLorg/omnetpp/simkernel/cExpression;JLorg/omnetpp/simkernel/cComponent;)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cExpression_1stringValue_1_1SWIG_11 },
    { (char *)"cExpression_stringValue__SWIG_2", (char *)"(JLorg/omnetpp/simkernel/cExpression;)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cExpression_1stringValue_1_1SWIG_12 },
    { (char *)"cExpression_xmlValue__SWIG_1", (char *)"(JLorg/omnetpp/simkernel/cExpression;JLorg/omnetpp/simkernel/cComponent;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cExpression_1xmlValue_1_1SWIG_11 },
    { (char *)"cExpression_xmlValue__SWIG_2", (char *)"(JLorg/omnetpp/simkernel/cExpression;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cExpression_1xmlValue_1_1SWIG_12 },
    { (char *)"cExpression_parse", (char *)"(JLorg/omnetpp/simkernel/cExpression;Ljava/lang/String;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cExpression_1parse },
    { (char *)"cExpression_compare", (char *)"(JLorg/omnetpp/simkernel/cExpression;JLorg/omnetpp/simkernel/cExpression;)I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cExpression_1compare },
    { (char *)"cExpression_containsConstSubexpressions", (char *)"(JLorg/omnetpp/simkernel/cExpression;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cExpression_1containsConstSubexpressions },
    { (char *)"cExpression_evaluateConstSubexpressions", (char *)"(JLorg/omnetpp/simkernel/cExpression;J)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cExpression_1evaluateConstSubexpressions },
    { (char *)"FSM_MAXT_get", (char *)"()I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_FSM_1MAXT_1get },
    { (char *)"new_cFSM__SWIG_0", (char *)"(Ljava/lang/String;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cFSM_1_1SWIG_10 },
    { (char *)"new_cFSM__SWIG_1", (char *)"()J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cFSM_1_1SWIG_11 },
    { (char *)"new_cFSM__SWIG_2", (char *)"(JLorg/omnetpp/simkernel/cFSM;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cFSM_1_1SWIG_12 },
    { (char *)"cFSM_assign", (char *)"(JLorg/omnetpp/simkernel/cFSM;JLorg/omnetpp/simkernel/cFSM;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cFSM_1assign },
    { (char *)"cFSM_dup", (char *)"(JLorg/omnetpp/simkernel/cFSM;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cFSM_1dup },
    { (char *)"cFSM_str", (char *)"(JLorg/omnetpp/simkernel/cFSM;)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cFSM_1str },
    { (char *)"cFSM_getState", (char *)"(JLorg/omnetpp/simkernel/cFSM;)I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cFSM_1getState },
    { (char *)"cFSM_getStateName", (char *)"(JLorg/omnetpp/simkernel/cFSM;)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cFSM_1getStateName },
    { (char *)"cFSM_isInTransientState", (char *)"(JLorg/omnetpp/simkernel/cFSM;)I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cFSM_1isInTransientState },
    { (char *)"cFSM_setState__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cFSM;ILjava/lang/String;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cFSM_1setState_1_1SWIG_10 },
    { (char *)"cFSM_setState__SWIG_1", (char *)"(JLorg/omnetpp/simkernel/cFSM;I)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cFSM_1setState_1_1SWIG_11 },
    { (char *)"delete_cFSM", (char *)"(J)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cFSM },
    { (char *)"GATEID_LBITS_get", (char *)"()I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_GATEID_1LBITS_1get },
    { (char *)"GATEID_HMASK_get", (char *)"()J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_GATEID_1HMASK_1get },
    { (char *)"GATEID_LMASK_get", (char *)"()J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_GATEID_1LMASK_1get },
    { (char *)"cGate_NONE_get", (char *)"()I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cGate_1NONE_1get },
    { (char *)"cGate_INPUT_get", (char *)"()I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cGate_1INPUT_1get },
    { (char *)"cGate_OUTPUT_get", (char *)"()I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cGate_1OUTPUT_1get },
    { (char *)"cGate_INOUT_get", (char *)"()I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cGate_1INOUT_1get },
    { (char *)"cGate_getName", (char *)"(JLorg/omnetpp/simkernel/cGate;)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cGate_1getName },
    { (char *)"cGate_getFullName", (char *)"(JLorg/omnetpp/simkernel/cGate;)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cGate_1getFullName },
    { (char *)"cGate_forEachChild", (char *)"(JLorg/omnetpp/simkernel/cGate;JLorg/omnetpp/simkernel/cVisitor;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cGate_1forEachChild },
    { (char *)"cGate_str", (char *)"(JLorg/omnetpp/simkernel/cGate;)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cGate_1str },
    { (char *)"cGate_getOwner", (char *)"(JLorg/omnetpp/simkernel/cGate;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cGate_1getOwner },
    { (char *)"cGate_deliver", (char *)"(JLorg/omnetpp/simkernel/cGate;JLorg/omnetpp/simkernel/cMessage;JLorg/omnetpp/simkernel/SimTime;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cGate_1deliver },
    { (char *)"cGate_connectTo__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cGate;JLorg/omnetpp/simkernel/cGate;JLorg/omnetpp/simkernel/cChannel;Z)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cGate_1connectTo_1_1SWIG_10 },
    { (char *)"cGate_connectTo__SWIG_1", (char *)"(JLorg/omnetpp/simkernel/cGate;JLorg/omnetpp/simkernel/cGate;JLorg/omnetpp/simkernel/cChannel;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cGate_1connectTo_1_1SWIG_11 },
    { (char *)"cGate_connectTo__SWIG_2", (char *)"(JLorg/omnetpp/simkernel/cGate;JLorg/omnetpp/simkernel/cGate;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cGate_1connectTo_1_1SWIG_12 },
    { (char *)"cGate_disconnect", (char *)"(JLorg/omnetpp/simkernel/cGate;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cGate_1disconnect },
    { (char *)"cGate_reconnectWith__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cGate;JLorg/omnetpp/simkernel/cChannel;Z)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cGate_1reconnectWith_1_1SWIG_10 },
    { (char *)"cGate_reconnectWith__SWIG_1", (char *)"(JLorg/omnetpp/simkernel/cGate;JLorg/omnetpp/simkernel/cChannel;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cGate_1reconnectWith_1_1SWIG_11 },
    { (char *)"cGate_getBaseName", (char *)"(JLorg/omnetpp/simkernel/cGate;)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cGate_1getBaseName },
    { (char *)"cGate_getNameSuffix", (char *)"(JLorg/omnetpp/simkernel/cGate;)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cGate_1getNameSuffix },
    { (char *)"cGate_getProperties", (char *)"(JLorg/omnetpp/simkernel/cGate;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cGate_1getProperties },
    { (char *)"cGate_getType", (char *)"(JLorg/omnetpp/simkernel/cGate;)I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cGate_1getType },
    { (char *)"cGate_getTypeName", (char *)"(I)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cGate_1getTypeName },
    { (char *)"cGate_getOwnerModule", (char *)"(JLorg/omnetpp/simkernel/cGate;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cGate_1getOwnerModule },
    { (char *)"cGate_getId", (char *)"(JLorg/omnetpp/simkernel/cGate;)I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cGate_1getId },
    { (char *)"cGate_isVector", (char *)"(JLorg/omnetpp/simkernel/cGate;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cGate_1isVector },
    { (char *)"cGate_getBaseId", (char *)"(JLorg/omnetpp/simkernel/cGate;)I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cGate_1getBaseId },
    { (char *)"cGate_getIndex", (char *)"(JLorg/omnetpp/simkernel/cGate;)I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cGate_1getIndex },
    { (char *)"cGate_getVectorSize", (char *)"(JLorg/omnetpp/simkernel/cGate;)I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cGate_1getVectorSize },
    { (char *)"cGate_size", (char *)"(JLorg/omnetpp/simkernel/cGate;)I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cGate_1size },
    { (char *)"cGate_getChannel", (char *)"(JLorg/omnetpp/simkernel/cGate;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cGate_1getChannel },
    { (char *)"cGate_setDeliverOnReceptionStart", (char *)"(JLorg/omnetpp/simkernel/cGate;Z)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cGate_1setDeliverOnReceptionStart },
    { (char *)"cGate_getDeliverOnReceptionStart", (char *)"(JLorg/omnetpp/simkernel/cGate;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cGate_1getDeliverOnReceptionStart },
    { (char *)"cGate_getTransmissionChannel", (char *)"(JLorg/omnetpp/simkernel/cGate;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cGate_1getTransmissionChannel },
    { (char *)"cGate_findTransmissionChannel", (char *)"(JLorg/omnetpp/simkernel/cGate;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cGate_1findTransmissionChannel },
    { (char *)"cGate_getIncomingTransmissionChannel", (char *)"(JLorg/omnetpp/simkernel/cGate;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cGate_1getIncomingTransmissionChannel },
    { (char *)"cGate_findIncomingTransmissionChannel", (char *)"(JLorg/omnetpp/simkernel/cGate;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cGate_1findIncomingTransmissionChannel },
    { (char *)"cGate_getPreviousGate", (char *)"(JLorg/omnetpp/simkernel/cGate;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cGate_1getPreviousGate },
    { (char *)"cGate_getNextGate", (char *)"(JLorg/omnetpp/simkernel/cGate;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cGate_1getNextGate },
    { (char *)"cGate_getConnectionId", (char *)"(JLorg/omnetpp/simkernel/cGate;)I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cGate_1getConnectionId },
    { (char *)"cGate_getPathStartGate", (char *)"(JLorg/omnetpp/simkernel/cGate;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cGate_1getPathStartGate },
    { (char *)"cGate_getPathEndGate", (char *)"(JLorg/omnetpp/simkernel/cGate;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cGate_1getPathEndGate },
    { (char *)"cGate_pathContains__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cGate;JLorg/omnetpp/simkernel/cModule;I)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cGate_1pathContains_1_1SWIG_10 },
    { (char *)"cGate_pathContains__SWIG_1", (char *)"(JLorg/omnetpp/simkernel/cGate;JLorg/omnetpp/simkernel/cModule;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cGate_1pathContains_1_1SWIG_11 },
    { (char *)"cGate_isConnectedOutside", (char *)"(JLorg/omnetpp/simkernel/cGate;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cGate_1isConnectedOutside },
    { (char *)"cGate_isConnectedInside", (char *)"(JLorg/omnetpp/simkernel/cGate;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cGate_1isConnectedInside },
    { (char *)"cGate_isConnected", (char *)"(JLorg/omnetpp/simkernel/cGate;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cGate_1isConnected },
    { (char *)"cGate_isPathOK", (char *)"(JLorg/omnetpp/simkernel/cGate;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cGate_1isPathOK },
    { (char *)"cGate_getDisplayString", (char *)"(JLorg/omnetpp/simkernel/cGate;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cGate_1getDisplayString },
    { (char *)"cGate_setDisplayString", (char *)"(JLorg/omnetpp/simkernel/cGate;Ljava/lang/String;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cGate_1setDisplayString },
    { (char *)"cGate_cast", (char *)"(JLorg/omnetpp/simkernel/cObject;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cGate_1cast },
    { (char *)"MAX_PARSIM_PARTITIONS_get", (char *)"()I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_MAX_1PARSIM_1PARTITIONS_1get },
    { (char *)"cMessage_privateDup", (char *)"(JLorg/omnetpp/simkernel/cMessage;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cMessage_1privateDup },
    { (char *)"cMessage_setSentFrom", (char *)"(JLorg/omnetpp/simkernel/cMessage;JLorg/omnetpp/simkernel/cModule;IJLorg/omnetpp/simkernel/SimTime;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cMessage_1setSentFrom },
    { (char *)"cMessage_setSrcProcId", (char *)"(JLorg/omnetpp/simkernel/cMessage;I)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cMessage_1setSrcProcId },
    { (char *)"cMessage_getSrcProcId", (char *)"(JLorg/omnetpp/simkernel/cMessage;)I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cMessage_1getSrcProcId },
    { (char *)"cMessage_getParListPtr", (char *)"(JLorg/omnetpp/simkernel/cMessage;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cMessage_1getParListPtr },
    { (char *)"new_cMessage__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cMessage;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cMessage_1_1SWIG_10 },
    { (char *)"new_cMessage__SWIG_1", (char *)"(Ljava/lang/String;S)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cMessage_1_1SWIG_11 },
    { (char *)"new_cMessage__SWIG_2", (char *)"(Ljava/lang/String;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cMessage_1_1SWIG_12 },
    { (char *)"new_cMessage__SWIG_3", (char *)"()J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cMessage_1_1SWIG_13 },
    { (char *)"delete_cMessage", (char *)"(J)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cMessage },
    { (char *)"cMessage_assign", (char *)"(JLorg/omnetpp/simkernel/cMessage;JLorg/omnetpp/simkernel/cMessage;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cMessage_1assign },
    { (char *)"cMessage_isPacket", (char *)"(JLorg/omnetpp/simkernel/cMessage;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cMessage_1isPacket },
    { (char *)"cMessage_dup", (char *)"(JLorg/omnetpp/simkernel/cMessage;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cMessage_1dup },
    { (char *)"cMessage_str", (char *)"(JLorg/omnetpp/simkernel/cMessage;)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cMessage_1str },
    { (char *)"cMessage_forEachChild", (char *)"(JLorg/omnetpp/simkernel/cMessage;JLorg/omnetpp/simkernel/cVisitor;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cMessage_1forEachChild },
    { (char *)"cMessage_setKind", (char *)"(JLorg/omnetpp/simkernel/cMessage;S)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cMessage_1setKind },
    { (char *)"cMessage_setTimestamp__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cMessage;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cMessage_1setTimestamp_1_1SWIG_10 },
    { (char *)"cMessage_setTimestamp__SWIG_1", (char *)"(JLorg/omnetpp/simkernel/cMessage;JLorg/omnetpp/simkernel/SimTime;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cMessage_1setTimestamp_1_1SWIG_11 },
    { (char *)"cMessage_setControlInfo", (char *)"(JLorg/omnetpp/simkernel/cMessage;JLorg/omnetpp/simkernel/cObject;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cMessage_1setControlInfo },
    { (char *)"cMessage_removeControlInfo", (char *)"(JLorg/omnetpp/simkernel/cMessage;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cMessage_1removeControlInfo },
    { (char *)"cMessage_getKind", (char *)"(JLorg/omnetpp/simkernel/cMessage;)S", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cMessage_1getKind },
    { (char *)"cMessage_getTimestamp", (char *)"(JLorg/omnetpp/simkernel/cMessage;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cMessage_1getTimestamp },
    { (char *)"cMessage_getControlInfo", (char *)"(JLorg/omnetpp/simkernel/cMessage;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cMessage_1getControlInfo },
    { (char *)"cMessage_getParList", (char *)"(JLorg/omnetpp/simkernel/cMessage;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cMessage_1getParList },
    { (char *)"cMessage_addPar__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cMessage;Ljava/lang/String;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cMessage_1addPar_1_1SWIG_10 },
    { (char *)"cMessage_addPar__SWIG_1", (char *)"(JLorg/omnetpp/simkernel/cMessage;JLorg/omnetpp/simkernel/cMsgPar;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cMessage_1addPar_1_1SWIG_11 },
    { (char *)"cMessage_par__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cMessage;I)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cMessage_1par_1_1SWIG_10 },
    { (char *)"cMessage_par__SWIG_1", (char *)"(JLorg/omnetpp/simkernel/cMessage;Ljava/lang/String;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cMessage_1par_1_1SWIG_11 },
    { (char *)"cMessage_findPar", (char *)"(JLorg/omnetpp/simkernel/cMessage;Ljava/lang/String;)I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cMessage_1findPar },
    { (char *)"cMessage_hasPar", (char *)"(JLorg/omnetpp/simkernel/cMessage;Ljava/lang/String;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cMessage_1hasPar },
    { (char *)"cMessage_addObject", (char *)"(JLorg/omnetpp/simkernel/cMessage;JLorg/omnetpp/simkernel/cObject;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cMessage_1addObject },
    { (char *)"cMessage_getObject", (char *)"(JLorg/omnetpp/simkernel/cMessage;Ljava/lang/String;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cMessage_1getObject },
    { (char *)"cMessage_hasObject", (char *)"(JLorg/omnetpp/simkernel/cMessage;Ljava/lang/String;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cMessage_1hasObject },
    { (char *)"cMessage_removeObject__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cMessage;Ljava/lang/String;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cMessage_1removeObject_1_1SWIG_10 },
    { (char *)"cMessage_removeObject__SWIG_1", (char *)"(JLorg/omnetpp/simkernel/cMessage;JLorg/omnetpp/simkernel/cObject;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cMessage_1removeObject_1_1SWIG_11 },
    { (char *)"cMessage_isSelfMessage", (char *)"(JLorg/omnetpp/simkernel/cMessage;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cMessage_1isSelfMessage },
    { (char *)"cMessage_getSenderModule", (char *)"(JLorg/omnetpp/simkernel/cMessage;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cMessage_1getSenderModule },
    { (char *)"cMessage_getSenderGate", (char *)"(JLorg/omnetpp/simkernel/cMessage;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cMessage_1getSenderGate },
    { (char *)"cMessage_getArrivalModule", (char *)"(JLorg/omnetpp/simkernel/cMessage;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cMessage_1getArrivalModule },
    { (char *)"cMessage_getArrivalGate", (char *)"(JLorg/omnetpp/simkernel/cMessage;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cMessage_1getArrivalGate },
    { (char *)"cMessage_getSenderModuleId", (char *)"(JLorg/omnetpp/simkernel/cMessage;)I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cMessage_1getSenderModuleId },
    { (char *)"cMessage_getSenderGateId", (char *)"(JLorg/omnetpp/simkernel/cMessage;)I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cMessage_1getSenderGateId },
    { (char *)"cMessage_getArrivalModuleId", (char *)"(JLorg/omnetpp/simkernel/cMessage;)I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cMessage_1getArrivalModuleId },
    { (char *)"cMessage_getArrivalGateId", (char *)"(JLorg/omnetpp/simkernel/cMessage;)I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cMessage_1getArrivalGateId },
    { (char *)"cMessage_getCreationTime", (char *)"(JLorg/omnetpp/simkernel/cMessage;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cMessage_1getCreationTime },
    { (char *)"cMessage_getSendingTime", (char *)"(JLorg/omnetpp/simkernel/cMessage;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cMessage_1getSendingTime },
    { (char *)"cMessage_getArrivalTime", (char *)"(JLorg/omnetpp/simkernel/cMessage;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cMessage_1getArrivalTime },
    { (char *)"cMessage_arrivedOn__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cMessage;I)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cMessage_1arrivedOn_1_1SWIG_10 },
    { (char *)"cMessage_arrivedOn__SWIG_1", (char *)"(JLorg/omnetpp/simkernel/cMessage;Ljava/lang/String;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cMessage_1arrivedOn_1_1SWIG_11 },
    { (char *)"cMessage_arrivedOn__SWIG_2", (char *)"(JLorg/omnetpp/simkernel/cMessage;Ljava/lang/String;I)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cMessage_1arrivedOn_1_1SWIG_12 },
    { (char *)"cMessage_getId", (char *)"(JLorg/omnetpp/simkernel/cMessage;)I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cMessage_1getId },
    { (char *)"cMessage_getTreeId", (char *)"(JLorg/omnetpp/simkernel/cMessage;)I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cMessage_1getTreeId },
    { (char *)"cMessage_getDisplayString", (char *)"(JLorg/omnetpp/simkernel/cMessage;)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cMessage_1getDisplayString },
    { (char *)"cMessage_setArrival__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cMessage;II)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cMessage_1setArrival_1_1SWIG_10 },
    { (char *)"cMessage_setArrival__SWIG_1", (char *)"(JLorg/omnetpp/simkernel/cMessage;IIJLorg/omnetpp/simkernel/SimTime;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cMessage_1setArrival_1_1SWIG_11 },
    { (char *)"cMessage_getTotalMessageCount", (char *)"()I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cMessage_1getTotalMessageCount },
    { (char *)"cMessage_getLiveMessageCount", (char *)"()I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cMessage_1getLiveMessageCount },
    { (char *)"cMessage_resetMessageCounters", (char *)"()V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cMessage_1resetMessageCounters },
    { (char *)"cMessage_cast", (char *)"(JLorg/omnetpp/simkernel/cObject;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cMessage_1cast },
    { (char *)"new_cMsgPar__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cMsgPar;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cMsgPar_1_1SWIG_10 },
    { (char *)"new_cMsgPar__SWIG_1", (char *)"(Ljava/lang/String;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cMsgPar_1_1SWIG_11 },
    { (char *)"new_cMsgPar__SWIG_2", (char *)"()J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cMsgPar_1_1SWIG_12 },
    { (char *)"new_cMsgPar__SWIG_3", (char *)"(Ljava/lang/String;JLorg/omnetpp/simkernel/cMsgPar;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cMsgPar_1_1SWIG_13 },
    { (char *)"delete_cMsgPar", (char *)"(J)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cMsgPar },
    { (char *)"cMsgPar_assign__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cMsgPar;JLorg/omnetpp/simkernel/cMsgPar;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cMsgPar_1assign_1_1SWIG_10 },
    { (char *)"cMsgPar_dup", (char *)"(JLorg/omnetpp/simkernel/cMsgPar;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cMsgPar_1dup },
    { (char *)"cMsgPar_str", (char *)"(JLorg/omnetpp/simkernel/cMsgPar;)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cMsgPar_1str },
    { (char *)"cMsgPar_forEachChild", (char *)"(JLorg/omnetpp/simkernel/cMsgPar;JLorg/omnetpp/simkernel/cVisitor;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cMsgPar_1forEachChild },
    { (char *)"cMsgPar_setBoolValue", (char *)"(JLorg/omnetpp/simkernel/cMsgPar;Z)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cMsgPar_1setBoolValue },
    { (char *)"cMsgPar_setLongValue", (char *)"(JLorg/omnetpp/simkernel/cMsgPar;I)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cMsgPar_1setLongValue },
    { (char *)"cMsgPar_setStringValue", (char *)"(JLorg/omnetpp/simkernel/cMsgPar;Ljava/lang/String;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cMsgPar_1setStringValue },
    { (char *)"cMsgPar_setDoubleValue", (char *)"(JLorg/omnetpp/simkernel/cMsgPar;D)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cMsgPar_1setDoubleValue },
    { (char *)"cMsgPar_setObjectValue", (char *)"(JLorg/omnetpp/simkernel/cMsgPar;JLorg/omnetpp/simkernel/cOwnedObject;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cMsgPar_1setObjectValue },
    { (char *)"cMsgPar_setXMLValue", (char *)"(JLorg/omnetpp/simkernel/cMsgPar;JLorg/omnetpp/simkernel/cXMLElement;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cMsgPar_1setXMLValue },
    { (char *)"cMsgPar_setTakeOwnership", (char *)"(JLorg/omnetpp/simkernel/cMsgPar;Z)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cMsgPar_1setTakeOwnership },
    { (char *)"cMsgPar_getTakeOwnership", (char *)"(JLorg/omnetpp/simkernel/cMsgPar;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cMsgPar_1getTakeOwnership },
    { (char *)"cMsgPar_boolValue", (char *)"(JLorg/omnetpp/simkernel/cMsgPar;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cMsgPar_1boolValue },
    { (char *)"cMsgPar_longValue", (char *)"(JLorg/omnetpp/simkernel/cMsgPar;)I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cMsgPar_1longValue },
    { (char *)"cMsgPar_stringValue", (char *)"(JLorg/omnetpp/simkernel/cMsgPar;)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cMsgPar_1stringValue },
    { (char *)"cMsgPar_doubleValue", (char *)"(JLorg/omnetpp/simkernel/cMsgPar;)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cMsgPar_1doubleValue },
    { (char *)"cMsgPar_pointerValue", (char *)"(JLorg/omnetpp/simkernel/cMsgPar;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cMsgPar_1pointerValue },
    { (char *)"cMsgPar_getObjectValue", (char *)"(JLorg/omnetpp/simkernel/cMsgPar;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cMsgPar_1getObjectValue },
    { (char *)"cMsgPar_xmlValue", (char *)"(JLorg/omnetpp/simkernel/cMsgPar;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cMsgPar_1xmlValue },
    { (char *)"cMsgPar_getType", (char *)"(JLorg/omnetpp/simkernel/cMsgPar;)C", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cMsgPar_1getType },
    { (char *)"cMsgPar_isNumeric", (char *)"(JLorg/omnetpp/simkernel/cMsgPar;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cMsgPar_1isNumeric },
    { (char *)"cMsgPar_isConstant", (char *)"(JLorg/omnetpp/simkernel/cMsgPar;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cMsgPar_1isConstant },
    { (char *)"cMsgPar_hasChanged", (char *)"(JLorg/omnetpp/simkernel/cMsgPar;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cMsgPar_1hasChanged },
    { (char *)"cMsgPar_convertToConst", (char *)"(JLorg/omnetpp/simkernel/cMsgPar;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cMsgPar_1convertToConst },
    { (char *)"cMsgPar_equalsTo", (char *)"(JLorg/omnetpp/simkernel/cMsgPar;JLorg/omnetpp/simkernel/cMsgPar;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cMsgPar_1equalsTo },
    { (char *)"cMsgPar_parse__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cMsgPar;Ljava/lang/String;C)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cMsgPar_1parse_1_1SWIG_10 },
    { (char *)"cMsgPar_parse__SWIG_1", (char *)"(JLorg/omnetpp/simkernel/cMsgPar;Ljava/lang/String;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cMsgPar_1parse_1_1SWIG_11 },
    { (char *)"cMsgPar_assign__SWIG_1", (char *)"(JLorg/omnetpp/simkernel/cMsgPar;Z)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cMsgPar_1assign_1_1SWIG_11 },
    { (char *)"cMsgPar_assign__SWIG_2", (char *)"(JLorg/omnetpp/simkernel/cMsgPar;Ljava/lang/String;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cMsgPar_1assign_1_1SWIG_12 },
    { (char *)"cMsgPar_assign__SWIG_3", (char *)"(JLorg/omnetpp/simkernel/cMsgPar;C)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cMsgPar_1assign_1_1SWIG_13 },
    { (char *)"cMsgPar_assign__SWIG_4", (char *)"(JLorg/omnetpp/simkernel/cMsgPar;S)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cMsgPar_1assign_1_1SWIG_14 },
    { (char *)"cMsgPar_assign__SWIG_5", (char *)"(JLorg/omnetpp/simkernel/cMsgPar;I)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cMsgPar_1assign_1_1SWIG_15 },
    { (char *)"cMsgPar_assign__SWIG_6", (char *)"(JLorg/omnetpp/simkernel/cMsgPar;J)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cMsgPar_1assign_1_1SWIG_16 },
    { (char *)"cMsgPar_assign__SWIG_11", (char *)"(JLorg/omnetpp/simkernel/cMsgPar;D)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cMsgPar_1assign_1_1SWIG_111 },
    { (char *)"cMsgPar_assign__SWIG_12", (char *)"(JLorg/omnetpp/simkernel/cMsgPar;JLorg/omnetpp/simkernel/cOwnedObject;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cMsgPar_1assign_1_1SWIG_112 },
    { (char *)"cMsgPar_assign__SWIG_13", (char *)"(JLorg/omnetpp/simkernel/cMsgPar;JLorg/omnetpp/simkernel/cXMLElement;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cMsgPar_1assign_1_1SWIG_113 },
    { (char *)"cMsgPar_cast", (char *)"(JLorg/omnetpp/simkernel/cObject;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cMsgPar_1cast },
    { (char *)"cEvent_getPreviousEventNumber", (char *)"(JLorg/omnetpp/simkernel/cEvent;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cEvent_1getPreviousEventNumber },
    { (char *)"cEvent_setPreviousEventNumber", (char *)"(JLorg/omnetpp/simkernel/cEvent;J)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cEvent_1setPreviousEventNumber },
    { (char *)"cEvent_getInsertOrder", (char *)"(JLorg/omnetpp/simkernel/cEvent;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cEvent_1getInsertOrder },
    { (char *)"cEvent_setArrivalTime", (char *)"(JLorg/omnetpp/simkernel/cEvent;JLorg/omnetpp/simkernel/SimTime;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cEvent_1setArrivalTime },
    { (char *)"cEvent_getSrcProcId", (char *)"(JLorg/omnetpp/simkernel/cEvent;)I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cEvent_1getSrcProcId },
    { (char *)"cEvent_compareBySchedulingOrder", (char *)"(JLorg/omnetpp/simkernel/cEvent;JLorg/omnetpp/simkernel/cEvent;)I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cEvent_1compareBySchedulingOrder },
    { (char *)"delete_cEvent", (char *)"(J)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cEvent },
    { (char *)"cEvent_assign", (char *)"(JLorg/omnetpp/simkernel/cEvent;JLorg/omnetpp/simkernel/cEvent;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cEvent_1assign },
    { (char *)"cEvent_dup", (char *)"(JLorg/omnetpp/simkernel/cEvent;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cEvent_1dup },
    { (char *)"cEvent_str", (char *)"(JLorg/omnetpp/simkernel/cEvent;)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cEvent_1str },
    { (char *)"cEvent_forEachChild", (char *)"(JLorg/omnetpp/simkernel/cEvent;JLorg/omnetpp/simkernel/cVisitor;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cEvent_1forEachChild },
    { (char *)"cEvent_setSchedulingPriority", (char *)"(JLorg/omnetpp/simkernel/cEvent;S)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cEvent_1setSchedulingPriority },
    { (char *)"cEvent_getSchedulingPriority", (char *)"(JLorg/omnetpp/simkernel/cEvent;)S", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cEvent_1getSchedulingPriority },
    { (char *)"cEvent_isScheduled", (char *)"(JLorg/omnetpp/simkernel/cEvent;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cEvent_1isScheduled },
    { (char *)"cEvent_getArrivalTime", (char *)"(JLorg/omnetpp/simkernel/cEvent;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cEvent_1getArrivalTime },
    { (char *)"cEvent_getTargetObject", (char *)"(JLorg/omnetpp/simkernel/cEvent;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cEvent_1getTargetObject },
    { (char *)"cEvent_isMessage", (char *)"(JLorg/omnetpp/simkernel/cEvent;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cEvent_1isMessage },
    { (char *)"cEvent_isStale", (char *)"(JLorg/omnetpp/simkernel/cEvent;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cEvent_1isStale },
    { (char *)"cEvent_shouldPrecede", (char *)"(JLorg/omnetpp/simkernel/cEvent;JLorg/omnetpp/simkernel/cEvent;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cEvent_1shouldPrecede },
    { (char *)"cEvent_execute", (char *)"(JLorg/omnetpp/simkernel/cEvent;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cEvent_1execute },
    { (char *)"cEventHeap_getUseCb", (char *)"(JLorg/omnetpp/simkernel/cEventHeap;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cEventHeap_1getUseCb },
    { (char *)"cEventHeap_setUseCb", (char *)"(JLorg/omnetpp/simkernel/cEventHeap;Z)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cEventHeap_1setUseCb },
    { (char *)"new_cEventHeap__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cEventHeap;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cEventHeap_1_1SWIG_10 },
    { (char *)"new_cEventHeap__SWIG_1", (char *)"(Ljava/lang/String;I)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cEventHeap_1_1SWIG_11 },
    { (char *)"new_cEventHeap__SWIG_2", (char *)"(Ljava/lang/String;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cEventHeap_1_1SWIG_12 },
    { (char *)"new_cEventHeap__SWIG_3", (char *)"()J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cEventHeap_1_1SWIG_13 },
    { (char *)"delete_cEventHeap", (char *)"(J)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cEventHeap },
    { (char *)"cEventHeap_assign", (char *)"(JLorg/omnetpp/simkernel/cEventHeap;JLorg/omnetpp/simkernel/cEventHeap;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cEventHeap_1assign },
    { (char *)"cEventHeap_dup", (char *)"(JLorg/omnetpp/simkernel/cEventHeap;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cEventHeap_1dup },
    { (char *)"cEventHeap_str", (char *)"(JLorg/omnetpp/simkernel/cEventHeap;)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cEventHeap_1str },
    { (char *)"cEventHeap_forEachChild", (char *)"(JLorg/omnetpp/simkernel/cEventHeap;JLorg/omnetpp/simkernel/cVisitor;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cEventHeap_1forEachChild },
    { (char *)"cEventHeap_insert", (char *)"(JLorg/omnetpp/simkernel/cEventHeap;JLorg/omnetpp/simkernel/cEvent;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cEventHeap_1insert },
    { (char *)"cEventHeap_peekFirst", (char *)"(JLorg/omnetpp/simkernel/cEventHeap;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cEventHeap_1peekFirst },
    { (char *)"cEventHeap_removeFirst", (char *)"(JLorg/omnetpp/simkernel/cEventHeap;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cEventHeap_1removeFirst },
    { (char *)"cEventHeap_putBackFirst", (char *)"(JLorg/omnetpp/simkernel/cEventHeap;JLorg/omnetpp/simkernel/cEvent;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cEventHeap_1putBackFirst },
    { (char *)"cEventHeap_remove", (char *)"(JLorg/omnetpp/simkernel/cEventHeap;JLorg/omnetpp/simkernel/cEvent;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cEventHeap_1remove },
    { (char *)"cEventHeap_isEmpty", (char *)"(JLorg/omnetpp/simkernel/cEventHeap;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cEventHeap_1isEmpty },
    { (char *)"cEventHeap_clear", (char *)"(JLorg/omnetpp/simkernel/cEventHeap;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cEventHeap_1clear },
    { (char *)"cEventHeap_getLength", (char *)"(JLorg/omnetpp/simkernel/cEventHeap;)I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cEventHeap_1getLength },
    { (char *)"cEventHeap_get", (char *)"(JLorg/omnetpp/simkernel/cEventHeap;I)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cEventHeap_1get },
    { (char *)"cEventHeap_sort", (char *)"(JLorg/omnetpp/simkernel/cEventHeap;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cEventHeap_1sort },
    { (char *)"new_cOutVector__SWIG_0", (char *)"(Ljava/lang/String;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cOutVector_1_1SWIG_10 },
    { (char *)"new_cOutVector__SWIG_1", (char *)"()J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cOutVector_1_1SWIG_11 },
    { (char *)"delete_cOutVector", (char *)"(J)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cOutVector },
    { (char *)"cOutVector_setName", (char *)"(JLorg/omnetpp/simkernel/cOutVector;Ljava/lang/String;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cOutVector_1setName },
    { (char *)"cOutVector_str", (char *)"(JLorg/omnetpp/simkernel/cOutVector;)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cOutVector_1str },
    { (char *)"cOutVector_setEnum__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cOutVector;Ljava/lang/String;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cOutVector_1setEnum_1_1SWIG_10 },
    { (char *)"cOutVector_setEnum__SWIG_1", (char *)"(JLorg/omnetpp/simkernel/cOutVector;JLorg/omnetpp/simkernel/cEnum;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cOutVector_1setEnum_1_1SWIG_11 },
    { (char *)"cOutVector_setUnit", (char *)"(JLorg/omnetpp/simkernel/cOutVector;Ljava/lang/String;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cOutVector_1setUnit },
    { (char *)"cOutVector_setType", (char *)"(JLorg/omnetpp/simkernel/cOutVector;I)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cOutVector_1setType },
    { (char *)"cOutVector_setInterpolationMode", (char *)"(JLorg/omnetpp/simkernel/cOutVector;I)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cOutVector_1setInterpolationMode },
    { (char *)"cOutVector_setMin", (char *)"(JLorg/omnetpp/simkernel/cOutVector;D)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cOutVector_1setMin },
    { (char *)"cOutVector_setMax", (char *)"(JLorg/omnetpp/simkernel/cOutVector;D)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cOutVector_1setMax },
    { (char *)"cOutVector_record__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cOutVector;D)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cOutVector_1record_1_1SWIG_10 },
    { (char *)"cOutVector_record__SWIG_1", (char *)"(JLorg/omnetpp/simkernel/cOutVector;JLorg/omnetpp/simkernel/SimTime;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cOutVector_1record_1_1SWIG_11 },
    { (char *)"cOutVector_recordWithTimestamp__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cOutVector;JLorg/omnetpp/simkernel/SimTime;D)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cOutVector_1recordWithTimestamp_1_1SWIG_10 },
    { (char *)"cOutVector_recordWithTimestamp__SWIG_1", (char *)"(JLorg/omnetpp/simkernel/cOutVector;JLorg/omnetpp/simkernel/SimTime;JLorg/omnetpp/simkernel/SimTime;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cOutVector_1recordWithTimestamp_1_1SWIG_11 },
    { (char *)"cOutVector_enable", (char *)"(JLorg/omnetpp/simkernel/cOutVector;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cOutVector_1enable },
    { (char *)"cOutVector_disable", (char *)"(JLorg/omnetpp/simkernel/cOutVector;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cOutVector_1disable },
    { (char *)"cOutVector_setEnabled", (char *)"(JLorg/omnetpp/simkernel/cOutVector;Z)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cOutVector_1setEnabled },
    { (char *)"cOutVector_isEnabled", (char *)"(JLorg/omnetpp/simkernel/cOutVector;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cOutVector_1isEnabled },
    { (char *)"cOutVector_setRecordDuringWarmupPeriod", (char *)"(JLorg/omnetpp/simkernel/cOutVector;Z)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cOutVector_1setRecordDuringWarmupPeriod },
    { (char *)"cOutVector_getRecordDuringWarmupPeriod", (char *)"(JLorg/omnetpp/simkernel/cOutVector;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cOutVector_1getRecordDuringWarmupPeriod },
    { (char *)"cOutVector_getValuesReceived", (char *)"(JLorg/omnetpp/simkernel/cOutVector;)I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cOutVector_1getValuesReceived },
    { (char *)"cOutVector_getValuesStored", (char *)"(JLorg/omnetpp/simkernel/cOutVector;)I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cOutVector_1getValuesStored },
    { (char *)"cOutVector_cast", (char *)"(JLorg/omnetpp/simkernel/cObject;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cOutVector_1cast },
    { (char *)"cPar_BOOL_get", (char *)"()I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cPar_1BOOL_1get },
    { (char *)"cPar_DOUBLE_get", (char *)"()I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cPar_1DOUBLE_1get },
    { (char *)"cPar_INT_get", (char *)"()I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cPar_1INT_1get },
    { (char *)"cPar_STRING_get", (char *)"()I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cPar_1STRING_1get },
    { (char *)"cPar_XML_get", (char *)"()I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cPar_1XML_1get },
    { (char *)"cPar_LONG_get", (char *)"()I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cPar_1LONG_1get },
    { (char *)"cPar_read", (char *)"(JLorg/omnetpp/simkernel/cPar;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cPar_1read },
    //XXX
    //{ (char *)"cPar_finalize", (char *)"(JLorg/omnetpp/simkernel/cPar;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cPar_1finalize },
    { (char *)"cPar_acceptDefault", (char *)"(JLorg/omnetpp/simkernel/cPar;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cPar_1acceptDefault },
    { (char *)"cPar_assign__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cPar;JLorg/omnetpp/simkernel/cPar;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cPar_1assign_1_1SWIG_10 },
    { (char *)"cPar_getName", (char *)"(JLorg/omnetpp/simkernel/cPar;)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cPar_1getName },
    { (char *)"cPar_str", (char *)"(JLorg/omnetpp/simkernel/cPar;)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cPar_1str },
    { (char *)"cPar_getOwner", (char *)"(JLorg/omnetpp/simkernel/cPar;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cPar_1getOwner },
    { (char *)"cPar_forEachChild", (char *)"(JLorg/omnetpp/simkernel/cPar;JLorg/omnetpp/simkernel/cVisitor;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cPar_1forEachChild },
    { (char *)"cPar_getType", (char *)"(JLorg/omnetpp/simkernel/cPar;)I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cPar_1getType },
    { (char *)"cPar_getTypeName", (char *)"(I)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cPar_1getTypeName },
    { (char *)"cPar_isNumeric", (char *)"(JLorg/omnetpp/simkernel/cPar;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cPar_1isNumeric },
    { (char *)"cPar_isVolatile", (char *)"(JLorg/omnetpp/simkernel/cPar;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cPar_1isVolatile },
    { (char *)"cPar_isExpression", (char *)"(JLorg/omnetpp/simkernel/cPar;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cPar_1isExpression },
    { (char *)"cPar_isShared", (char *)"(JLorg/omnetpp/simkernel/cPar;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cPar_1isShared },
    { (char *)"cPar_isSet", (char *)"(JLorg/omnetpp/simkernel/cPar;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cPar_1isSet },
    { (char *)"cPar_containsValue", (char *)"(JLorg/omnetpp/simkernel/cPar;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cPar_1containsValue },
    { (char *)"cPar_getProperties", (char *)"(JLorg/omnetpp/simkernel/cPar;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cPar_1getProperties },
    { (char *)"cPar_setBoolValue", (char *)"(JLorg/omnetpp/simkernel/cPar;Z)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cPar_1setBoolValue },
    { (char *)"cPar_setIntValue", (char *)"(JLorg/omnetpp/simkernel/cPar;J)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cPar_1setIntValue },
    { (char *)"cPar_setLongValue", (char *)"(JLorg/omnetpp/simkernel/cPar;J)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cPar_1setLongValue },
    { (char *)"cPar_setDoubleValue", (char *)"(JLorg/omnetpp/simkernel/cPar;D)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cPar_1setDoubleValue },
    { (char *)"cPar_setStringValue__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cPar;Ljava/lang/String;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cPar_1setStringValue_1_1SWIG_10 },
    { (char *)"cPar_setXMLValue", (char *)"(JLorg/omnetpp/simkernel/cPar;JLorg/omnetpp/simkernel/cXMLElement;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cPar_1setXMLValue },
    { (char *)"cPar_setExpression__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cPar;JLorg/omnetpp/simkernel/cExpression;JLorg/omnetpp/simkernel/cComponent;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cPar_1setExpression_1_1SWIG_10 },
    { (char *)"cPar_setExpression__SWIG_1", (char *)"(JLorg/omnetpp/simkernel/cPar;JLorg/omnetpp/simkernel/cExpression;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cPar_1setExpression_1_1SWIG_11 },
    { (char *)"cPar_setEvaluationContext", (char *)"(JLorg/omnetpp/simkernel/cPar;JLorg/omnetpp/simkernel/cComponent;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cPar_1setEvaluationContext },
    { (char *)"cPar_boolValue", (char *)"(JLorg/omnetpp/simkernel/cPar;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cPar_1boolValue },
    { (char *)"cPar_intValue", (char *)"(JLorg/omnetpp/simkernel/cPar;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cPar_1intValue },
    { (char *)"cPar_longValue", (char *)"(JLorg/omnetpp/simkernel/cPar;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cPar_1longValue },
    { (char *)"cPar_doubleValue", (char *)"(JLorg/omnetpp/simkernel/cPar;)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cPar_1doubleValue },
    { (char *)"cPar_doubleValueInUnit", (char *)"(JLorg/omnetpp/simkernel/cPar;Ljava/lang/String;)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cPar_1doubleValueInUnit },
    { (char *)"cPar_getUnit", (char *)"(JLorg/omnetpp/simkernel/cPar;)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cPar_1getUnit },
    { (char *)"cPar_stringValue", (char *)"(JLorg/omnetpp/simkernel/cPar;)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cPar_1stringValue },
    { (char *)"cPar_stdstringValue", (char *)"(JLorg/omnetpp/simkernel/cPar;)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cPar_1stdstringValue },
    { (char *)"cPar_isEmptyString", (char *)"(JLorg/omnetpp/simkernel/cPar;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cPar_1isEmptyString },
    { (char *)"cPar_xmlValue", (char *)"(JLorg/omnetpp/simkernel/cPar;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cPar_1xmlValue },
    { (char *)"cPar_getExpression", (char *)"(JLorg/omnetpp/simkernel/cPar;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cPar_1getExpression },
    { (char *)"cPar_getEvaluationContext", (char *)"(JLorg/omnetpp/simkernel/cPar;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cPar_1getEvaluationContext },
    { (char *)"cPar_convertToConst", (char *)"(JLorg/omnetpp/simkernel/cPar;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cPar_1convertToConst },
    { (char *)"cPar_parse", (char *)"(JLorg/omnetpp/simkernel/cPar;Ljava/lang/String;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cPar_1parse },
    { (char *)"cPar_assign__SWIG_1", (char *)"(JLorg/omnetpp/simkernel/cPar;Z)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cPar_1assign_1_1SWIG_11 },
    { (char *)"cPar_assign__SWIG_2", (char *)"(JLorg/omnetpp/simkernel/cPar;C)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cPar_1assign_1_1SWIG_12 },
    { (char *)"cPar_assign__SWIG_3", (char *)"(JLorg/omnetpp/simkernel/cPar;S)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cPar_1assign_1_1SWIG_13 },
    { (char *)"cPar_assign__SWIG_4", (char *)"(JLorg/omnetpp/simkernel/cPar;I)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cPar_1assign_1_1SWIG_14 },
    { (char *)"cPar_assign__SWIG_5", (char *)"(JLorg/omnetpp/simkernel/cPar;J)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cPar_1assign_1_1SWIG_15 },
    // XXX This methods leads into a crash of registerNatives ==> commented out
    //{ (char *)"cPar_assign__SWIG_11", (char *)"(JLorg/omnetpp/simkernel/cPar;Lorg/omnetpp/simkernel/java;.Lorg/omnetpp/simkernel/math;.Lorg/omnetpp/simkernel/BigInteger;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cPar_1assign_1_1SWIG_111 },
    { (char *)"cPar_assign__SWIG_12", (char *)"(JLorg/omnetpp/simkernel/cPar;D)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cPar_1assign_1_1SWIG_112 },
    { (char *)"cPar_assign__SWIG_13", (char *)"(JLorg/omnetpp/simkernel/cPar;J)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cPar_1assign_1_1SWIG_113 },
    { (char *)"cPar_assign__SWIG_14", (char *)"(JLorg/omnetpp/simkernel/cPar;Ljava/lang/String;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cPar_1assign_1_1SWIG_114 },
    { (char *)"cPar_assign__SWIG_16", (char *)"(JLorg/omnetpp/simkernel/cPar;JLorg/omnetpp/simkernel/cXMLElement;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cPar_1assign_1_1SWIG_116 },
    { (char *)"cPar_cast", (char *)"(JLorg/omnetpp/simkernel/cObject;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cPar_1cast },
    { (char *)"PARSIM_ANY_TAG_get", (char *)"()I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_PARSIM_1ANY_1TAG_1get },
    { (char *)"cProperty_DEFAULTKEY_set", (char *)"(Ljava/lang/String;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cProperty_1DEFAULTKEY_1set },
    { (char *)"cProperty_DEFAULTKEY_get", (char *)"()Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cProperty_1DEFAULTKEY_1get },
    { (char *)"cProperty_updateWith", (char *)"(JLorg/omnetpp/simkernel/cProperty;JLorg/omnetpp/simkernel/cProperty;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cProperty_1updateWith },
    { (char *)"cProperty_lock", (char *)"(JLorg/omnetpp/simkernel/cProperty;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cProperty_1lock },
    { (char *)"cProperty_isLocked", (char *)"(JLorg/omnetpp/simkernel/cProperty;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cProperty_1isLocked },
    { (char *)"cProperty_setOwner", (char *)"(JLorg/omnetpp/simkernel/cProperty;JLorg/omnetpp/simkernel/cProperties;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cProperty_1setOwner },
    { (char *)"new_cProperty__SWIG_0", (char *)"(Ljava/lang/String;Ljava/lang/String;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cProperty_1_1SWIG_10 },
    { (char *)"new_cProperty__SWIG_1", (char *)"(Ljava/lang/String;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cProperty_1_1SWIG_11 },
    { (char *)"new_cProperty__SWIG_2", (char *)"()J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cProperty_1_1SWIG_12 },
    { (char *)"new_cProperty__SWIG_3", (char *)"(JLorg/omnetpp/simkernel/cProperty;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cProperty_1_1SWIG_13 },
    { (char *)"delete_cProperty", (char *)"(J)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cProperty },
    { (char *)"cProperty_assign", (char *)"(JLorg/omnetpp/simkernel/cProperty;JLorg/omnetpp/simkernel/cProperty;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cProperty_1assign },
    { (char *)"cProperty_setName", (char *)"(JLorg/omnetpp/simkernel/cProperty;Ljava/lang/String;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cProperty_1setName },
    { (char *)"cProperty_getFullName", (char *)"(JLorg/omnetpp/simkernel/cProperty;)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cProperty_1getFullName },
    { (char *)"cProperty_dup", (char *)"(JLorg/omnetpp/simkernel/cProperty;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cProperty_1dup },
    { (char *)"cProperty_str", (char *)"(JLorg/omnetpp/simkernel/cProperty;)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cProperty_1str },
    { (char *)"cProperty_setIndex", (char *)"(JLorg/omnetpp/simkernel/cProperty;Ljava/lang/String;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cProperty_1setIndex },
    { (char *)"cProperty_getIndex", (char *)"(JLorg/omnetpp/simkernel/cProperty;)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cProperty_1getIndex },
    { (char *)"cProperty_setIsImplicit", (char *)"(JLorg/omnetpp/simkernel/cProperty;Z)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cProperty_1setIsImplicit },
    { (char *)"cProperty_isImplicit", (char *)"(JLorg/omnetpp/simkernel/cProperty;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cProperty_1isImplicit },
    { (char *)"cProperty_getKeys", (char *)"(JLorg/omnetpp/simkernel/cProperty;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cProperty_1getKeys },
    { (char *)"cProperty_containsKey", (char *)"(JLorg/omnetpp/simkernel/cProperty;Ljava/lang/String;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cProperty_1containsKey },
    { (char *)"cProperty_addKey", (char *)"(JLorg/omnetpp/simkernel/cProperty;Ljava/lang/String;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cProperty_1addKey },
    { (char *)"cProperty_getNumValues", (char *)"(JLorg/omnetpp/simkernel/cProperty;Ljava/lang/String;)I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cProperty_1getNumValues },
    { (char *)"cProperty_setNumValues", (char *)"(JLorg/omnetpp/simkernel/cProperty;Ljava/lang/String;I)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cProperty_1setNumValues },
    { (char *)"cProperty_getValue__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cProperty;Ljava/lang/String;I)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cProperty_1getValue_1_1SWIG_10 },
    { (char *)"cProperty_getValue__SWIG_1", (char *)"(JLorg/omnetpp/simkernel/cProperty;Ljava/lang/String;)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cProperty_1getValue_1_1SWIG_11 },
    { (char *)"cProperty_getValue__SWIG_2", (char *)"(JLorg/omnetpp/simkernel/cProperty;)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cProperty_1getValue_1_1SWIG_12 },
    { (char *)"cProperty_setValue", (char *)"(JLorg/omnetpp/simkernel/cProperty;Ljava/lang/String;ILjava/lang/String;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cProperty_1setValue },
    { (char *)"cProperty_erase", (char *)"(JLorg/omnetpp/simkernel/cProperty;Ljava/lang/String;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cProperty_1erase },
    { (char *)"cProperty_cast", (char *)"(JLorg/omnetpp/simkernel/cObject;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cProperty_1cast },
    { (char *)"cProperties_lock", (char *)"(JLorg/omnetpp/simkernel/cProperties;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cProperties_1lock },
    { (char *)"cProperties_addRef", (char *)"(JLorg/omnetpp/simkernel/cProperties;)I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cProperties_1addRef },
    { (char *)"cProperties_removeRef", (char *)"(JLorg/omnetpp/simkernel/cProperties;)I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cProperties_1removeRef },
    { (char *)"new_cProperties__SWIG_0", (char *)"()J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cProperties_1_1SWIG_10 },
    { (char *)"new_cProperties__SWIG_1", (char *)"(JLorg/omnetpp/simkernel/cProperties;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cProperties_1_1SWIG_11 },
    { (char *)"delete_cProperties", (char *)"(J)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cProperties },
    { (char *)"cProperties_assign", (char *)"(JLorg/omnetpp/simkernel/cProperties;JLorg/omnetpp/simkernel/cProperties;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cProperties_1assign },
    { (char *)"cProperties_dup", (char *)"(JLorg/omnetpp/simkernel/cProperties;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cProperties_1dup },
    { (char *)"cProperties_getName", (char *)"(JLorg/omnetpp/simkernel/cProperties;)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cProperties_1getName },
    { (char *)"cProperties_str", (char *)"(JLorg/omnetpp/simkernel/cProperties;)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cProperties_1str },
    { (char *)"cProperties_getNumProperties", (char *)"(JLorg/omnetpp/simkernel/cProperties;)I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cProperties_1getNumProperties },
    { (char *)"cProperties_getNames", (char *)"(JLorg/omnetpp/simkernel/cProperties;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cProperties_1getNames },
    { (char *)"cProperties_get__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cProperties;I)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cProperties_1get_1_1SWIG_10 },
    { (char *)"cProperties_get__SWIG_1", (char *)"(JLorg/omnetpp/simkernel/cProperties;Ljava/lang/String;Ljava/lang/String;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cProperties_1get_1_1SWIG_11 },
    { (char *)"cProperties_get__SWIG_2", (char *)"(JLorg/omnetpp/simkernel/cProperties;Ljava/lang/String;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cProperties_1get_1_1SWIG_12 },
    { (char *)"cProperties_getAsBool__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cProperties;Ljava/lang/String;Ljava/lang/String;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cProperties_1getAsBool_1_1SWIG_10 },
    { (char *)"cProperties_getAsBool__SWIG_1", (char *)"(JLorg/omnetpp/simkernel/cProperties;Ljava/lang/String;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cProperties_1getAsBool_1_1SWIG_11 },
    { (char *)"cProperties_getIndicesFor", (char *)"(JLorg/omnetpp/simkernel/cProperties;Ljava/lang/String;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cProperties_1getIndicesFor },
    { (char *)"cProperties_add", (char *)"(JLorg/omnetpp/simkernel/cProperties;JLorg/omnetpp/simkernel/cProperty;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cProperties_1add },
    { (char *)"cProperties_remove", (char *)"(JLorg/omnetpp/simkernel/cProperties;I)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cProperties_1remove },
    { (char *)"cProperties_cast", (char *)"(JLorg/omnetpp/simkernel/cObject;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cProperties_1cast },
    { (char *)"new_cSimulation", (char *)"(Ljava/lang/String;JLorg/omnetpp/simkernel/cEnvir;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cSimulation },
    { (char *)"delete_cSimulation", (char *)"(J)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cSimulation },
    { (char *)"cSimulation_forEachChild", (char *)"(JLorg/omnetpp/simkernel/cSimulation;JLorg/omnetpp/simkernel/cVisitor;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cSimulation_1forEachChild },
    { (char *)"cSimulation_getFullPath", (char *)"(JLorg/omnetpp/simkernel/cSimulation;)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cSimulation_1getFullPath },
    { (char *)"cSimulation_getActiveSimulation", (char *)"()J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cSimulation_1getActiveSimulation },
    { (char *)"cSimulation_getActiveEnvir", (char *)"()J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cSimulation_1getActiveEnvir },
    { (char *)"cSimulation_setActiveSimulation", (char *)"(JLorg/omnetpp/simkernel/cSimulation;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cSimulation_1setActiveSimulation },
    { (char *)"cSimulation_setStaticEnvir", (char *)"(JLorg/omnetpp/simkernel/cEnvir;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cSimulation_1setStaticEnvir },
    { (char *)"cSimulation_getStaticEnvir", (char *)"()J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cSimulation_1getStaticEnvir },
    { (char *)"cSimulation_getEnvir", (char *)"(JLorg/omnetpp/simkernel/cSimulation;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cSimulation_1getEnvir },
    { (char *)"cSimulation_registerComponent", (char *)"(JLorg/omnetpp/simkernel/cSimulation;JLorg/omnetpp/simkernel/cComponent;)I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cSimulation_1registerComponent },
    { (char *)"cSimulation_deregisterComponent", (char *)"(JLorg/omnetpp/simkernel/cSimulation;JLorg/omnetpp/simkernel/cComponent;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cSimulation_1deregisterComponent },
    { (char *)"cSimulation_getLastComponentId", (char *)"(JLorg/omnetpp/simkernel/cSimulation;)I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cSimulation_1getLastComponentId },
    { (char *)"cSimulation_getModuleByPath", (char *)"(JLorg/omnetpp/simkernel/cSimulation;Ljava/lang/String;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cSimulation_1getModuleByPath },
    { (char *)"cSimulation_getComponent", (char *)"(JLorg/omnetpp/simkernel/cSimulation;I)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cSimulation_1getComponent },
    { (char *)"cSimulation_getModule", (char *)"(JLorg/omnetpp/simkernel/cSimulation;I)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cSimulation_1getModule },
    { (char *)"cSimulation_getChannel", (char *)"(JLorg/omnetpp/simkernel/cSimulation;I)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cSimulation_1getChannel },
    { (char *)"cSimulation_getSystemModule", (char *)"(JLorg/omnetpp/simkernel/cSimulation;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cSimulation_1getSystemModule },
    { (char *)"cSimulation_loadNedSourceFolder", (char *)"(Ljava/lang/String;)I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cSimulation_1loadNedSourceFolder },
    { (char *)"cSimulation_loadNedText__SWIG_0", (char *)"(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cSimulation_1loadNedText_1_1SWIG_10 },
    { (char *)"cSimulation_loadNedText__SWIG_1", (char *)"(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cSimulation_1loadNedText_1_1SWIG_11 },
    { (char *)"cSimulation_loadNedText__SWIG_2", (char *)"(Ljava/lang/String;Ljava/lang/String;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cSimulation_1loadNedText_1_1SWIG_12 },
    { (char *)"cSimulation_doneLoadingNedFiles", (char *)"()V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cSimulation_1doneLoadingNedFiles },
    { (char *)"cSimulation_getNedPackageForFolder", (char *)"(Ljava/lang/String;)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cSimulation_1getNedPackageForFolder },
    { (char *)"cSimulation_setFES", (char *)"(JLorg/omnetpp/simkernel/cSimulation;J)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cSimulation_1setFES },
    { (char *)"cSimulation_getFES", (char *)"(JLorg/omnetpp/simkernel/cSimulation;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cSimulation_1getFES },
    { (char *)"cSimulation_setSimulationTimeLimit", (char *)"(JLorg/omnetpp/simkernel/cSimulation;JLorg/omnetpp/simkernel/SimTime;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cSimulation_1setSimulationTimeLimit },
    { (char *)"cSimulation_callInitialize", (char *)"(JLorg/omnetpp/simkernel/cSimulation;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cSimulation_1callInitialize },
    { (char *)"cSimulation_getSimulationStage", (char *)"(JLorg/omnetpp/simkernel/cSimulation;)I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cSimulation_1getSimulationStage },
    { (char *)"cSimulation_getSimTime", (char *)"(JLorg/omnetpp/simkernel/cSimulation;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cSimulation_1getSimTime },
    { (char *)"cSimulation_getEventNumber", (char *)"(JLorg/omnetpp/simkernel/cSimulation;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cSimulation_1getEventNumber },
    { (char *)"cSimulation_getWarmupPeriod", (char *)"(JLorg/omnetpp/simkernel/cSimulation;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cSimulation_1getWarmupPeriod },
    { (char *)"cSimulation_setWarmupPeriod", (char *)"(JLorg/omnetpp/simkernel/cSimulation;JLorg/omnetpp/simkernel/SimTime;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cSimulation_1setWarmupPeriod },
    { (char *)"cSimulation_takeNextEvent", (char *)"(JLorg/omnetpp/simkernel/cSimulation;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cSimulation_1takeNextEvent },
    { (char *)"cSimulation_putBackEvent", (char *)"(JLorg/omnetpp/simkernel/cSimulation;JLorg/omnetpp/simkernel/cEvent;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cSimulation_1putBackEvent },
    { (char *)"cSimulation_executeEvent", (char *)"(JLorg/omnetpp/simkernel/cSimulation;JLorg/omnetpp/simkernel/cEvent;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cSimulation_1executeEvent },
    { (char *)"cSimulation_insertEvent", (char *)"(JLorg/omnetpp/simkernel/cSimulation;JLorg/omnetpp/simkernel/cEvent;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cSimulation_1insertEvent },
    { (char *)"cSimulation_getActivityModule", (char *)"(JLorg/omnetpp/simkernel/cSimulation;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cSimulation_1getActivityModule },
    { (char *)"cSimulation_getContext", (char *)"(JLorg/omnetpp/simkernel/cSimulation;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cSimulation_1getContext },
    { (char *)"cSimulation_getContextType", (char *)"(JLorg/omnetpp/simkernel/cSimulation;)I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cSimulation_1getContextType },
    { (char *)"cSimulation_getContextModule", (char *)"(JLorg/omnetpp/simkernel/cSimulation;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cSimulation_1getContextModule },
    { (char *)"cSimulation_getContextSimpleModule", (char *)"(JLorg/omnetpp/simkernel/cSimulation;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cSimulation_1getContextSimpleModule },
    { (char *)"cSimulation_requestTrapOnNextEvent", (char *)"(JLorg/omnetpp/simkernel/cSimulation;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cSimulation_1requestTrapOnNextEvent },
    { (char *)"cSimulation_clearTrapOnNextEvent", (char *)"(JLorg/omnetpp/simkernel/cSimulation;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cSimulation_1clearTrapOnNextEvent },
    { (char *)"cSimulation_isTrapOnNextEventRequested", (char *)"(JLorg/omnetpp/simkernel/cSimulation;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cSimulation_1isTrapOnNextEventRequested },
    { (char *)"cSimulation_getUniqueNumber", (char *)"(JLorg/omnetpp/simkernel/cSimulation;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cSimulation_1getUniqueNumber },
    { (char *)"cSimulation_snapshot", (char *)"(JLorg/omnetpp/simkernel/cSimulation;JLorg/omnetpp/simkernel/cObject;Ljava/lang/String;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cSimulation_1snapshot },
    { (char *)"cSimulation_getFingerprintCalculator", (char *)"(JLorg/omnetpp/simkernel/cSimulation;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cSimulation_1getFingerprintCalculator },
    { (char *)"cSimulation_setFingerprintCalculator", (char *)"(JLorg/omnetpp/simkernel/cSimulation;J)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cSimulation_1setFingerprintCalculator },
    { (char *)"cSimulation_cast", (char *)"(JLorg/omnetpp/simkernel/cObject;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cSimulation_1cast },
    { (char *)"simTime", (char *)"()J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_simTime },
    { (char *)"getSimulation", (char *)"()J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_getSimulation },
    { (char *)"getEnvir", (char *)"()J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_getEnvir },
    { (char *)"cClassDescriptor_FD_ISARRAY_get", (char *)"()I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cClassDescriptor_1FD_1ISARRAY_1get },
    { (char *)"cClassDescriptor_FD_ISCOMPOUND_get", (char *)"()I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cClassDescriptor_1FD_1ISCOMPOUND_1get },
    { (char *)"cClassDescriptor_FD_ISPOINTER_get", (char *)"()I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cClassDescriptor_1FD_1ISPOINTER_1get },
    { (char *)"cClassDescriptor_FD_ISCOBJECT_get", (char *)"()I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cClassDescriptor_1FD_1ISCOBJECT_1get },
    { (char *)"cClassDescriptor_FD_ISCOWNEDOBJECT_get", (char *)"()I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cClassDescriptor_1FD_1ISCOWNEDOBJECT_1get },
    { (char *)"cClassDescriptor_FD_ISEDITABLE_get", (char *)"()I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cClassDescriptor_1FD_1ISEDITABLE_1get },
    { (char *)"cClassDescriptor_FD_NONE_get", (char *)"()I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cClassDescriptor_1FD_1NONE_1get },
    { (char *)"delete_cClassDescriptor", (char *)"(J)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cClassDescriptor },
    { (char *)"cClassDescriptor_getDescriptorFor__SWIG_0", (char *)"(Ljava/lang/String;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cClassDescriptor_1getDescriptorFor_1_1SWIG_10 },
    { (char *)"cClassDescriptor_getDescriptorFor__SWIG_1", (char *)"(JLorg/omnetpp/simkernel/cObject;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cClassDescriptor_1getDescriptorFor_1_1SWIG_11 },
    { (char *)"cClassDescriptor_doesSupport", (char *)"(JLorg/omnetpp/simkernel/cClassDescriptor;JLorg/omnetpp/simkernel/cObject;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cClassDescriptor_1doesSupport },
    { (char *)"cClassDescriptor_getBaseClassDescriptor", (char *)"(JLorg/omnetpp/simkernel/cClassDescriptor;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cClassDescriptor_1getBaseClassDescriptor },
    { (char *)"cClassDescriptor_extendsCObject", (char *)"(JLorg/omnetpp/simkernel/cClassDescriptor;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cClassDescriptor_1extendsCObject },
    { (char *)"cClassDescriptor_getNamespace", (char *)"(JLorg/omnetpp/simkernel/cClassDescriptor;)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cClassDescriptor_1getNamespace },
    { (char *)"cClassDescriptor_getInheritanceChainLength", (char *)"(JLorg/omnetpp/simkernel/cClassDescriptor;)I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cClassDescriptor_1getInheritanceChainLength },
    { (char *)"cClassDescriptor_getPropertyNames", (char *)"(JLorg/omnetpp/simkernel/cClassDescriptor;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cClassDescriptor_1getPropertyNames },
    { (char *)"cClassDescriptor_getProperty", (char *)"(JLorg/omnetpp/simkernel/cClassDescriptor;Ljava/lang/String;)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cClassDescriptor_1getProperty },
    { (char *)"cClassDescriptor_getFieldCount", (char *)"(JLorg/omnetpp/simkernel/cClassDescriptor;)I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cClassDescriptor_1getFieldCount },
    { (char *)"cClassDescriptor_getFieldName", (char *)"(JLorg/omnetpp/simkernel/cClassDescriptor;I)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cClassDescriptor_1getFieldName },
    { (char *)"cClassDescriptor_findField", (char *)"(JLorg/omnetpp/simkernel/cClassDescriptor;Ljava/lang/String;)I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cClassDescriptor_1findField },
    { (char *)"cClassDescriptor_getFieldTypeFlags", (char *)"(JLorg/omnetpp/simkernel/cClassDescriptor;I)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cClassDescriptor_1getFieldTypeFlags },
    { (char *)"cClassDescriptor_getFieldIsArray", (char *)"(JLorg/omnetpp/simkernel/cClassDescriptor;I)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cClassDescriptor_1getFieldIsArray },
    { (char *)"cClassDescriptor_getFieldIsCompound", (char *)"(JLorg/omnetpp/simkernel/cClassDescriptor;I)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cClassDescriptor_1getFieldIsCompound },
    { (char *)"cClassDescriptor_getFieldIsPointer", (char *)"(JLorg/omnetpp/simkernel/cClassDescriptor;I)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cClassDescriptor_1getFieldIsPointer },
    { (char *)"cClassDescriptor_getFieldIsCObject", (char *)"(JLorg/omnetpp/simkernel/cClassDescriptor;I)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cClassDescriptor_1getFieldIsCObject },
    { (char *)"cClassDescriptor_getFieldIsCOwnedObject", (char *)"(JLorg/omnetpp/simkernel/cClassDescriptor;I)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cClassDescriptor_1getFieldIsCOwnedObject },
    { (char *)"cClassDescriptor_getFieldIsEditable", (char *)"(JLorg/omnetpp/simkernel/cClassDescriptor;I)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cClassDescriptor_1getFieldIsEditable },
    { (char *)"cClassDescriptor_getFieldDeclaredOn", (char *)"(JLorg/omnetpp/simkernel/cClassDescriptor;I)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cClassDescriptor_1getFieldDeclaredOn },
    { (char *)"cClassDescriptor_getFieldTypeString", (char *)"(JLorg/omnetpp/simkernel/cClassDescriptor;I)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cClassDescriptor_1getFieldTypeString },
    { (char *)"cClassDescriptor_getFieldPropertyNames", (char *)"(JLorg/omnetpp/simkernel/cClassDescriptor;I)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cClassDescriptor_1getFieldPropertyNames },
    { (char *)"cClassDescriptor_getFieldProperty", (char *)"(JLorg/omnetpp/simkernel/cClassDescriptor;ILjava/lang/String;)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cClassDescriptor_1getFieldProperty },
    { (char *)"cClassDescriptor_getFieldArraySize", (char *)"(JLorg/omnetpp/simkernel/cClassDescriptor;JI)I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cClassDescriptor_1getFieldArraySize },
    { (char *)"cClassDescriptor_getFieldDynamicTypeString", (char *)"(JLorg/omnetpp/simkernel/cClassDescriptor;JII)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cClassDescriptor_1getFieldDynamicTypeString },
    { (char *)"cClassDescriptor_getFieldValueAsString", (char *)"(JLorg/omnetpp/simkernel/cClassDescriptor;JII)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cClassDescriptor_1getFieldValueAsString },
    { (char *)"cClassDescriptor_setFieldValueAsString", (char *)"(JLorg/omnetpp/simkernel/cClassDescriptor;JIILjava/lang/String;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cClassDescriptor_1setFieldValueAsString },
    { (char *)"cClassDescriptor_getFieldStructName", (char *)"(JLorg/omnetpp/simkernel/cClassDescriptor;I)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cClassDescriptor_1getFieldStructName },
    { (char *)"cClassDescriptor_getFieldStructValuePointer", (char *)"(JLorg/omnetpp/simkernel/cClassDescriptor;JII)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cClassDescriptor_1getFieldStructValuePointer },
    { (char *)"cClassDescriptor_getFieldAsCObject", (char *)"(JLorg/omnetpp/simkernel/cClassDescriptor;JII)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cClassDescriptor_1getFieldAsCObject },
    { (char *)"delete_cVisitor", (char *)"(J)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cVisitor },
    { (char *)"cVisitor_process", (char *)"(JLorg/omnetpp/simkernel/cVisitor;JLorg/omnetpp/simkernel/cObject;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cVisitor_1process },
    { (char *)"cVisitor_processChildrenOf", (char *)"(JLorg/omnetpp/simkernel/cVisitor;JLorg/omnetpp/simkernel/cObject;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cVisitor_1processChildrenOf },
    { (char *)"cVisitor_visit", (char *)"(JLorg/omnetpp/simkernel/cVisitor;JLorg/omnetpp/simkernel/cObject;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cVisitor_1visit },
    { (char *)"cStdVectorWatcherBase_str", (char *)"(JLorg/omnetpp/simkernel/cStdVectorWatcherBase;)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cStdVectorWatcherBase_1str },
    { (char *)"cStdVectorWatcherBase_supportsAssignment", (char *)"(JLorg/omnetpp/simkernel/cStdVectorWatcherBase;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cStdVectorWatcherBase_1supportsAssignment },
    { (char *)"cStdVectorWatcherBase_getElemTypeName", (char *)"(JLorg/omnetpp/simkernel/cStdVectorWatcherBase;)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cStdVectorWatcherBase_1getElemTypeName },
    { (char *)"cStdVectorWatcherBase_size", (char *)"(JLorg/omnetpp/simkernel/cStdVectorWatcherBase;)I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cStdVectorWatcherBase_1size },
    { (char *)"cStdVectorWatcherBase_at", (char *)"(JLorg/omnetpp/simkernel/cStdVectorWatcherBase;I)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cStdVectorWatcherBase_1at },
    { (char *)"cStdVectorWatcherBase_getDescriptor", (char *)"(JLorg/omnetpp/simkernel/cStdVectorWatcherBase;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cStdVectorWatcherBase_1getDescriptor },
    { (char *)"delete_cStdVectorWatcherBase", (char *)"(J)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cStdVectorWatcherBase },
    { (char *)"new_cXMLElement", (char *)"(Ljava/lang/String;Ljava/lang/String;JLorg/omnetpp/simkernel/cXMLElement;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cXMLElement },
    { (char *)"cXMLElement_setNodeValue", (char *)"(JLorg/omnetpp/simkernel/cXMLElement;Ljava/lang/String;I)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cXMLElement_1setNodeValue },
    { (char *)"cXMLElement_appendNodeValue", (char *)"(JLorg/omnetpp/simkernel/cXMLElement;Ljava/lang/String;I)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cXMLElement_1appendNodeValue },
    { (char *)"delete_cXMLElement", (char *)"(J)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cXMLElement },
    { (char *)"cXMLElement_setAttribute", (char *)"(JLorg/omnetpp/simkernel/cXMLElement;Ljava/lang/String;Ljava/lang/String;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cXMLElement_1setAttribute },
    { (char *)"cXMLElement_appendChild", (char *)"(JLorg/omnetpp/simkernel/cXMLElement;JLorg/omnetpp/simkernel/cXMLElement;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cXMLElement_1appendChild },
    { (char *)"cXMLElement_insertChildBefore", (char *)"(JLorg/omnetpp/simkernel/cXMLElement;JLorg/omnetpp/simkernel/cXMLElement;JLorg/omnetpp/simkernel/cXMLElement;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cXMLElement_1insertChildBefore },
    { (char *)"cXMLElement_removeChild", (char *)"(JLorg/omnetpp/simkernel/cXMLElement;JLorg/omnetpp/simkernel/cXMLElement;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cXMLElement_1removeChild },
    { (char *)"cXMLElement_getName", (char *)"(JLorg/omnetpp/simkernel/cXMLElement;)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cXMLElement_1getName },
    { (char *)"cXMLElement_str", (char *)"(JLorg/omnetpp/simkernel/cXMLElement;)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cXMLElement_1str },
    { (char *)"cXMLElement_getOwner", (char *)"(JLorg/omnetpp/simkernel/cXMLElement;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cXMLElement_1getOwner },
    { (char *)"cXMLElement_forEachChild", (char *)"(JLorg/omnetpp/simkernel/cXMLElement;JLorg/omnetpp/simkernel/cVisitor;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cXMLElement_1forEachChild },
    { (char *)"cXMLElement_getTagName", (char *)"(JLorg/omnetpp/simkernel/cXMLElement;)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cXMLElement_1getTagName },
    { (char *)"cXMLElement_getSourceLocation", (char *)"(JLorg/omnetpp/simkernel/cXMLElement;)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cXMLElement_1getSourceLocation },
    { (char *)"cXMLElement_getNodeValue", (char *)"(JLorg/omnetpp/simkernel/cXMLElement;)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cXMLElement_1getNodeValue },
    { (char *)"cXMLElement_getAttribute", (char *)"(JLorg/omnetpp/simkernel/cXMLElement;Ljava/lang/String;)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cXMLElement_1getAttribute },
    { (char *)"cXMLElement_hasAttributes", (char *)"(JLorg/omnetpp/simkernel/cXMLElement;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cXMLElement_1hasAttributes },
    { (char *)"cXMLElement_getAttributes", (char *)"(JLorg/omnetpp/simkernel/cXMLElement;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cXMLElement_1getAttributes },
    { (char *)"cXMLElement_getXML", (char *)"(JLorg/omnetpp/simkernel/cXMLElement;)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cXMLElement_1getXML },
    { (char *)"cXMLElement_getParentNode", (char *)"(JLorg/omnetpp/simkernel/cXMLElement;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cXMLElement_1getParentNode },
    { (char *)"cXMLElement_hasChildren", (char *)"(JLorg/omnetpp/simkernel/cXMLElement;)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cXMLElement_1hasChildren },
    { (char *)"cXMLElement_getFirstChild", (char *)"(JLorg/omnetpp/simkernel/cXMLElement;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cXMLElement_1getFirstChild },
    { (char *)"cXMLElement_getLastChild", (char *)"(JLorg/omnetpp/simkernel/cXMLElement;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cXMLElement_1getLastChild },
    { (char *)"cXMLElement_getNextSibling", (char *)"(JLorg/omnetpp/simkernel/cXMLElement;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cXMLElement_1getNextSibling },
    { (char *)"cXMLElement_getPreviousSibling", (char *)"(JLorg/omnetpp/simkernel/cXMLElement;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cXMLElement_1getPreviousSibling },
    { (char *)"cXMLElement_getFirstChildWithTag", (char *)"(JLorg/omnetpp/simkernel/cXMLElement;Ljava/lang/String;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cXMLElement_1getFirstChildWithTag },
    { (char *)"cXMLElement_getNextSiblingWithTag", (char *)"(JLorg/omnetpp/simkernel/cXMLElement;Ljava/lang/String;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cXMLElement_1getNextSiblingWithTag },
    { (char *)"cXMLElement_getChildren", (char *)"(JLorg/omnetpp/simkernel/cXMLElement;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cXMLElement_1getChildren },
    { (char *)"cXMLElement_getChildrenByTagName", (char *)"(JLorg/omnetpp/simkernel/cXMLElement;Ljava/lang/String;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cXMLElement_1getChildrenByTagName },
    { (char *)"cXMLElement_getElementsByTagName", (char *)"(JLorg/omnetpp/simkernel/cXMLElement;Ljava/lang/String;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cXMLElement_1getElementsByTagName },
    { (char *)"cXMLElement_getFirstChildWithAttribute__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cXMLElement;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cXMLElement_1getFirstChildWithAttribute_1_1SWIG_10 },
    { (char *)"cXMLElement_getFirstChildWithAttribute__SWIG_1", (char *)"(JLorg/omnetpp/simkernel/cXMLElement;Ljava/lang/String;Ljava/lang/String;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cXMLElement_1getFirstChildWithAttribute_1_1SWIG_11 },
    { (char *)"cXMLElement_getElementById", (char *)"(JLorg/omnetpp/simkernel/cXMLElement;Ljava/lang/String;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cXMLElement_1getElementById },
    { (char *)"intrand", (char *)"(JLorg/omnetpp/simkernel/cRNG;I)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_intrand },
    { (char *)"dblrand", (char *)"(JLorg/omnetpp/simkernel/cRNG;)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_dblrand },
    { (char *)"uniform__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cRNG;DD)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_uniform_1_1SWIG_10 },
    { (char *)"uniform__SWIG_1", (char *)"(JLorg/omnetpp/simkernel/cRNG;JLorg/omnetpp/simkernel/SimTime;JLorg/omnetpp/simkernel/SimTime;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_uniform_1_1SWIG_11 },
    { (char *)"exponential__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cRNG;D)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_exponential_1_1SWIG_10 },
    { (char *)"exponential__SWIG_1", (char *)"(JLorg/omnetpp/simkernel/cRNG;JLorg/omnetpp/simkernel/SimTime;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_exponential_1_1SWIG_11 },
    { (char *)"normal__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cRNG;DD)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_normal_1_1SWIG_10 },
    { (char *)"normal__SWIG_1", (char *)"(JLorg/omnetpp/simkernel/cRNG;JLorg/omnetpp/simkernel/SimTime;JLorg/omnetpp/simkernel/SimTime;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_normal_1_1SWIG_11 },
    { (char *)"truncnormal__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cRNG;DD)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_truncnormal_1_1SWIG_10 },
    { (char *)"truncnormal__SWIG_1", (char *)"(JLorg/omnetpp/simkernel/cRNG;JLorg/omnetpp/simkernel/SimTime;JLorg/omnetpp/simkernel/SimTime;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_truncnormal_1_1SWIG_11 },
    { (char *)"gamma_d", (char *)"(JLorg/omnetpp/simkernel/cRNG;DD)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_gamma_1d },
    { (char *)"beta", (char *)"(JLorg/omnetpp/simkernel/cRNG;DD)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_beta },
    { (char *)"erlang_k", (char *)"(JLorg/omnetpp/simkernel/cRNG;JD)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_erlang_1k },
    { (char *)"chi_square", (char *)"(JLorg/omnetpp/simkernel/cRNG;J)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_chi_1square },
    { (char *)"student_t", (char *)"(JLorg/omnetpp/simkernel/cRNG;J)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_student_1t },
    { (char *)"cauchy", (char *)"(JLorg/omnetpp/simkernel/cRNG;DD)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cauchy },
    { (char *)"triang", (char *)"(JLorg/omnetpp/simkernel/cRNG;DDD)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_triang },
    { (char *)"lognormal", (char *)"(JLorg/omnetpp/simkernel/cRNG;DD)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_lognormal },
    { (char *)"weibull", (char *)"(JLorg/omnetpp/simkernel/cRNG;DD)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_weibull },
    { (char *)"pareto_shifted", (char *)"(JLorg/omnetpp/simkernel/cRNG;DDD)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_pareto_1shifted },
    { (char *)"intuniform", (char *)"(JLorg/omnetpp/simkernel/cRNG;II)I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_intuniform },
    { (char *)"bernoulli", (char *)"(JLorg/omnetpp/simkernel/cRNG;D)I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_bernoulli },
    { (char *)"binomial", (char *)"(JLorg/omnetpp/simkernel/cRNG;ID)I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_binomial },
    { (char *)"geometric", (char *)"(JLorg/omnetpp/simkernel/cRNG;D)I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_geometric },
    { (char *)"negbinomial", (char *)"(JLorg/omnetpp/simkernel/cRNG;ID)I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_negbinomial },
    { (char *)"poisson", (char *)"(JLorg/omnetpp/simkernel/cRNG;D)I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_poisson },
    { (char *)"delete_cIOutputVectorManager", (char *)"(J)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cIOutputVectorManager },
    { (char *)"cIOutputVectorManager_startRun", (char *)"(JLorg/omnetpp/simkernel/cIOutputVectorManager;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cIOutputVectorManager_1startRun },
    { (char *)"cIOutputVectorManager_endRun", (char *)"(JLorg/omnetpp/simkernel/cIOutputVectorManager;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cIOutputVectorManager_1endRun },
    { (char *)"cIOutputVectorManager_registerVector", (char *)"(JLorg/omnetpp/simkernel/cIOutputVectorManager;Ljava/lang/String;Ljava/lang/String;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cIOutputVectorManager_1registerVector },
    { (char *)"cIOutputVectorManager_deregisterVector", (char *)"(JLorg/omnetpp/simkernel/cIOutputVectorManager;J)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cIOutputVectorManager_1deregisterVector },
    { (char *)"cIOutputVectorManager_setVectorAttribute", (char *)"(JLorg/omnetpp/simkernel/cIOutputVectorManager;JLjava/lang/String;Ljava/lang/String;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cIOutputVectorManager_1setVectorAttribute },
    { (char *)"cIOutputVectorManager_record", (char *)"(JLorg/omnetpp/simkernel/cIOutputVectorManager;JJLorg/omnetpp/simkernel/SimTime;D)Z", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cIOutputVectorManager_1record },
    { (char *)"cIOutputVectorManager_getFileName", (char *)"(JLorg/omnetpp/simkernel/cIOutputVectorManager;)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cIOutputVectorManager_1getFileName },
    { (char *)"cIOutputVectorManager_flush", (char *)"(JLorg/omnetpp/simkernel/cIOutputVectorManager;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cIOutputVectorManager_1flush },
    { (char *)"delete_cIOutputScalarManager", (char *)"(J)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cIOutputScalarManager },
    { (char *)"cIOutputScalarManager_startRun", (char *)"(JLorg/omnetpp/simkernel/cIOutputScalarManager;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cIOutputScalarManager_1startRun },
    { (char *)"cIOutputScalarManager_endRun", (char *)"(JLorg/omnetpp/simkernel/cIOutputScalarManager;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cIOutputScalarManager_1endRun },
    { (char *)"cIOutputScalarManager_recordScalar__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cIOutputScalarManager;JLorg/omnetpp/simkernel/cComponent;Ljava/lang/String;DJ)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cIOutputScalarManager_1recordScalar_1_1SWIG_10 },
    { (char *)"cIOutputScalarManager_recordScalar__SWIG_1", (char *)"(JLorg/omnetpp/simkernel/cIOutputScalarManager;JLorg/omnetpp/simkernel/cComponent;Ljava/lang/String;D)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cIOutputScalarManager_1recordScalar_1_1SWIG_11 },
    { (char *)"cIOutputScalarManager_recordStatistic__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cIOutputScalarManager;JLorg/omnetpp/simkernel/cComponent;Ljava/lang/String;JLorg/omnetpp/simkernel/cStatistic;J)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cIOutputScalarManager_1recordStatistic_1_1SWIG_10 },
    { (char *)"cIOutputScalarManager_recordStatistic__SWIG_1", (char *)"(JLorg/omnetpp/simkernel/cIOutputScalarManager;JLorg/omnetpp/simkernel/cComponent;Ljava/lang/String;JLorg/omnetpp/simkernel/cStatistic;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cIOutputScalarManager_1recordStatistic_1_1SWIG_11 },
    { (char *)"cIOutputScalarManager_getFileName", (char *)"(JLorg/omnetpp/simkernel/cIOutputScalarManager;)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cIOutputScalarManager_1getFileName },
    { (char *)"cIOutputScalarManager_flush", (char *)"(JLorg/omnetpp/simkernel/cIOutputScalarManager;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cIOutputScalarManager_1flush },
    { (char *)"delete_cISnapshotManager", (char *)"(J)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cISnapshotManager },
    { (char *)"cISnapshotManager_startRun", (char *)"(JLorg/omnetpp/simkernel/cISnapshotManager;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cISnapshotManager_1startRun },
    { (char *)"cISnapshotManager_endRun", (char *)"(JLorg/omnetpp/simkernel/cISnapshotManager;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cISnapshotManager_1endRun },
    { (char *)"cISnapshotManager_getStreamForSnapshot", (char *)"(JLorg/omnetpp/simkernel/cISnapshotManager;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cISnapshotManager_1getStreamForSnapshot },
    { (char *)"cISnapshotManager_releaseStreamForSnapshot", (char *)"(JLorg/omnetpp/simkernel/cISnapshotManager;J)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cISnapshotManager_1releaseStreamForSnapshot },
    { (char *)"cISnapshotManager_getFileName", (char *)"(JLorg/omnetpp/simkernel/cISnapshotManager;)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cISnapshotManager_1getFileName },
    { (char *)"delete_cIEventlogManager", (char *)"(J)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cIEventlogManager },
    { (char *)"cIEventlogManager_startRecording", (char *)"(JLorg/omnetpp/simkernel/cIEventlogManager;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cIEventlogManager_1startRecording },
    { (char *)"cIEventlogManager_stopRecording", (char *)"(JLorg/omnetpp/simkernel/cIEventlogManager;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cIEventlogManager_1stopRecording },
    { (char *)"cIEventlogManager_flush", (char *)"(JLorg/omnetpp/simkernel/cIEventlogManager;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cIEventlogManager_1flush },
    { (char *)"cIEventlogManager_getFileName", (char *)"(JLorg/omnetpp/simkernel/cIEventlogManager;)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cIEventlogManager_1getFileName },
    { (char *)"cIEventlogManager_simulationEvent", (char *)"(JLorg/omnetpp/simkernel/cIEventlogManager;JLorg/omnetpp/simkernel/cEvent;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cIEventlogManager_1simulationEvent },
    { (char *)"cIEventlogManager_bubble", (char *)"(JLorg/omnetpp/simkernel/cIEventlogManager;JLorg/omnetpp/simkernel/cComponent;Ljava/lang/String;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cIEventlogManager_1bubble },
    { (char *)"cIEventlogManager_messageScheduled", (char *)"(JLorg/omnetpp/simkernel/cIEventlogManager;JLorg/omnetpp/simkernel/cMessage;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cIEventlogManager_1messageScheduled },
    { (char *)"cIEventlogManager_messageCancelled", (char *)"(JLorg/omnetpp/simkernel/cIEventlogManager;JLorg/omnetpp/simkernel/cMessage;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cIEventlogManager_1messageCancelled },
    { (char *)"cIEventlogManager_beginSend", (char *)"(JLorg/omnetpp/simkernel/cIEventlogManager;JLorg/omnetpp/simkernel/cMessage;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cIEventlogManager_1beginSend },
    { (char *)"cIEventlogManager_messageSendDirect", (char *)"(JLorg/omnetpp/simkernel/cIEventlogManager;JLorg/omnetpp/simkernel/cMessage;JLorg/omnetpp/simkernel/cGate;JLorg/omnetpp/simkernel/SimTime;JLorg/omnetpp/simkernel/SimTime;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cIEventlogManager_1messageSendDirect },
    { (char *)"cIEventlogManager_messageSendHop__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cIEventlogManager;JLorg/omnetpp/simkernel/cMessage;JLorg/omnetpp/simkernel/cGate;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cIEventlogManager_1messageSendHop_1_1SWIG_10 },
    { (char *)"cIEventlogManager_messageSendHop__SWIG_1", (char *)"(JLorg/omnetpp/simkernel/cIEventlogManager;JLorg/omnetpp/simkernel/cMessage;JLorg/omnetpp/simkernel/cGate;JLorg/omnetpp/simkernel/SimTime;JLorg/omnetpp/simkernel/SimTime;Z)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cIEventlogManager_1messageSendHop_1_1SWIG_11 },
    { (char *)"cIEventlogManager_endSend", (char *)"(JLorg/omnetpp/simkernel/cIEventlogManager;JLorg/omnetpp/simkernel/cMessage;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cIEventlogManager_1endSend },
    { (char *)"cIEventlogManager_messageCreated", (char *)"(JLorg/omnetpp/simkernel/cIEventlogManager;JLorg/omnetpp/simkernel/cMessage;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cIEventlogManager_1messageCreated },
    { (char *)"cIEventlogManager_messageCloned", (char *)"(JLorg/omnetpp/simkernel/cIEventlogManager;JLorg/omnetpp/simkernel/cMessage;JLorg/omnetpp/simkernel/cMessage;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cIEventlogManager_1messageCloned },
    { (char *)"cIEventlogManager_messageDeleted", (char *)"(JLorg/omnetpp/simkernel/cIEventlogManager;JLorg/omnetpp/simkernel/cMessage;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cIEventlogManager_1messageDeleted },
    { (char *)"cIEventlogManager_moduleReparented", (char *)"(JLorg/omnetpp/simkernel/cIEventlogManager;JLorg/omnetpp/simkernel/cModule;JLorg/omnetpp/simkernel/cModule;I)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cIEventlogManager_1moduleReparented },
    { (char *)"cIEventlogManager_componentMethodBegin", (char *)"(JLorg/omnetpp/simkernel/cIEventlogManager;JLorg/omnetpp/simkernel/cComponent;JLorg/omnetpp/simkernel/cComponent;Ljava/lang/String;J)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cIEventlogManager_1componentMethodBegin },
    { (char *)"cIEventlogManager_componentMethodEnd", (char *)"(JLorg/omnetpp/simkernel/cIEventlogManager;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cIEventlogManager_1componentMethodEnd },
    { (char *)"cIEventlogManager_moduleCreated", (char *)"(JLorg/omnetpp/simkernel/cIEventlogManager;JLorg/omnetpp/simkernel/cModule;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cIEventlogManager_1moduleCreated },
    { (char *)"cIEventlogManager_moduleDeleted", (char *)"(JLorg/omnetpp/simkernel/cIEventlogManager;JLorg/omnetpp/simkernel/cModule;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cIEventlogManager_1moduleDeleted },
    { (char *)"cIEventlogManager_gateCreated", (char *)"(JLorg/omnetpp/simkernel/cIEventlogManager;JLorg/omnetpp/simkernel/cGate;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cIEventlogManager_1gateCreated },
    { (char *)"cIEventlogManager_gateDeleted", (char *)"(JLorg/omnetpp/simkernel/cIEventlogManager;JLorg/omnetpp/simkernel/cGate;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cIEventlogManager_1gateDeleted },
    { (char *)"cIEventlogManager_connectionCreated", (char *)"(JLorg/omnetpp/simkernel/cIEventlogManager;JLorg/omnetpp/simkernel/cGate;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cIEventlogManager_1connectionCreated },
    { (char *)"cIEventlogManager_connectionDeleted", (char *)"(JLorg/omnetpp/simkernel/cIEventlogManager;JLorg/omnetpp/simkernel/cGate;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cIEventlogManager_1connectionDeleted },
    { (char *)"cIEventlogManager_displayStringChanged", (char *)"(JLorg/omnetpp/simkernel/cIEventlogManager;JLorg/omnetpp/simkernel/cComponent;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cIEventlogManager_1displayStringChanged },
    { (char *)"cIEventlogManager_logLine", (char *)"(JLorg/omnetpp/simkernel/cIEventlogManager;Ljava/lang/String;Ljava/lang/String;I)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cIEventlogManager_1logLine },
    { (char *)"cIEventlogManager_stoppedWithException", (char *)"(JLorg/omnetpp/simkernel/cIEventlogManager;ZILjava/lang/String;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cIEventlogManager_1stoppedWithException },
    { (char *)"E_OK_get", (char *)"()I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_E_1OK_1get },
    { (char *)"new_cErrorMessages", (char *)"()J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cErrorMessages },
    { (char *)"cErrorMessages_get", (char *)"(I)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cErrorMessages_1get },
    { (char *)"delete_cErrorMessages", (char *)"(J)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cErrorMessages },
    { (char *)"resultFilters_set", (char *)"(JLorg/omnetpp/simkernel/cGlobalRegistrationList;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_resultFilters_1set },
    { (char *)"resultFilters_get", (char *)"()J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_resultFilters_1get },
    { (char *)"resultRecorders_set", (char *)"(JLorg/omnetpp/simkernel/cGlobalRegistrationList;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_resultRecorders_1set },
    { (char *)"resultRecorders_get", (char *)"()J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_resultRecorders_1get },
    { (char *)"messagePrinters_set", (char *)"(JLorg/omnetpp/simkernel/cGlobalRegistrationList;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_messagePrinters_1set },
    { (char *)"messagePrinters_get", (char *)"()J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_messagePrinters_1get },
    { (char *)"figureTypes_set", (char *)"(JLorg/omnetpp/simkernel/StringMap;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_figureTypes_1set },
    { (char *)"figureTypes_get", (char *)"()J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_figureTypes_1get },
    { (char *)"RELEASE_OPPSIM_MAGIC_NUMBER_get", (char *)"()I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_RELEASE_1OPPSIM_1MAGIC_1NUMBER_1get },
    { (char *)"new_CodeFragments", (char *)"(JI)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1CodeFragments },
    { (char *)"delete_CodeFragments", (char *)"(J)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_delete_1CodeFragments },
    { (char *)"CodeFragments_executeAll", (char *)"(I)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_CodeFragments_1executeAll },
    { (char *)"delete_cRandom", (char *)"(J)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cRandom },
    { (char *)"cRandom_setRNG", (char *)"(JLorg/omnetpp/simkernel/cRandom;JLorg/omnetpp/simkernel/cRNG;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cRandom_1setRNG },
    { (char *)"cRandom_getRNG", (char *)"(JLorg/omnetpp/simkernel/cRandom;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cRandom_1getRNG },
    { (char *)"cRandom_draw", (char *)"(JLorg/omnetpp/simkernel/cRandom;)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cRandom_1draw },
    { (char *)"new_cUniform__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cRNG;DD)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cUniform_1_1SWIG_10 },
    { (char *)"new_cUniform__SWIG_1", (char *)"(Ljava/lang/String;JLorg/omnetpp/simkernel/cRNG;DD)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cUniform_1_1SWIG_11 },
    { (char *)"new_cUniform__SWIG_2", (char *)"(Ljava/lang/String;JLorg/omnetpp/simkernel/cRNG;D)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cUniform_1_1SWIG_12 },
    { (char *)"new_cUniform__SWIG_3", (char *)"(Ljava/lang/String;JLorg/omnetpp/simkernel/cRNG;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cUniform_1_1SWIG_13 },
    { (char *)"new_cUniform__SWIG_4", (char *)"(Ljava/lang/String;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cUniform_1_1SWIG_14 },
    { (char *)"new_cUniform__SWIG_5", (char *)"()J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cUniform_1_1SWIG_15 },
    { (char *)"new_cUniform__SWIG_6", (char *)"(JLorg/omnetpp/simkernel/cUniform;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cUniform_1_1SWIG_16 },
    { (char *)"cUniform_dup", (char *)"(JLorg/omnetpp/simkernel/cUniform;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cUniform_1dup },
    { (char *)"cUniform_assign", (char *)"(JLorg/omnetpp/simkernel/cUniform;JLorg/omnetpp/simkernel/cUniform;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cUniform_1assign },
    { (char *)"cUniform_str", (char *)"(JLorg/omnetpp/simkernel/cUniform;)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cUniform_1str },
    { (char *)"cUniform_setA", (char *)"(JLorg/omnetpp/simkernel/cUniform;D)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cUniform_1setA },
    { (char *)"cUniform_getA", (char *)"(JLorg/omnetpp/simkernel/cUniform;)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cUniform_1getA },
    { (char *)"cUniform_setB", (char *)"(JLorg/omnetpp/simkernel/cUniform;D)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cUniform_1setB },
    { (char *)"cUniform_getB", (char *)"(JLorg/omnetpp/simkernel/cUniform;)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cUniform_1getB },
    { (char *)"cUniform_draw", (char *)"(JLorg/omnetpp/simkernel/cUniform;)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cUniform_1draw },
    { (char *)"delete_cUniform", (char *)"(J)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cUniform },
    { (char *)"new_cExponential__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cRNG;D)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cExponential_1_1SWIG_10 },
    { (char *)"new_cExponential__SWIG_1", (char *)"(Ljava/lang/String;JLorg/omnetpp/simkernel/cRNG;D)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cExponential_1_1SWIG_11 },
    { (char *)"new_cExponential__SWIG_2", (char *)"(Ljava/lang/String;JLorg/omnetpp/simkernel/cRNG;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cExponential_1_1SWIG_12 },
    { (char *)"new_cExponential__SWIG_3", (char *)"(Ljava/lang/String;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cExponential_1_1SWIG_13 },
    { (char *)"new_cExponential__SWIG_4", (char *)"()J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cExponential_1_1SWIG_14 },
    { (char *)"new_cExponential__SWIG_5", (char *)"(JLorg/omnetpp/simkernel/cExponential;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cExponential_1_1SWIG_15 },
    { (char *)"cExponential_dup", (char *)"(JLorg/omnetpp/simkernel/cExponential;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cExponential_1dup },
    { (char *)"cExponential_assign", (char *)"(JLorg/omnetpp/simkernel/cExponential;JLorg/omnetpp/simkernel/cExponential;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cExponential_1assign },
    { (char *)"cExponential_str", (char *)"(JLorg/omnetpp/simkernel/cExponential;)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cExponential_1str },
    { (char *)"cExponential_setMean", (char *)"(JLorg/omnetpp/simkernel/cExponential;D)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cExponential_1setMean },
    { (char *)"cExponential_getMean", (char *)"(JLorg/omnetpp/simkernel/cExponential;)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cExponential_1getMean },
    { (char *)"cExponential_draw", (char *)"(JLorg/omnetpp/simkernel/cExponential;)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cExponential_1draw },
    { (char *)"delete_cExponential", (char *)"(J)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cExponential },
    { (char *)"new_cNormal__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cRNG;DD)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cNormal_1_1SWIG_10 },
    { (char *)"new_cNormal__SWIG_1", (char *)"(Ljava/lang/String;JLorg/omnetpp/simkernel/cRNG;DD)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cNormal_1_1SWIG_11 },
    { (char *)"new_cNormal__SWIG_2", (char *)"(Ljava/lang/String;JLorg/omnetpp/simkernel/cRNG;D)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cNormal_1_1SWIG_12 },
    { (char *)"new_cNormal__SWIG_3", (char *)"(Ljava/lang/String;JLorg/omnetpp/simkernel/cRNG;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cNormal_1_1SWIG_13 },
    { (char *)"new_cNormal__SWIG_4", (char *)"(Ljava/lang/String;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cNormal_1_1SWIG_14 },
    { (char *)"new_cNormal__SWIG_5", (char *)"()J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cNormal_1_1SWIG_15 },
    { (char *)"new_cNormal__SWIG_6", (char *)"(JLorg/omnetpp/simkernel/cNormal;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cNormal_1_1SWIG_16 },
    { (char *)"cNormal_dup", (char *)"(JLorg/omnetpp/simkernel/cNormal;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cNormal_1dup },
    { (char *)"cNormal_assign", (char *)"(JLorg/omnetpp/simkernel/cNormal;JLorg/omnetpp/simkernel/cNormal;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cNormal_1assign },
    { (char *)"cNormal_str", (char *)"(JLorg/omnetpp/simkernel/cNormal;)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cNormal_1str },
    { (char *)"cNormal_setMean", (char *)"(JLorg/omnetpp/simkernel/cNormal;D)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cNormal_1setMean },
    { (char *)"cNormal_getMean", (char *)"(JLorg/omnetpp/simkernel/cNormal;)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cNormal_1getMean },
    { (char *)"cNormal_setStddev", (char *)"(JLorg/omnetpp/simkernel/cNormal;D)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cNormal_1setStddev },
    { (char *)"cNormal_getStddev", (char *)"(JLorg/omnetpp/simkernel/cNormal;)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cNormal_1getStddev },
    { (char *)"cNormal_draw", (char *)"(JLorg/omnetpp/simkernel/cNormal;)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cNormal_1draw },
    { (char *)"delete_cNormal", (char *)"(J)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cNormal },
    { (char *)"new_cTruncNormal__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cRNG;DD)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cTruncNormal_1_1SWIG_10 },
    { (char *)"new_cTruncNormal__SWIG_1", (char *)"(Ljava/lang/String;JLorg/omnetpp/simkernel/cRNG;DD)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cTruncNormal_1_1SWIG_11 },
    { (char *)"new_cTruncNormal__SWIG_2", (char *)"(Ljava/lang/String;JLorg/omnetpp/simkernel/cRNG;D)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cTruncNormal_1_1SWIG_12 },
    { (char *)"new_cTruncNormal__SWIG_3", (char *)"(Ljava/lang/String;JLorg/omnetpp/simkernel/cRNG;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cTruncNormal_1_1SWIG_13 },
    { (char *)"new_cTruncNormal__SWIG_4", (char *)"(Ljava/lang/String;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cTruncNormal_1_1SWIG_14 },
    { (char *)"new_cTruncNormal__SWIG_5", (char *)"()J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cTruncNormal_1_1SWIG_15 },
    { (char *)"new_cTruncNormal__SWIG_6", (char *)"(JLorg/omnetpp/simkernel/cTruncNormal;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cTruncNormal_1_1SWIG_16 },
    { (char *)"cTruncNormal_dup", (char *)"(JLorg/omnetpp/simkernel/cTruncNormal;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cTruncNormal_1dup },
    { (char *)"cTruncNormal_assign", (char *)"(JLorg/omnetpp/simkernel/cTruncNormal;JLorg/omnetpp/simkernel/cTruncNormal;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cTruncNormal_1assign },
    { (char *)"cTruncNormal_str", (char *)"(JLorg/omnetpp/simkernel/cTruncNormal;)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cTruncNormal_1str },
    { (char *)"cTruncNormal_setMean", (char *)"(JLorg/omnetpp/simkernel/cTruncNormal;D)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cTruncNormal_1setMean },
    { (char *)"cTruncNormal_getMean", (char *)"(JLorg/omnetpp/simkernel/cTruncNormal;)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cTruncNormal_1getMean },
    { (char *)"cTruncNormal_setStddev", (char *)"(JLorg/omnetpp/simkernel/cTruncNormal;D)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cTruncNormal_1setStddev },
    { (char *)"cTruncNormal_getStddev", (char *)"(JLorg/omnetpp/simkernel/cTruncNormal;)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cTruncNormal_1getStddev },
    { (char *)"cTruncNormal_draw", (char *)"(JLorg/omnetpp/simkernel/cTruncNormal;)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cTruncNormal_1draw },
    { (char *)"delete_cTruncNormal", (char *)"(J)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cTruncNormal },
    { (char *)"new_cGamma__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cRNG;DD)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cGamma_1_1SWIG_10 },
    { (char *)"new_cGamma__SWIG_1", (char *)"(Ljava/lang/String;JLorg/omnetpp/simkernel/cRNG;DD)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cGamma_1_1SWIG_11 },
    { (char *)"new_cGamma__SWIG_2", (char *)"(Ljava/lang/String;JLorg/omnetpp/simkernel/cRNG;D)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cGamma_1_1SWIG_12 },
    { (char *)"new_cGamma__SWIG_3", (char *)"(Ljava/lang/String;JLorg/omnetpp/simkernel/cRNG;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cGamma_1_1SWIG_13 },
    { (char *)"new_cGamma__SWIG_4", (char *)"(Ljava/lang/String;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cGamma_1_1SWIG_14 },
    { (char *)"new_cGamma__SWIG_5", (char *)"()J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cGamma_1_1SWIG_15 },
    { (char *)"new_cGamma__SWIG_6", (char *)"(JLorg/omnetpp/simkernel/cGamma;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cGamma_1_1SWIG_16 },
    { (char *)"cGamma_dup", (char *)"(JLorg/omnetpp/simkernel/cGamma;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cGamma_1dup },
    { (char *)"cGamma_assign", (char *)"(JLorg/omnetpp/simkernel/cGamma;JLorg/omnetpp/simkernel/cGamma;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cGamma_1assign },
    { (char *)"cGamma_str", (char *)"(JLorg/omnetpp/simkernel/cGamma;)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cGamma_1str },
    { (char *)"cGamma_setAlpha", (char *)"(JLorg/omnetpp/simkernel/cGamma;D)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cGamma_1setAlpha },
    { (char *)"cGamma_getAlpha", (char *)"(JLorg/omnetpp/simkernel/cGamma;)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cGamma_1getAlpha },
    { (char *)"cGamma_setTheta", (char *)"(JLorg/omnetpp/simkernel/cGamma;D)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cGamma_1setTheta },
    { (char *)"cGamma_getTheta", (char *)"(JLorg/omnetpp/simkernel/cGamma;)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cGamma_1getTheta },
    { (char *)"cGamma_draw", (char *)"(JLorg/omnetpp/simkernel/cGamma;)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cGamma_1draw },
    { (char *)"delete_cGamma", (char *)"(J)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cGamma },
    { (char *)"new_cBeta__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cRNG;DD)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cBeta_1_1SWIG_10 },
    { (char *)"new_cBeta__SWIG_1", (char *)"(Ljava/lang/String;JLorg/omnetpp/simkernel/cRNG;DD)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cBeta_1_1SWIG_11 },
    { (char *)"new_cBeta__SWIG_2", (char *)"(Ljava/lang/String;JLorg/omnetpp/simkernel/cRNG;D)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cBeta_1_1SWIG_12 },
    { (char *)"new_cBeta__SWIG_3", (char *)"(Ljava/lang/String;JLorg/omnetpp/simkernel/cRNG;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cBeta_1_1SWIG_13 },
    { (char *)"new_cBeta__SWIG_4", (char *)"(Ljava/lang/String;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cBeta_1_1SWIG_14 },
    { (char *)"new_cBeta__SWIG_5", (char *)"()J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cBeta_1_1SWIG_15 },
    { (char *)"new_cBeta__SWIG_6", (char *)"(JLorg/omnetpp/simkernel/cBeta;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cBeta_1_1SWIG_16 },
    { (char *)"cBeta_dup", (char *)"(JLorg/omnetpp/simkernel/cBeta;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cBeta_1dup },
    { (char *)"cBeta_assign", (char *)"(JLorg/omnetpp/simkernel/cBeta;JLorg/omnetpp/simkernel/cBeta;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cBeta_1assign },
    { (char *)"cBeta_str", (char *)"(JLorg/omnetpp/simkernel/cBeta;)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cBeta_1str },
    { (char *)"cBeta_setAlpha1", (char *)"(JLorg/omnetpp/simkernel/cBeta;D)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cBeta_1setAlpha1 },
    { (char *)"cBeta_getAlpha1", (char *)"(JLorg/omnetpp/simkernel/cBeta;)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cBeta_1getAlpha1 },
    { (char *)"cBeta_setAlpha2", (char *)"(JLorg/omnetpp/simkernel/cBeta;D)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cBeta_1setAlpha2 },
    { (char *)"cBeta_getAlpha2", (char *)"(JLorg/omnetpp/simkernel/cBeta;)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cBeta_1getAlpha2 },
    { (char *)"cBeta_draw", (char *)"(JLorg/omnetpp/simkernel/cBeta;)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cBeta_1draw },
    { (char *)"delete_cBeta", (char *)"(J)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cBeta },
    { (char *)"new_cErlang__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cRNG;JD)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cErlang_1_1SWIG_10 },
    { (char *)"new_cErlang__SWIG_1", (char *)"(Ljava/lang/String;JLorg/omnetpp/simkernel/cRNG;JD)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cErlang_1_1SWIG_11 },
    { (char *)"new_cErlang__SWIG_2", (char *)"(Ljava/lang/String;JLorg/omnetpp/simkernel/cRNG;J)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cErlang_1_1SWIG_12 },
    { (char *)"new_cErlang__SWIG_3", (char *)"(Ljava/lang/String;JLorg/omnetpp/simkernel/cRNG;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cErlang_1_1SWIG_13 },
    { (char *)"new_cErlang__SWIG_4", (char *)"(Ljava/lang/String;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cErlang_1_1SWIG_14 },
    { (char *)"new_cErlang__SWIG_5", (char *)"()J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cErlang_1_1SWIG_15 },
    { (char *)"new_cErlang__SWIG_6", (char *)"(JLorg/omnetpp/simkernel/cErlang;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cErlang_1_1SWIG_16 },
    { (char *)"cErlang_dup", (char *)"(JLorg/omnetpp/simkernel/cErlang;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cErlang_1dup },
    { (char *)"cErlang_assign", (char *)"(JLorg/omnetpp/simkernel/cErlang;JLorg/omnetpp/simkernel/cErlang;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cErlang_1assign },
    { (char *)"cErlang_str", (char *)"(JLorg/omnetpp/simkernel/cErlang;)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cErlang_1str },
    { (char *)"cErlang_setK", (char *)"(JLorg/omnetpp/simkernel/cErlang;J)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cErlang_1setK },
    { (char *)"cErlang_getK", (char *)"(JLorg/omnetpp/simkernel/cErlang;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cErlang_1getK },
    { (char *)"cErlang_setMean", (char *)"(JLorg/omnetpp/simkernel/cErlang;D)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cErlang_1setMean },
    { (char *)"cErlang_getMean", (char *)"(JLorg/omnetpp/simkernel/cErlang;)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cErlang_1getMean },
    { (char *)"cErlang_draw", (char *)"(JLorg/omnetpp/simkernel/cErlang;)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cErlang_1draw },
    { (char *)"delete_cErlang", (char *)"(J)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cErlang },
    { (char *)"new_cChiSquare__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cRNG;J)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cChiSquare_1_1SWIG_10 },
    { (char *)"new_cChiSquare__SWIG_1", (char *)"(Ljava/lang/String;JLorg/omnetpp/simkernel/cRNG;J)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cChiSquare_1_1SWIG_11 },
    { (char *)"new_cChiSquare__SWIG_2", (char *)"(Ljava/lang/String;JLorg/omnetpp/simkernel/cRNG;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cChiSquare_1_1SWIG_12 },
    { (char *)"new_cChiSquare__SWIG_3", (char *)"(Ljava/lang/String;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cChiSquare_1_1SWIG_13 },
    { (char *)"new_cChiSquare__SWIG_4", (char *)"()J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cChiSquare_1_1SWIG_14 },
    { (char *)"new_cChiSquare__SWIG_5", (char *)"(JLorg/omnetpp/simkernel/cChiSquare;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cChiSquare_1_1SWIG_15 },
    { (char *)"cChiSquare_dup", (char *)"(JLorg/omnetpp/simkernel/cChiSquare;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cChiSquare_1dup },
    { (char *)"cChiSquare_assign", (char *)"(JLorg/omnetpp/simkernel/cChiSquare;JLorg/omnetpp/simkernel/cChiSquare;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cChiSquare_1assign },
    { (char *)"cChiSquare_str", (char *)"(JLorg/omnetpp/simkernel/cChiSquare;)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cChiSquare_1str },
    { (char *)"cChiSquare_setK", (char *)"(JLorg/omnetpp/simkernel/cChiSquare;J)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cChiSquare_1setK },
    { (char *)"cChiSquare_getK", (char *)"(JLorg/omnetpp/simkernel/cChiSquare;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cChiSquare_1getK },
    { (char *)"cChiSquare_draw", (char *)"(JLorg/omnetpp/simkernel/cChiSquare;)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cChiSquare_1draw },
    { (char *)"delete_cChiSquare", (char *)"(J)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cChiSquare },
    { (char *)"new_cStudentT__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cRNG;J)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cStudentT_1_1SWIG_10 },
    { (char *)"new_cStudentT__SWIG_1", (char *)"(Ljava/lang/String;JLorg/omnetpp/simkernel/cRNG;J)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cStudentT_1_1SWIG_11 },
    { (char *)"new_cStudentT__SWIG_2", (char *)"(Ljava/lang/String;JLorg/omnetpp/simkernel/cRNG;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cStudentT_1_1SWIG_12 },
    { (char *)"new_cStudentT__SWIG_3", (char *)"(Ljava/lang/String;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cStudentT_1_1SWIG_13 },
    { (char *)"new_cStudentT__SWIG_4", (char *)"()J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cStudentT_1_1SWIG_14 },
    { (char *)"new_cStudentT__SWIG_5", (char *)"(JLorg/omnetpp/simkernel/cStudentT;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cStudentT_1_1SWIG_15 },
    { (char *)"cStudentT_dup", (char *)"(JLorg/omnetpp/simkernel/cStudentT;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cStudentT_1dup },
    { (char *)"cStudentT_assign", (char *)"(JLorg/omnetpp/simkernel/cStudentT;JLorg/omnetpp/simkernel/cStudentT;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cStudentT_1assign },
    { (char *)"cStudentT_str", (char *)"(JLorg/omnetpp/simkernel/cStudentT;)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cStudentT_1str },
    { (char *)"cStudentT_setI", (char *)"(JLorg/omnetpp/simkernel/cStudentT;J)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cStudentT_1setI },
    { (char *)"cStudentT_getI", (char *)"(JLorg/omnetpp/simkernel/cStudentT;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cStudentT_1getI },
    { (char *)"cStudentT_draw", (char *)"(JLorg/omnetpp/simkernel/cStudentT;)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cStudentT_1draw },
    { (char *)"delete_cStudentT", (char *)"(J)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cStudentT },
    { (char *)"new_cCauchy__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cRNG;DD)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cCauchy_1_1SWIG_10 },
    { (char *)"new_cCauchy__SWIG_1", (char *)"(Ljava/lang/String;JLorg/omnetpp/simkernel/cRNG;DD)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cCauchy_1_1SWIG_11 },
    { (char *)"new_cCauchy__SWIG_2", (char *)"(Ljava/lang/String;JLorg/omnetpp/simkernel/cRNG;D)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cCauchy_1_1SWIG_12 },
    { (char *)"new_cCauchy__SWIG_3", (char *)"(Ljava/lang/String;JLorg/omnetpp/simkernel/cRNG;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cCauchy_1_1SWIG_13 },
    { (char *)"new_cCauchy__SWIG_4", (char *)"(Ljava/lang/String;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cCauchy_1_1SWIG_14 },
    { (char *)"new_cCauchy__SWIG_5", (char *)"()J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cCauchy_1_1SWIG_15 },
    { (char *)"new_cCauchy__SWIG_6", (char *)"(JLorg/omnetpp/simkernel/cCauchy;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cCauchy_1_1SWIG_16 },
    { (char *)"cCauchy_dup", (char *)"(JLorg/omnetpp/simkernel/cCauchy;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cCauchy_1dup },
    { (char *)"cCauchy_assign", (char *)"(JLorg/omnetpp/simkernel/cCauchy;JLorg/omnetpp/simkernel/cCauchy;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cCauchy_1assign },
    { (char *)"cCauchy_str", (char *)"(JLorg/omnetpp/simkernel/cCauchy;)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cCauchy_1str },
    { (char *)"cCauchy_setA", (char *)"(JLorg/omnetpp/simkernel/cCauchy;D)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cCauchy_1setA },
    { (char *)"cCauchy_getA", (char *)"(JLorg/omnetpp/simkernel/cCauchy;)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cCauchy_1getA },
    { (char *)"cCauchy_setB", (char *)"(JLorg/omnetpp/simkernel/cCauchy;D)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cCauchy_1setB },
    { (char *)"cCauchy_getB", (char *)"(JLorg/omnetpp/simkernel/cCauchy;)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cCauchy_1getB },
    { (char *)"cCauchy_draw", (char *)"(JLorg/omnetpp/simkernel/cCauchy;)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cCauchy_1draw },
    { (char *)"delete_cCauchy", (char *)"(J)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cCauchy },
    { (char *)"new_cTriang__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cRNG;DDD)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cTriang_1_1SWIG_10 },
    { (char *)"new_cTriang__SWIG_1", (char *)"(Ljava/lang/String;JLorg/omnetpp/simkernel/cRNG;DDD)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cTriang_1_1SWIG_11 },
    { (char *)"new_cTriang__SWIG_2", (char *)"(Ljava/lang/String;JLorg/omnetpp/simkernel/cRNG;DD)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cTriang_1_1SWIG_12 },
    { (char *)"new_cTriang__SWIG_3", (char *)"(Ljava/lang/String;JLorg/omnetpp/simkernel/cRNG;D)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cTriang_1_1SWIG_13 },
    { (char *)"new_cTriang__SWIG_4", (char *)"(Ljava/lang/String;JLorg/omnetpp/simkernel/cRNG;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cTriang_1_1SWIG_14 },
    { (char *)"new_cTriang__SWIG_5", (char *)"(Ljava/lang/String;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cTriang_1_1SWIG_15 },
    { (char *)"new_cTriang__SWIG_6", (char *)"()J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cTriang_1_1SWIG_16 },
    { (char *)"new_cTriang__SWIG_7", (char *)"(JLorg/omnetpp/simkernel/cTriang;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cTriang_1_1SWIG_17 },
    { (char *)"cTriang_dup", (char *)"(JLorg/omnetpp/simkernel/cTriang;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cTriang_1dup },
    { (char *)"cTriang_assign", (char *)"(JLorg/omnetpp/simkernel/cTriang;JLorg/omnetpp/simkernel/cTriang;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cTriang_1assign },
    { (char *)"cTriang_str", (char *)"(JLorg/omnetpp/simkernel/cTriang;)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cTriang_1str },
    { (char *)"cTriang_setA", (char *)"(JLorg/omnetpp/simkernel/cTriang;D)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cTriang_1setA },
    { (char *)"cTriang_getA", (char *)"(JLorg/omnetpp/simkernel/cTriang;)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cTriang_1getA },
    { (char *)"cTriang_setB", (char *)"(JLorg/omnetpp/simkernel/cTriang;D)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cTriang_1setB },
    { (char *)"cTriang_getB", (char *)"(JLorg/omnetpp/simkernel/cTriang;)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cTriang_1getB },
    { (char *)"cTriang_setC", (char *)"(JLorg/omnetpp/simkernel/cTriang;D)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cTriang_1setC },
    { (char *)"cTriang_getC", (char *)"(JLorg/omnetpp/simkernel/cTriang;)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cTriang_1getC },
    { (char *)"cTriang_draw", (char *)"(JLorg/omnetpp/simkernel/cTriang;)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cTriang_1draw },
    { (char *)"delete_cTriang", (char *)"(J)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cTriang },
    { (char *)"new_cWeibull__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cRNG;DD)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cWeibull_1_1SWIG_10 },
    { (char *)"new_cWeibull__SWIG_1", (char *)"(Ljava/lang/String;JLorg/omnetpp/simkernel/cRNG;DD)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cWeibull_1_1SWIG_11 },
    { (char *)"new_cWeibull__SWIG_2", (char *)"(Ljava/lang/String;JLorg/omnetpp/simkernel/cRNG;D)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cWeibull_1_1SWIG_12 },
    { (char *)"new_cWeibull__SWIG_3", (char *)"(Ljava/lang/String;JLorg/omnetpp/simkernel/cRNG;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cWeibull_1_1SWIG_13 },
    { (char *)"new_cWeibull__SWIG_4", (char *)"(Ljava/lang/String;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cWeibull_1_1SWIG_14 },
    { (char *)"new_cWeibull__SWIG_5", (char *)"()J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cWeibull_1_1SWIG_15 },
    { (char *)"new_cWeibull__SWIG_6", (char *)"(JLorg/omnetpp/simkernel/cWeibull;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cWeibull_1_1SWIG_16 },
    { (char *)"cWeibull_dup", (char *)"(JLorg/omnetpp/simkernel/cWeibull;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cWeibull_1dup },
    { (char *)"cWeibull_assign", (char *)"(JLorg/omnetpp/simkernel/cWeibull;JLorg/omnetpp/simkernel/cWeibull;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cWeibull_1assign },
    { (char *)"cWeibull_str", (char *)"(JLorg/omnetpp/simkernel/cWeibull;)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cWeibull_1str },
    { (char *)"cWeibull_setA", (char *)"(JLorg/omnetpp/simkernel/cWeibull;D)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cWeibull_1setA },
    { (char *)"cWeibull_getA", (char *)"(JLorg/omnetpp/simkernel/cWeibull;)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cWeibull_1getA },
    { (char *)"cWeibull_setB", (char *)"(JLorg/omnetpp/simkernel/cWeibull;D)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cWeibull_1setB },
    { (char *)"cWeibull_getB", (char *)"(JLorg/omnetpp/simkernel/cWeibull;)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cWeibull_1getB },
    { (char *)"cWeibull_draw", (char *)"(JLorg/omnetpp/simkernel/cWeibull;)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cWeibull_1draw },
    { (char *)"delete_cWeibull", (char *)"(J)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cWeibull },
    { (char *)"new_cParetoShifted__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cRNG;DDD)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cParetoShifted_1_1SWIG_10 },
    { (char *)"new_cParetoShifted__SWIG_1", (char *)"(Ljava/lang/String;JLorg/omnetpp/simkernel/cRNG;DDD)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cParetoShifted_1_1SWIG_11 },
    { (char *)"new_cParetoShifted__SWIG_2", (char *)"(Ljava/lang/String;JLorg/omnetpp/simkernel/cRNG;DD)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cParetoShifted_1_1SWIG_12 },
    { (char *)"new_cParetoShifted__SWIG_3", (char *)"(Ljava/lang/String;JLorg/omnetpp/simkernel/cRNG;D)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cParetoShifted_1_1SWIG_13 },
    { (char *)"new_cParetoShifted__SWIG_4", (char *)"(Ljava/lang/String;JLorg/omnetpp/simkernel/cRNG;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cParetoShifted_1_1SWIG_14 },
    { (char *)"new_cParetoShifted__SWIG_5", (char *)"(Ljava/lang/String;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cParetoShifted_1_1SWIG_15 },
    { (char *)"new_cParetoShifted__SWIG_6", (char *)"()J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cParetoShifted_1_1SWIG_16 },
    { (char *)"new_cParetoShifted__SWIG_7", (char *)"(JLorg/omnetpp/simkernel/cParetoShifted;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cParetoShifted_1_1SWIG_17 },
    { (char *)"cParetoShifted_dup", (char *)"(JLorg/omnetpp/simkernel/cParetoShifted;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cParetoShifted_1dup },
    { (char *)"cParetoShifted_assign", (char *)"(JLorg/omnetpp/simkernel/cParetoShifted;JLorg/omnetpp/simkernel/cParetoShifted;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cParetoShifted_1assign },
    { (char *)"cParetoShifted_str", (char *)"(JLorg/omnetpp/simkernel/cParetoShifted;)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cParetoShifted_1str },
    { (char *)"cParetoShifted_setA", (char *)"(JLorg/omnetpp/simkernel/cParetoShifted;D)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cParetoShifted_1setA },
    { (char *)"cParetoShifted_getA", (char *)"(JLorg/omnetpp/simkernel/cParetoShifted;)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cParetoShifted_1getA },
    { (char *)"cParetoShifted_setB", (char *)"(JLorg/omnetpp/simkernel/cParetoShifted;D)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cParetoShifted_1setB },
    { (char *)"cParetoShifted_getB", (char *)"(JLorg/omnetpp/simkernel/cParetoShifted;)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cParetoShifted_1getB },
    { (char *)"cParetoShifted_setC", (char *)"(JLorg/omnetpp/simkernel/cParetoShifted;D)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cParetoShifted_1setC },
    { (char *)"cParetoShifted_getC", (char *)"(JLorg/omnetpp/simkernel/cParetoShifted;)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cParetoShifted_1getC },
    { (char *)"cParetoShifted_draw", (char *)"(JLorg/omnetpp/simkernel/cParetoShifted;)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cParetoShifted_1draw },
    { (char *)"delete_cParetoShifted", (char *)"(J)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cParetoShifted },
    { (char *)"new_cIntUniform__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cRNG;II)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cIntUniform_1_1SWIG_10 },
    { (char *)"new_cIntUniform__SWIG_1", (char *)"(Ljava/lang/String;JLorg/omnetpp/simkernel/cRNG;II)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cIntUniform_1_1SWIG_11 },
    { (char *)"new_cIntUniform__SWIG_2", (char *)"(Ljava/lang/String;JLorg/omnetpp/simkernel/cRNG;I)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cIntUniform_1_1SWIG_12 },
    { (char *)"new_cIntUniform__SWIG_3", (char *)"(Ljava/lang/String;JLorg/omnetpp/simkernel/cRNG;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cIntUniform_1_1SWIG_13 },
    { (char *)"new_cIntUniform__SWIG_4", (char *)"(Ljava/lang/String;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cIntUniform_1_1SWIG_14 },
    { (char *)"new_cIntUniform__SWIG_5", (char *)"()J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cIntUniform_1_1SWIG_15 },
    { (char *)"new_cIntUniform__SWIG_6", (char *)"(JLorg/omnetpp/simkernel/cIntUniform;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cIntUniform_1_1SWIG_16 },
    { (char *)"cIntUniform_dup", (char *)"(JLorg/omnetpp/simkernel/cIntUniform;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cIntUniform_1dup },
    { (char *)"cIntUniform_assign", (char *)"(JLorg/omnetpp/simkernel/cIntUniform;JLorg/omnetpp/simkernel/cIntUniform;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cIntUniform_1assign },
    { (char *)"cIntUniform_str", (char *)"(JLorg/omnetpp/simkernel/cIntUniform;)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cIntUniform_1str },
    { (char *)"cIntUniform_setA", (char *)"(JLorg/omnetpp/simkernel/cIntUniform;I)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cIntUniform_1setA },
    { (char *)"cIntUniform_getA", (char *)"(JLorg/omnetpp/simkernel/cIntUniform;)I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cIntUniform_1getA },
    { (char *)"cIntUniform_setB", (char *)"(JLorg/omnetpp/simkernel/cIntUniform;I)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cIntUniform_1setB },
    { (char *)"cIntUniform_getB", (char *)"(JLorg/omnetpp/simkernel/cIntUniform;)I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cIntUniform_1getB },
    { (char *)"cIntUniform_draw", (char *)"(JLorg/omnetpp/simkernel/cIntUniform;)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cIntUniform_1draw },
    { (char *)"delete_cIntUniform", (char *)"(J)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cIntUniform },
    { (char *)"new_cBernoulli__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cRNG;D)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cBernoulli_1_1SWIG_10 },
    { (char *)"new_cBernoulli__SWIG_1", (char *)"(Ljava/lang/String;JLorg/omnetpp/simkernel/cRNG;D)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cBernoulli_1_1SWIG_11 },
    { (char *)"new_cBernoulli__SWIG_2", (char *)"(Ljava/lang/String;JLorg/omnetpp/simkernel/cRNG;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cBernoulli_1_1SWIG_12 },
    { (char *)"new_cBernoulli__SWIG_3", (char *)"(Ljava/lang/String;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cBernoulli_1_1SWIG_13 },
    { (char *)"new_cBernoulli__SWIG_4", (char *)"()J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cBernoulli_1_1SWIG_14 },
    { (char *)"new_cBernoulli__SWIG_5", (char *)"(JLorg/omnetpp/simkernel/cBernoulli;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cBernoulli_1_1SWIG_15 },
    { (char *)"cBernoulli_dup", (char *)"(JLorg/omnetpp/simkernel/cBernoulli;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cBernoulli_1dup },
    { (char *)"cBernoulli_assign", (char *)"(JLorg/omnetpp/simkernel/cBernoulli;JLorg/omnetpp/simkernel/cBernoulli;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cBernoulli_1assign },
    { (char *)"cBernoulli_str", (char *)"(JLorg/omnetpp/simkernel/cBernoulli;)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cBernoulli_1str },
    { (char *)"cBernoulli_setP", (char *)"(JLorg/omnetpp/simkernel/cBernoulli;D)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cBernoulli_1setP },
    { (char *)"cBernoulli_getP", (char *)"(JLorg/omnetpp/simkernel/cBernoulli;)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cBernoulli_1getP },
    { (char *)"cBernoulli_draw", (char *)"(JLorg/omnetpp/simkernel/cBernoulli;)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cBernoulli_1draw },
    { (char *)"delete_cBernoulli", (char *)"(J)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cBernoulli },
    { (char *)"new_cBinomial__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cRNG;ID)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cBinomial_1_1SWIG_10 },
    { (char *)"new_cBinomial__SWIG_1", (char *)"(Ljava/lang/String;JLorg/omnetpp/simkernel/cRNG;ID)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cBinomial_1_1SWIG_11 },
    { (char *)"new_cBinomial__SWIG_2", (char *)"(Ljava/lang/String;JLorg/omnetpp/simkernel/cRNG;I)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cBinomial_1_1SWIG_12 },
    { (char *)"new_cBinomial__SWIG_3", (char *)"(Ljava/lang/String;JLorg/omnetpp/simkernel/cRNG;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cBinomial_1_1SWIG_13 },
    { (char *)"new_cBinomial__SWIG_4", (char *)"(Ljava/lang/String;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cBinomial_1_1SWIG_14 },
    { (char *)"new_cBinomial__SWIG_5", (char *)"()J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cBinomial_1_1SWIG_15 },
    { (char *)"new_cBinomial__SWIG_6", (char *)"(JLorg/omnetpp/simkernel/cBinomial;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cBinomial_1_1SWIG_16 },
    { (char *)"cBinomial_dup", (char *)"(JLorg/omnetpp/simkernel/cBinomial;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cBinomial_1dup },
    { (char *)"cBinomial_assign", (char *)"(JLorg/omnetpp/simkernel/cBinomial;JLorg/omnetpp/simkernel/cBinomial;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cBinomial_1assign },
    { (char *)"cBinomial_str", (char *)"(JLorg/omnetpp/simkernel/cBinomial;)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cBinomial_1str },
    { (char *)"cBinomial_setN", (char *)"(JLorg/omnetpp/simkernel/cBinomial;I)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cBinomial_1setN },
    { (char *)"cBinomial_getN", (char *)"(JLorg/omnetpp/simkernel/cBinomial;)I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cBinomial_1getN },
    { (char *)"cBinomial_setP", (char *)"(JLorg/omnetpp/simkernel/cBinomial;D)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cBinomial_1setP },
    { (char *)"cBinomial_getP", (char *)"(JLorg/omnetpp/simkernel/cBinomial;)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cBinomial_1getP },
    { (char *)"cBinomial_draw", (char *)"(JLorg/omnetpp/simkernel/cBinomial;)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cBinomial_1draw },
    { (char *)"delete_cBinomial", (char *)"(J)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cBinomial },
    { (char *)"new_cGeometric__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cRNG;D)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cGeometric_1_1SWIG_10 },
    { (char *)"new_cGeometric__SWIG_1", (char *)"(Ljava/lang/String;JLorg/omnetpp/simkernel/cRNG;D)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cGeometric_1_1SWIG_11 },
    { (char *)"new_cGeometric__SWIG_2", (char *)"(Ljava/lang/String;JLorg/omnetpp/simkernel/cRNG;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cGeometric_1_1SWIG_12 },
    { (char *)"new_cGeometric__SWIG_3", (char *)"(Ljava/lang/String;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cGeometric_1_1SWIG_13 },
    { (char *)"new_cGeometric__SWIG_4", (char *)"()J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cGeometric_1_1SWIG_14 },
    { (char *)"new_cGeometric__SWIG_5", (char *)"(JLorg/omnetpp/simkernel/cGeometric;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cGeometric_1_1SWIG_15 },
    { (char *)"cGeometric_dup", (char *)"(JLorg/omnetpp/simkernel/cGeometric;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cGeometric_1dup },
    { (char *)"cGeometric_assign", (char *)"(JLorg/omnetpp/simkernel/cGeometric;JLorg/omnetpp/simkernel/cGeometric;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cGeometric_1assign },
    { (char *)"cGeometric_str", (char *)"(JLorg/omnetpp/simkernel/cGeometric;)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cGeometric_1str },
    { (char *)"cGeometric_setP", (char *)"(JLorg/omnetpp/simkernel/cGeometric;D)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cGeometric_1setP },
    { (char *)"cGeometric_getP", (char *)"(JLorg/omnetpp/simkernel/cGeometric;)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cGeometric_1getP },
    { (char *)"cGeometric_draw", (char *)"(JLorg/omnetpp/simkernel/cGeometric;)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cGeometric_1draw },
    { (char *)"delete_cGeometric", (char *)"(J)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cGeometric },
    { (char *)"new_cNegBinomial__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cRNG;ID)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cNegBinomial_1_1SWIG_10 },
    { (char *)"new_cNegBinomial__SWIG_1", (char *)"(Ljava/lang/String;JLorg/omnetpp/simkernel/cRNG;ID)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cNegBinomial_1_1SWIG_11 },
    { (char *)"new_cNegBinomial__SWIG_2", (char *)"(Ljava/lang/String;JLorg/omnetpp/simkernel/cRNG;I)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cNegBinomial_1_1SWIG_12 },
    { (char *)"new_cNegBinomial__SWIG_3", (char *)"(Ljava/lang/String;JLorg/omnetpp/simkernel/cRNG;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cNegBinomial_1_1SWIG_13 },
    { (char *)"new_cNegBinomial__SWIG_4", (char *)"(Ljava/lang/String;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cNegBinomial_1_1SWIG_14 },
    { (char *)"new_cNegBinomial__SWIG_5", (char *)"()J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cNegBinomial_1_1SWIG_15 },
    { (char *)"new_cNegBinomial__SWIG_6", (char *)"(JLorg/omnetpp/simkernel/cNegBinomial;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cNegBinomial_1_1SWIG_16 },
    { (char *)"cNegBinomial_dup", (char *)"(JLorg/omnetpp/simkernel/cNegBinomial;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cNegBinomial_1dup },
    { (char *)"cNegBinomial_assign", (char *)"(JLorg/omnetpp/simkernel/cNegBinomial;JLorg/omnetpp/simkernel/cNegBinomial;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cNegBinomial_1assign },
    { (char *)"cNegBinomial_str", (char *)"(JLorg/omnetpp/simkernel/cNegBinomial;)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cNegBinomial_1str },
    { (char *)"cNegBinomial_setN", (char *)"(JLorg/omnetpp/simkernel/cNegBinomial;I)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cNegBinomial_1setN },
    { (char *)"cNegBinomial_getN", (char *)"(JLorg/omnetpp/simkernel/cNegBinomial;)I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cNegBinomial_1getN },
    { (char *)"cNegBinomial_setP", (char *)"(JLorg/omnetpp/simkernel/cNegBinomial;D)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cNegBinomial_1setP },
    { (char *)"cNegBinomial_getP", (char *)"(JLorg/omnetpp/simkernel/cNegBinomial;)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cNegBinomial_1getP },
    { (char *)"cNegBinomial_draw", (char *)"(JLorg/omnetpp/simkernel/cNegBinomial;)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cNegBinomial_1draw },
    { (char *)"delete_cNegBinomial", (char *)"(J)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cNegBinomial },
    { (char *)"new_cPoisson__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cRNG;D)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cPoisson_1_1SWIG_10 },
    { (char *)"new_cPoisson__SWIG_1", (char *)"(Ljava/lang/String;JLorg/omnetpp/simkernel/cRNG;D)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cPoisson_1_1SWIG_11 },
    { (char *)"new_cPoisson__SWIG_2", (char *)"(Ljava/lang/String;JLorg/omnetpp/simkernel/cRNG;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cPoisson_1_1SWIG_12 },
    { (char *)"new_cPoisson__SWIG_3", (char *)"(Ljava/lang/String;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cPoisson_1_1SWIG_13 },
    { (char *)"new_cPoisson__SWIG_4", (char *)"()J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cPoisson_1_1SWIG_14 },
    { (char *)"new_cPoisson__SWIG_5", (char *)"(JLorg/omnetpp/simkernel/cPoisson;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cPoisson_1_1SWIG_15 },
    { (char *)"cPoisson_dup", (char *)"(JLorg/omnetpp/simkernel/cPoisson;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cPoisson_1dup },
    { (char *)"cPoisson_assign", (char *)"(JLorg/omnetpp/simkernel/cPoisson;JLorg/omnetpp/simkernel/cPoisson;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cPoisson_1assign },
    { (char *)"cPoisson_str", (char *)"(JLorg/omnetpp/simkernel/cPoisson;)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cPoisson_1str },
    { (char *)"cPoisson_setLambda", (char *)"(JLorg/omnetpp/simkernel/cPoisson;D)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cPoisson_1setLambda },
    { (char *)"cPoisson_getLambda", (char *)"(JLorg/omnetpp/simkernel/cPoisson;)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cPoisson_1getLambda },
    { (char *)"cPoisson_draw", (char *)"(JLorg/omnetpp/simkernel/cPoisson;)D", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cPoisson_1draw },
    { (char *)"delete_cPoisson", (char *)"(J)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cPoisson },
    { (char *)"new_cRegistrationList", (char *)"(Ljava/lang/String;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cRegistrationList },
    { (char *)"delete_cRegistrationList", (char *)"(J)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cRegistrationList },
    { (char *)"cRegistrationList_str", (char *)"(JLorg/omnetpp/simkernel/cRegistrationList;)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cRegistrationList_1str },
    { (char *)"cRegistrationList_forEachChild", (char *)"(JLorg/omnetpp/simkernel/cRegistrationList;JLorg/omnetpp/simkernel/cVisitor;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cRegistrationList_1forEachChild },
    { (char *)"cRegistrationList_begin", (char *)"(JLorg/omnetpp/simkernel/cRegistrationList;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cRegistrationList_1begin },
    { (char *)"cRegistrationList_end", (char *)"(JLorg/omnetpp/simkernel/cRegistrationList;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cRegistrationList_1end },
    { (char *)"cRegistrationList_add", (char *)"(JLorg/omnetpp/simkernel/cRegistrationList;JLorg/omnetpp/simkernel/cOwnedObject;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cRegistrationList_1add },
    { (char *)"cRegistrationList_size", (char *)"(JLorg/omnetpp/simkernel/cRegistrationList;)I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cRegistrationList_1size },
    { (char *)"cRegistrationList_get", (char *)"(JLorg/omnetpp/simkernel/cRegistrationList;I)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cRegistrationList_1get },
    { (char *)"cRegistrationList_find", (char *)"(JLorg/omnetpp/simkernel/cRegistrationList;Ljava/lang/String;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cRegistrationList_1find },
    { (char *)"cRegistrationList_lookup__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cRegistrationList;Ljava/lang/String;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cRegistrationList_1lookup_1_1SWIG_10 },
    { (char *)"cRegistrationList_lookup__SWIG_1", (char *)"(JLorg/omnetpp/simkernel/cRegistrationList;Ljava/lang/String;Ljava/lang/String;Z)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cRegistrationList_1lookup_1_1SWIG_11 },
    { (char *)"cRegistrationList_lookup__SWIG_2", (char *)"(JLorg/omnetpp/simkernel/cRegistrationList;Ljava/lang/String;Ljava/lang/String;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cRegistrationList_1lookup_1_1SWIG_12 },
    { (char *)"cRegistrationList_sort", (char *)"(JLorg/omnetpp/simkernel/cRegistrationList;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cRegistrationList_1sort },
    { (char *)"new_cGlobalRegistrationList__SWIG_0", (char *)"()J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cGlobalRegistrationList_1_1SWIG_10 },
    { (char *)"new_cGlobalRegistrationList__SWIG_1", (char *)"(Ljava/lang/String;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cGlobalRegistrationList_1_1SWIG_11 },
    { (char *)"delete_cGlobalRegistrationList", (char *)"(J)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cGlobalRegistrationList },
    { (char *)"cGlobalRegistrationList_getInstance", (char *)"(JLorg/omnetpp/simkernel/cGlobalRegistrationList;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cGlobalRegistrationList_1getInstance },
    { (char *)"cGlobalRegistrationList_clear", (char *)"(JLorg/omnetpp/simkernel/cGlobalRegistrationList;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cGlobalRegistrationList_1clear },
    { (char *)"cGlobalRegistrationList_begin", (char *)"(JLorg/omnetpp/simkernel/cGlobalRegistrationList;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cGlobalRegistrationList_1begin },
    { (char *)"cGlobalRegistrationList_end", (char *)"(JLorg/omnetpp/simkernel/cGlobalRegistrationList;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cGlobalRegistrationList_1end },
    { (char *)"CTX_NONE_get", (char *)"()I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_CTX_1NONE_1get },
    { (char *)"CTX_BUILD_get", (char *)"()I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_CTX_1BUILD_1get },
    { (char *)"CTX_INITIALIZE_get", (char *)"()I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_CTX_1INITIALIZE_1get },
    { (char *)"CTX_EVENT_get", (char *)"()I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_CTX_1EVENT_1get },
    { (char *)"CTX_REFRESHDISPLAY_get", (char *)"()I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_CTX_1REFRESHDISPLAY_1get },
    { (char *)"CTX_FINISH_get", (char *)"()I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_CTX_1FINISH_1get },
    { (char *)"CTX_CLEANUP_get", (char *)"()I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_CTX_1CLEANUP_1get },
    { (char *)"intCastError__SWIG_0", (char *)"(Ljava/lang/String;Ljava/lang/String;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_intCastError_1_1SWIG_10 },
    { (char *)"intCastError__SWIG_1", (char *)"(Ljava/lang/String;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_intCastError_1_1SWIG_11 },
    { (char *)"intCastError__SWIG_2", (char *)"(Ljava/lang/String;JLorg/omnetpp/simkernel/cObject;Ljava/lang/String;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_intCastError_1_1SWIG_12 },
    { (char *)"intCastError__SWIG_3", (char *)"(Ljava/lang/String;JLorg/omnetpp/simkernel/cObject;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_intCastError_1_1SWIG_13 },
    { (char *)"opp_strlen", (char *)"(Ljava/lang/String;)I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_opp_1strlen },
    { (char *)"opp_strdup__SWIG_0", (char *)"(Ljava/lang/String;)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_opp_1strdup_1_1SWIG_10 },
    { (char *)"opp_strcpy", (char *)"(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_opp_1strcpy },
    { (char *)"opp_strcmp", (char *)"(Ljava/lang/String;Ljava/lang/String;)I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_opp_1strcmp },
    { (char *)"opp_strprettytrunc", (char *)"(Ljava/lang/String;Ljava/lang/String;J)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_opp_1strprettytrunc },
    { (char *)"opp_get_monotonic_clock_nsecs", (char *)"()J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_opp_1get_1monotonic_1clock_1nsecs },
    { (char *)"opp_get_monotonic_clock_usecs", (char *)"()J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_opp_1get_1monotonic_1clock_1usecs },
    { (char *)"opp_demangle_typename", (char *)"(Ljava/lang/String;)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_opp_1demangle_1typename },
    { (char *)"new_cMethodCallContextSwitcher", (char *)"(JLorg/omnetpp/simkernel/cComponent;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1cMethodCallContextSwitcher },
    { (char *)"delete_cMethodCallContextSwitcher", (char *)"(J)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_delete_1cMethodCallContextSwitcher },
    { (char *)"cMethodCallContextSwitcher_methodCall", (char *)"(JLorg/omnetpp/simkernel/cMethodCallContextSwitcher;Ljava/lang/String;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cMethodCallContextSwitcher_1methodCall },
    { (char *)"cMethodCallContextSwitcher_methodCallSilent__SWIG_0", (char *)"(JLorg/omnetpp/simkernel/cMethodCallContextSwitcher;Ljava/lang/String;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cMethodCallContextSwitcher_1methodCallSilent_1_1SWIG_10 },
    { (char *)"cMethodCallContextSwitcher_methodCallSilent__SWIG_1", (char *)"(JLorg/omnetpp/simkernel/cMethodCallContextSwitcher;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cMethodCallContextSwitcher_1methodCallSilent_1_1SWIG_11 },
    { (char *)"cMethodCallContextSwitcher_getDepth", (char *)"()I", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_cMethodCallContextSwitcher_1getDepth },
    { (char *)"opp_strdup__SWIG_1", (char *)"(Ljava/lang/String;I)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_opp_1strdup_1_1SWIG_11 },
    { (char *)"opp_appendindex", (char *)"(Ljava/lang/String;J)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_opp_1appendindex },
    { (char *)"double_to_str", (char *)"(D)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_double_1to_1str },
    { (char *)"delete_JSimpleModule", (char *)"(J)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_delete_1JSimpleModule },
    { (char *)"JSimpleModule_retrieveMsgToBeHandled", (char *)"(JLorg/omnetpp/simkernel/JSimpleModule;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_JSimpleModule_1retrieveMsgToBeHandled },
    { (char *)"JSimpleModule_swigJavaPeer", (char *)"(JLorg/omnetpp/simkernel/JSimpleModule;)Ljava/lang/Object;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_JSimpleModule_1swigJavaPeer },
    { (char *)"JSimpleModule_swigJavaPeerOf", (char *)"(JLorg/omnetpp/simkernel/cModule;)Ljava/lang/Object;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_JSimpleModule_1swigJavaPeerOf },
    { (char *)"new_JMessage", (char *)"(Ljava/lang/String;II)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_new_1JMessage },
    { (char *)"delete_JMessage", (char *)"(J)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_delete_1JMessage },
    { (char *)"JMessage_dup", (char *)"(JLorg/omnetpp/simkernel/JMessage;)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_JMessage_1dup },
    { (char *)"JMessage_info", (char *)"(JLorg/omnetpp/simkernel/JMessage;)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_JMessage_1info },
    { (char *)"JMessage_detailedInfo", (char *)"(JLorg/omnetpp/simkernel/JMessage;)Ljava/lang/String;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_JMessage_1detailedInfo },
    { (char *)"JMessage_swigSetJavaPeer", (char *)"(JLorg/omnetpp/simkernel/JMessage;Ljava/lang/Object;)V", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_JMessage_1swigSetJavaPeer },
    { (char *)"JMessage_swigJavaPeer", (char *)"(JLorg/omnetpp/simkernel/JMessage;)Ljava/lang/Object;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_JMessage_1swigJavaPeer },
    { (char *)"JMessage_swigJavaPeerOf", (char *)"(JLorg/omnetpp/simkernel/cMessage;)Ljava/lang/Object;", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_JMessage_1swigJavaPeerOf },
    { (char *)"SWIGcNamedObjectUpcast", (char *)"(J)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_SWIGcNamedObjectUpcast },
    { (char *)"SWIGcOwnedObjectUpcast", (char *)"(J)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_SWIGcOwnedObjectUpcast },
    { (char *)"SWIGcNoncopyableOwnedObjectUpcast", (char *)"(J)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_SWIGcNoncopyableOwnedObjectUpcast },
    { (char *)"SWIGcDefaultListUpcast", (char *)"(J)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_SWIGcDefaultListUpcast },
    { (char *)"SWIGcListenerUpcast", (char *)"(J)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_SWIGcListenerUpcast },
    { (char *)"SWIGcComponentUpcast", (char *)"(J)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_SWIGcComponentUpcast },
    { (char *)"SWIGcChannelUpcast", (char *)"(J)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_SWIGcChannelUpcast },
    { (char *)"SWIGcIdealChannelUpcast", (char *)"(J)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_SWIGcIdealChannelUpcast },
    { (char *)"SWIGcDelayChannelUpcast", (char *)"(J)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_SWIGcDelayChannelUpcast },
    { (char *)"SWIGcDatarateChannelUpcast", (char *)"(J)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_SWIGcDatarateChannelUpcast },
    { (char *)"SWIGcModuleUpcast", (char *)"(J)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_SWIGcModuleUpcast },
    { (char *)"SWIGcSimpleModuleUpcast", (char *)"(J)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_SWIGcSimpleModuleUpcast },
    { (char *)"SWIGcComponentTypeUpcast", (char *)"(J)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_SWIGcComponentTypeUpcast },
    { (char *)"SWIGcModuleTypeUpcast", (char *)"(J)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_SWIGcModuleTypeUpcast },
    { (char *)"SWIGcChannelTypeUpcast", (char *)"(J)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_SWIGcChannelTypeUpcast },
    { (char *)"SWIGcArrayUpcast", (char *)"(J)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_SWIGcArrayUpcast },
    { (char *)"SWIGcQueueUpcast", (char *)"(J)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_SWIGcQueueUpcast },
    { (char *)"SWIGcStdDevUpcast", (char *)"(J)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_SWIGcStdDevUpcast },
    { (char *)"SWIGcWeightedStdDevUpcast", (char *)"(J)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_SWIGcWeightedStdDevUpcast },
    { (char *)"SWIGcRNGUpcast", (char *)"(J)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_SWIGcRNGUpcast },
    { (char *)"SWIGcObjectFactoryUpcast", (char *)"(J)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_SWIGcObjectFactoryUpcast },
    { (char *)"SWIGcEnumUpcast", (char *)"(J)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_SWIGcEnumUpcast },
    { (char *)"SWIGcRuntimeErrorUpcast", (char *)"(J)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_SWIGcRuntimeErrorUpcast },
    { (char *)"SWIGcDeleteModuleExceptionUpcast", (char *)"(J)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_SWIGcDeleteModuleExceptionUpcast },
    { (char *)"SWIGcExpressionUpcast", (char *)"(J)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_SWIGcExpressionUpcast },
    { (char *)"SWIGcFSMUpcast", (char *)"(J)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_SWIGcFSMUpcast },
    { (char *)"SWIGcGateUpcast", (char *)"(J)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_SWIGcGateUpcast },
    { (char *)"SWIGcMessageUpcast", (char *)"(J)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_SWIGcMessageUpcast },
    { (char *)"SWIGcMsgParUpcast", (char *)"(J)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_SWIGcMsgParUpcast },
    { (char *)"SWIGcEventUpcast", (char *)"(J)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_SWIGcEventUpcast },
    { (char *)"SWIGcOutVectorUpcast", (char *)"(J)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_SWIGcOutVectorUpcast },
    { (char *)"SWIGcParUpcast", (char *)"(J)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_SWIGcParUpcast },
    { (char *)"SWIGcPropertyUpcast", (char *)"(J)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_SWIGcPropertyUpcast },
    { (char *)"SWIGcPropertiesUpcast", (char *)"(J)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_SWIGcPropertiesUpcast },
    { (char *)"SWIGcSimulationUpcast", (char *)"(J)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_SWIGcSimulationUpcast },
    { (char *)"SWIGcClassDescriptorUpcast", (char *)"(J)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_SWIGcClassDescriptorUpcast },
    { (char *)"SWIGcXMLElementUpcast", (char *)"(J)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_SWIGcXMLElementUpcast },
    { (char *)"SWIGcIOutputVectorManagerUpcast", (char *)"(J)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_SWIGcIOutputVectorManagerUpcast },
    { (char *)"SWIGcIOutputScalarManagerUpcast", (char *)"(J)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_SWIGcIOutputScalarManagerUpcast },
    { (char *)"SWIGcISnapshotManagerUpcast", (char *)"(J)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_SWIGcISnapshotManagerUpcast },
    { (char *)"SWIGcIEventlogManagerUpcast", (char *)"(J)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_SWIGcIEventlogManagerUpcast },
    { (char *)"SWIGcRandomUpcast", (char *)"(J)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_SWIGcRandomUpcast },
    { (char *)"SWIGcUniformUpcast", (char *)"(J)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_SWIGcUniformUpcast },
    { (char *)"SWIGcExponentialUpcast", (char *)"(J)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_SWIGcExponentialUpcast },
    { (char *)"SWIGcNormalUpcast", (char *)"(J)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_SWIGcNormalUpcast },
    { (char *)"SWIGcTruncNormalUpcast", (char *)"(J)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_SWIGcTruncNormalUpcast },
    { (char *)"SWIGcGammaUpcast", (char *)"(J)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_SWIGcGammaUpcast },
    { (char *)"SWIGcBetaUpcast", (char *)"(J)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_SWIGcBetaUpcast },
    { (char *)"SWIGcErlangUpcast", (char *)"(J)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_SWIGcErlangUpcast },
    { (char *)"SWIGcChiSquareUpcast", (char *)"(J)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_SWIGcChiSquareUpcast },
    { (char *)"SWIGcStudentTUpcast", (char *)"(J)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_SWIGcStudentTUpcast },
    { (char *)"SWIGcCauchyUpcast", (char *)"(J)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_SWIGcCauchyUpcast },
    { (char *)"SWIGcTriangUpcast", (char *)"(J)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_SWIGcTriangUpcast },
    { (char *)"SWIGcWeibullUpcast", (char *)"(J)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_SWIGcWeibullUpcast },
    { (char *)"SWIGcParetoShiftedUpcast", (char *)"(J)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_SWIGcParetoShiftedUpcast },
    { (char *)"SWIGcIntUniformUpcast", (char *)"(J)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_SWIGcIntUniformUpcast },
    { (char *)"SWIGcBernoulliUpcast", (char *)"(J)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_SWIGcBernoulliUpcast },
    { (char *)"SWIGcBinomialUpcast", (char *)"(J)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_SWIGcBinomialUpcast },
    { (char *)"SWIGcGeometricUpcast", (char *)"(J)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_SWIGcGeometricUpcast },
    { (char *)"SWIGcNegBinomialUpcast", (char *)"(J)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_SWIGcNegBinomialUpcast },
    { (char *)"SWIGcPoissonUpcast", (char *)"(J)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_SWIGcPoissonUpcast },
    { (char *)"SWIGcRegistrationListUpcast", (char *)"(J)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_SWIGcRegistrationListUpcast },
    { (char *)"SWIGJSimpleModuleUpcast", (char *)"(J)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_SWIGJSimpleModuleUpcast },
    { (char *)"SWIGJMessageUpcast", (char *)"(J)J", (void *)Java_org_omnetpp_simkernel_SimkernelJNI_SWIGJMessageUpcast },
};

void SimkernelJNI_registerNatives(JNIEnv *jenv)
{
    jclass clazz = jenv->FindClass("org/omnetpp/simkernel/SimkernelJNI");
    if (!clazz) {
        fprintf(stderr, "ERROR: Cannot find SimkernelJNI class\n");
        exit(1);
    }
    //int ret = jenv->RegisterNatives(clazz, SimkernelJNI_methods, 1976); // Original
    int ret = jenv->RegisterNatives(clazz, SimkernelJNI_methods, 1973); //XXX 2 methods are commented ==> register 2 less
    if (ret!=0) {
        fprintf(stderr, "ERROR: Cannot register native methods for SimkernelJNI: RegisterNatives() returned %d\n", ret);
        exit(1);
    }
}

