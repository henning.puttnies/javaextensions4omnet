package jUDPApp;

/**
 * This class substitutes to the class java.net.InetAddress. It uses a
 * long to save the IP-Address as a value and implements ways to parse strings
 * as an IP-Address. In addition, it also allows stored longs to be returned as strings in an
 * IP manner (xxx.xxx.xxx.xxx).
 * <p>
 * The InetAddress class contains some additional methods to help using it as
 * normal. It is possible to parse a given IP-Address to an array of bits and
 * those to an Integer-value. This should help using the class and to combine it
 * with the L3Address class from Omnet due to the fact that it uses 64bit
 * Integers.
 * You can create a Inetaddress from a string or an integer. If you want to
 * get the InetAddress you can either get it as a string with {@link #getAddress() getAddress }or as 
 * int ({@link #getAddressAsInt() getAddressAsInt}). 
 * <p> 
 * Known issue: At the moment, only IPv4 addresses are supported. 
 * 
 */
public class InetAddress {
	// Attributes
	private long ip; // This is the stored ip address as an integer
	private String ipAsString; // This is the string representation of the
								// ip-Address

	// Constructors
	/**
	 * Create a new InetAddres object with a given ip value. A value beneath
	 * 0 will be treated as 0. To parse a given IP-Address to long simply use
	 * the static method {@link #ParseIPToInt(String) ParseIPToInt}.
	 * 
	 * @param ip
	 *            The ip you want to assign to this InetAddress. 
	 * */
	public InetAddress(long ip) {
		if (ip < 0)
			ip = 0;
		this.ip = ip;
		this.ipAsString = ReturnAsIP();
	}

	/**
	 * Creates a new InetAddress object with a given ip value. A value beneath
	 * 0 will be treated as 0.
	 * 
	 * @param ipAddress
	 *            The ipAddress you want to assign to this InetAddress, given
	 *            as a string in the proper format (xxx.xxx.xxx.xxx). 
	 * */
	public InetAddress(String ipAddress) {
		long ip = InetAddress.ParseIPToInt(ipAddress);

		if (ip < 0)
			ip = 0;
		this.ip = ip;
		this.ipAsString = ReturnAsIP();
	}

	// Getters and Setters
	/**
	 * It is not recommended to change the Address while the program is running.
	 * This could lead to unexpected behavior.
	 * 
	 * @param ip
	 *            The ip you want to set. 
	 * */
	protected void SetAddress(int ip) {
		this.ip = ip;
		this.ipAsString = ReturnAsIP();
	}

	/**
	 * This method returns the IP address of an InetAddress object as a long. 
	 * If you want to get an int instead of a long, use getAddressAsInt() instead.
	 * */
	public long getAddress() {
		if (ip >= 0)
			return ip;
		else
			return 0;
	}

	/**
	 * This method is used to allow the class to return its IP address as an int
	 * instead of a long. It can be used to implement old functionality of this
	 * class or to explicitly cast the IP to int. Warning: Casting a long to int
	 * can cause loss of information.
	 */
	public int getAddressAsInt() {
		if (ip >= 0)
			return (int) ip;
		else
			return 0;
	}

	/**
	 * This method returns the initialized ip value as a string in an IP-manner
	 * (xxx.xxx.xxx.xxx).
	 * */
	public String ReturnAsIP() {
		return InetAddress.ReturnAsIP(this.ip);
	}

	/**
	 * Returns the IP of this object as String This method is a bit faster than
	 * ReturnAsIP (if that is needed).
	 * */
	public String getIP() {
		return ipAsString;
	}

	// Additional Methods
	/**
	 * This method takes a string of an ip and gives back a string which
	 * contains bits of the given value.
	 * 
	 * @param ip
	 *            The string you want to parse to bits. 
	 * */
	protected static String ParseIP(String ip) {
		String res = ""; // The result string

		char[] ipArr = ip.toCharArray(); // This is the ip written in single
											// letters
		int currVal = 0; // The current value of the part of the ip
		int position = 1; // Which position in decimal are we at? (e.g. 125 => 2
							// is the second position, counting from the right)

		for (int i = ipArr.length - 1; i >= 0; i--) {
			// If the char is a number, this condition is true
			if (ipArr[i] != '.' && (int) ipArr[i] >= 48 && (int) ipArr[i] <= 57) {
				// The currentvalue is raised by the found number
				currVal += Character.getNumericValue(ipArr[i]) * position;
				position *= 10; // Then we increase the position by 10, this way
								// we can get the correct value next time
				if (i == 0) // If this is the last number we will have to do as
							// if it would be a '.' as well
				{
					position = 1;
					res = toBitString(currVal) + res;
				}
			} else if (ipArr[i] == '.') // If we found a dot we will have to
										// save the currentvalue and then reset
										// all values
			{
				position = 1;
				res = toBitString(currVal) + res;
				currVal = 0;
			} else
				// This method throws an exception if something unexpected
				// happens
				throw new IllegalArgumentException("Invalid value detected. Check the IP format!");
		}
		return res;
	}

	/**
	 * This method takes an IP as a string and returns it as an integer value.
	 * 
	 * This Method takes a string of a formated IP address and converts it to a
	 * 64-bit long (because the value is non negative it effectively uses 32
	 * bits for the ipv4-address). You can use this method to create a new
	 * InetAddress object like this:
	 * 
	 * <pre>
	 * InetAddress mInet = new InetAddress(InetAddress.ParseIPToInt("127.0.0.1"));
	 * </pre>
	 * 
	 * Note: A new constructor has been added which can be called with a string
	 * directly InetAddress("127.0.0.1"). Note: 'Int' refers to the Integer
	 * being an integral data type, not to the data type int!
	 * 
	 * @param ip
	 *            The ip you want to parse to an integer value.
	 * */
	public static long ParseIPToInt(String ip) {
		return BitStringToBitInt(ParseIP(ip)); // This is a wrapper to return a
												// parsed ip as int
	}

	/**
	 * This method returns the given ip value as a string in an IP-manner
	 * (xxx.xxx.xxx.xxx).
	 * 
	 * @param ip
	 *            The value you want to be returned as an IP.
	 * */
	public static String ReturnAsIP(long ip) {
		String res = "";

		for (int i = 0; i < 32; i++) // Here the ip is converted to a bit-string
		{
			res += Long.toString(ip % 2);
			ip /= 2;
		}
		res = ReverseString(res);
		// until here res contains 32 bits of information (on a hardware-level
		// its actually 32*16=512 bit space used)

		String[] sArr = getFourBitStrings(res); // Contains 8 bit in every field
												// (4 total)
		res = ""; // We delete everything in res to be able to use it again
		res += BitStringToBitInt(sArr[0]) + "." + BitStringToBitInt(sArr[1]) + "." + BitStringToBitInt(sArr[2]) + "."
				+ BitStringToBitInt(sArr[3]);
		// Note we could also save a line by writing res = BitString....

		return res;
	}

	/**
	 * Returns an int array that contains the four numbers of the given
	 * ip-Address.
	 * 
	 * @param ip
	 *            The ip (as a string) you want to Parse.
	 * */
	public static int[] GetIPAddressAsArray(String ip) {
		String[] ipArr = getFourBitStrings(ParseIP(ip));

		int[] res = new int[4];

		for (int i = 0; i < 4; i++) {
			res[i] = (int) BitStringToBitInt(ipArr[i]);
		}

		return res;
	}

	/**
	 * This method takes an integer value and transforms it into 8 bit and
	 * returns that as a string.
	 * */
	protected static String toBitString(long val) {
		String res = "";

		while (val != 0) // As long as the value isnt 0 we will write a new bit
							// into the result string
		{
			// These two lines calulate the bit string
			res += Long.toString(val % 2);
			val /= 2;
		}
		return toByteLength(ReverseString(res)); // Then we will have to reverse
													// the string and add
													// leading 0's
		// Note: We have to reverse the string because it has the wrong format
		// because of the calculation
		// return res;
	}

	/**
	 * This method takes a string of bit values and returns the same string with
	 * leading 0's.
	 * */
	protected static String toByteLength(String val) {
		char[] value = val.toCharArray();
		int needed = 8 - value.length; // How many bits do we need to have 8?
		if (needed > 0) // If we still need at least one bit, this condition
						// will be true
		{
			String res = "";
			for (int i = 0; i < needed; i++) // Then we will add leading 0's
			{
				res += "0";
			}
			for (int i = 0; i < value.length; i++)
				// And then we will add the bit string to the 0's
				res += value[i];
			return res;
		}
		return val; // If there are already 8 or more bits, we will simply
					// return the value
	}

	/**
	 * This method returns an integer from a given bit string created by the
	 * method {@link #ParseIP(String) ParseIP}.
	 * */
	protected static long BitStringToBitInt(String bitString) {
		long res = 0;
		char[] strArr = bitString.toCharArray();
		int twos = 0; // This is used for the conversion to the decimal system
		for (int i = strArr.length - 1; i >= 0; i--) {
			if (Character.getNumericValue(strArr[i]) == 0) // If the current
															// value is zero we
															// will add nothing
				res += 0;
			else if (Character.getNumericValue(strArr[i]) == 1) // If the
																// current value
																// is 1 we will
																// add a value
				res += Math.pow(2, twos); // This value is calculated as mostly
											// known 2^position counting from
											// the right and starting at 0
			else
				// This method throws an exception if something unexpected
				// happens
				throw new IllegalArgumentException("Invalid value detected. Check the IP format!");

			twos++; // After one iteration we will have to increase the position
					// in the binary system
		}
		return res;
	}

	/**
	 * This method takes a String which should contain 32 bits and returns an
	 * array with length 4 with 8 bits in each array.
	 * */
	protected static String[] getFourBitStrings(String bitString) {
		char[] val = bitString.toCharArray();

		String[] rArr = new String[4]; // Our new string array has four fields
		String content = "";

		int i = 0;
		for (int j = 0; j < 4; j++) {
			for (i = 8 * j; i < 8 * j + 8; i++) {
				content += val[i]; // Every array field gets exactly 8 bits
			}
			rArr[j] = content;
			content = "";
		}
		return rArr;
	}

	/**
	 * This method reverses a given string. It is used to reverse the bit-order
	 * of strings.
	 * */
	protected static String ReverseString(String string) {
		char[] cArr = string.toCharArray();
		String res = "";

		// This is a simple reversion of a string
		for (int i = cArr.length - 1; i >= 0; i--)// It simply starts from the
													// right and stores all
													// values in a new string
		{
			res += cArr[i];
		}

		return res;
	}

	/**
	 * Returns the IP of this object as String
	 * */
	@Override
	public String toString() {
		return getIP();
	}
	// Additional Methods End
}
