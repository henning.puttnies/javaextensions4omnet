package jUDPApp;

import org.omnetpp.simkernel.JSimpleModule;
import org.omnetpp.simkernel.SWIGTYPE_p_simsignal_t;
import org.omnetpp.simkernel.cMessage;
import org.omnetpp.simkernel.cObject;
import org.omnetpp.simkernel.cObjectFactory;

/**
 * This class is a Java implementation of the INET module UDPEchoApp.
 * It sends an incoming message back to its origin and adds "echo"
 * to the name of the message. 
 */
public class JUDPEchoApp extends JSimpleModule {
	
	// Constants for kinds of self message
	protected static final short START = 1;

	// Constants for kinds of UDPControlInfo // 
	 protected static final short UDP_I_DATA = 0;
	 protected static final short UDP_I_ERROR = 1;

	// parameters
	protected InetAddress localAddress;
	protected int localPort = -1; 

	// state
	protected DatagramSocket socket;
	protected cMessage selfMsg;

	// statistics
	protected int numEchoed;
	
	// creating a echoedPK Signal, that sends out a Signal each time a packet gets echoed
	protected int echoedPk = registerSignal("echoedPk");
	
	//Methods
	
	protected void initialize() {
		// statistics
		numEchoed = 0;
		// TODO: add statistics functionalities(WATCH)?
		
		// send a self message to start this node
		selfMsg = new cMessage("sendTimer");
		selfMsg.setKind(START);
		scheduleAt(simTime(), selfMsg);
	}

	/**
	 * Simple handleMessage() method. Distinguish between self messages and
	 * messages from other modules. This method gets called by the kernel, when a 
	 * message is received .
	 * @param msg
	 * 			message, that arrived at the module
	 */
	protected void handleMessage(cMessage msg) {
		if (msg.isSelfMessage()) {
			if (msg.getKind() == START)
				{	
				processStart();
				}
				else
				{
				ev.println("Error at " + getFullPath() + ": received self message of unknown kind");
				}
			}

		 else if (msg.getKind() == UDP_I_DATA) { // received Message is from another UDPApp
			processPacket(msg);

		} else if (msg.getKind() == UDP_I_ERROR) {// is set, if the transmission failed
			ev.println("Error at: " + getFullPath());
			cancelAndDelete(msg);
		}
		else{
			System.err.println("Unrecognized message " + msg.getClassName());
		}

	}
	/**
	 * This method is deleting selfMsg canceling sendTimers and recording statistics.
	 * It will print out an exception if the socket is already closed.  
	 */
	protected void finish() {
		// close socket
		try {
			socket.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		recordScalar("packets Echoed",numEchoed);
		
		cancelAndDelete(selfMsg);		//deleting selfMsg and canceling and sendTimers
	}

	/**
	 * This method sets up and binds the app to a DatagramSocket. It gets called by handleMessage(). 
	 */
	protected void processStart() {
		// Parse parameters
		localPort = (int)par("localPort").doubleValue();
		localAddress = new InetAddress(par("localAddress").stdstringValue());
			
		// binding the socket		
		socket = new DatagramSocket();
		socket.setOutputGate(gate("udpOut"));
		socket.bind(localAddress, localPort);
	};
	
	/**
	 * Echoes the received packet back to the sending Module gets called, when a packet arrived
	 * from another UDPApp. 
	 * @param msg
	 * 			message received by the app
	 */
	protected void processPacket(cMessage msg){

		cObject controlInfo = msg.getControlInfo();
		//getting srcAddr and srcPort
		String msgSrcAddr = controlInfo.getField("srcAddr");
		String msgSrcPort = controlInfo.getField("srcPort");
		// creating packet to be sent back
		cMessage repMsg = cMessage.cast(cObjectFactory.createOne("inet::ApplicationPacket"));
		
		String retName = msg.getName() + " echo";	// copying name and adding echo
		repMsg.setField("name", retName);
		repMsg.setKind(UDP_I_DATA);
		
		//debug output
		//System.out.println("srcAddr:"+msgSrcAddr);
		//System.out.println("msgSrcPort:"+msgSrcPort);

		//echo functionality
		int prevSrcPort = Integer.parseInt(msgSrcPort);
		InetAddress prevSrcAddr = new InetAddress(msgSrcAddr);
		socket.sendTo(repMsg, prevSrcAddr, prevSrcPort);
		//socket.sendTo(repMsg, prevSrcAddr, prevSrcPort); // original

		
		// statistics
		numEchoed++;
		emit(echoedPk,msg);
		
		// Call destructors explicitly (use cancelAndDelete() as
		// cMessage.delete() is not working)
		cancelAndDelete(msg);
		
	}

} // class end
