The INET-UDP functionalities from a Java simple module
=======================================================

Minimum requirementa are: 
- Omnet++ 5.4 with Java extensions
- Inet 3.4.0
- jsimple project

Usage instructions: 
- Check if you met the minimum requirements
- import the project from the Github respository
- Ensure the imported project is referenced to your INET 3.4 and jsimple
- build the jUDP_Wrapper project
- start the project via the jUDP_Wrapper.ini in /simulations folder
- additional information can be found in /src/jUDPApp/UDPNetworkSimple.ned

Project structure:
- doc: NED/JavaDoc/Doxygen documentation of the project (generate this from the source code using NED/Doxygen/Javadoc)
- sim: simulation files
- src.jinet_samples.inet_ethernet: example for using INET and JAVA simple modules.
- src.jUDP_Wrapper: sources for the JAVA UDP-Wrapper
