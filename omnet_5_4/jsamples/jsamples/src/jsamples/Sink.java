package jsamples;

import java.util.ArrayList;
import java.util.List;

import org.omnetpp.simkernel.*;

/**
 * Just discards messages.
 */
public class Sink extends JSimpleModule {

	
	List<cMessage> receivedMessagesList;
	
	// mod Henning
	protected void initialize(){
		receivedMessagesList = new ArrayList<cMessage>();
	}
	
	// mod Henning
    public void handleMessage(cMessage msg) {
        ev.println(msg.getName()+" arrived at sink");
        
        cancelAndDelete(msg);
        
        //receivedMessagesList.add(msg);

    }
	
//	// original
//    public void handleMessage(cMessage msg) {
//        ev.println(msg.getName()+" arrived at sink");
//        msg.delete();
//
//    }
    
    // mod Henning
    protected void finish() {
        ev.println("finish of "+getFullPath());
        
        // delete all messages in the List
        int indexHigh = 0;
        while (receivedMessagesList.size()!=0){
        	indexHigh = receivedMessagesList.size()-1;
        	//cancelAndDelete(receivedMessagesList.get(indexHigh));
        	receivedMessagesList.get(indexHigh).delete();
        	receivedMessagesList.remove(indexHigh);
        }
                   
    }

//    // original
//    protected void finish() {
//        ev.println("finish of "+getFullPath());
//                   
//    }
}