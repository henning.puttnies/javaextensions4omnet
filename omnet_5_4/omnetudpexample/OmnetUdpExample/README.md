A UDP Simulation example for OMNeT++/INET
=========================================

Minimum requirements are: 
- OMNeT++ 5.4 (modifications needed for newer Versions)
- INET 3.4.0
- Windows Operating System
	
Usage instructions:
- Check if you meet the minimum requirements 
- Import the project from the Github respository
- Ensure that the imported project is referenced to your INET 3.4 -> project properties -> project references -> Select your INET installation
- compile the imported Project
- Start the Project via the omnetpp.ini in /simulations -> Run/Debug As -> OMNeT++ Simulation
- detailed description can be found in /src/network1.ned

Directory structure: 
- /simulations --> contains omnetpp.ini where settings can be defined
- /src --> contains the network and Source files for all used modules(under /udpapp)
