//
// Copyright (C) 2000 Institut fuer Telematik, Universitaet Karlsruhe
// Copyright (C) 2004,2011 Andras Varga
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//

#ifndef __INET_MYUDPAPP_H
#define __INET_MYUDPAPP_H

#include <vector>

#include "inet/common/INETDefs.h"
#include "inet/networklayer/common/L3AddressResolver.h"
#include "inet/applications/base/ApplicationBase.h"
#include "inet/transportlayer/contract/udp/UDPSocket.h"
#include "MyPayload_m.h"
#include <string>

#include "myL3AddressResolver.h"

namespace inet {


class MyUdpApp : public ApplicationBase
{
  protected:
    enum SelfMsgKinds {START = 1, SEND, STOP };

    // initialization of your own data with default values
    int testint = 0;
    const char  *customMessage = "";

    // parameters
    std::vector<L3Address> destAddresses;
    int localPort = -1, destPort = -1;
    simtime_t startTime;
    simtime_t stopTime;
    const char *packetName = nullptr;

    // state
    UDPSocket socket;
    cMessage *selfMsg = nullptr;

    // statistics
    int numSent = 0;
    int numReceived = 0;
    int maxNumSent = -1;

    static simsignal_t sentPkSignal;
    static simsignal_t rcvdPkSignal;


    // Dummy myL3AddressResolver to get correct vtable.
    MyL3AddressResolver *dummyL3AddressResolver;
    L3Address *dummyL3Address;


  protected:
    virtual int numInitStages() const override { return NUM_INIT_STAGES; }
    virtual void initialize(int stage) override;
    virtual void handleMessageWhenUp(cMessage *msg) override;
    virtual void finish() override;

    // chooses random destination address
    virtual L3Address chooseDestAddr();
    virtual void sendPacket();
    virtual void processPacket(cPacket *msg);
    virtual void setSocketOptions();

    virtual void processStart();
    virtual void processSend();
    virtual void processStop();

    virtual bool handleNodeStart(IDoneCallback *doneCallback) override;
    virtual bool handleNodeShutdown(IDoneCallback *doneCallback) override;
    virtual void handleNodeCrash() override;
    virtual MyPayload* generateMessage(int in,const char *str, const char *name);
    virtual void socketBind();


  public:
    MyUdpApp() {}
    ~MyUdpApp();
};

} // namespace inet

#endif // ifndef __INET_MYUDPAPP_H

