//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see http://www.gnu.org/licenses/.
//
package OmnetUdpExample;

import inet.networklayer.configurator.ipv4.IPv4NetworkConfigurator;
import inet.node.inet.StandardHost;
import inet.node.ethernet.EtherLink;
import inet.node.ethernet.EtherSwitch;
import inet.visualizer.integrated.IntegratedVisualizer;

import OmnetUdpExample.udpapp.MyUdpApp;

//The Network in this simulation works with the StandardHost and 
//EtherSwitch INET-nodes. The StandardHost nodes run one instance of MyUdpApp. 
//This is a modified version of the INET-Module UDPBasicApp. It sends MyPayload 
//Packets to its designated target(defined in the string destAdresses in omnetpp.ini). 
//The name of the target, not its address is used. The packages are filled with 
//information in the sendPacket function of MyUdpApp. If you want to add data to the package
//you have to initialize it in the MyUdpApp.h(declaration of variables in header file), 
//MyUdpApp.ned(declaration of variable in ned file), MyUdpApp.cc(initialize(), generateMessage())
//and MyPayload.msg. After initializing the data you can set it for each individual MyUdpApp 
//in the omnepp.ini. 

channel ethcable extends EtherLink
{
    parameters:
        length = 20m; //simulates a delay of (length / 2e8)
        datarate = 10Mbps;
}

network Network1
{
    parameters:
        @display("bgb=650,500;bgg=100,1,cyan");

    submodules:
        hostA: StandardHost {
            @display("i=block/app2;p=100,300");
        }
        hostB: StandardHost {
            @display("i=block/app2;p=258,47");
        }
        hostC: StandardHost {
            @display("i=block/app2;p=258,300");
        }
        hostD: StandardHost {
            @display("i=block/app2;p=100,47");
        }
        sw1: EtherSwitch {
            @display("i=block/routing;p=179,172");
        }
        configurator: IPv4NetworkConfigurator;
    connections:
        sw1.ethg++ <--> ethcable <--> hostA.ethg++;
        sw1.ethg++ <--> ethcable <--> hostB.ethg++;
        sw1.ethg++ <--> ethcable <--> hostC.ethg++;
        sw1.ethg++ <--> ethcable <--> hostD.ethg++;
}

network simulationTimeTestNetwork
{
   parameters:
        @display("bgb=650,500;bgg=100,1,cyan");

    submodules:
        hostA: StandardHost {
            @display("i=block/app2;p=100,300");
        }
        hostB: StandardHost {
            @display("i=block/app2;p=258,47");
        }
        sw1: EtherSwitch {
            @display("i=block/routing;p=179,172");
        }
        configurator: IPv4NetworkConfigurator;
    connections:
        sw1.ethg++ <--> ethcable <--> hostA.ethg++;
        sw1.ethg++ <--> ethcable <--> hostB.ethg++;
}
    